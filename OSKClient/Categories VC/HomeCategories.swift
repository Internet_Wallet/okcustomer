//
//  HomeCategories.swift
//  VMart
//
//  Created by Shobhit Singhal on 10/15/18.
//  Copyright © 2018 Shobhit. All rights reserved.
//

import UIKit

class HomeCategories: MartBaseViewController {
    
    var ListArray: [String]?
    var aPIManager = APIManager()
    var apiManagerClient: APIManagerClient!
    var headerCategoryArray     = [GetAllCategories]()
    var categoryId                  : NSInteger!
    var categoryName: String = ""
    
    fileprivate let cellOne = "UserViewCell"
    fileprivate let cellTwo = "CategoaryViewCell"
    fileprivate let cellThree = "SettingViewCell"
    fileprivate let cellFour = "SegmentViewCell"
    fileprivate let cellFive = "VersionCell"
    
    
    @IBOutlet weak var CategoaryTbl: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
        statusBarView.applyViewnGradient(colors: [UIColor.colorWithRedValue(redValue: 13, greenValue: 177, blueValue: 75, alpha: 1), UIColor.init(red: 20.0/255.0, green: 185.0/255.0, blue: 85.0/255.0, alpha: 1.0)])
        view.addSubview(statusBarView)
        CategoaryTbl.estimatedRowHeight = 50
        CategoaryTbl.rowHeight = UITableView.automaticDimension
        self.apiManagerClient = APIManagerClient.sharedInstance
        self.loadInitCategoary()
        NotificationCenter.default.addObserver(self, selector: #selector(reloadTableView), name: NSNotification.Name("CategoryReload"), object: nil)
        
        if let DrawerViewController = navigationController?.parent as? ViewController{
            DrawerViewController.KYDCDelegate = self
            print(DrawerViewController)
        }
        
    }
    
    @objc func reloadTableView() {
        loadInitCategoary()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.CategoaryTbl.setContentOffset(CGPoint.zero, animated: true)
    }
    
    private func hasConnectivity() -> Bool {
        
        let status = Reachability2.instance.networkConnectionStatus()
        guard status != NetworkConnectionStatus.notConnection else {
            showAlertView(message: UsersAlertConstant.checkInternet)
            //   AppUtility.showInternetErrorToast(AppConstants.InternetErrorText.message, view: self.view)
            return false
        }
        return true
    }
    
    private func showAlertView(message: String?) {
        let alertController = UIAlertController(title: "1 Stop Mart", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: UsersAlertConstant.okAction, style: .default,
                                                handler: nil))
        //  self.dismiss(animated: true, completion: nil)
        present(alertController, animated: true)
    }
    
    func animate () {
        self.CategoaryTbl.reloadData()
        let cells = self.CategoaryTbl.visibleCells
        let tableHeight: CGFloat = self.CategoaryTbl.bounds.size.height
        for i in cells {
            let cell: UITableViewCell = i as UITableViewCell
            cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
        }
        var index = 0
        for a in cells {
            let cell: UITableViewCell = a as UITableViewCell
            UIView.animate(withDuration: 1.3, delay: 0.05 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: [], animations: {
                cell.transform = CGAffineTransform(translationX: 0, y: 0);
            }, completion: nil)
            index += 1
        }
    }
    
    func loadInitCategoary() {
        
        if UserDefaults.standard.bool(forKey: "EnabledLogOut") {
            ListArray = ["My Account","My Cart","My WishList","My Order","Help & Support", "Log Out"]
        }else if UserDefaults.standard.bool(forKey: "LoginStatus") {
            ListArray = ["My Account","My Cart","My WishList","My Order","Help & Support", "Log Out"]
        }else {
            ListArray =  ["My Account","My Cart","My WishList","My Order","Help & Support"]
        }
        
        self.headerCategoryArray = []
        for category in self.apiManagerClient.leftMenuCategoriesArray {
            if category.isLowestItem == false {
                println_debug(category.name)
                self.headerCategoryArray.append(category)
            }
        }
        DispatchQueue.main.async {
            self.CategoaryTbl.delegate = self
            self.CategoaryTbl.dataSource = self
            self.CategoaryTbl.reloadData()
        }
    }
    
    private func showLogin(screen: String) {
        
        let regitration_Opened = UserDefaults.standard.bool(forKey: "REGITRATION_OPENED")
        if regitration_Opened {
            DispatchQueue.main.async {
                if let registrationVC = UIStoryboard(name: "VMartLogin", bundle: Bundle.main).instantiateViewController(withIdentifier: "VMRegistrationViewController_ID") as? VMRegistrationViewController {
                    let nav = UINavigationController()
                    nav.viewControllers = [registrationVC]
                    registrationVC.screenFrom = "Other"
                    RegistrationModel.share.MobileNumber = UserDefaults.standard.value(forKey: "MOBILE_NUMBER") as! String
                    registrationVC.continueAsGuest = false
                    registrationVC.delegate = self
                    nav.modalPresentationStyle = .fullScreen
                    self.present(nav, animated: true, completion: nil)
                }
            }
        }else {
            DispatchQueue.main.async {
                if let loginVC = UIStoryboard(name: "VMartLogin", bundle: nil).instantiateViewController(withIdentifier: "VMLoginViewController_ID") as? VMLoginViewController {
                    loginVC.modalPresentationStyle = .fullScreen
                    let nav = UINavigationController()
                    nav.viewControllers = [loginVC]
                    
                    loginVC.screenFrm = "DashBoard"
                    loginVC.asGuest = false
                    nav.modalPresentationStyle = .fullScreen
                    self.present(nav, animated: true, completion: nil)
                }
            }
        }
    }
    
    private func showCart() {
        DispatchQueue.main.async {
            let viewController = UIStoryboard(name: "Cart", bundle: Bundle.main).instantiateViewController(withIdentifier: "CartRoot")
            self.present(viewController, animated: true, completion: nil)
        }
    }
    
    private func showRegistration() {
        DispatchQueue.main.async {
            if  let viewController = UIStoryboard(name: "VMartLogin", bundle: Bundle.main).instantiateViewController(withIdentifier: "VMRegistrationViewController_ID") as? VMRegistrationViewController {
                viewController.delegate = self
                RegistrationModel.share.MobileNumber = UserDefaults.standard.value(forKey: "MOBILE_NUMBER") as! String
                let nav = UINavigationController()
                nav.viewControllers = [viewController]
                nav.modalPresentationStyle = .fullScreen
                self.present(nav, animated: true, completion: nil)
            }
        }
    }
    
    
    func showLoginVC() {
        if UserDefaults.standard.bool(forKey: "GuestLogin") ||  UserDefaults.standard.bool(forKey: "LoginStatus") {
            if UserDefaults.standard.bool(forKey: "RegistrationDone") {
                DispatchQueue.main.async {
                    self.showMyProfile()
                }
            }else {
                self.showRegistration()
            }
        }else {
            DispatchQueue.main.async {
                if UserDefaults.standard.bool(forKey: "LoginStatus") {
                    self.showMyProfile()
                } else {
                    self.showLogin(screen: "")
                }
            }
        }
    }
    
    private func showMyProfile() {
        if let myAccountVC = UIStoryboard(name: "VMartLogin", bundle: nil).instantiateViewController(withIdentifier: "VMMyAccountViewController_ID") as? VMMyAccountViewController {
            let nav = UINavigationController()
            myAccountVC.screenFrom = "DashBoard"
            nav.modalPresentationStyle = .fullScreen
            nav.viewControllers = [myAccountVC]
            self.present(nav, animated: true, completion: nil)
        }
    }
    
    
    private func showMyWishListVC() {
        self.performSegue(withIdentifier: "HomeToMyWishlistSegue", sender: self)
    }
    
    private func showMyOrderVC() {
        if UserDefaults.standard.bool(forKey: "GuestLogin") || UserDefaults.standard.bool(forKey: "LoginStatus") {
            self.performSegue(withIdentifier: "HomeToMyOrderSegue", sender: self)
        }else if UserDefaults.standard.bool(forKey: "LoginStatus"){
            self.performSegue(withIdentifier: "HomeToMyOrderSegue", sender: self)
        }else {
            self.showLogin(screen: "MyOrders")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "HomeCategoryToProductCollectionSegue" {
            if let vc = segue.destination as? UINavigationController {
                if let rootViewController = vc.viewControllers.first as? ProductCollections {
                    rootViewController.categoryId = self.categoryId
                    rootViewController.categoryName = self.categoryName
                }
            }
        }
        else if segue.identifier == "HomeCategoryToSubCategoriesSegue" {
            if let vc = segue.destination as? UINavigationController {
                if let rootViewController = vc.viewControllers.first as? ProductSubCategories {
                    rootViewController.categoryId = self.categoryId
                    rootViewController.categoryName = self.categoryName
                    rootViewController.Root = "home"
                }
            }
        }
        else if segue.identifier == "MenuToHomeUnwindSegue" {
            if let vc = segue.destination as? ViewController {
                vc.isLogoutTrue = true
            }
        }
    }
    
    private func showAlert(message: String) {
        if message == "Success" {
            DispatchQueue.main.async {
                let alert  = UIAlertController(title: message.localized, message: "Updated successfully".localized, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: { (alert) in
                    self.dismiss(animated: true, completion: nil)
                }))
                self.present(alert, animated: true, completion: nil)
            }
        } else {
            DispatchQueue.main.async {
                let alert  = UIAlertController(title: "Warning".localized, message: "Try again".localized, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("CategoryReload"), object: nil)
    }
    
    //MARK: UIGuesture Method
    @objc func tappedOnIconImage() {
        if let DrawerTableViewController = navigationController?.parent as? KYDrawerController{
            DrawerTableViewController.setDrawerState(.closed, animated: true)
        }
        performSegue(withIdentifier: "MenuToHomeUnwindSegue", sender: self)
    }
    
}

extension HomeCategories: KYDrawerCustomDelegate{
    func resetAllData() {
        self.loadInitCategoary()
    }
}

//MARK: - UITableViewDataSource, UITableViewDelegate
extension HomeCategories: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 7
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return 1
        case 2:
            return 1
        case 3:
            return self.headerCategoryArray.count
        case 4:
            return self.ListArray!.count
        case 5:
            return 0
        default:
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 2 || section == 4{
            return 1
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: CategoaryViewCell!
        switch indexPath.section {
        case 0:
            cell = tableView.dequeueReusableCell(withIdentifier: cellOne, for: indexPath) as? CategoaryViewCell
            let tap = UITapGestureRecognizer(target: self, action: #selector(HomeCategories.tappedOnIconImage))
            cell.imgPersonIcon.addGestureRecognizer(tap)
            cell.imgPersonIcon.isUserInteractionEnabled = true
            let firstName = VMLoginModel.shared.firstName
            cell.lblheader.font = UIFont(name: appFont, size: 15.0)
            if UserDefaults.standard.bool(forKey: "LoginStatus") {
                cell.lblheader.text = firstName?.localized
            }else {
                cell.lblheader.text = "1 Stop Mart".localized
            }
            cell.contentView.applyViewnGradient(colors: [UIColor.colorWithRedValue(redValue: 13, greenValue: 177, blueValue: 75, alpha: 1), UIColor.init(red: 20.0/255.0, green: 185.0/255.0, blue: 85.0/255.0, alpha: 1.0)])
            
        case 1:
            cell = tableView.dequeueReusableCell(withIdentifier: cellTwo, for: indexPath) as? CategoaryViewCell
            //let name = "Choose Language English\n(အဂၤလိပ္ဘာသာ)".localized
            if appLang == "English" {
                let name = "ဘာသာစကား ေရြးပါ" + " (" + "အဂၤလိပ္" + ")" + "\n" + "Choose Language" + " (" + appLang + ")"
                cell.wrapCategoary(string: name)
            } else {
                let name = "Choose Language".localized + " (" + appLang.localized + ")" + "\n" + "Choose Language" + " (" + appLang + ")"
                cell.wrapCategoary(string: name)
            }
            cell.accessoryType = .disclosureIndicator
            cell.borderView.isHidden = true
            cell.selectionStyle = .none
        case 2:
            cell = tableView.dequeueReusableCell(withIdentifier: cellTwo, for: indexPath) as? CategoaryViewCell
            let name = "Home".localized
            cell.borderView.isHidden = false
            cell.wrapCategoary(string: name)
        case 3:
            cell = tableView.dequeueReusableCell(withIdentifier: cellTwo, for: indexPath) as? CategoaryViewCell
            let name = self.headerCategoryArray[indexPath.row].name
            cell.wrapCategoary(string: name)
            if name == "Food Order Delivery" {
                cell.borderView.isHidden = false
            }
            else {
                cell.borderView.isHidden = true
            }
            cell.accessoryType = .none
            cell.selectionStyle = .none
        case 4:
            cell = tableView.dequeueReusableCell(withIdentifier: cellThree, for: indexPath) as? CategoaryViewCell
            if let name = self.ListArray?[indexPath.row]{
                cell.wrapCategoary(string: name)
            }
            cell.accessoryType = .none
            cell.selectionStyle = .none
        case 5:
            cell = tableView.dequeueReusableCell(withIdentifier: cellFour, for: indexPath) as? CategoaryViewCell
            //cell.delegate = self
            cell.lblEnlishLanguage.font = UIFont(name: appFont, size: 14.0)
            cell.lblBurmeseLanguage.font = UIFont(name: appFont, size: 14.0)
            cell.lblEnlishLanguage.text = "English\n(အဂၤလိပ္ဘာသာ)"
            cell.lblBurmeseLanguage.text = "Myanmar\n(ျမန္မာဘာသာ)"
        //cell.configureUI()
        default:
            cell = tableView.dequeueReusableCell(withIdentifier: cellFive, for: indexPath) as? CategoaryViewCell
            //            cell.viewBGVersion.applyViewnGradient(colors: [UIColor.colorWithRedValue(redValue: 32, greenValue: 89, blueValue: 235, alpha: 1), UIColor.magenta])
            cell.viewBGVersion.applyViewnGradient(colors: [UIColor.colorWithRedValue(redValue: 13, greenValue: 177, blueValue: 75, alpha: 1), UIColor.init(red: 20.0/255.0, green: 185.0/255.0, blue: 85.0/255.0, alpha: 1.0)])
            cell.lblVersion.font = UIFont(name: appFont, size: 15.0)
            
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 180
        case 1:
            return UITableView.automaticDimension
        case 2:
            return UITableView.automaticDimension
        case 3:
            return UITableView.automaticDimension
        case 4:
            return UITableView.automaticDimension
        case 5:
            return 100
        default:
            return 80
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 2))
        headerView.backgroundColor = UIColor.colorWithRedValue(redValue: 220, greenValue: 220, blueValue: 220, alpha: 1)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.section {
        case 0:
            performSegue(withIdentifier: "MenuToHomeUnwindSegue", sender: self)
            break
        case 1:
            performSegue(withIdentifier: "LanguageSetUpSeque", sender: self)
            break
        case 2:
            if let DrawerTableViewController = navigationController?.parent as? KYDrawerController{
                DrawerTableViewController.setDrawerState(.closed, animated: true)
            }
            performSegue(withIdentifier: "MenuToHomeUnwindSegue", sender: self)            
            break
        case 3:
            if hasConnectivity() {
                categoryId = self.headerCategoryArray[indexPath.row].idd
                categoryName = self.headerCategoryArray[indexPath.row].name
                if self.headerCategoryArray[indexPath.row].subCategoriesCount > 0{
                    self.performSegue(withIdentifier: "HomeCategoryToSubCategoriesSegue", sender: self)
                } else {
                    self.performSegue(withIdentifier: "HomeCategoryToProductCollectionSegue", sender: self)
                }
            }else {
            }
            break
        case 4:
            if let DrawerTableViewController = navigationController?.parent as? KYDrawerController{
                DrawerTableViewController.setDrawerState(.closed, animated: true)
            }else if let DrawerTableViewController = self.parent as? KYDrawerController{
                DrawerTableViewController.setDrawerState(.closed, animated: true)
            }
            if indexPath.row == 0 {
                if hasConnectivity() {
                    self.showLoginVC()
                }else {
                }
            }else if indexPath.row == 1 {
                if hasConnectivity() {
                    DispatchQueue.main.async {
                        let viewController = UIStoryboard(name: "Cart", bundle: Bundle.main).instantiateViewController(withIdentifier: "CartRoot")
                        self.present(viewController, animated: true, completion: nil)
                    }
                }else{ }
            }else if indexPath.row == 2 {
                if hasConnectivity() {
                    self.showMyWishListVC()
                }else {
                }
            }else if indexPath.row == 3 {
                if hasConnectivity() {
                    self.showMyOrderVC()
                }else {
                }
            }else if indexPath.row == 4 {
                self.performSegue(withIdentifier: "HometoHelpAndSupport", sender: self)
            }else  if indexPath.row == 5 {
                if UserDefaults.standard.bool(forKey: "LoginStatus") {
                    if let DrawerTableViewController = navigationController?.parent as? KYDrawerController{
                        UserDefaults.standard.set(true, forKey: "isLogin")
                        UserDefaults.standard.synchronize()
                        DrawerTableViewController.setDrawerState(.closed, animated: true)
                    }else if let DrawerTableViewController = self.parent as? KYDrawerController{
                        DrawerTableViewController.setDrawerState(.closed, animated: true)
                    }
                    UserDefaults.standard.synchronize()
                    performSegue(withIdentifier: "MenuToHomeUnwindSegue", sender: self)
                }
            }
            break
        default:
            break
        }
    }
}


/*
 
 protocol ChangeLanguageProtocol: class {
 func changeAppLanguage(index: Int)
 }
 */

//MARK: - PromotionalViewCell
class CategoaryViewCell: UITableViewCell {
    
    @IBOutlet var lblheader: UILabel!{
        didSet {
            self.lblheader.font = UIFont(name: appFont, size: 15.0)
            self.lblheader.text = self.lblheader.text?.localized
            
        }
    }
    
    //MARK: - Outlet
    @IBOutlet var lblCatName: UILabel!{
        didSet {
            self.lblCatName.font = UIFont(name: appFont, size: 15.0)
            self.lblCatName.text = self.lblCatName.text?.localized
            
        }
    }
    @IBOutlet var borderView: UIView!
    
    @IBOutlet var lblCompanyName: UILabel!{
        didSet {
            self.lblCompanyName.font = UIFont(name: appFont, size: 15.0)
            self.lblCompanyName.text = self.lblCompanyName.text?.localized
            
        }
    }
    @IBOutlet var lblDesc: UILabel!{
        didSet {
            self.lblDesc.font = UIFont(name: appFont, size: 15.0)
            self.lblDesc.text = self.lblDesc.text?.localized
            
        }
    }
    @IBOutlet var lblDate: UILabel!{
        didSet {
            self.lblDate.font = UIFont(name: appFont, size: 15.0)
            self.lblDate.text = self.lblDate.text?.localized
            
        }
    }
    @IBOutlet var imgPersonIcon: UIImageView!
    @IBOutlet weak var englishLangView: UIView! {
        didSet {
            englishLangView.roundCorners([.topRight, .bottomRight], radius: 20)
        }
    }
    
    @IBOutlet weak var stackLanguage: UIStackView!
    
    
    @IBOutlet weak var burmeseLangView: UIView! {
        didSet {
            burmeseLangView.roundCorners([.topLeft, .bottomLeft], radius: 20)
        }
    }
    
    @IBOutlet weak var lblEnlishLanguage: UILabel!{
        didSet {
            self.lblEnlishLanguage.font = UIFont(name: appFont, size: 15.0)
            self.lblEnlishLanguage.text = self.lblEnlishLanguage.text?.localized
            
        }
    }
    @IBOutlet weak var lblBurmeseLanguage: UILabel!{
        didSet {
            self.lblBurmeseLanguage.font = UIFont(name: appFont, size: 15.0)
            self.lblBurmeseLanguage.text = self.lblBurmeseLanguage.text?.localized
            
        }
    }
    
    @IBOutlet weak var viewBGVersion: UIView!
    @IBOutlet weak var lblVersion: UILabel!{
        didSet {
            self.lblVersion.font = UIFont(name: appFont, size: 15.0)
            self.lblVersion.text = self.lblVersion.text?.localized
            
        }
    }
    
    weak var delegate: ChangeLanguageProtocol?
    
    //MARK: - Views Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        //        self.imgPersonIcon?.layer.cornerRadius = (self.imgPersonIcon?.frame.width ?? 0) / 2
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func wrapCategoary(string: String) {
        lblCatName.font = UIFont(name: appFont, size: 15.0)
        println_debug("item list : \(string.localized)")
        lblCatName.text = string.localized
    }
}

extension HomeCategories : RegistrationDelegate {
    
    func backToMainPage() {
        self.dismiss(animated: true, completion: nil)
    }
}

extension UIView {
    
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
}
