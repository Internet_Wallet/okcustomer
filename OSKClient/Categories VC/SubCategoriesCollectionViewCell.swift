//
//  SubCategoriesCollectionViewCell.swift
//  VMart
//
//  Created by Shobhit Singhal on 10/16/18.
//  Copyright © 2018 Shobhit. All rights reserved.
//

import UIKit

class SubCategoriesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var mainBackView: UIView!
    @IBOutlet var subCategoryImageView: UIImageView!
    @IBOutlet var subCategoryNameLbl: UILabel!{
        didSet {
            self.subCategoryNameLbl.font = UIFont(name: appFont, size: 15.0)
            self.subCategoryNameLbl.numberOfLines = 2
            self.subCategoryNameLbl.text = self.subCategoryNameLbl.text?.localized
            
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        mainBackView.shadowToColleectionView()
    }
    
    func updateCellWithData(FeaturedProductsAndCategoryData: FeaturedProductsAndCategory, index : Int) {
        subCategoryImageView.sd_setImage(with: URL(string: FeaturedProductsAndCategoryData.imageUrl))
        
        
        //        AppUtility.NKPlaceholderImage(image: UIImage(named: FeaturedProductsAndCategoryData.imageUrl), imageView: self.subCategoryImageView, imgUrl: FeaturedProductsAndCategoryData.imageUrl) { (image) in }
        
        
        
        
        //        AppUtility.getData(from: URL(string: FeaturedProductsAndCategoryData.imageUrl)!) { data, response, error in
        //            guard let data = data, error == nil else { return }
        //            print("Download Finished")
        //            DispatchQueue.main.async() {
        //                self.subCategoryImageView.image = UIImage(data: data)
        //            }
        //        }
        
        
        subCategoryNameLbl.text = FeaturedProductsAndCategoryData.name
    }
}
