//
//  RequestDetails.swift
//  VMart
//
//  Created by Mohit on 7/13/19.
//  Copyright © 2019 Shobhit. All rights reserved.
//

import UIKit

class RequestDetails: MartBaseViewController {
    
    var aPIManager = APIManager()
    var imgArr = [String]()
    var responseObject: medicineRequest?
    
    @IBOutlet var cartBarButton: UIBarButtonItem!
    @IBOutlet weak var productCollectionView2: UICollectionView!
    
    @IBOutlet weak var ProceedToPayBtn: UIButton! {
        didSet {
            ProceedToPayBtn.applyButtonGradient(colors: [UIColor.colorWithRedValue(redValue: 13, greenValue: 177, blueValue: 75, alpha: 1), UIColor.init(red: 20.0/255.0, green: 185.0/255.0, blue: 85.0/255.0, alpha: 1.0)])
            self.ProceedToPayBtn.setTitle("Proceed To Pay".localized, for: .normal)
            self.ProceedToPayBtn.layer.cornerRadius = 4
            self.ProceedToPayBtn.layer.masksToBounds = true
            ProceedToPayBtn.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    @IBOutlet weak var requestDetailTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Medicine Request Detail".localized
        
        self.requestDetailTableView.dataSource = self
        self.requestDetailTableView.delegate = self
        self.requestDetailTableView.reloadData()
        
        let status = responseObject?.requestStatusMessage
        if (status == "Approved")  { self.ProceedToPayBtn.isHidden = false }
        else { self.ProceedToPayBtn.isHidden = true }
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let drawerController = self.navigationController?.parent as? KYDrawerController {
            drawerController.setDrawerState(.opened, animated: true)
        }
        self.navigationController?.navigationBar.applyNavigationGradient(colors: [UIColor.colorWithRedValue(redValue: 13, greenValue: 177, blueValue: 75, alpha: 1), UIColor.init(red: 20.0/255.0, green: 185.0/255.0, blue: 85.0/255.0, alpha: 1.0)])
        
        self.cartBarButton = UIBarButtonItem.init(badge: UserDefaults.standard.string(forKey: "badge") ?? "", title: "", target: self, action: #selector(self.cartAction(_:)))
        self.navigationItem.rightBarButtonItem  = self.cartBarButton
        
    }
    
    @IBAction func cartAction(_ sender: UIBarButtonItem) {
        let viewController = UIStoryboard(name: "Cart", bundle: Bundle.main).instantiateViewController(withIdentifier: "CartRoot")
        self.present(viewController, animated: true, completion: nil)
    }
    
    @IBAction func backAction(_ sender: UIBarButtonItem) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func ProceedToPayClick(_ sender: UIButton) {
        if AppUtility.isConnectedToNetwork() {
            self.AddMedicineRequest()
        } else {
            AppUtility.showInternetErrorToast(AppConstants.InternetErrorText.message, view: self.view)
        }
    }
    
    func AddMedicineRequest() {
        AppUtility.showLoading(self.view)
        // shoppingCartTypeId: 1 used for add to cart and 2 for wishlistt
        aPIManager.AddMedicineToCartRequest(requestId: responseObject!.iD, shoppingCartTypeId: 1,onSuccess: { [weak self] in
            if let viewLoc = self?.view {
                AppUtility.hideLoading(viewLoc)
            }
            
            self?.moveToCart()
            
            },onError: { [weak self] message in
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                    AppUtility.showToast(message,view: viewLoc)
                }})
    }
    
    func moveToCart(){
        
        self.dismiss(animated: true, completion: {
            
            let viewController = UIStoryboard(name: "Cart", bundle: Bundle.main).instantiateViewController(withIdentifier: "CartRoot")
            self.present(viewController, animated: true, completion: nil)
            
        })
    }
}

extension RequestDetails: UITableViewDataSource, UITableViewDelegate {
    // MARK: - TableView Delegate and DataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 { return 1 }
        else {
            let status = responseObject?.requestStatusMessage
            if (status == "Rejected") { return 1 }
            else if (status == "Pending") { return 0 }
            else {
                self.ProceedToPayBtn.isHidden = false
                return responseObject?.medicineItems?.count ?? 0 }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "RequestListCell1") as! RequestDetailsCell1
            //cell.deleteBtn.tag = indexPath.row
            //        cell.Delegate = self
            cell.setCollectionViewDataSourceDelegate(self , forRow: indexPath.row)
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            cell.updateCellData(object: self.responseObject!, indexPath: indexPath)
            return cell
        }
        else {
            let status = responseObject?.requestStatusMessage
            if indexPath.row == 0 && (status == "Rejected")  {
                let cell = tableView.dequeueReusableCell(withIdentifier: "RequestListCell3") as! RejectedDetailsCell
                cell.updateCellData(object: self.responseObject!, indexPath: indexPath)
                return cell
            }
            else {
                let cell : MedicineTableViewCell = tableView.dequeueReusableCell(withIdentifier: "RequestListCell2") as! MedicineTableViewCell
                //cell.deleteBtn.tag = indexPath.row
                //        cell.Delegate = self
                cell.selectionStyle = UITableViewCell.SelectionStyle.none
                let medicineObjectArray = self.responseObject!.medicineItems?[indexPath.row]
                cell.updateCellData(object: (medicineObjectArray)!, indexPath: indexPath)
                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0 { return 250.0 }
        else {
            let status = responseObject?.requestStatusMessage
            if (status == "Rejected") { return UITableView.automaticDimension }
            else if (status == "Pending") { return 0 }
            else { return 180.0 }
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}


extension RequestDetails: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (responseObject?.prescriptionImageUrl!.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CustomCollectionCell", for: indexPath) as! CustomCollectionCell
        //        cell.roundCell(cell)
        
        cell.wrapCustomCollectionCellData(index: indexPath.row, image: (self.responseObject?.prescriptionImageUrl![indexPath.row])!)
        
        //cell.Delegate = self
        //        cell.contentView.layer.borderWidth = 1
        return cell
        
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        print(String(format: "%i", scrollView.subviews.count))
        let vv = UIView()
        for v: UIView in scrollView.subviews {
            if (v is UIImageView) {
                return v
            }
        }
        return vv
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if let arr = self.responseObject!.prescriptionImageUrl{
            let viewController = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ProductImagesCollection") as? ProductImagesCollection
            viewController?.fullImageUrlArray = arr.map({ (arrObj) -> String in
                return arrObj.replacingOccurrences(of: " ", with: "%20")
            })
            viewController?.selectedIndex = indexPath.row
            viewController?.productName = "Image Viewer"
            viewController?.wishListSelected = false
            self.present(viewController!, animated: true, completion: nil)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 75, height: collectionView.frame.height)
        
    }
}

class RequestDetailsCell1 : UITableViewCell {
    
    
    @IBOutlet var patientNameTitle: UILabel!{
        didSet {
            self.patientNameTitle.font = UIFont(name: appFont, size: 15.0)
            self.patientNameTitle.text = self.patientNameTitle.text?.localized
            
        }
    }
    
    @IBOutlet var doctorNameTitle: UILabel!{
        didSet {
            self.doctorNameTitle.font = UIFont(name: appFont, size: 15.0)
            self.doctorNameTitle.text = self.doctorNameTitle.text?.localized
            
        }
    }
    
    @IBOutlet var hospitalNameTitle: UILabel!{
        didSet {
            self.hospitalNameTitle.font = UIFont(name: appFont, size: 15.0)
            self.hospitalNameTitle.text = self.hospitalNameTitle.text?.localized
            
        }
    }
    
    @IBOutlet var commentsRemarks: UILabel!{
        didSet {
            self.commentsRemarks.font = UIFont(name: appFont, size: 15.0)
            self.commentsRemarks.text = self.commentsRemarks.text?.localized
            
        }
    }
    
    @IBOutlet var statusTitle: UILabel!{
        didSet {
            self.statusTitle.font = UIFont(name: appFont, size: 15.0)
            self.statusTitle.text = self.statusTitle.text?.localized
            
        }
    }
    
    
    @IBOutlet var imgCollection : UICollectionView!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    func updateCellData(object: medicineRequest ,indexPath: IndexPath) {
        
        patientNameTitle.text = object.patientName
        doctorNameTitle.text = object.doctorName
        hospitalNameTitle.text = object.hospitalName
        commentsRemarks.text = object.remarks
        let status = object.requestStatusMessage
        statusTitle.text = status
        switch status {
        case "Pending":
            statusTitle.backgroundColor = UIColor.init(red: (253.0/255.0), green: (193.0/255.0), blue: (10.0/255.0), alpha: 1.0)
        case "Rejected":
            statusTitle.backgroundColor = UIColor.init(red: (220.0/255.0), green: (53.0/255.0), blue: (69.0/255.0), alpha: 1.0)
        default:
            statusTitle.backgroundColor = UIColor.init(red: (40.0/255.0), green: (166.0/255.0), blue: (69.0/255.0), alpha: 1.0)
        }
    }
    
}

extension RequestDetailsCell1 {
    
    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {
        
        imgCollection.delegate = dataSourceDelegate
        imgCollection.dataSource = dataSourceDelegate
        imgCollection.tag = row
        imgCollection.setContentOffset(imgCollection.contentOffset, animated:false) // Stops collection view if it was scrolling.
        imgCollection.reloadData()
    }
    
    var collectionViewOffset: CGFloat {
        set { imgCollection.contentOffset.x = newValue }
        get { return imgCollection.contentOffset.x }
    }
}

class RejectedDetailsCell : UITableViewCell {
    
    
    @IBOutlet var rejectionTitle: UILabel!{
        didSet {
            self.rejectionTitle.font = UIFont(name: appFont, size: 15.0)
            self.rejectionTitle.text = self.rejectionTitle.text?.localized
            
        }
    }
    
    @IBOutlet var rejectionRemarks: UILabel!{
        didSet {
             self.rejectionRemarks.font = UIFont(name: appFont, size: 15.0)
            self.rejectionRemarks.text = self.rejectionRemarks.text?.localized
           
        }
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    func updateCellData(object: medicineRequest ,indexPath: IndexPath) {
        rejectionTitle.text = object.rejectedReason
        rejectionRemarks.text = object.remarks
    }
    
}
class MedicineTableViewCell: UITableViewCell {
    
    
    
    @IBOutlet weak var medicineName : UILabel!{
        didSet {
            self.medicineName.font = UIFont(name: appFont, size: 15.0)
            self.medicineName.text = self.medicineName.text?.localized
            
        }
    }
    
    @IBOutlet weak var quantity: UILabel!{
        didSet {
            self.quantity.font = UIFont(name: appFont, size: 15.0)
            self.quantity.text = self.quantity.text?.localized
            
        }
    }
    @IBOutlet weak var price: UILabel!{
        didSet {
            self.price.font = UIFont(name: appFont, size: 15.0)
            self.price.text = self.price.text?.localized
            
        }
    }
    @IBOutlet weak var total: UILabel!{
        didSet {
            self.total.font = UIFont(name: appFont, size: 15.0)
            self.total.text = self.total.text?.localized
            
        }
    }
    @IBOutlet weak var isAvailable: UILabel!{
        didSet {
            self.isAvailable.font = UIFont(name: appFont, size: 15.0)
            self.isAvailable.text = self.isAvailable.text?.localized
            
        }
    }
    
    @IBOutlet weak var medicineNameTitle : UILabel!{
        didSet {
            self.medicineNameTitle.font = UIFont(name: appFont, size: 15.0)
            self.medicineNameTitle.text = self.medicineNameTitle.text?.localized
            
        }
    }
    
    @IBOutlet weak var quantityTitle: UILabel!{
        didSet {
            self.quantityTitle.font = UIFont(name: appFont, size: 15.0)
            self.quantityTitle.text = self.quantityTitle.text?.localized
            
        }
    }
    @IBOutlet weak var priceTitle: UILabel!{
        didSet {
            self.priceTitle.font = UIFont(name: appFont, size: 15.0)
            self.priceTitle.text = self.priceTitle.text?.localized
            
        }
    }
    @IBOutlet weak var totalTitle: UILabel!{
        didSet {
            self.totalTitle.font = UIFont(name: appFont, size: 15.0)
            self.totalTitle.text = self.totalTitle.text?.localized
            
        }
    }
    @IBOutlet weak var isAvailableTitle: UILabel!{
        didSet {
            self.isAvailableTitle.font = UIFont(name: appFont, size: 15.0)
            self.isAvailableTitle.text = self.isAvailableTitle.text?.localized
            
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func updateCellData(object: [String : Any] ,indexPath: IndexPath) {
        
        medicineNameTitle.text = object["MedicineName"] as? String
        quantityTitle.text = String(describing: object["Quantity"] as? Int)
        priceTitle.text = object["UnitPrice"] as? String
        totalTitle.text = object["TotalAmount"] as? String
        let availability : Bool  = (object["IsAvailable"] as? Bool)!
        if availability {
            isAvailableTitle.text = "Available"
            isAvailableTitle.textColor = .green
        }else {
            isAvailableTitle.text = "No Available"
            isAvailableTitle.textColor = .green
        }
    }
    
    
    func convertBase64ToImage(imageString: String) -> UIImage {
        let imageData = Data(base64Encoded: imageString, options: Data.Base64DecodingOptions.ignoreUnknownCharacters)!
        return UIImage(data: imageData)!
    }
    
    
}



class CustomCollectionCell: UICollectionViewCell {
    
    @IBOutlet var productImageView: UIImageView!
    
    func wrapCustomCollectionCellData(index:Int, image: String) {
        
        self.productImageView.sd_setImage(with: URL(string: image.replacingOccurrences(of: " ", with: "%20")))
        
        
        
        //        AppUtility.NKPlaceholderImage(image: UIImage(named: image.replacingOccurrences(of: " ", with: "%20")), imageView: self.productImageView, imgUrl: image.replacingOccurrences(of: " ", with: "%20")) { (image) in }
        
        
        //        AppUtility.getData(from: URL(string: image.replacingOccurrences(of: " ", with: "%20"))!) { data, response, error in
        //            guard let data = data, error == nil else { return }
        //            print("Download Finished")
        //            DispatchQueue.main.async() {
        //                self.productImageView.image = UIImage(data: data)
        //            }
        //        }
        
        
        
        
    }
    
}
