//
//  InfoTableViewCell.swift
//  sample-videochat-webrtc-swift
//
//  Created by Injoit on 12/30/18.
//  Copyright © 2018 QuickBlox. All rights reserved.
//

import UIKit

class InfoTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleInfoLabel: UILabel!{
        didSet{
            self.titleInfoLabel.font = UIFont(name: appFont, size: 15.0)
            self.titleInfoLabel.text = self.titleInfoLabel.text?.localized
        }
    }
    @IBOutlet weak var descriptInfoLabel: UILabel!{
        didSet{
            self.descriptInfoLabel.font = UIFont(name: appFont, size: 15.0)
            self.descriptInfoLabel.text = self.descriptInfoLabel.text?.localized
        }
    }
    
    func applyInfo(model: InfoModel) {
        titleInfoLabel.text = model.title
        descriptInfoLabel.text = model.info
    }
}
