//
//  ChatViewController.swift
//  sample-chat-swift
//
//  Created by Injoit on 1/28/19.
//  Copyright © 2019 Quickblox. All rights reserved.
//

import UIKit
import Photos
import TTTAttributedLabel
import SafariServices
import CoreTelephony

// VAudio/Video Purpose //
import Quickblox
import QuickbloxWebRTC
import PushKit
import SVProgressHUD

//


var messageTimeDateFormatter: DateFormatter {
    struct Static {
        static let instance : DateFormatter = {
            let formatter = DateFormatter()
            formatter.dateFormat = "HH:mm"
            return formatter
        }()
    }
    return Static.instance
}

enum MessageStatus: Int {
    case sent
    case sending
    case notSent
}

struct ChatViewControllerConstant {
    static let messagePadding: CGFloat = 40.0
    static let attachmentBarHeight: CGFloat = 100.0
}

class ChatViewController: UIViewController {
    
    // Audio/Video //
    
    
    //MARK: - IBOutlets
    @IBOutlet private weak var audioCallButton: UIButton!
    @IBOutlet private weak var videoCallButton: UIButton!
    
    
    //MARK: - Properties
    lazy private var VdataSource: UsersDataSource = {
        let dataSource = UsersDataSource()
        return dataSource
    }()
    
    lazy private var navViewController: UINavigationController = {
        let navViewController = UINavigationController()
        navViewController.navigationBar.tintColor = UIColor.white
        return navViewController
        
    }()
    
    private weak var session: QBRTCSession?
    lazy private var voipRegistry: PKPushRegistry = {
        let voipRegistry = PKPushRegistry(queue: DispatchQueue.main)
        return voipRegistry
    }()
    
    
    private var callUUID: UUID?
    lazy private var backgroundTask: UIBackgroundTaskIdentifier = {
        let backgroundTask = UIBackgroundTaskIdentifier.invalid
        return backgroundTask
    }()
    
    
    
    //
    var storeValue = UserDefaults.standard.integer(forKey: "KeyAudioOrVideo")
    
    
    
    //MARK: - IBOutlets
    @IBOutlet private weak var collectionView: ChatCollectionView!
    /**
     *  Returns the input toolbar view object managed by this view controller.
     *  This view controller is the toolbar's delegate.
     */
    @IBOutlet private weak var inputToolbar: InputToolbar!
    
    
    //MARK: - Private IBOutlets
    @IBOutlet private weak var collectionBottomConstraint: NSLayoutConstraint!
    @IBOutlet private weak var toolbarBottomLayoutGuide: NSLayoutConstraint!
    
    private lazy var infoItem = UIBarButtonItem(title: "Chat Info",
                                                style: .plain,
                                                target: self,
                                                action:#selector(didTapInfo(_:)))
    
    
    
    private lazy var deleteBtn = UIBarButtonItem(image: UIImage(named: "more"),
                                                 style: .plain,
                                                 target: self,
                                                 action:#selector(didPressDeleteChat(_:)))
    
    
    
    //MARK: - Properties
    private lazy var dataSource: ChatDataSource = {
        let dataSource = ChatDataSource()
        dataSource.delegate = self
        return dataSource
    }()
    private let chatManager = ChatManager.instance
    private var offsetY: CGFloat = 0.0
    private let blueBubble = UIImage(named: "ios_bubble_blue")
    private let grayBubble = UIImage(named: "ios_bubble_gray")
    
    private var isDeviceLocked = false
    
    private var isUploading = false
    private var attachmentMessage: QBChatMessage?
    
    /**
     *  This property is required when creating a ChatViewController.
     */
    var dialogID: String! {
        didSet {
            self.dialog = chatManager.storage.dialog(withID: dialogID)
        }
    }
    private var dialog: QBChatDialog!
    /**
     *  Cell's contact request delegate.
     */
    private var actionsHandler: ChatActionsHandler?
    /**
     *  The display name of the current user who is sending messages.
     *
     *  @discussion This value does not have to be unique. This value must not be `nil`.
     */
    internal var senderDisplayName = ""
    /**
     *  The string identifier that uniquely identifies the current user sending messages.
     *
     *  @discussion This property is used to determine if a message is incoming or outgoing.
     *  All message data objects returned by `collectionView:messageDataForItemAtIndexPath:` are
     *  checked against this identifier. This value must not be `nil`.
     */
    internal var senderID: UInt = 0
    /**
     *  Specifies whether or not the view controller should automatically scroll to the most recent message
     *  when the view appears and when sending, receiving, and composing a new message.
     *
     *  @discussion The default value is `true`, which allows the view controller to scroll automatically to the most recent message.
     *  Set to `false` if you want to manage scrolling yourself.
     */
    private var automaticallyScrollsToMostRecentMessage = true
    /**
     *  Specifies an additional inset amount to be added to the collectionView's contentInsets.top value.
     *
     *  @discussion Use this property to adjust the top content inset to account for a custom subview at the top of your view controller.
     */
    private var topContentAdditionalInset: CGFloat = 0.0 {
        didSet {
            updateCollectionViewInsets()
        }
    }
    /**
     *  Enable text checking types for cells. Must be set in view did load.
     */
    private var enableTextCheckingTypes: NSTextCheckingTypes = NSTextCheckingAllTypes
    /**
     Input bar start pos
     */
    private var inputToolBarStartPos: UInt = 0
    private var collectionBottomConstant: CGFloat = 0.0
    //MARK: - Private Properties
    private var isMenuVisible: Bool {
        return selectedIndexPathForMenu != nil && UIMenuController.shared.isMenuVisible
    }
    
    private lazy var pickerController: UIImagePickerController = {
        let pickerController = UIImagePickerController()
        pickerController.modalPresentationStyle = .fullScreen
        pickerController.delegate = self
        return pickerController
    }()
    
    private var cancel = false
    
    private var willResignActiveBlock: AnyObject?
    private var willActiveBlock: AnyObject?
    
    private var selectedIndexPathForMenu: IndexPath?
    
    private lazy var systemInputToolbar: KVOView = {
        let inputToolbar = KVOView()
        inputToolbar.collectionView = collectionView
        inputToolbar.chatInputView = inputToolbar;
        inputToolbar.frame = .zero
        inputToolbar.hostViewFrameChangeBlock = { [weak self] (view: UIView?, animated: Bool) -> Void in
            guard let self = self,
                let superview = self.view.superview else {
                    return
            }
            
            let inputToolBarStartPos = CGFloat(self.inputToolBarStartPos)
            
            guard let view = view else {
                self.setupToolbarBottom(constraintValue:inputToolBarStartPos, animated: animated)
                return
            }
            
            let convertedViewPoint = superview.convert(view.frame.origin, to: view)
            var pos = view.frame.size.height - convertedViewPoint.y
            
            if self.inputToolbar.contentView.textView.isFirstResponder,
                superview.frame.origin.y > 0.0,
                pos <= 0.0 {
                return
            }
            if pos < inputToolBarStartPos {
                pos = inputToolBarStartPos
            }
            self.setupToolbarBottom(constraintValue:pos, animated: animated)
        }
        return inputToolbar
    }()
    
    private lazy var attachmentBar: AttachmentBar = {
        let attachmentBar = AttachmentBar()
        attachmentBar.setRoundBorderEdgeView(cornerRadius: 0.0, borderWidth: 0.5, borderColor: #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1))
        return attachmentBar
    }()
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.navigationController?.navigationBar.applyNavigationGradient(colors: [UIColor.colorWithRedValue(redValue: 13, greenValue: 177, blueValue: 75, alpha: 1), UIColor.init(red: 20.0/255.0, green: 185.0/255.0, blue: 85.0/255.0, alpha: 1.0)])
        
        
        
        let button = UIButton()
        button.setImage(UIImage(named: "back"), for: .normal)
        button.addTarget(self, action: #selector(backClick), for: .touchUpInside)
        button.imageEdgeInsets.left = -35
        let item = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = item
        
        
        QBChat.instance.addDelegate(self)
        setupViewMessages()
        dataSource.delegate = self
        inputToolbar.inputToolbarDelegate = self
        inputToolbar.setupBarButtonsEnabled(left: true, right: false)
        configureTableViewController()
        automaticallyAdjustsScrollViewInsets = false
        edgesForExtendedLayout = [] //same UIRectEdgeNone
        
        
        
        
        // Audio / Video
        QBRTCClient.instance().add(self)
        
        // Reachability2
        if Reachability2.instance.networkConnectionStatus() != NetworkConnectionStatus.notConnection {
            loadUsers()
        }
        
        
        
        voipRegistry.delegate = self
        voipRegistry.desiredPushTypes = Set<PKPushType>([.voIP])
        
        
        //
        
        
        
    }
    
    
    @objc func backClick(){
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        QBChat.instance.addDelegate(self)
        SVProgressHUD.dismiss()
        let currentUser = Profile()
        guard currentUser.isFull == true else {
            return
        }
        
        if QBChat.instance.isConnected == true {
            loadMessages()
        }
        
        senderID = currentUser.ID
        title = dialog.name ?? ""
        registerForNotifications(true)
        
        willResignActiveBlock = NotificationCenter.default.addObserver(forName: UIApplication.willResignActiveNotification, object: nil,   queue: nil) { [weak self] (notification) in  self?.isDeviceLocked = true
        }
        willActiveBlock = NotificationCenter.default.addObserver(forName: UIApplication.didBecomeActiveNotification, object: nil,  queue: nil) { [weak self] (notification) in
            self?.isDeviceLocked = false
            self?.collectionView.reloadData()
        }
        
        if inputToolbar.contentView.textView.isFirstResponder == false {
            toolbarBottomLayoutGuide.constant = CGFloat(inputToolBarStartPos)
        }
        updateCollectionViewInsets()
        collectionBottomConstraint.constant = collectionBottomConstant
        if dialog.type != .publicGroup {
            navigationItem.rightBarButtonItem?.tintColor = UIColor.white
            
            //navigationItem.rightBarButtonItem = infoItem
            
            
        }
        
        //MARK: - Reachability2
        let updateConnectionStatus: ((_ status: NetworkConnectionStatus) -> Void)? = { [weak self] status in
            let notConnection = status == .notConnection
            if notConnection == true, self?.isUploading == true {
                self?.cancelUploadFile()
                self?.cancelCallAlert()
            }
            else{
                self?.loadUsers()
            }
        }
        Reachability2.instance.networkStatusBlock = { status in
            updateConnectionStatus?(status)
        }
        updateConnectionStatus?(Reachability2.instance.networkConnectionStatus())
        
        
    }
    
    
    
    private func cancelCallAlert() {
        let alert = UIAlertController(title: UsersAlertConstant.checkInternet, message: nil, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action) in
            
            CallKitManager.instance.endCall(with: self.callUUID) {
                debugPrint("[UsersViewController] endCall")
                
            }
            self.prepareCloseCall()
        }
        alert.addAction(cancelAction)
        present(alert, animated: false) {
        }
    }
    
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if let willResignActive = willResignActiveBlock {
            NotificationCenter.default.removeObserver(willResignActive)
        }
        if let willActiveBlock = willActiveBlock {
            NotificationCenter.default.removeObserver(willActiveBlock)
        }
        NotificationCenter.default.removeObserver(self)
        // clearing typing status blocks
        dialog.clearTypingStatusBlocks()
        registerForNotifications(false)
    }
    
    
    // MARK: - UI Configuration
    private func configureTableViewController() {
        VdataSource = UsersDataSource()
        CallKitManager.instance.usersDatasource = VdataSource
        //        tableView.dataSource = dataSource
        //        tableView.rowHeight = 44
        //        refreshControl?.beginRefreshing()
    }
    
    
    
    
    //MARK: - Internal Methods
    //MARK: - Setup
    private func setupViewMessages() {
        registerCells()
        collectionView.transform = CGAffineTransform(a: 1.0, b: 0.0, c: 0.0, d: -1.0, tx: 0.0, ty: 0.0)
        setupInputToolbar()
    }
    
    private func registerCells() {
        if let headerNib = HeaderCollectionReusableView.nib(),
            let headerIdentifier = HeaderCollectionReusableView.cellReuseIdentifier() {
            collectionView.register(headerNib,
                                    forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter,
                                    withReuseIdentifier: headerIdentifier)
        }
        ChatNotificationCell.registerForReuse(inView: collectionView)
        ChatOutgoingCell.registerForReuse(inView: collectionView)
        ChatIncomingCell.registerForReuse(inView: collectionView)
        ChatAttachmentIncomingCell.registerForReuse(inView: collectionView)
        ChatAttachmentOutgoingCell.registerForReuse(inView: collectionView)
    }
    
    private func setupInputToolbar() {
        inputToolbar.delegate = self
        inputToolbar.contentView.textView.delegate = self
        
        inputToolbar.contentView.textView.text = "SA_STR_NEW_MESSAGE".localized
        inputToolbar.contentView.textView.textColor = UIColor.white
        //inputToolbar.contentView.textView.placeHolder = "SA_STR_NEW_MESSAGE".localized
        
        let accessoryImage = UIImage(named: "attachment_ic")
        let normalImage = accessoryImage?.imageMasked(color: .white)
        let highlightedImage = accessoryImage?.imageMasked(color: .white)
        let accessorySize = CGSize(width: accessoryImage?.size.width ?? 32.0, height: 32.0)
        let accessoryButton = UIButton(frame: CGRect(origin: .zero, size: accessorySize))
        accessoryButton.setImage(normalImage, for: .normal)
        accessoryButton.setImage(highlightedImage, for: .highlighted)
        accessoryButton.contentMode = .scaleAspectFit
        accessoryButton.backgroundColor = .clear
        accessoryButton.tintColor = .white
        
        inputToolbar.contentView.leftBarButtonItem = accessoryButton
        
        //        let sendTitle = ""
        //
        //        let titleMaxHeight:CGFloat = 32.0
        //        let titleMaxSize = CGSize(width: .greatestFiniteMagnitude, height: titleMaxHeight)
        //        let titleLabel = UILabel(frame: CGRect(origin: .zero, size: titleMaxSize))
        //        let font = UIFont.boldSystemFont(ofSize: 17.0)
        //        titleLabel.font = font
        //        titleLabel.text = sendTitle
        //        titleLabel.sizeToFit()
        //        let titleSize = CGSize(width: titleLabel.frame.width, height: titleMaxHeight)
        
        let sendButton = UIButton()
        //        sendButton.titleLabel?.font = font
        sendButton.setImage(UIImage.init(named: "send"), for: .normal)
        //sendButton.setTitle(sendTitle, for: .normal)
        sendButton.setTitleColor(.white, for: .normal)
        
        //        sendButton.setTitleColor(.white, for: .highlighted)
        //        sendButton.setTitleColor(.white, for: .disabled)
        //        sendButton.backgroundColor = .white
        //        sendButton.tintColor = .white
        
        //        sendButton.applyButtonGradient(colors: [UIColor.colorWithRedValue(redValue: 13, greenValue: 177, blueValue: 75, alpha: 1), UIColor.init(red: 20.0/255.0, green: 185.0/255.0, blue: 85.0/255.0, alpha: 1.0)])
        //        inputToolbar.contentView.applyViewnGradient(colors: [UIColor.colorWithRedValue(redValue: 13, greenValue: 177, blueValue: 75, alpha: 1), UIColor.init(red: 20.0/255.0, green: 185.0/255.0, blue: 85.0/255.0, alpha: 1.0)])
        inputToolbar.contentView.rightBarButtonItem = sendButton
        inputToolbar.contentView.textView.inputAccessoryView = systemInputToolbar
    }
    
    private func setupToolbarBottom(constraintValue: CGFloat, animated: Bool) {
        if constraintValue < 0.0 {
            return
        }
        if animated == false {
            let offsetY = collectionView.contentOffset.y + constraintValue - toolbarBottomLayoutGuide.constant
            collectionView.contentOffset = CGPoint(x: collectionView.contentOffset.x, y: offsetY)
        }
        toolbarBottomLayoutGuide.constant = constraintValue
        if animated {
            view.layoutIfNeeded()
        }
    }
    
    //MARK: - Actions
    private func cancelUploadFile() {
        hideAttacnmentBar()
        isUploading = false
        let alertController = UIAlertController(title: "SA_STR_ERROR".localized,
                                                message: "SA_STR_FAILED_UPLOAD_ATTACHMENT".localized,
                                                preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "SA_STR_CANCEL".localized, style: .cancel) { (action) in
            self.inputToolbar.setupBarButtonsEnabled(left: true, right: false)
        }
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
    }
    
    private func viewClass(forItem item: QBChatMessage) -> ChatReusableViewProtocol.Type {
        
        if item.customParameters["notification_type"] != nil || item.customParameters[ChatDataSourceConstant.dateDividerKey] as? Bool == true {
            return ChatNotificationCell.self
        }
        let hasAttachment = item.attachments?.isEmpty == false
        if item.senderID != senderID {
            return hasAttachment ? ChatAttachmentIncomingCell.self : ChatIncomingCell.self
        } else {
            return hasAttachment ? ChatAttachmentOutgoingCell.self : ChatOutgoingCell.self
        }
    }
    
    private func attributedString(forItem messageItem: QBChatMessage) -> NSAttributedString? {
        guard let text = messageItem.text  else {
            return nil
        }
        var textString = text
        var textColor = messageItem.senderID == senderID ? UIColor.white : .black
        if messageItem.customParameters["notification_type"] != nil || messageItem.customParameters[ChatDataSourceConstant.dateDividerKey] as? Bool == true {
            textColor = .black
        }
        if messageItem.customParameters["notification_type"] != nil {
            if let dateSent = messageItem.dateSent {
                textString = messageTimeDateFormatter.string(from: dateSent) + "\n" + textString
            }
        }
        let font = UIFont(name: "Helvetica", size: 15)
        let attributes: [NSAttributedString.Key: Any] = [.foregroundColor: textColor,
                                                         .font: font as Any]
        return NSAttributedString(string: textString, attributes: attributes)
    }
    
    private func topLabelAttributedString(forItem messageItem: QBChatMessage) -> NSAttributedString? {
        if dialog.type == .private,
            messageItem.senderID == senderID {
            return nil
        }
        let paragrpahStyle = NSMutableParagraphStyle()
        paragrpahStyle.lineBreakMode = .byTruncatingTail
        let color = UIColor(red: 11.0/255.0, green: 96.0/255.0, blue: 255.0/255.0, alpha: 1.0)
        let font = UIFont(name: "Helvetica", size: 17)
        let attributes: [NSAttributedString.Key: Any] = [.foregroundColor: color,
                                                         .font: font as Any,
                                                         .paragraphStyle: paragrpahStyle]
        let topLabelString = chatManager.storage.user(withID: messageItem.senderID)?.fullName ?? "@\(messageItem.senderID)"
        return NSAttributedString(string: topLabelString, attributes: attributes)
    }
    
    private func bottomLabelAttributedString(forItem messageItem: QBChatMessage) -> NSAttributedString {
        let textColor = messageItem.senderID == senderID ? UIColor.white : .black
        let paragrpahStyle = NSMutableParagraphStyle()
        paragrpahStyle.lineBreakMode = .byWordWrapping
        let font = UIFont(name: "Helvetica", size: 13)
        let attributes: [NSAttributedString.Key: Any] = [.foregroundColor: textColor,
                                                         .font: font as Any,
                                                         .paragraphStyle: paragrpahStyle]
        guard let dateSent = messageItem.dateSent else {
            return NSAttributedString(string: "")
        }
        var text = messageTimeDateFormatter.string(from: dateSent)
        if messageItem.senderID == self.senderID {
            text = text + "\n" + statusStringFromMessage(message: messageItem)
        }
        return NSAttributedString(string: text, attributes: attributes)
    }
    
    /**
     Builds a string
     Read: login1, login2, login3
     Delivered: login1, login3, @12345
     
     If user does not exist in usersMemoryStorage, then ID will be used instead of login
     
     - parameter message: QBChatMessage instance
     
     - returns: status string
     */
    private func statusStringFromMessage(message: QBChatMessage) -> String {
        var statusString = ""
        var readLogins: [String] = []
        //check and add users who read the message
        if let readIDs = message.readIDs?.filter({ $0 != NSNumber(value: senderID) }),
            readIDs.isEmpty == false {
            for readID in readIDs {
                guard let user = chatManager.storage.user(withID: readID.uintValue) else {
                    let userLogin = "@\(readID)"
                    readLogins.append(userLogin)
                    continue
                }
                let userName = user.fullName ?? user.login ?? ""
                if readLogins.contains(userName) {
                    continue
                }
                readLogins.append(userName)
            }
            statusString += message.attachments?.isEmpty == false ? "SA_STR_SEEN_STATUS".localized : "SA_STR_READ_STATUS".localized;
            statusString += ": " + readLogins.joined(separator: ", ")
        }
        //check and add users to whom the message was delivered
        if let deliveredIDs = message.deliveredIDs?.filter({ $0 != NSNumber(value: senderID) }) {
            var deliveredLogins: [String] = []
            for deliveredID in deliveredIDs {
                guard let user = chatManager.storage.user(withID: deliveredID.uintValue) else {
                    let userLogin = "@\(deliveredID)"
                    if readLogins.contains(userLogin) == false {
                        deliveredLogins.append(userLogin)
                    }
                    continue
                }
                let userName = user.fullName ?? user.login ?? ""
                if readLogins.contains(userName) {
                    continue
                }
                if deliveredLogins.contains(userName) {
                    continue
                }
                
                deliveredLogins.append(userName)
            }
            if deliveredLogins.isEmpty == false {
                if statusString.isEmpty == false {
                    statusString += "\n"
                }
                statusString += "SA_STR_DELIVERED_STATUS".localized + ": " + deliveredLogins.joined(separator: ", ")
            }
        }
        return statusString.isEmpty ? "SA_STR_SENT_STATUS".localized : statusString
    }
    
    
    @objc private func didTapInfo(_ sender: UIBarButtonItem) {
        performSegue(withIdentifier: "SA_STR_SEGUE_GO_TO_INFO".localized, sender: nil)
    }
    
    /**
     *  Animates the sending of a new message. See `finishSendingMessageAnimated:` for more details.
     *
     *  @see `finishSendingMessageAnimated:`.
     */
    private func finishSendingMessage() {
        finishSendingMessage(animated: true)
    }
    
    /**
     *  Completes the "receiving" of a new message by adding a new collection view cell in the collection view,
     *  reloading the collection view, and scrolling to the newly sent message as specified by `automaticallyScrollsToMostRecentMessage`.
     *  Scrolling to the new message can be animated as specified by the animated parameter.
     *
     *  @param animated Specifies whether the receiving of a message should be animated or not. Pass `true` to animate changes, `false` otherwise.
     *
     *  @discussion You should call this method after adding a new "received" message to your data source and performing any related tasks.
     *
     *  @see `automaticallyScrollsToMostRecentMessage`.
     */
    private func finishSendingMessage(animated: Bool) {
        let textView = inputToolbar.contentView.textView
        textView?.setupDefaultSettings()
        
        textView?.text = nil
        textView?.attributedText = nil
        textView?.undoManager?.removeAllActions()
        
        if attachmentMessage != nil {
            attachmentMessage = nil
        }
        
        if isUploading == true {
            inputToolbar.setupBarButtonsEnabled(left: false, right: false)
        } else {
            inputToolbar.setupBarButtonsEnabled(left: true, right: false)
        }
        
        NotificationCenter.default.post(name: UITextView.textDidChangeNotification, object: textView)
        
        if automaticallyScrollsToMostRecentMessage {
            scrollToBottomAnimated(animated)
        }
    }
    
    /**
     *  Scrolls the collection view such that the bottom most cell is completely visible, above the `inputToolbar`.
     *
     *  @param animated Pass `true` if you want to animate scrolling, `false` if it should be immediate.
     */
    private func scrollToBottomAnimated(_ animated: Bool) {
        if collectionView.numberOfItems(inSection: 0) == 0 {
            return
        }
        
        var contentOffset = collectionView.contentOffset
        
        if contentOffset.y == 0 {
            return
        }
        contentOffset.y = 0
        collectionView.setContentOffset(contentOffset, animated: animated)
    }
    
    /**
     *  Hides keyboard
     *
     *  @param animated Pass `true` if you want to animate hiding, `false` if it should be immediate.
     */
    private func hideKeyboard(animated: Bool) {
        let hideKeyboardBlock = { [weak self] in
            if self?.inputToolbar.contentView.textView.isFirstResponder == true {
                self?.inputToolbar.contentView.resignFirstResponder()
            }
        }
        if animated {
            hideKeyboardBlock()
        } else {
            UIView.performWithoutAnimation(hideKeyboardBlock)
        }
    }
    
    private func loadMessages(with skip: Int = 0) {
        SVProgressHUD.show()
        chatManager.messages(withID: dialogID, skip: skip, successCompletion: { [weak self] (messages, cancel) in
            self?.cancel = cancel
            self?.dataSource.addMessages(messages)
            SVProgressHUD.dismiss()
            }, errorHandler: { [weak self] (error) in
                if error == ChatManagerConstant.notFound {
                    self?.dataSource.clear()
                    self?.dialog.clearTypingStatusBlocks()
                    self?.inputToolbar.isUserInteractionEnabled = false
                    self?.collectionView.isScrollEnabled = false
                    self?.collectionView.reloadData()
                    self?.title = ""
                    self?.navigationItem.rightBarButtonItem?.isEnabled = false
                }
                SVProgressHUD.showError(withStatus: error)
        })
    }
    
    private func updateCollectionViewInsets() {
        if topContentAdditionalInset > 0.0 {
            var contentInset = collectionView.contentInset
            contentInset.top = topContentAdditionalInset
            collectionView.contentInset = contentInset
            collectionView.scrollIndicatorInsets = contentInset
        }
    }
    
    private func showPickerController(_ pickerController: UIImagePickerController,
                                      withSourceType sourceType: UIImagePickerController.SourceType) {
        pickerController.sourceType = sourceType
        pickerController.modalPresentationStyle = .fullScreen
        let show: (UIImagePickerController) -> Void = { [weak self] (pickerController) in
            DispatchQueue.main.async {
                pickerController.sourceType = sourceType
                self?.present(pickerController, animated: true, completion: nil)
                self?.inputToolbar.setupBarButtonsEnabled(left: false, right: false)
            }
        }
        
        let accessDenied: (_ withSourceType: UIImagePickerController.SourceType) -> Void = { [weak self] (sourceType) in
            let typeName = sourceType == .camera ? "Camera" : "Photos"
            let title = "\(typeName) Access Disabled"
            let message = "You can allow access to \(typeName) in Settings"
            let alertController = UIAlertController(title: title,
                                                    message: message,
                                                    preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Open Settings", style: .default, handler: { (action) in
                guard let settingsURL = URL(string: UIApplication.openSettingsURLString) else {
                    return
                }
                if UIApplication.shared.canOpenURL(settingsURL) {
                    UIApplication.shared.open(settingsURL, options: [:])
                }
            }))
            
            alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            DispatchQueue.main.async {
                self?.present(alertController, animated: true, completion: nil)
            }
        }
        //Check Access
        if sourceType == .camera {
            switch AVCaptureDevice.authorizationStatus(for: .video) {
            case .authorized:
                show(pickerController)
            case .notDetermined:
                AVCaptureDevice.requestAccess(for: .video) { (granted) in
                    if granted {
                        show(pickerController)
                    } else {
                        accessDenied(sourceType)
                    }
                }
            case .denied, .restricted:
                accessDenied(sourceType)
                
            }
        } else {
            //Photo Library Access
            switch PHPhotoLibrary.authorizationStatus() {
            case .authorized:
                show(pickerController)
            case .notDetermined:
                PHPhotoLibrary.requestAuthorization { (status) in
                    if status == .authorized {
                        show(pickerController)
                    } else {
                        accessDenied(sourceType)
                    }
                }
            case .denied, .restricted:
                accessDenied(sourceType)
            }
        }
    }
    
    private func showAttachmentBar(with image: UIImage) {
        view.addSubview(attachmentBar)
        attachmentBar.delegate = self
        attachmentBar.translatesAutoresizingMaskIntoConstraints = false
        attachmentBar.leftAnchor.constraint(equalTo: inputToolbar.leftAnchor).isActive = true
        attachmentBar.rightAnchor.constraint(equalTo: inputToolbar.rightAnchor).isActive = true
        attachmentBar.bottomAnchor.constraint(equalTo: inputToolbar.topAnchor).isActive = true
        attachmentBar.heightAnchor.constraint(equalToConstant: ChatViewControllerConstant.attachmentBarHeight).isActive = true
        
        attachmentBar.uploadAttachmentImage(image, sourceType: pickerController.sourceType)
        attachmentBar.cancelButton.isHidden = true
        collectionBottomConstant = ChatViewControllerConstant.attachmentBarHeight
        isUploading = true
        inputToolbar.setupBarButtonsEnabled(left: false, right: false)
        
    }
    
    private func hideAttacnmentBar() {
        attachmentBar.removeFromSuperview()
        attachmentBar.imageView.image = nil
        collectionBottomConstant = 0.0
        collectionBottomConstraint.constant = collectionBottomConstant
        UIView.animate(withDuration: 0.3, animations: { [weak self] in
            self?.view.layoutIfNeeded()
        })
    }
    
    private func createAttachmentMessage(with attachment: QBChatAttachment) -> QBChatMessage {
        let message = QBChatMessage.markable()
        message.text = "[Attachment]"
        message.senderID = senderID
        message.dialogID = dialogID
        message.deliveredIDs = [(NSNumber(value: senderID))]
        message.readIDs = [(NSNumber(value: senderID))]
        message.dateSent = Date()
        message.customParameters["save_to_history"] = true
        message.attachments = [attachment]
        return message
    }
    
    private func didPressSend(_ button: UIButton) {
        
        if let attacmentMessage = attachmentMessage, isUploading == false {
            send(withAttachmentMessage: attacmentMessage)
        }
        if let messageText = currentlyComposedMessageText(), messageText.isEmpty == false {
            send(withMessageText: messageText)
        }
    }
    
    private func send(withAttachmentMessage attachmentMessage: QBChatMessage) {
        hideAttacnmentBar()
        sendMessage(message: attachmentMessage)
    }
    
    private func send(withMessageText text: String) {
        let message = QBChatMessage.markable()
        message.text = text
        message.senderID = senderID
        message.dialogID = dialogID
        message.deliveredIDs = [(NSNumber(value: senderID))]
        message.readIDs = [(NSNumber(value: senderID))]
        message.dateSent = Date()
        message.customParameters["save_to_history"] = true
        sendMessage(message: message)
    }
    
    private func sendMessage(message: QBChatMessage) {
        chatManager.send(message, to: dialog) { [weak self] (error) in
            if let error = error {
                debugPrint("[ChatViewController] sendMessage error: \(error.localizedDescription)")
                return
            }
            self?.dataSource.addMessage(message)
            self?.finishSendingMessage(animated: true)
        }
    }
    
    private func currentlyComposedMessageText() -> String? {
        //  auto-accept any auto-correct suggestions
        if let inputDelegate = inputToolbar.contentView.textView.inputDelegate {
            inputDelegate.selectionWillChange(inputToolbar.contentView.textView)
            inputDelegate.selectionDidChange(inputToolbar.contentView.textView)
        }
        return inputToolbar.contentView.textView.text.stringByTrimingWhitespace()
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SA_STR_SEGUE_GO_TO_INFO".localized {
            if let chatInfoViewController = segue.destination as? UsersInfoTableViewController {
                chatInfoViewController.dialogID = dialogID
            }
        }
        
        switch segue.identifier {
        case UsersSegueConstant.settings:
            let settingsViewController = (segue.destination as? UINavigationController)?.topViewController
                as? SessionSettingsViewController
            settingsViewController?.delegate = self
        case UsersSegueConstant.call:
            debugPrint("[UsersViewController] UsersSegueConstant.call")
        default:
            break
        }
    }
    
    //MARK: - Notifications
    private func registerForNotifications(_ registerForNotifications: Bool) {
        let defaultCenter = NotificationCenter.default
        if registerForNotifications {
            defaultCenter.addObserver(self,
                                      selector: #selector(didReceiveMenuWillShow(notification:)),
                                      name: UIMenuController.willShowMenuNotification,
                                      object: nil)
            
            defaultCenter.addObserver(self,
                                      selector: #selector(didReceiveMenuWillHide(notification:)),
                                      name: UIMenuController.willHideMenuNotification,
                                      object: nil)
        } else {
            defaultCenter.removeObserver(self, name: UIMenuController.willShowMenuNotification, object: nil)
            defaultCenter.removeObserver(self, name: UIMenuController.willHideMenuNotification, object: nil)
        }
    }
    
    @objc private func didReceiveMenuWillShow(notification: Notification) {
        guard let selectedIndexPath = selectedIndexPathForMenu,
            let menu = notification.object as? UIMenuController,
            let selectedCell = collectionView.cellForItem(at: selectedIndexPath) else {
                return
        }
        let defaultCenter = NotificationCenter.default
        defaultCenter.removeObserver(self, name: UIMenuController.willShowMenuNotification, object: nil)
        
        menu.setMenuVisible(false, animated: false)
        
        let selectedMessageBubbleFrame = selectedCell.convert(selectedCell.contentView.frame, to: view)
        
        menu.setTargetRect(selectedMessageBubbleFrame, in: view)
        menu.setMenuVisible(true, animated: true)
        
        defaultCenter.addObserver(self,
                                  selector: #selector(didReceiveMenuWillShow(notification:)),
                                  name: UIMenuController.willShowMenuNotification,
                                  object: nil)
    }
    
    @objc private func didReceiveMenuWillHide(notification: Notification) {
        if selectedIndexPathForMenu == nil {
            return
        }
        
        selectedIndexPathForMenu = nil
    }
    
    //MARK - Orientation
    override func viewWillTransition(to size: CGSize,
                                     with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        coordinator.animate(alongsideTransition: nil,
                            completion: { [weak self] (context) in
                                self?.updateCollectionViewInsets()
        })
        
        if inputToolbar.contentView.textView.isFirstResponder,
            let splitViewController = splitViewController,
            splitViewController.isCollapsed == false {
            inputToolbar.contentView.textView.resignFirstResponder()
        }
    }
    
    
    // Audio/Video
    
    
    /**
     *  Load all (Recursive) users for current room (tag)
     */
    @objc func loadUsers() {
        
        let firstPage = QBGeneralResponsePage(currentPage: 1, perPage: 100)
        QBRequest.users(withExtendedRequest: ["order": "desc date updated_at"],
                        page: firstPage,
                        successBlock: { [weak self] (response, page, users) in
                            self?.VdataSource.update(users: users)
                            
                            //                            for n in 0...objectCount - 1 {
                            //                                let indexPath = IndexPath(row: n, section: 0)
                            //                                self?.VdataSource.selectUser(at:indexPath)
                            //                            }
                            //                            self?.tableView.reloadData()
                            //                            self?.refreshControl?.endRefreshing()
                            
            }, errorBlock: { response in
                //                self.refreshControl?.endRefreshing()
                debugPrint("[UsersViewController] loadUsers error: \(self.errorMessage(response: response) ?? "")")
        })
        
        
        
    }
    
    
    
    @IBAction func didPressAudioCall(_ sender: UIButton?) {
        
        self.VdataSource.selectUser(at:IndexPath(row: 0, section: 0))
        call(with: QBRTCConferenceType.audio)
        
    }
    
    @IBAction func didPressVideoCall(_ sender: UIButton?) {
        
        self.VdataSource.selectUser(at:IndexPath(row: 0, section: 0))
        call(with: QBRTCConferenceType.video)
        
    }
    
    
    @IBAction func didPressDeleteChat(_ sender: UIButton?) {
        
        
        
    }
    
    
    
    // MARK: - Internal Methods
    private func hasConnectivity() -> Bool {
        
        let status = Reachability2.instance.networkConnectionStatus()
        guard status != NetworkConnectionStatus.notConnection else {
            showAlertView(message: UsersAlertConstant.checkInternet)
            if CallKitManager.instance.isCallStarted() == false {
                CallKitManager.instance.endCall(with: callUUID) {
                    debugPrint("[UsersViewController] endCall")
                }
            }
            return false
        }
        return true
    }
    
    
    private func showAlertView(message: String?) {
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: UsersAlertConstant.okAction, style: .default,
                                                handler: nil))
        present(alertController, animated: true)
    }
    
    
    @objc func didPressSettingsButton(_ item: UIBarButtonItem?) {
        let settingsStoryboard =  UIStoryboard(name: "Settings", bundle: nil)
        if let settingsController = settingsStoryboard.instantiateViewController(withIdentifier: "SessionSettingsViewController") as? SessionSettingsViewController {
            settingsController.delegate = self
            navigationController?.pushViewController(settingsController, animated: true)
        }
    }
    
    
    
    
    
    private func call(with conferenceType: QBRTCConferenceType) {
        
        if session != nil {
            return
        }
        
        if hasConnectivity() {
            CallPermissions.check(with: conferenceType) { granted in
                if granted {
                    let opponentsIDs = self.VdataSource.ids(forUsers: self.VdataSource.selectedUsers)
                    
                    //Create new session
                    let session = QBRTCClient.instance().createNewSession(withOpponents: opponentsIDs, with: conferenceType)
                    if session.id.isEmpty == false {
                        self.session = session
                        let uuid = UUID()
                        self.callUUID = uuid
                        
                        CallKitManager.instance.startCall(withUserIDs: opponentsIDs, session: session, uuid: uuid)
                        
                        
                        if let callViewController = UIStoryboard(name: "Call", bundle: nil).instantiateViewController(withIdentifier: UsersSegueConstant.call) as? CallViewController {
                            callViewController.session = self.session
                            callViewController.usersDataSource = self.VdataSource
                            callViewController.callUUID = uuid
                            let nav = UINavigationController(rootViewController: callViewController)
                            nav.navigationBar.tintColor = UIColor.white
                            nav.modalPresentationStyle = .fullScreen
                            self.present(nav , animated: false)
                            self.audioCallButton.isEnabled = false
                            self.videoCallButton.isEnabled = false
                            self.navViewController = nav
                        }
                        let profile = Profile()
                        guard profile.isFull == true else {
                            return
                        }
                        let opponentName = profile.fullName.isEmpty == false ? profile.fullName : "Unknown user"
                        let payload = ["message": "\(opponentName) is calling you.",
                            "ios_voip": "1", UsersConstant.voipEvent: "1"]
                        let data = try? JSONSerialization.data(withJSONObject: payload,
                                                               options: .prettyPrinted)
                        var message = ""
                        if let data = data {
                            message = String(data: data, encoding: .utf8) ?? ""
                        }
                        let event = QBMEvent()
                        event.notificationType = QBMNotificationType.push
                        let arrayUserIDs = opponentsIDs.map({"\($0)"})
                        event.usersIDs = arrayUserIDs.joined(separator: ",")
                        event.type = QBMEventType.oneShot
                        event.message = message
                        QBRequest.createEvent(event, successBlock: { response, events in
                            debugPrint("[UsersViewController] Send voip push - Success")
                        }, errorBlock: { response in
                            debugPrint("[UsersViewController] Send voip push - Error")
                        })
                    } else {
                        SVProgressHUD.showError(withStatus: UsersAlertConstant.shouldLogin)
                    }
                }
            }
        }
    }
    
    
    
    
    //Handle Error
    private func errorMessage(response: QBResponse) -> String? {
        var errorMessage : String
        if response.status.rawValue == 502 {
            errorMessage = "Bad Gateway, please try again"
        } else if response.status.rawValue == 0 {
            errorMessage = "Connection network error, please try again"
        } else {
            guard let qberror = response.error,
                let error = qberror.error else {
                    return nil
            }
            
            errorMessage = error.localizedDescription.replacingOccurrences(of: "(",
                                                                           with: "",
                                                                           options:.caseInsensitive,
                                                                           range: nil)
            errorMessage = errorMessage.replacingOccurrences(of: ")",
                                                             with: "",
                                                             options: .caseInsensitive,
                                                             range: nil)
        }
        return errorMessage
    }
    
    
    
    //
    
}

//MARK: - UIScrollViewDelegate
extension ChatViewController: UIScrollViewDelegate {
    func scrollViewShouldScrollToTop(_ scrollView: UIScrollView) -> Bool {
        // disabling scroll to bottom when tapping status bar
        return false
    }
}

//MARK: - UIImagePickerControllerDelegate
extension ChatViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
        guard let image = info[UIImagePickerController.InfoKey.originalImage.rawValue] as? UIImage else {
            return
        }
        inputToolbar.setupBarButtonsEnabled(left: false, right: false)
        showAttachmentBar(with: image)
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    // Helper function.
    private func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
        return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})}
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
        inputToolbar.setupBarButtonsEnabled(left: true, right: false)
    }
}

//MARK: - ChatDataSourceDelegate
extension ChatViewController: ChatDataSourceDelegate {
    func chatDataSource(_ chatDataSource: ChatDataSource,
                        willChangeWithMessageIDs IDs: [String]) {
        IDs.forEach{ collectionView.chatCollectionViewLayout?.removeSizeFromCache(forItemID: $0) }
    }
    
    func chatDataSource(_ chatDataSource: ChatDataSource,
                        changeWithMessages messages: [QBChatMessage],
                        action: ChatDataSourceAction) {
        if messages.isEmpty {
            return
        }
        
        collectionView.performBatchUpdates({ [weak self] in
            guard let self = self else {
                return
            }
            
            let indexPaths = chatDataSource.performChangesFor(messages: messages, action: action)
            
            if indexPaths.isEmpty {
                return
            }
            
            switch action {
            case .add: self.collectionView.insertItems(at: indexPaths)
            case .update: self.collectionView.reloadItems(at: indexPaths)
            case .remove: self.collectionView.deleteItems(at: indexPaths)
            }
            
            }, completion: nil)
    }
}

//MARK: - InputToolbarDelegate
extension ChatViewController: InputToolbarDelegate {
    func messagesInputToolbar(_ toolbar: InputToolbar, didPressRightBarButton sender: UIButton) {
        if toolbar.sendButtonOnRight {
            didPressSend(sender)
        } else {
            didPressAccessoryButton(sender)
        }
    }
    
    func messagesInputToolbar(_ toolbar: InputToolbar, didPressLeftBarButton sender: UIButton) {
        if toolbar.sendButtonOnRight {
            didPressAccessoryButton(sender)
        } else {
            didPressSend(sender)
        }
    }
    
    func didPressAccessoryButton(_ sender: UIButton) {
        let alertController = UIAlertController(title: nil,
                                                message: nil,
                                                preferredStyle: .actionSheet)
        
        alertController.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            self.showPickerController(self.pickerController, withSourceType: .camera)
        }))
        
        alertController.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action) in
            self.showPickerController(self.pickerController, withSourceType: .photoLibrary)
        }))
        
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        if let popoverPresentationController = alertController.popoverPresentationController {
            // iPad support
            popoverPresentationController.sourceView = sender
            popoverPresentationController.sourceRect = sender.bounds
        }
        present(alertController, animated: true, completion: nil)
    }
}

//MARK: - UICollectionViewDelegate
extension ChatViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView,
                        shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        selectedIndexPathForMenu = indexPath
        return true
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        canPerformAction action: Selector,
                        forItemAt indexPath: IndexPath,
                        withSender sender: Any?) -> Bool {
        if action != #selector(copy(_:)) {
            return false
        }
        guard let item = dataSource.messageWithIndexPath(indexPath) else {
            return false
        }
        if  self.viewClass(forItem: item) === ChatNotificationCell.self {
            return false
        }
        return true
    }
    
    func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
        guard let message = dataSource.messageWithIndexPath(indexPath) else {
            return
        }
        if message.attachments?.isEmpty == false {
            return
        }
        UIPasteboard.general.string = message.text
    }
}

//MARK: - ChatCollectionViewDataSource
extension ChatViewController: ChatCollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.messages.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing:ChatIncomingCell.self),
                                                      for: indexPath)
        guard let message = dataSource.messageWithIndexPath(indexPath) else {
            return cell
        }
        
        let cellClass = viewClass(forItem: message)
        
        guard let identifier = cellClass.cellReuseIdentifier() else {
            return cell
        }
        
        let chatCell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier,
                                                          for: indexPath)
        
        if let chatCollectionView = collectionView as? ChatCollectionView {
            self.collectionView(chatCollectionView, configureCell: chatCell, for: indexPath)
        }
        
        let lastSection = collectionView.numberOfSections - 1
        let lastItem = collectionView.numberOfItems(inSection: lastSection) - 1
        
        if indexPath.section == lastSection,
            indexPath.item == lastItem,
            cancel == false  {
            loadMessages(with: dataSource.loadMessagesCount)
        }
        
        return chatCell
    }
    
    func collectionView(_ collectionView: ChatCollectionView, itemIdAt indexPath: IndexPath) -> String {
        guard let message = dataSource.messageWithIndexPath(indexPath), let ID = message.id else {
            return "0"
        }
        return ID
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        // marking message as read if needed
        if isDeviceLocked == true {
            return
        }
        guard let message = dataSource.messageWithIndexPath(indexPath) else {
            return
        }
        if message.readIDs?.contains(NSNumber(value: senderID)) == false {
            chatManager.read([message], dialog: dialog, completion: nil)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        didEndDisplaying cell: UICollectionViewCell,
                        forItemAt indexPath: IndexPath) {
        guard let item = dataSource.messageWithIndexPath(indexPath),
            let attachment = item.attachments?.first,
            let attachmentID = attachment.id else {
                return
        }
        let attachmentDownloadManager = AttachmentDownloadManager()
        attachmentDownloadManager.slowDownloadAttachment(attachmentID)
    }
    
    private func collectionView(_ collectionView: ChatCollectionView,
                                configureCell cell: UICollectionViewCell,
                                for indexPath: IndexPath) {
        
        guard let item = dataSource.messageWithIndexPath(indexPath) else {
            return
        }
        
        if let notificationCell = cell as? ChatNotificationCell {
            notificationCell.isUserInteractionEnabled = false
            notificationCell.notificationLabel.attributedText = attributedString(forItem: item)
            return
        }
        
        guard let chatCell = cell as? ChatCell else {
            return
        }
        
        if cell is ChatIncomingCell
            || cell is ChatOutgoingCell {
            chatCell.textView.enabledTextCheckingTypes = enableTextCheckingTypes
        }
        
        chatCell.topLabel.text = topLabelAttributedString(forItem: item)
        chatCell.bottomLabel.text = bottomLabelAttributedString(forItem: item)
        if let textView = chatCell.textView {
            textView.text = attributedString(forItem: item)
        }
        
        chatCell.delegate = self
        
        if let attachmentCell = cell as? ChatAttachmentCell {
            
            guard let attachment = item.attachments?.first,
                let attachmentID = attachment.id,
                attachment.type == "image" else {
                    return
            }
            //setup image to attachmentCell
            attachmentCell.setupAttachmentWithID(attachmentID)
            
            if attachmentCell is ChatAttachmentIncomingCell {
                chatCell.containerView.bubbleImageView.image = grayBubble
            }
            else if attachmentCell is ChatAttachmentOutgoingCell {
                chatCell.containerView.bubbleImageView.image = blueBubble
            }
            
        }
        else if chatCell is ChatIncomingCell {
            chatCell.containerView.bubbleImageView.image = grayBubble
        }
        else if chatCell is ChatOutgoingCell {
            chatCell.containerView.bubbleImageView.image = blueBubble
        }
    }
}


//MARK: - ChatCollectionViewDelegateFlowLayout
extension ChatViewController: ChatCollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard let chatLayout = collectionViewLayout as? ChatCollectionViewFlowLayout else {
            return .zero
        }
        return chatLayout.sizeForItem(at: indexPath)
    }
    
    func collectionView(_ collectionView: ChatCollectionView, layoutModelAt indexPath: IndexPath) -> ChatCellLayoutModel {
        guard let item = dataSource.messageWithIndexPath(indexPath),
            let _ = item.id,
            let cellClass = viewClass(forItem: item) as? ChatCellProtocol.Type else {
                return ChatCell.layoutModel()
        }
        var layoutModel = cellClass.layoutModel()
        
        layoutModel.avatarSize = .zero
        layoutModel.topLabelHeight = 0.0
        layoutModel.spaceBetweenTextViewAndBottomLabel = 5.0
        layoutModel.maxWidthMarginSpace = 20.0
        
        if cellClass == ChatIncomingCell.self || cellClass == ChatAttachmentIncomingCell.self {
            
            if dialog.type != .private {
                let topAttributedString = topLabelAttributedString(forItem: item)
                let size = TTTAttributedLabel.sizeThatFitsAttributedString(topAttributedString,
                                                                           withConstraints: CGSize(width: collectionView.frame.width - ChatViewControllerConstant.messagePadding,
                                                                                                   height: CGFloat.greatestFiniteMagnitude),
                                                                           limitedToNumberOfLines:1)
                layoutModel.topLabelHeight = size.height
                layoutModel.avatarSize = CGSize(width: 35.0, height: 36.0)
            }
            layoutModel.spaceBetweenTopLabelAndTextView = 5
        }
        
        let bottomAttributedString = bottomLabelAttributedString(forItem: item)
        let size = TTTAttributedLabel.sizeThatFitsAttributedString(bottomAttributedString, withConstraints: CGSize(width: collectionView.frame.width - ChatViewControllerConstant.messagePadding, height: CGFloat.greatestFiniteMagnitude), limitedToNumberOfLines:0)
        layoutModel.bottomLabelHeight = floor(size.height)
        
        return layoutModel
    }
    
    func collectionView(_ collectionView: ChatCollectionView,
                        minWidthAt indexPath: IndexPath) -> CGFloat {
        guard let item = dataSource.messageWithIndexPath(indexPath),
            let _ = item.id else {
                return 0.0
        }
        
        let frameWidth = collectionView.frame.width
        let constraintsSize = CGSize(width:frameWidth - ChatViewControllerConstant.messagePadding,
                                     height: .greatestFiniteMagnitude)
        
        let attributedString = bottomLabelAttributedString(forItem: item)
        var size = TTTAttributedLabel.sizeThatFitsAttributedString(
            attributedString,
            withConstraints: constraintsSize,
            limitedToNumberOfLines:0)
        
        if dialog.type != .private {
            let attributedString = topLabelAttributedString(forItem: item)
            
            let topLabelSize = TTTAttributedLabel.sizeThatFitsAttributedString(
                attributedString,
                withConstraints: constraintsSize,
                limitedToNumberOfLines:0)
            
            if topLabelSize.width > size.width {
                size = topLabelSize
            }
        }
        return size.width
    }
    
    func collectionView(_ collectionView: ChatCollectionView, dynamicSizeAt indexPath: IndexPath, maxWidth: CGFloat) -> CGSize {
        var size: CGSize = .zero
        guard let message = dataSource.messageWithIndexPath(indexPath) else {
            return size
        }
        let messageCellClass = viewClass(forItem: message)
        if messageCellClass === ChatAttachmentIncomingCell.self {
            size = CGSize(width: min(200, maxWidth), height: 200)
        } else if messageCellClass === ChatAttachmentOutgoingCell.self {
            let attributedString = bottomLabelAttributedString(forItem: message)
            let bottomLabelSize = TTTAttributedLabel.sizeThatFitsAttributedString(attributedString,
                                                                                  withConstraints: CGSize(width: min(200, maxWidth),
                                                                                                          height: CGFloat.greatestFiniteMagnitude),
                                                                                  limitedToNumberOfLines: 0)
            size = CGSize(width: min(200, maxWidth), height: 200 + ceil(bottomLabelSize.height))
        } else if messageCellClass === ChatNotificationCell.self {
            let attributedString = self.attributedString(forItem: message)
            size = TTTAttributedLabel.sizeThatFitsAttributedString(attributedString,
                                                                   withConstraints: CGSize(width: maxWidth,
                                                                                           height: CGFloat.greatestFiniteMagnitude),
                                                                   limitedToNumberOfLines: 0)
        } else {
            let attributedString = self.attributedString(forItem: message)
            size = TTTAttributedLabel.sizeThatFitsAttributedString(attributedString,
                                                                   withConstraints: CGSize(width: maxWidth,
                                                                                           height: CGFloat.greatestFiniteMagnitude),
                                                                   limitedToNumberOfLines: 0)
        }
        return size
    }
}

//MARK: - UITextViewDelegate
extension ChatViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.white
        }
        textView.text = ""
        
        if textView != inputToolbar.contentView.textView {
            return
        }
        if automaticallyScrollsToMostRecentMessage == true {
            collectionBottomConstraint.constant = collectionBottomConstant
            scrollToBottomAnimated(true)
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if textView != inputToolbar.contentView.textView {
            return
        }
        if isUploading == true || attachmentMessage != nil {
            inputToolbar.setupBarButtonsEnabled(left: false, right: true)
        } else {
            inputToolbar.setupBarButtonsEnabled(left: true, right: true)
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "SA_STR_NEW_MESSAGE".localized
            textView.textColor = UIColor.lightGray
        }
        
        if textView != inputToolbar.contentView.textView {
            return
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if let char = text.cString(using: String.Encoding.utf8) {
            let isBackSpace = strcmp(char, "\\b")
            if (isBackSpace == -92) {
                return true
            }
            
        }
        
        if textView.isValid(name: textView.text){
            return true
        }
        
        
        if range.length + range.location > textView.text.count {
            return false
        }
        return true
    }
    
    
    
    
}

//MARK: - ChatCellDelegate
extension ChatViewController: ChatCellDelegate {
    
    private func handleNotSentMessage(_ message: QBChatMessage,
                                      forCell cell: ChatCell) {
        
        let alertController = UIAlertController(title: "", message: "SA_STR_MESSAGE_FAILED_TO_SEND".localized, preferredStyle:.actionSheet)
        
        let resend = UIAlertAction(title: "SA_STR_TRY_AGAIN_MESSAGE".localized, style: .default) { (action) in
        }
        alertController.addAction(resend)
        
        let delete = UIAlertAction(title: "SA_STR_DELETE_MESSAGE".localized, style: .destructive) { (action) in
            
            self.dataSource.deleteMessage(message)
        }
        alertController.addAction(delete)
        
        let cancelAction = UIAlertAction(title: "SA_STR_CANCEL".localized, style: .cancel) { (action) in
        }
        
        alertController.addAction(cancelAction)
        
        if alertController.popoverPresentationController != nil {
            view.endEditing(true)
            alertController.popoverPresentationController!.sourceView = cell.containerView
            alertController.popoverPresentationController!.sourceRect = cell.containerView.bounds
        }
        
        self.present(alertController, animated: true) {
        }
    }
    
    func chatCellDidTapAvatar(_ cell: ChatCell) {
    }
    
    private func openZoomVC(image: UIImage) {
        let zoomedVC = ZoomedAttachmentViewController()
        zoomedVC.zoomImageView.image = image
        zoomedVC.modalPresentationStyle = .overCurrentContext
        zoomedVC.modalTransitionStyle = .crossDissolve
        present(zoomedVC, animated: true, completion: nil)
    }
    
    func chatCellDidTapContainer(_ cell: ChatCell) {
        if let attachmentCell = cell as? ChatAttachmentCell, let attachmentImage = attachmentCell.attachmentImageView.image {
            self.openZoomVC(image: attachmentImage)
        }
    }
    func chatCell(_ cell: ChatCell, didTapAtPosition position: CGPoint) {}
    func chatCell(_ cell: ChatCell, didPerformAction action: Selector, withSender sender: Any) {}
    func chatCell(_ cell: ChatCell, didTapOn result: NSTextCheckingResult) {
        
        switch result.resultType {
        case NSTextCheckingResult.CheckingType.link:
            guard let strUrl = result.url?.absoluteString else {
                return
            }
            let hasPrefix = strUrl.lowercased().hasPrefix("https://") || strUrl.lowercased().hasPrefix("http://")
            if hasPrefix == true {
                guard let url = URL(string: strUrl) else {
                    return
                }
                let controller = SFSafariViewController(url: url)
                present(controller, animated: true, completion: nil)
            }
        case NSTextCheckingResult.CheckingType.phoneNumber:
            if canMakeACall() == false {
                SVProgressHUD.showInfo(withStatus: "Your Device can't make a phone call".localized, maskType: .none)
                break
            }
            view.endEditing(true)
            let alertController = UIAlertController(title: "",
                                                    message: result.phoneNumber,
                                                    preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "SA_STR_CANCEL".localized, style: .cancel, handler: nil)
            alertController.addAction(cancelAction)
            
            let openAction = UIAlertAction(title: "SA_STR_CALL".localized, style: .destructive) { (action) in
                if let phoneNumber = result.phoneNumber,
                    let url = URL(string: "tel:" + phoneNumber) {
                    UIApplication.shared.open(url, options: [:])
                }
            }
            alertController.addAction(openAction)
            present(alertController, animated: true) {
            }
        default:
            break
        }
    }
    
    private func canMakeACall() -> Bool {
        var canMakeACall = false
        if let url = URL.init(string: "tel://"), UIApplication.shared.canOpenURL(url) == true {
            // Check if iOS Device supports phone calls
            let networkInfo = CTTelephonyNetworkInfo()
            guard let carrier = networkInfo.subscriberCellularProvider else {
                return false
            }
            let mobileNetworkCode = carrier.mobileNetworkCode
            if mobileNetworkCode?.isEmpty == true {
                // Device cannot place a call at this time.  SIM might be removed.
            } else {
                // iOS Device is capable for making calls
                canMakeACall = true
            }
        } else {
            // iOS Device is not capable for making calls
        }
        return canMakeACall
    }
}

//MARK: - QBChatDelegate
extension ChatViewController: QBChatDelegate {
    func chatDidReadMessage(withID messageID: String, dialogID: String, readerID: UInt) {
        if senderID == readerID || dialogID != self.dialogID {
            return
        }
        guard let message = dataSource.messageWithID(messageID) else {
            return
        }
        message.readIDs?.append(NSNumber(value: readerID))
        dataSource.updateMessage(message)
    }
    
    func chatDidDeliverMessage(withID messageID: String, dialogID: String, toUserID userID: UInt) {
        if senderID == userID || dialogID != self.dialogID {
            return
        }
        guard let message = dataSource.messageWithID(messageID) else {
            return
        }
        message.deliveredIDs?.append(NSNumber(value: userID))
        dataSource.updateMessage(message)
    }
    
    func chatDidReceive(_ message: QBChatMessage) {
        if message.dialogID == self.dialogID {
            dataSource.addMessage(message)
        }
    }
    func chatRoomDidReceive(_ message: QBChatMessage, fromDialogID dialogID: String) {
        if dialogID == self.dialogID {
            dataSource.addMessage(message)
        }
    }
    
    func chatDidConnect() {
        refreshAndReadMessages()
    }
    
    func chatDidReconnect() {
        refreshAndReadMessages()
    }
    
    //MARK - Help
    private func refreshAndReadMessages() {
        SVProgressHUD.show(withStatus: "SA_STR_LOADING_MESSAGES".localized, maskType: .clear)
        loadMessages()
    }
}

//MARK: - AttachmentBarDelegate
extension ChatViewController: AttachmentBarDelegate {
    func attachmentBarFailedUpLoadImage(_ attachmentBar: AttachmentBar) {
        cancelUploadFile()
    }
    
    func attachmentBar(_ attachmentBar: AttachmentBar, didUpLoadAttachment attachment: QBChatAttachment) {
        attachmentMessage = createAttachmentMessage(with: attachment)
        isUploading = false
        inputToolbar.setupBarButtonsEnabled(left: false, right: true)
    }
    
    func attachmentBar(_ attachmentBar: AttachmentBar, didTapCancelButton: UIButton) {
        attachmentMessage = nil
        inputToolbar.setupBarButtonsEnabled(left: true, right: false)
        hideAttacnmentBar()
    }
}




// Audio/Video


// MARK: - QBRTCClientDelegate
extension ChatViewController: QBRTCClientDelegate {
    func session(_ session: QBRTCSession, hungUpByUser userID: NSNumber, userInfo: [String : String]? = nil) {
        if CallKitManager.instance.isCallStarted() == false && self.session?.id == session.id && self.session?.initiatorID == userID {
            CallKitManager.instance.endCall(with: callUUID) {
                debugPrint("[UsersViewController] endCall")
            }
            prepareCloseCall()
        }
    }
    
    func didReceiveNewSession(_ session: QBRTCSession, userInfo: [String : String]? = nil) {
        if self.session != nil {
            session.rejectCall(["reject": "busy"])
            return
        }
        
        self.session = session
        let uuid = UUID()
        callUUID = uuid
        var opponentIDs = [session.initiatorID]
        let profile = Profile()
        guard profile.isFull == true else {
            return
        }
        for userID in session.opponentsIDs {
            if userID.uintValue != profile.ID {
                opponentIDs.append(userID)
            }
        }
        
        var callerName = ""
        var opponentNames = [String]()
        var newUsers = [NSNumber]()
        for userID in opponentIDs {
            
            // Getting recipient from users.
            if let user = VdataSource.user(withID: userID.uintValue),
                let fullName = user.fullName {
                opponentNames.append(fullName)
            } else {
                newUsers.append(userID)
            }
        }
        
        if newUsers.isEmpty == false {
            let loadGroup = DispatchGroup()
            for userID in newUsers {
                loadGroup.enter()
                VdataSource.loadUser(userID.uintValue) { (user) in
                    if let user = user {
                        opponentNames.append(user.fullName ?? user.login ?? "")
                    } else {
                        opponentNames.append("\(userID)")
                    }
                    loadGroup.leave()
                }
            }
            loadGroup.notify(queue: DispatchQueue.main) {
                callerName = opponentNames.joined(separator: ", ")
                self.reportIncomingCall(withUserIDs: opponentIDs, outCallerName: callerName, session: session, uuid: uuid)
            }
        } else {
            callerName = opponentNames.joined(separator: ", ")
            self.reportIncomingCall(withUserIDs: opponentIDs, outCallerName: callerName, session: session, uuid: uuid)
        }
    }
    
    private func reportIncomingCall(withUserIDs userIDs: [NSNumber], outCallerName: String, session: QBRTCSession, uuid: UUID) {
        if hasConnectivity() {
            CallKitManager.instance.reportIncomingCall(withUserIDs: userIDs,
                                                       outCallerName: outCallerName,
                                                       session: session,
                                                       uuid: uuid,
                                                       onAcceptAction: { [weak self] in
                                                        guard let self = self else {
                                                            return
                                                        }
                                                        
                                                        
                                                        
                                                        if let callViewController = UIStoryboard(name: "Call", bundle: nil).instantiateViewController(withIdentifier: UsersSegueConstant.call) as? CallViewController {
                                                            
                                                            //                                                        if let callViewController = self.storyboard?.instantiateViewController(withIdentifier: UsersSegueConstant.call) as? CallViewController {
                                                            
                                                            callViewController.session = session
                                                            callViewController.usersDataSource = self.VdataSource
                                                            callViewController.callUUID = self.callUUID
                                                            self.navViewController = UINavigationController(rootViewController: callViewController)
                                                            self.navViewController.modalPresentationStyle = .fullScreen
                                                            //                                                            self.navViewController.modalTransitionStyle = .crossDissolve
                                                            self.present(self.navViewController , animated: false)
                                                            
                                                        }
                }, completion: { (end) in
                    debugPrint("[UsersViewController] endCall")
            })
        } else {
            
        }
    }
    
    func sessionDidClose(_ session: QBRTCSession) {
        if let sessionID = self.session?.id,
            sessionID == session.id {
            if self.navViewController.presentingViewController?.presentedViewController == self.navViewController {
                self.navViewController.view.isUserInteractionEnabled = false
                self.navViewController.dismiss(animated: false)
            }
            CallKitManager.instance.endCall(with: self.callUUID) {
                debugPrint("[UsersViewController] endCall")
                
            }
            prepareCloseCall()
        }
    }
    
    private func prepareCloseCall() {
        self.callUUID = nil
        self.session = nil
        if QBChat.instance.isConnected == false {
            self.connectToChat()
        }
        //self.setupToolbarButtons()
    }
    
    private func connectToChat() {
        let profile = Profile()
        guard profile.isFull == true else {
            return
        }
        
        QBChat.instance.connect(withUserID: profile.ID,
                                password: LoginConstant.defaultPassword,
                                completion: { [weak self] error in
                                    guard let self = self else { return }
                                    if let error = error {
                                        if error._code == QBResponseStatusCode.unAuthorized.rawValue {
                                            self.logoutAction()
                                        } else {
                                            debugPrint("[UsersViewController] login error response:\n \(error.localizedDescription)")
                                        }
                                    } else {
                                        //did Login action
                                        SVProgressHUD.dismiss()
                                    }
        })
    }
}



extension ChatViewController: PKPushRegistryDelegate {
    // MARK: - PKPushRegistryDelegate
    func pushRegistry(_ registry: PKPushRegistry, didUpdate pushCredentials: PKPushCredentials, for type: PKPushType) {
        guard let deviceIdentifier = UIDevice.current.identifierForVendor?.uuidString else {
            return
        }
        let subscription = QBMSubscription()
        subscription.notificationChannel = .APNSVOIP
        subscription.deviceUDID = deviceIdentifier
        subscription.deviceToken = pushCredentials.token
        
        QBRequest.createSubscription(subscription, successBlock: { response, objects in
            debugPrint("[UsersViewController] Create Subscription request - Success")
        }, errorBlock: { response in
            debugPrint("[UsersViewController] Create Subscription request - Error")
        })
    }
    
    func pushRegistry(_ registry: PKPushRegistry, didInvalidatePushTokenFor type: PKPushType) {
        guard let deviceIdentifier = UIDevice.current.identifierForVendor?.uuidString else {
            return
        }
        QBRequest.unregisterSubscription(forUniqueDeviceIdentifier: deviceIdentifier, successBlock: { response in
            debugPrint("[UsersViewController] Unregister Subscription request - Success")
        }, errorBlock: { error in
            debugPrint("[UsersViewController] Unregister Subscription request - Error")
        })
    }
    
    func pushRegistry(_ registry: PKPushRegistry,
                      didReceiveIncomingPushWith payload: PKPushPayload,
                      for type: PKPushType) {
        if payload.dictionaryPayload[UsersConstant.voipEvent] != nil {
            let application = UIApplication.shared
            if application.applicationState == .background && backgroundTask == .invalid {
                backgroundTask = application.beginBackgroundTask(expirationHandler: {
                    application.endBackgroundTask(self.backgroundTask)
                    self.backgroundTask = UIBackgroundTaskIdentifier.invalid
                })
            }
            if QBChat.instance.isConnected == false {
                connectToChat()
            }
        }
    }
}



// MARK: - SettingsViewControllerDelegate
extension ChatViewController: SettingsViewControllerDelegate {
    func settingsViewController(_ vc: SessionSettingsViewController, didPressLogout sender: Any) {
        logoutAction()
    }
    
    private func logoutAction() {
        if QBChat.instance.isConnected == false {
            SVProgressHUD.showError(withStatus: "Error")
            return
        }
        SVProgressHUD.show(withStatus: UsersAlertConstant.logout)
        SVProgressHUD.setDefaultMaskType(.clear)
        
        guard let identifierForVendor = UIDevice.current.identifierForVendor else {
            return
        }
        let uuidString = identifierForVendor.uuidString
        #if targetEnvironment(simulator)
        disconnectUser()
        #else
        QBRequest.subscriptions(successBlock: { (response, subscriptions) in
            
            if let subscriptions = subscriptions {
                for subscription in subscriptions {
                    if let subscriptionsUIUD = subscriptions.first?.deviceUDID,
                        subscriptionsUIUD == uuidString,
                        subscription.notificationChannel == .APNSVOIP {
                        self.unregisterSubscription(forUniqueDeviceIdentifier: uuidString)
                        return
                    }
                }
            }
            self.disconnectUser()
            
        }) { response in
            if response.status.rawValue == 404 {
                self.disconnectUser()
            }
        }
        #endif
    }
    
    private func disconnectUser() {
        QBChat.instance.disconnect(completionBlock: { error in
            if let error = error {
                SVProgressHUD.showError(withStatus: error.localizedDescription)
                return
            }
            self.logOut()
        })
    }
    
    private func unregisterSubscription(forUniqueDeviceIdentifier uuidString: String) {
        QBRequest.unregisterSubscription(forUniqueDeviceIdentifier: uuidString, successBlock: { response in
            self.disconnectUser()
        }, errorBlock: { error in
            if let error = error.error {
                SVProgressHUD.showError(withStatus: error.localizedDescription)
                return
            }
            SVProgressHUD.dismiss()
        })
    }
    
    private func logOut() {
        QBRequest.logOut(successBlock: { [weak self] response in
            //ClearProfile
            Profile.clearProfile()
            SVProgressHUD.dismiss()
            //Dismiss Settings view controller
            self?.dismiss(animated: false)
            
            DispatchQueue.main.async(execute: {
                self?.navigationController?.popToRootViewController(animated: false)
            })
        }) { response in
            debugPrint("QBRequest.logOut error\(response)")
        }
    }
}




extension UITextView{
    
    
    func isValid(name:String)->Bool{
        
        
        if self.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "" == ""{
            return false
        }else  if self.text?.containsEmoji == true{
            return true
            
        }else{
            return true
        }
    }
    
    
    
    func trimmSpace()->String{
        return self.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
    }
    
    
}



extension UnicodeScalar {
    
    var isEmoji: Bool {
        
        switch value {
        case 0x1F600...0x1F64F, // Emoticons
        0x1F600...0x1F636,
        0x1F681...0x1F6C5,
        0x1F30D...0x1F567,
        0x2702...0x27B0,
        0x1F680...0x1F6C0,
        0x24C2...0x1F251,
        0x1F300...0x1F5FF, // Misc Symbols and Pictographs
        0x1F680...0x1F6FF, // Transport and Map
        0x1F1E6...0x1F1FF, // Regional country flags
        0x2600...0x26FF,   // Misc symbols
        0x2700...0x27BF,   // Dingbats
        0xFE00...0xFE0F,   // Variation Selectors
        0x1F900...0x1F9FF,  // Supplemental Symbols and Pictographs
        127000...127600, // Various asian characters
        65024...65039, // Variation selector
        9100...9300, // Misc items
        8400...8447: // Combining Diacritical Marks for Symbols
            
            
            
            return true
            
        default: return false
        }
    }
    
    var isZeroWidthJoiner: Bool {
        
        return value == 8205
    }
}


extension String {
    
    var glyphCount: Int {
        
        let richText = NSAttributedString(string: self)
        let line = CTLineCreateWithAttributedString(richText)
        return CTLineGetGlyphCount(line)
    }
    
    var isSingleEmoji: Bool {
        
        return glyphCount == 1 && containsEmoji
    }
    
    var containsEmoji: Bool {
        
        return unicodeScalars.contains { $0.isEmoji }
    }
    
    var containsOnlyEmoji: Bool {
        
        return !isEmpty
            && !unicodeScalars.contains(where: {
                !$0.isEmoji
                    && !$0.isZeroWidthJoiner
            })
    }
    
    // The next tricks are mostly to demonstrate how tricky it can be to determine emoji's
    // If anyone has suggestions how to improve this, please let me know
    var emojiString: String {
        
        return emojiScalars.map { String($0) }.reduce("", +)
    }
    
    var emojis: [String] {
        
        var scalars: [[UnicodeScalar]] = []
        var currentScalarSet: [UnicodeScalar] = []
        var previousScalar: UnicodeScalar?
        
        for scalar in emojiScalars {
            
            if let prev = previousScalar, !prev.isZeroWidthJoiner && !scalar.isZeroWidthJoiner {
                
                scalars.append(currentScalarSet)
                currentScalarSet = []
            }
            currentScalarSet.append(scalar)
            
            previousScalar = scalar
        }
        
        scalars.append(currentScalarSet)
        
        return scalars.map { $0.map{ String($0) } .reduce("", +) }
    }
    
    fileprivate var emojiScalars: [UnicodeScalar] {
        
        var chars: [UnicodeScalar] = []
        var previous: UnicodeScalar?
        for cur in unicodeScalars {
            
            if let previous = previous, previous.isZeroWidthJoiner && cur.isEmoji {
                chars.append(previous)
                chars.append(cur)
                
            } else if cur.isEmoji {
                chars.append(cur)
            }
            
            previous = cur
        }
        
        return chars
    }
}
