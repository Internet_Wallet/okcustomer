//
//  DialogTableViewCell.swift
//  sample-chat-swift
//
//  Created by Injoit on 1/28/19.
//  Copyright © 2019 Quickblox. All rights reserved.
//

import UIKit

protocol CheckMarkDelegate {
    func passCheckMarkTag(tag:Int)
}
extension UIImageView {
    
    func setRounded() {
        self.layer.cornerRadius = (self.frame.width / 2) //instead of let radius = CGRectGetWidth(self.frame) / 2
        self.layer.masksToBounds = true
    }
}



class DialogTableViewCell: UITableViewCell {
    
    @IBOutlet weak var checkMarkButton: UIButton!
    @IBOutlet weak var dialogLastMessage: UILabel!{
        didSet{
            self.dialogLastMessage.font = UIFont(name: appFont, size: 15.0)
            self.dialogLastMessage.text = self.dialogLastMessage.text?.localized
        }
    }
    @IBOutlet weak var dialogName: UILabel!{
        didSet{
            self.dialogName.font = UIFont(name: appFont, size: 15.0)
            self.dialogName.text = self.dialogName.text?.localized
        }
    }
    @IBOutlet weak var dialogTypeImage: UIImageView!{
        didSet{
            dialogTypeImage.setRounded()
        }
    }
    @IBOutlet weak var unreadMessageCounterLabel: UILabel!{
        didSet{
            self.unreadMessageCounterLabel.font = UIFont(name: appFont, size: 15.0)
            self.unreadMessageCounterLabel.text = self.unreadMessageCounterLabel.text?.localized
        }
    }
    @IBOutlet weak var onlineOffline: UILabel!{
        didSet{
            self.onlineOffline.font = UIFont(name: appFont, size: 15.0)
            self.onlineOffline.text = self.onlineOffline.text?.localized
        }
    }
    @IBOutlet weak var unreadMessageCounterHolder: UIView!
    var delegate: CheckMarkDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib() 
        //self.unreadMessageCounterHolder.layer.cornerRadius = 10.0
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        
        let markerColor = self.unreadMessageCounterHolder.backgroundColor
        super.setSelected(selected, animated: animated)
        self.unreadMessageCounterHolder.backgroundColor = .red
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        
        let markerColor = self.unreadMessageCounterHolder.backgroundColor
        super.setHighlighted(highlighted, animated: animated)
        self.unreadMessageCounterHolder.backgroundColor = .red
    }
    
    
    @IBAction func onClickCheckMark(_ sender: Any) {
        delegate?.passCheckMarkTag(tag: checkMarkButton.tag)
    }
    
    
    func checkOnlineOffline(user: QBUUser){
        let currentTimeInterval = Int(Date().timeIntervalSince1970)
        if let hasValue = user.lastRequestAt{
            let userLastRequestAtTimeInterval = Int(hasValue.timeIntervalSince1970)
            // if user didn't do anything last 5 minutes (5*60 seconds)
            if (currentTimeInterval - userLastRequestAtTimeInterval) > 5 * 60 {
                // user is offline now
                print("offline")
                
                self.onlineOffline.backgroundColor = UIColor.red
            }else{
                print("online")
                self.onlineOffline.backgroundColor = UIColor.green
            }
        }else{
            self.onlineOffline.backgroundColor = UIColor.red
        }
        
    }
    
    func checkOnlineOfflineAndShowOnLabel(user: QBUUser,storeValue: Int){
        // unreadMessageCounterHolder.layer.borderWidth = 2.0
        // unreadMessageCounterLabel.layer.borderColor = UIColor.gray.cgColor
        
        
        if storeValue == 1{
            
            let currentTimeInterval = Int(Date().timeIntervalSince1970)
            if let hasValue = user.lastRequestAt{
                let userLastRequestAtTimeInterval = Int(hasValue.timeIntervalSince1970)
                // if user didn't do anything last 5 minutes (5*60 seconds)
                if (currentTimeInterval - userLastRequestAtTimeInterval) > 5 * 60 {
                    // user is offline now
                    print("offline")
                    self.dialogLastMessage.text = "Offline".localized
                    self.onlineOffline.backgroundColor = UIColor.red
                }else{
                    print("online")
                    self.dialogLastMessage.text = "Online".localized
                    self.onlineOffline.backgroundColor = UIColor.green
                }
            }else{
                self.dialogLastMessage.text = "Offline".localized
                self.onlineOffline.backgroundColor = UIColor.red
            }
            
        }
    }
}
