import UIKit

class FPNCountryView: NibLoadingView {
    
    @IBOutlet weak var flagImageView: UIImageView!
    @IBOutlet weak var countryNameLabel: UILabel!{
        didSet{
            self.countryNameLabel.font = UIFont(name: appFont, size: 15.0)
            self.countryNameLabel.text = self.countryNameLabel.text?.localized
        }
    }
    @IBOutlet weak var countryCodeLabel: UILabel!{
        didSet{
            self.countryCodeLabel.font = UIFont(name: appFont, size: 15.0)
            self.countryCodeLabel.text = self.countryCodeLabel.text?.localized
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setup(_ country: FPNCountry) {
        flagImageView.image = country.flag
        countryCodeLabel.text = country.phoneCode
        countryNameLabel.text = country.name
    }
}
