//
//  FPNSearchCountryViewController.swift
//  FlagPhoneNumber
//
//  Created by Aurélien Grifasi on 06/08/2017.
//  Copyright (c) 2017 Aurélien Grifasi. All rights reserved.
//

import UIKit

class FPNSearchCountryViewController: UITableViewController, UISearchResultsUpdating, UISearchControllerDelegate {
    
    var searchController: UISearchController?
    var list: [FPNCountry]?
    var results: [FPNCountry]?
    
    var delegate: FPNDelegate?
    
    init(countries: [FPNCountry]) {
        super.init(nibName: nil, bundle: nil)
        
        self.list = countries
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let lblNoCountryFound = UILabel()
    override func viewDidLoad() {
        super.viewDidLoad()
        lblNoCountryFound.frame = CGRect(x: 0, y: 120, width: self.view.frame.width, height: 50)
        lblNoCountryFound.text = "No country found".localized
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationItem.title = "Countries List".localized
        addBackButton()
        lblNoCountryFound.textAlignment = .center
        lblNoCountryFound.isHidden = true
        self.view.addSubview(lblNoCountryFound)
        self.tableView.tableFooterView = UIView()
        initSearchBarController()
    }
    
    func addBackButton() {
        let button = UIButton()
        button.setImage(UIImage(named: "back"), for: .normal)
        button.addTarget(self, action: #selector(popScreen), for: .touchUpInside)
        button.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        button.imageEdgeInsets.left = -35
        let item = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = item
    }
    
    @objc func popScreen()  {
        dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        searchController?.isActive = true
    }
    
    @objc private func dismissController() {
        results?.removeAll()
        tableView.reloadData()
    }
    
    private func initSearchBarController() {
        searchController = UISearchController(searchResultsController: nil)
        searchController?.searchResultsUpdater = self
        searchController?.delegate = self
        
        searchController?.dimsBackgroundDuringPresentation = false
        searchController?.hidesNavigationBarDuringPresentation = false
        searchController?.definesPresentationContext = false
        tableView.tableHeaderView = searchController?.searchBar
        definesPresentationContext = true
    }
    
    private func getItem(at indexPath: IndexPath) -> FPNCountry {
        var array: [FPNCountry]!
        
        if let searchController = searchController, searchController.isActive && results != nil && results!.count > 0 {
            array = results
        } else {
            array = list
        }
        
        return array[indexPath.row]
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let searchController = searchController, searchController.isActive {
            if let count = searchController.searchBar.text?.count, count > 0 {
                return results?.count ?? 0
            }
        }
        return list?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .value1, reuseIdentifier: nil)
        let country = getItem(at: indexPath)
        
        cell.textLabel?.text = country.name
        cell.detailTextLabel?.text = country.phoneCode
        cell.imageView?.image = country.flag
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        delegate?.fpnDidSelect(country: getItem(at: indexPath))
        searchController?.isActive = false
        searchController?.searchBar.resignFirstResponder()
        dismiss(animated: true, completion: nil)
    }
    
    // UISearchResultsUpdating
    
    func updateSearchResults(for searchController: UISearchController) {
        if list == nil {
            results?.removeAll()
            return
        }else if searchController.searchBar.text == "" {
            results?.removeAll()
            tableView.reloadData()
            return
        }
        
        if let searchText = searchController.searchBar.text, searchText.count > 0 {
            results = list!.filter({(item: FPNCountry) -> Bool in
                if item.name.lowercased().range(of: searchText.lowercased()) != nil {
                    return true
                } else if item.code.rawValue.lowercased().range(of: searchText.lowercased()) != nil {
                    return true
                } else if item.phoneCode.lowercased().range(of: searchText.lowercased()) != nil {
                    return true
                }
                return false
            })
            if results?.count == 0 {
                self.lblNoCountryFound.isHidden = false
            }else {
                self.lblNoCountryFound.isHidden = true
            }
        }
        tableView.reloadData()
    }
    
    // UISearchControllerDelegate
    
    func didPresentSearchController(_ searchController: UISearchController) {
        DispatchQueue.main.async { [unowned self] in
            self.searchController?.searchBar.becomeFirstResponder()
        }
    }
    
    func willDismissSearchController(_ searchController: UISearchController) {
        results?.removeAll()
    }
    
    func didDismissSearchController(_ searchController: UISearchController) {
        dismissController()
    }
}
