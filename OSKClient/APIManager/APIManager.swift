//
//  APIManager.swift
//  NopCommerce
//
//  Created by BS-125 on 11/4/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit
import Foundation
import Alamofire
import SwiftyJSON
import ObjectMapper
import JWT

open class APIManager: NSObject {
    
    var homePageProducts                = [GetHomePageProducts]()
    //var homePageBanner                = [String]()
    var homePageBanner                  = [HomeBanner]()
    var homePageMenuFacture             = [HomePageMenuFacture]()
    var recentlyViewedProduct           = [GetHomePageProducts] ()
    var allCategories                   = [GetAllCategories]()
    var homePageCategoryModels          = [HomePageCategoryModel]()
    var baseURL                         = APIManagerClient.sharedInstance.baseURL
    var NST_KEY_Value                   = APIManagerClient.sharedInstance.NST_KEY_Value
    var NST_SECRET_Value                = APIManagerClient.sharedInstance.NST_SECRET_Value
    var baseURL1                        = APIManagerClient.sharedInstance.base_url
    var barcodeResponse :                NSNumber?
    var featuredProductsAndCategory     = [FeaturedProductsAndCategory]()
    let deviceId                        = UIDevice.current.identifierForVendor?.uuidString ?? ""
    let headers                         = ["DeviceId": UIDevice.current.identifierForVendor?.uuidString ?? ""]
    var categoryArray                   = [CategoryDetailsInfo]()
    //var productsArray                   = [ProductDetailsInfo]()
    var shoppingCartArray               = [ShoppingCartInfo]()
    var RecentSearcHistoryArray               = [String]()
    
    var NearByServiceArray              = [NearByServiceModel]()
    
    func getHeaderDic() -> [String:String] {
        Alamofire.SessionManager.default.session.configuration.requestCachePolicy = .reloadIgnoringLocalCacheData
        println_debug("\nDevice id : \(self.deviceId)\n")
        let manager = APIManagerClient.sharedInstance
        let key = manager.NST_KEY_Value
        let secret = manager.NST_SECRET_Value
        print("APP Key : \(key) And Secret : \(secret)")
        let encodedString = JWT.encode(claims: ["NST_KEY": "\(key)"], algorithm: .hs512("\(secret)".data(using: .utf8)!))
        var headerDictionary = self.headers
        if let token = VMLoginModel.shared.token {
            headerDictionary = ["Token":token,"DeviceId": self.deviceId,"NST":encodedString]
            println_debug("Header bb : \(headerDictionary)\n")
        } else {
            headerDictionary = ["DeviceId": self.deviceId,"NST":encodedString]
            println_debug("Without Access Token of Header : \(headerDictionary)\n")
        }
        return headerDictionary
    }
    
    func displayErrorMessage(json: JSON) -> String {
        var errorMessage = ""
        let errorList = json["ErrorList"].arrayObject
        if errorList != nil{
            for error in errorList! {
                errorMessage += "\(error as! String)\n"
            }
        }
        return errorMessage
    }
    
    // MARK: ShoppingCartCount
    open func getShoppingCartCount(onSuccess successBlock: (() -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        let urlParameters = String(format: "%@/categories",  APIManagerClient.sharedInstance.base_url) as String
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {  response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                APIManagerClient.sharedInstance.shoppingCartCount = json["Count"].intValue
                successBlock()
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    // MARK: ShoppingCartCount
    open func SaveNotifyRequest(_ productId: String,onSuccess successBlock: (() -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        let urlParameters = String(format: "/product/SaveNotifyRequest/%@",  APIManagerClient.sharedInstance.base_url,productId) as String
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {  response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                print(json)
                successBlock()
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    
    open func ScanBarcodeId(_ productId: Int,  onSuccess successBlock: @escaping ((JSON) -> Void), onError errorBlock: ((String?) -> Void)!) {
        let urlParameters = String(format: "%@/product/GetProductIdbyBarcode/\(productId)", APIManagerClient.sharedInstance.base_url ) as String
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {   response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                println_debug(json)
                successBlock(json)
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    
    
    open func RateAndReview(_ data:[String:Any],  onSuccess successBlock: ((NSNumber) -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        let urlParameters = String(format: "%@/reviews/ReviewsOfAppAndAddress", APIManagerClient.sharedInstance.base_url) as String
        Alamofire.request(URL(string: urlParameters)!, method: .post, parameters: data, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {   response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                println_debug(json)
                successBlock(self.barcodeResponse ?? 0)
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    
    open func NearByOkServiceAPI(_ data:[String:Any],  onSuccess successBlock: (([NearByServiceModel]) -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        let urlParameters = String(format: "http://120.50.43.157:1318/UserService/SearchAgentsForOkDollar") as String
        Alamofire.request(URL(string: urlParameters)!, method: .post, parameters: data, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {   response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                println_debug(json)
                
                if let resData = json["Content"].array {
                    for data in resData{
                        
                        //                        let BusinessName         = data["BusinessName"].stringValue
                        //                        let PhoneNumber         = data["PhoneNumber"].stringValue
                        //                        let Country         = data["Country"].stringValue
                        //                        let TownShipName         = data["TownShipName"].stringValue
                        //                        println_debug(BusinessName)
                        //                        println_debug(PhoneNumber)
                        //                        println_debug(Country)
                        //                        println_debug(TownShipName)
                        //                        let storeObject = NearByServiceModel(BusinessName: BusinessName, PhoneNumber: PhoneNumber, Country: Country, TownShipName: TownShipName)
                        
                        let storeObject = NearByServiceModel(ClosingTime: data["ClosingTime"].stringValue, CashOut: data["CashOut"].boolValue, Country: data["Country"].stringValue, DivisionName: data["DivisionName"].stringValue, BusinessName: data["BusinessName"].stringValue, HomeLocation: data["HomeLocation"].stringValue, AppVersion: data["AppVersion"].stringValue, UserName: data["UserName"].stringValue, Line1: data["Line1"].stringValue, Category: data["Category"].stringValue, State: data["BusinessName"].stringValue, Rating: data["Rating"].stringValue, Password: data["Password"].stringValue, FirstName: data["FirstName"].stringValue, PhoneNumber: data["PhoneNumber"].stringValue, CellId: data["CellId"].stringValue, Email: data["Email"].stringValue, SubCategory: data["SubCategory"].stringValue, IsDeleted: data["IsDeleted"].boolValue, IsOpenAlways: data["IsOpenAlways"].boolValue, LastName: data["LastName"].stringValue, ModifiedDate: data["ModifiedDate"].stringValue, CreatedBy: data["CreatedBy"].stringValue, IsActive: data["IsActive"].boolValue, Address: data["Address"].stringValue, Id: data["Id"].stringValue, DistanceInMiles: data["DistanceInMiles"].stringValue, TownShipName: data["TownShipName"].stringValue, MessageRequestRejectedTime: data["MessageRequestRejectedTime"].stringValue, CashIn: data["CashIn"].boolValue, OpeningTime: data["OpeningTime"].stringValue, CountryCode: data["CountryCode"].stringValue, TypeID: data["TypeID"].stringValue, CityName: data["CityName"].stringValue, IsOpen: data["IsOpen"].stringValue, DistanceInKm: data["DistanceInKm"].stringValue, CreatedDate: data["CreatedDate"].stringValue, Gender: data["Gender"].stringValue, TransactionId: data["TransactionId"].stringValue, TotalRateCount: data["TotalRateCount"].stringValue, Line2: data["Line2"].stringValue, OkDollarBalance: data["OkDollarBalance"].stringValue, AgentType: data["AgentType"].stringValue)
                        
                        self.NearByServiceArray.append(storeObject)
                    }
                }
                
                successBlock(self.NearByServiceArray)
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    
    
    // MARK: HomePageProducts
    open func loadHomePageProducts(onSuccess successBlock: (([GetHomePageProducts]) -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        let urlParameters = String(format: "%@/homepageproducts", APIManagerClient.sharedInstance.base_url) as String
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: ["thumbPictureSize":"320"], encoding: URLEncoding.default, headers: self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                println_debug(json)
                println_debug("\n")
                self.homePageProducts.removeAll()
                if let resData = json["Data"].array {
                    for data in resData{
                        var customProperties = data["CustomProperties"].string
                        let imageUrl    = data["DefaultPictureModel"]["ImageUrl"].string
                        let idd         = data["Id"].intValue
                        let name        = data["Name"].string
                        let oldPrice    = data["ProductPrice"]["OldPrice"].string
                        let price       = data["ProductPrice"]["Price"].string
                        let priceValue       = data["ProductPrice"]["PriceValue"].floatValue
                        let priceWithDiscount       = data["ProductPrice"]["PriceWithDiscount"].string
                        let discountPercentage       = data["ProductPrice"]["DiscountPercentage"].intValue
                        let allowCustomerReviews = data["ReviewOverviewModel"]["AllowCustomerReviews"].boolValue
                        let productId = data["ReviewOverviewModel"]["ProductId"].intValue
                        let ratingSum = data["ReviewOverviewModel"]["RatingSum"].intValue
                        let totalReviews = data["ReviewOverviewModel"]["TotalReviews"].intValue
                        var shortDescription = data["ShortDescription"].string
                        let isWishList = data["IsWishList"].boolValue
                        let DisableWishlistButton = data["DisableWishlistButton"].boolValue
                        let MarkAsNew = data["MarkAsNew"].boolValue
                        if customProperties==nil{
                            customProperties = ""
                        }
                        if shortDescription==nil{
                            shortDescription = ""
                        }
                        let getHomePageProducts = GetHomePageProducts(CustomProperties: customProperties ?? "", ImageUrl: imageUrl ?? "", Id: idd, Name: name ?? "", OldPrice: oldPrice ?? "", Price: price ?? "", AllowCustomerReviews: allowCustomerReviews, MarkAsNew: MarkAsNew, ProductId: productId, RatingSum: ratingSum, TotalReviews: totalReviews, ShortDescription: shortDescription ?? "", PriceWithDiscount: priceWithDiscount ?? "", DiscountPercentage: discountPercentage , PriceValue: priceValue, IsWishList: isWishList, DisableWishlistButton: DisableWishlistButton)
                        self.homePageProducts.append(getHomePageProducts)
                    }
                    successBlock(self.homePageProducts)
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    // MARK: HomePageBanner
    open func loadHomePageBanner(onSuccess successBlock: (([HomeBanner]) -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        //        let urlParameters = String(format: "%@/homepagebanner", baseURL) as String
        //        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { //(response) -> Void in
        let urlParameters = String(format: "%@/homepagebanner", APIManagerClient.sharedInstance.base_url) as String
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                println_debug(json)
                println_debug("\n")
                self.homePageBanner = []
                if let banner  = Mapper<HomeBanner>().mapArray(JSONObject: json["Data"].arrayObject){
                    successBlock(banner)
                }else{
                    errorBlock("Could not print JSON")
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    
    
    
    // MARK: HomePageBanner
    open func loadenableTownship(onSuccess successBlock: (([TownShip]) -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        
        let urlParameters = String(format: "%@/township/getallshippingenabledtownship", APIManagerClient.sharedInstance.base_url) as String
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
                
            case .success(let data):
                let json = JSON(data)
                println_debug(json)
                println_debug("\n")
                
                if let banner  = Mapper<TownShip>().mapArray(JSONObject: json["TownShipDetails"].arrayObject){
                    successBlock(banner)
                }else{
                    errorBlock("Could not print JSON")
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    
    
    
    // MARK: Get languages from server
    func getLanguagesFromServer(onSuccess successBlock: ((GetLanguageModel) -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        //        let urlParameters = String(format: "%@/GetLanguage", baseURL) as String
        //        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { //(response) -> Void in
        let urlParameters = String(format: "%@/GetLanguage", APIManagerClient.sharedInstance.base_url) as String
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                println_debug(json)
                println_debug("\n")
                
                if let languageModelObj  = Mapper<GetLanguageModel>().map(JSONObject: json.dictionaryObject){
                    println_debug("\nPlay")
                    successBlock(languageModelObj)
                }else{
                    errorBlock("Could not print JSON")
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    // MARK: Set languages In Server
    func setLanguageInServer(languageId : Int, onSuccess successBlock: (() -> Void)!, onError errorBlock: ((String?) -> Void)!){
        //        let urlParameters = String(format: "%@/SetLanguage/%d",baseURL, languageId) as String
        //        Alamofire.request(URL(string: urlParameters)!, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {
        let urlParameters = String(format: "%@/SetLanguage/%d",APIManagerClient.sharedInstance.base_url, languageId) as String
        Alamofire.request(URL(string: urlParameters)!, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                println_debug(json)
                println_debug("\n")
                let statusCode = json["StatusCode"].int
                if statusCode == 200 {
                    successBlock()
                }else{
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    // MARK: LoadAboutUs
    func loadAboutUs(onSuccess successBlock: ((WebViewAboutUS) -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        //        let urlParameters = String(format: "%@/topics/AboutUs", baseURL) as String
        //        Alamofire.request(URL(string: urlParameters)!, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {
        let urlParameters = String(format: "%@/AboutUs", APIManagerClient.sharedInstance.base_url) as String
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                println_debug(json)
                println_debug("\n")
                let statusCode = json["StatusCode"].int
                if statusCode == 200 {
                    if let aboutUS  = Mapper<WebViewAboutUS>().map(JSONObject: json["Data"].dictionaryObject){
                        successBlock(aboutUS)
                    }
                }else{
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    // MARK: LoadAboutUs
    func loadPrivacyPolicy(onSuccess successBlock: ((WebViewAboutUS) -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        //        let urlParameters = String(format: "%@/topics/PrivacyInfo", baseURL) as String
        //        Alamofire.request(URL(string: urlParameters)!, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {
        let urlParameters = String(format: "%@/PrivacyInfo", APIManagerClient.sharedInstance.base_url) as String
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                println_debug(json)
                println_debug("\n")
                let statusCode = json["StatusCode"].int
                if statusCode == 200 {
                    if let privacy  = Mapper<WebViewAboutUS>().map(JSONObject: json["Data"].dictionaryObject){
                        successBlock(privacy)
                    }
                }else{
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    // MARK: HomePageMenuFacture
    open func loadHomePageMenuFacture(onSuccess successBlock: (([HomePageMenuFacture]) -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        let urlParameters = String(format: "%@/homepagemanufacture", APIManagerClient.sharedInstance.base_url) as String
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: ["thumbPictureSize":"320"], encoding: URLEncoding.default, headers: self.getHeaderDic()).responseJSON {  response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                if let resData = json["Data"].array {
                    self.homePageMenuFacture.removeAll()
                    for data in resData{
                        let name = data["Name"].string
                        let imageUrl = data["DefaultPictureModel"]["ImageUrl"].string
                        let idd = data["Id"].intValue
                        let homePageMenu = HomePageMenuFacture(name: name!, imageUrl: imageUrl!, idd: idd)
                        self.homePageMenuFacture.append(homePageMenu)
                    }
                    successBlock(self.homePageMenuFacture)
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    open func loadHomePageRecentlyViewedProduct(onSuccess successBlock: (([GetHomePageProducts]) -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        let urlParameters = String(format: "%@/product/RecentlyViewedProducts", APIManagerClient.sharedInstance.base_url) as String
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: ["thumbPictureSize":"320"], encoding: URLEncoding.default, headers: self.getHeaderDic()).responseJSON {  response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                if let resData = json["Data"].array {
                    self.recentlyViewedProduct.removeAll()
                    for data in resData{
                        var customProperties = data["CustomProperties"].string
                        let imageUrl    = data["DefaultPictureModel"]["ImageUrl"].string
                        let idd         = data["Id"].intValue
                        let name        = data["Name"].string
                        let oldPrice    = data["ProductPrice"]["OldPrice"].string
                        let price       = data["ProductPrice"]["Price"].string
                        let priceValue       = data["ProductPrice"]["PriceValue"].floatValue
                        let priceWithDiscount       = data["ProductPrice"]["PriceWithDiscount"].string
                        let discountPercentage       = data["ProductPrice"]["DiscountPercentage"].intValue
                        let allowCustomerReviews = data["ReviewOverviewModel"]["AllowCustomerReviews"].boolValue
                        let productId = data["ReviewOverviewModel"]["ProductId"].intValue
                        let ratingSum = data["ReviewOverviewModel"]["RatingSum"].intValue
                        let totalReviews = data["ReviewOverviewModel"]["TotalReviews"].intValue
                        var shortDescription = data["ShortDescription"].string
                        let isWishList = data["IsWishList"].boolValue
                        let DisableWishlistButton = data["DisableWishlistButton"].boolValue
                        let MarkAsNew = data["MarkAsNew"].boolValue
                        if customProperties==nil{
                            customProperties = ""
                        }
                        if shortDescription==nil{
                            shortDescription = ""
                        }
                        let getHomePageProducts = GetHomePageProducts(CustomProperties: customProperties ?? "", ImageUrl: imageUrl ?? "", Id: idd, Name: name ?? "", OldPrice: oldPrice ?? "", Price: price ?? "", AllowCustomerReviews: allowCustomerReviews, MarkAsNew: MarkAsNew, ProductId: productId, RatingSum: ratingSum, TotalReviews: totalReviews, ShortDescription: shortDescription ?? "", PriceWithDiscount: priceWithDiscount ?? "", DiscountPercentage: discountPercentage , PriceValue: priceValue, IsWishList: isWishList, DisableWishlistButton: DisableWishlistButton)
                        self.recentlyViewedProduct.append(getHomePageProducts)
                    }
                    successBlock(self.recentlyViewedProduct)
                }
                else{
                    successBlock(self.recentlyViewedProduct)
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    
    
    open func testNewApi(){
        //        let urlParameters = String(format: "%@/v1/categories", APIManagerClient.sharedInstance.base_url) as String
        let urlParameters = String(format: "%@/catalog/homepagecategorieswithproduct", APIManagerClient.sharedInstance.base_url) as String
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                println_debug(json)
                println_debug("\n")
                break
            case .failure(_): break
                
            }
        }
    }
    
    // MARK: AllCategories
    open func loadAllCategories(onSuccess successBlock: (([GetAllCategories], _ languageStr: String) -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        //        let urlParameters = String(format: "%@/categories", baseURL) as String
        let urlParameters = String(format: "%@/v1/categories", APIManagerClient.sharedInstance.base_url) as String
        //        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { //(response) -> Void in
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                println_debug(json)
                println_debug("\n")
                self.allCategories = []
                var languageStr = String()
                APIManagerClient.sharedInstance.shoppingCartCount = json["Count"].intValue
                
                if let availableLanguages = json["Language"]["AvailableLanguages"].array{
                    for data in availableLanguages{
                        if(data["Id"].int! == json["Language"]["CurrentLanguageId"].int!){
                            //                            println_debug("Current Language is : \(data["Name"].string!,"\n")")
                            
                            UserDefaults.standard.set(data["Name"].string!, forKey: "appCurrentLanguage")
                            
                            if(data["Name"].string! == "Arabic" || data["Name"].string! == "arabic" || data["Name"].string! == "عربي"){
                                languageStr = "ar"
                                APIManagerClient.sharedInstance.isRequestedToReloadOnlyOnce = true
                            }else{
                                languageStr = "en"
                            }
                            break
                        }
                    }
                }
                
                if let resData = json["Data"].array {
                    for data in resData{
                        let name                = data["Name"].string
                        let idd                 = data["Id"].intValue
                        let parentCategoryId    = data["ParentCategoryId"].intValue
                        let displayOrder        = data["DisplayOrder"].intValue
                        let subCategoriesCount  = data["Children"].count
                        let iconPath            = data["IconPath"].string
                        let allCatagories       = GetAllCategories(parentCategoryId: parentCategoryId, name: name!, idd: idd, displayOrder: displayOrder, iconPath: iconPath!, subCategoriesCount: subCategoriesCount)
                        self.allCategories.append(allCatagories)
                    }
                    successBlock(self.allCategories,languageStr as String)
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    // MARK: TestURL
    open func testUrl(_ url : String , onSuccess successBlock: ((String) -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        let testUrlString = String(format: "%@/api", url)
        let urlParameters = String(format: "%@/categories", testUrlString) as String
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {  response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                let statusCode = json["StatusCode"].int
                if statusCode == 200{
                    successBlock(testUrlString)
                } else{
                    let errorMessage = "Invalid URL"
                    errorBlock(errorMessage)
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    // MARK: HomePageCategories
    open func loadHomePageCategories(onSuccess successBlock: (([HomePageCategoryModel]) -> Void)!, onError errorBlock: ((String?) -> Void)!) {        
        //        let urlParameters = String(format: "%@/catalog/homepagecategorieswithproduct", baseURL) as String
        //        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { //(response) -> Void in
        let urlParameters = String(format: "%@/catalog/homepagecategorieswithproduct/1", APIManagerClient.sharedInstance.base_url) as String
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                let statusCode = json["StatusCode"].int
                if statusCode == 200 {
                    self.homePageCategoryModels.removeAll()
                    if let resData = json["Data"].array {
                        for data in resData {
                            let homePageCategory = HomePageCategoryModel(JSON:data.dictionaryObject! as AnyObject)
                            self.homePageCategoryModels.append(homePageCategory)
                        }
                    }
                    successBlock(self.homePageCategoryModels)
                } else{
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    // MARK: LoadProductDetails
    func loadProductDetails(_ productId:NSInteger,  onSuccess successBlock: ((ProductDetailsInfo) -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        let urlParameters = String(format: "%@/productdetails/%d",APIManagerClient.sharedInstance.base_url, productId) as String
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {  response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                println_debug(json)
                println_debug("\n")
                let statusCode = json["StatusCode"].int
                if statusCode == 200 {
                    //                    let productsInfo: ProductDetailsViewModel = ProductDetailsViewModel.init(data: json["Data"])
                    //                    successBlock(productsInfo)
                    if let categorisInfo  = Mapper<ProductDetailsInfo>().map(JSONObject: json["Data"].dictionaryObject){
                        successBlock(categorisInfo)
                    }
                }else{
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    // MARK: AddProductToCartEdit
    func AddProductToCartEdit(_ productId:NSInteger, shoppingCartTypeId : NSInteger,  onSuccess successBlock: ((ProductDetailsInfo) -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        let urlParameters = String(format: "%@/productdetails/%d?updatecartitemid=%d", baseURL, productId, shoppingCartTypeId) as String
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { //(response) -> Void in
            response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                let statusCode = json["StatusCode"].int
                if statusCode == 200 {
                    if let categorisInfo  = Mapper<ProductDetailsInfo>().map(JSONObject: json["Data"].dictionaryObject){
                        successBlock(categorisInfo)
                    }
                }else{
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    // MARK: FeaturedProductsAndCategory
    open  func loadFeaturedProductsAndCategory(_ categoryId: NSInteger, onSuccess successBlock: (([FeaturedProductsAndCategory]) -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        //        let urlParameters = String(format: "%@/categoryfeaturedproductandsubcategory/%d", baseURL,categoryId) as String
        //        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { //(response) -> Void in
        self.featuredProductsAndCategory.removeAll()
        let urlParameters = String(format: "%@/categoryfeaturedproductandsubcategory/%d", APIManagerClient.sharedInstance.base_url,categoryId) as String
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                if let resData = json["SubCategories"].array {
                    for data in resData{
                        let name = data["Name"].string
                        let imageUrl = data["PictureModel"]["ImageUrl"].string
                        let idd = data["Id"].intValue
                        let featured_Product_and_category = FeaturedProductsAndCategory(name: name!, imageUrl: imageUrl!, idd: idd)
                        self.featuredProductsAndCategory.append(featured_Product_and_category)
                    }
                    successBlock(self.featuredProductsAndCategory)
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    // MARK: CategoryDetails
    open  func loadCategoryDetails( _ manufacturerFlag: Bool, categoryId: NSInteger, params: [String: AnyObject], onSuccess successBlock: ((CategoryDetailsInfo) -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        var urlSubString = "Category"
        if manufacturerFlag == true{
            urlSubString = "Manufacturer"
        }
        println_debug(params)
        //        let urlParameters = String(format: "%@/%@/%d", baseURL, urlSubString, categoryId) as String
        //        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: params, encoding: URLEncoding.default, headers: self.getHeaderDic()).responseJSON { //(response) -> Void in
        let urlParameters = String(format: "%@/%@/%d", APIManagerClient.sharedInstance.base_url, urlSubString, categoryId) as String
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: params, encoding: URLEncoding.default, headers: self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                println_debug(json)
                println_debug("\n")
                let categoryDetailsInfo = CategoryDetailsInfo ()
                //                categoryDetailsInfo.fromPrice = json["PriceRange"]["From"].double
                //                categoryDetailsInfo.toPrice = json["PriceRange"]["To"].double
                categoryDetailsInfo.TotalPages = json["TotalPages"].int!
                
                if let resData = json["Products"].arrayObject {
                    for productDictionary in resData {
                        let products = ProductsInfo(JSON: productDictionary as! [String : AnyObject] as AnyObject)
                        categoryDetailsInfo.productsInfo.append(products)
                    }
                }
                if let resData = json["PriceRange"].dictionaryObject {
                    if let defaultMinValue = UserDefaults.standard.value(forKey: "min") as? String , defaultMinValue != "" {
                        guard let minVal = String(format: "%@", UserDefaults.standard.value(forKey: "min") as! CVarArg).toDouble() as? Double else { return }
                        let minValueNew = json["PriceRange"]["From"].doubleValue
                        if minVal > minValueNew {
                                                    categoryDetailsInfo.fromPrice = json["PriceRange"]["From"].double
                                                    UserDefaults.standard.set(json["PriceRange"]["From"].stringValue, forKey: "min")
                                                    UserDefaults.standard.synchronize()
                                                    let notFilteredItem = PriceRange(JSON: resData as [String : AnyObject] as AnyObject)
                                                    categoryDetailsInfo.PriceRange.append(notFilteredItem)
                       }
                        else {
                            categoryDetailsInfo.fromPrice = minVal
                        }
                    }
                    if let defaultMaxValue = UserDefaults.standard.value(forKey: "max") as? String , defaultMaxValue != "" {
                        guard let maxVal = String(format: "%@", UserDefaults.standard.value(forKey: "max") as! CVarArg).toDouble() as? Double else { return }
                        let maxValueNew = json["PriceRange"]["To"].doubleValue
                                     if maxVal < maxValueNew {
                                                    categoryDetailsInfo.toPrice = json["PriceRange"]["To"].double
                                                    UserDefaults.standard.set(json["PriceRange"]["To"].stringValue, forKey: "max")
                                                    UserDefaults.standard.synchronize()
                                                    let notFilteredItem = PriceRange(JSON: resData as [String : AnyObject] as AnyObject)
                                                    categoryDetailsInfo.PriceRange.append(notFilteredItem)
                                            }
                                     else {
                                        categoryDetailsInfo.toPrice = maxVal
                                      }
                         }
                    else {
                        categoryDetailsInfo.fromPrice = json["PriceRange"]["From"].double
                        categoryDetailsInfo.toPrice = json["PriceRange"]["To"].double
                        if categoryDetailsInfo.toPrice != nil {
                            UserDefaults.standard.set(json["PriceRange"]["From"].stringValue, forKey: "min")
                            UserDefaults.standard.set(json["PriceRange"]["To"].stringValue, forKey: "max")
                            UserDefaults.standard.synchronize()
                            let notFilteredItem = PriceRange(JSON: resData as [String : AnyObject] as AnyObject)
                            categoryDetailsInfo.PriceRange.append(notFilteredItem)
                        }
                    }
                }
                if let resData = json["NotFilteredItems"].arrayObject {
                    for notFilteredItemDictionary in resData {
                        let notFilteredItem = NotFilteredItems(JSON: notFilteredItemDictionary as! [String : AnyObject] as AnyObject)
                        categoryDetailsInfo.notFilteredItems.append(notFilteredItem)
                    }
                }
                if let resData = json["AlreadyFilteredItems"].arrayObject {
                    for alreadyFilteredItemDictionary in resData {
                        let alreadyFilteredItem = NotFilteredItems(JSON: alreadyFilteredItemDictionary as! [String : AnyObject] as AnyObject)
                        categoryDetailsInfo.alreadyFilteredItems.append(alreadyFilteredItem)
                    }
                }
                if let resData = json["AvailableSortOptions"].arrayObject {
                    for availableSortOptionDictionary in resData {
                        let availableSortOptionItem = AvailableSortOptions(JSON: availableSortOptionDictionary as! [String : AnyObject] as AnyObject)
                        categoryDetailsInfo.availableSortOptions.append(availableSortOptionItem)
                    }
                }
                successBlock(categoryDetailsInfo)
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    // MARK: LoadRelatedProduct
    open  func loadRelatedProduct( _ productId: NSInteger, onSuccess successBlock: ((CategoryDetailsInfo) -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        let urlParameters = String(format: "%@/relatedproducts/%d", APIManagerClient.sharedInstance.base_url, productId) as String
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: ["thumbPictureSize":"320"], encoding: URLEncoding.default, headers: self.getHeaderDic()).responseJSON { //(response) -> Void in
            response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                let categoryDetailsInfo = CategoryDetailsInfo ()
                if let resData = json["Data"].arrayObject {
                    for productDictionary in resData {
                        let products = ProductsInfo(JSON: productDictionary as AnyObject)
                        categoryDetailsInfo.productsInfo.append(products)
                    }
                }
                if let resData = json["NotFilteredItems"].arrayObject {
                    for notFilteredItemDictionary in resData {
                        let notFilteredItem = NotFilteredItems(JSON: notFilteredItemDictionary as AnyObject )
                        categoryDetailsInfo.notFilteredItems.append(notFilteredItem)
                    }
                }
                if let resData = json["AvailableSortOptions"].arrayObject {
                    for availableSortOptionDictionary in resData {
                        
                        let availableSortOptionItem = AvailableSortOptions(JSON: availableSortOptionDictionary as AnyObject )
                        categoryDetailsInfo.availableSortOptions.append(availableSortOptionItem)
                    }
                }
                successBlock(categoryDetailsInfo)
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    // MARK: ProductDetailsPagePrice
    open  func loadProductDetailsPagePrice( _ productId: NSInteger, parameters: NSMutableArray , onSuccess successBlock: ((JSON) -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        var request = URLRequest(url: URL(string: NSString(format: "%@/ProductDetailsPagePrice/%d", APIManagerClient.sharedInstance.base_url, productId) as String)!)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.allHTTPHeaderFields = self.getHeaderDic()
        request.httpBody = try! JSONSerialization.data(withJSONObject: parameters, options: [])
        Alamofire.request(request).responseJSON { response in
            switch response.result {
            case .success(let data):
                //successBlock(data as AnyObject)
                let json = JSON(data)
                println_debug(json)
                println_debug("\n")
                let statusCode = json["StatusCode"].int
                if statusCode == 200 {
                    successBlock(json)
                } else {
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure( let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    
    //MARK:- Remove product from wishlist
    open  func removeProductFromWishList(_ cartTypeId: NSInteger, productId: NSInteger, parameters: NSMutableArray , onSuccess successBlock: (() -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        var request = URLRequest(url: URL(string: NSString(format: "%@/ShoppingCart/RemoveProduct/%d/%d", APIManagerClient.sharedInstance.base_url, productId, cartTypeId) as String)!)
        request.httpMethod = "DELETE"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.allHTTPHeaderFields = self.getHeaderDic()
        request.httpBody = try! JSONSerialization.data(withJSONObject: parameters, options: [])
        println_debug("API Parameters : \(parameters)")
        Alamofire.request(request).responseJSON { response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                println_debug(json)
                println_debug("\n")
                let statusCode = json["StatusCode"].int
                if statusCode == 200 {
                    successBlock()
                } else {
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure( let error):
                errorBlock("\(error.localizedDescription)")
                
            }
        }
    }
    
    
    
    
    
    
    // MARK: AddProductToCart
    open  func loadAddProductToCart( _ cartTypeId: NSInteger, productId: NSInteger, parameters: NSMutableArray , onSuccess successBlock: ((AnyObject) -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        var request = URLRequest(url: URL(string: NSString(format: "%@/AddProductToCart/%d/%d", APIManagerClient.sharedInstance.base_url, productId, cartTypeId) as String)!)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.allHTTPHeaderFields = self.getHeaderDic()
        request.httpBody = try! JSONSerialization.data(withJSONObject: parameters, options: [])
        println_debug("API Parameters : \(parameters)")
        Alamofire.request(request).responseJSON { response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                println_debug(json)
                println_debug("\n")
                let statusCode = json["StatusCode"].int
                if statusCode == 200 {
                    //                    UserDefaults.standard.setValue(json["Count"].stringValue, forKey: "badge")
                    
                    if  json["Count"] == 0{
                        UserDefaults.standard.set("", forKey: "badge")
                    }else{
                        UserDefaults.standard.set(json["Count"].stringValue, forKey: "badge")
                    }
                    UserDefaults.standard.synchronize()
                    APIManagerClient.sharedInstance.shoppingCartCount = json["Count"].intValue
                    successBlock(data as AnyObject)
                } else {
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure( let error):
                errorBlock("\(error.localizedDescription)")
                
            }
        }
    }
    
    // MARK: LoadShoppingCart
    open  func loadShoppingCart( onSuccess successBlock: (([ShoppingCartInfo]) -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        //        var request = URLRequest(url: URL(string: NSString(format: "%@/ShoppingCart", baseURL) as String)!)
        var request = URLRequest(url: URL(string: NSString(format: "%@/ShoppingCart", APIManagerClient.sharedInstance.base_url) as String)!)
        request.httpMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        //        request.allHTTPHeaderFields = self.getHeaderDic()
        request.allHTTPHeaderFields = self.getHeaderDic()
        self.shoppingCartArray.removeAll()
        Alamofire.request(request).responseJSON { response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                println_debug(json)
                if let resData = json.dictionaryObject{
                    let shoppingInfo = ShoppingCartInfo(JSON: resData as AnyObject)
                    APIManagerClient.sharedInstance.shoppingCartCount = json["Count"].intValue
                    self.shoppingCartArray.append(shoppingInfo)
                    successBlock(self.shoppingCartArray)
                }
            case .failure( let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    
    
    // MARK: LoadShoppingCart
    open  func loadRecentSearchHistory( onSuccess successBlock: (([String]?) -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        //        var request = URLRequest(url: URL(string: NSString(format: "%@/ShoppingCart", baseURL) as String)!)
        var request = URLRequest(url: URL(string: NSString(format: "%@/catalog/RecentlySearchedKeyWords", APIManagerClient.sharedInstance.base_url) as String)!)
        request.httpMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        //        request.allHTTPHeaderFields = self.getHeaderDic()
        request.allHTTPHeaderFields = self.getHeaderDic()
        self.RecentSearcHistoryArray.removeAll()
        Alamofire.request(request).responseJSON { response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                println_debug(json)
                let statusCode = json["StatusCode"].int
                if statusCode == 200 {
                    if let HistoryArray = json["Data"].arrayObject as? [String]{
                        self.RecentSearcHistoryArray = HistoryArray
                    }
                    successBlock(self.RecentSearcHistoryArray)
                }
                
            case .failure( let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    
    
    
    // MARK: LoadWishList
    open func loadWishList(onSuccess successBlock: (([WishListItems]) -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        //        let urlParameters = String(format: "%@/shoppingCart/wishlist", baseURL) as String
        //        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { //(response) -> Void in
        let urlParameters = String(format: "%@/shoppingCart/wishlist", APIManagerClient.sharedInstance.base_url) as String
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                println_debug(json)
                let statusCode = json["StatusCode"].int
                if statusCode == 200 {
                    if let wishListItems  = Mapper<WishListItems>().mapArray(JSONObject: json["Items"].arrayObject){
                        successBlock(wishListItems)
                    } else {
                        errorBlock("Could not print JSON")
                    }
                } else {
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    // MARK: BillingAddress
    
    open func loadBillingAddressForm( onSuccess successBlock: ((BillingAddressModel) -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        //        let urlParameters = String(format: "%@/checkout/billingform", baseURL) as String
        //        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { //(response) -> Void in
        let urlParameters = String(format: "%@/checkout/billingform", APIManagerClient.sharedInstance.base_url) as String
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {  response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                println_debug(json)
                println_debug("\n")
                let statusCode = json["StatusCode"].int
                if statusCode == 200 {
                    
                    if json["NewAddress"].dictionaryObject as [String : AnyObject]? != nil{
                        let newBillingAddress = GenericBillingAddress(address: json["NewAddress"].dictionaryObject! as [String : AnyObject])
                        var existingBillingAddresses = [GenericBillingAddress]()
                        for address in json["ExistingAddresses"].arrayObject! {
                            let existingBillingAddress = GenericBillingAddress(address: address as! [String : AnyObject])
                            existingBillingAddresses.append(existingBillingAddress)
                        }
                        let billingAddress = BillingAddressModel()
                        billingAddress.newBillingAddress = newBillingAddress
                        billingAddress.existingBillingAddresses = existingBillingAddresses
                        successBlock(billingAddress)
                    }else{
                        successBlock(BillingAddressModel())
                    }
                    
                } else {
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    // MARK: GetBillingStates
    open func getBillingStates(_ countryId:NSInteger,  onSuccess successBlock: (([BillingState]) -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        //        let urlParameters = String(format: "%@/country/getstatesbycountryid/%d",baseURL, countryId) as String
        //        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { //(response) -> Void in
        let urlParameters = String(format: "%@/country/getstatesbycountryid/%d",APIManagerClient.sharedInstance.base_url, countryId) as String
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                let statusCode = json["StatusCode"].int
                if statusCode == 200 {
                    var states = [BillingState]()
                    if let resData = json["Data"].arrayObject {
                        for stateDictionary in resData {
                            let dic         = stateDictionary as! NSDictionary
                            let stateName   = dic["name"] as! String
                            let stateId     = dic["id"] as! NSInteger
                            let state       = BillingState(stateName: stateName as String , stateId: String(stateId))
                            states.append(state)
                        }
                    }
                    successBlock(states)
                } else {
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    // MARK: saveBillingOrShippingAddress
    open func saveBillingOrShippingAddress(_ address:NSInteger, from existingAddress:NSInteger,  onSuccess successBlock: (() -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        //        let urlParameters = String(format: "%@/checkout/checkoutsaveadressid/%d", baseURL, address) as String
        
        //        Alamofire.request(URL(string: urlParameters)!, method: .post, parameters: ["value":existingAddress], encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { response in
        let urlParameters = String(format: "%@/checkout/checkoutsaveadressid/%d", APIManagerClient.sharedInstance.base_url, address) as String
        Alamofire.request(URL(string: urlParameters)!, method: .post, parameters: ["PaymentMethod":"","VersionCode":"","value":existingAddress], encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                let statusCode = json["StatusCode"].int
                if statusCode == 200 {
                    successBlock()
                } else {
                    var errorMessage = "Billing Address"
                    if address == 2 {
                        errorMessage = "Shipping Address"
                    }
                    errorMessage += "couldn't be saved, try again."
                    errorBlock(errorMessage)
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    
    
    open func UserDelete(_ userName:String,  onSuccess successBlock: (() -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        
        let urlParameters = String(format: "%@/customer/deletecustomerbyusername/%@", APIManagerClient.sharedInstance.base_url, userName) as String
        Alamofire.request(URL(string: urlParameters)!, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                let statusCode = json["StatusCode"].int
                if statusCode == 200 {
                    successBlock()
                } else {
                    
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    /*
     
     open func medicineRequest(params: [String: Any],  onSuccess successBlock: (() -> Void)!, onError errorBlock: ((String?) -> Void)!) {
     
     let urlParameters = String(format: "%@/medicine/medicinerequest", APIManagerClient.sharedInstance.base_url) as String
     Alamofire.request(URL(string: urlParameters)!, method: .post, parameters: params, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { response in
     switch response.result {
     case .success(let data):
     let json = JSON(data)
     let statusCode = json["StatusCode"].int
     if statusCode == 200 {
     successBlock()
     } else {
     successBlock()
     }
     case .failure(let error):
     errorBlock("\(error.localizedDescription)")
     }
     }
     }
     
     */
    
    
    
    open func medicineRequest(params: [String: Any], onSuccess successBlock: ((Dictionary<String,AnyObject>) -> Void)!, onError errorBlock: ((String?) -> Void)!){
        
        let url1 =  URL(string: String(format: "%@/medicine/medicinerequest", APIManagerClient.sharedInstance.base_url) as String)
        println_debug(url1)
        let session             = URLSession.shared
        let request             = NSMutableURLRequest(url:url1!)
        request.timeoutInterval = 90
        request.cachePolicy     = NSURLRequest.CachePolicy.reloadIgnoringLocalCacheData
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        //request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.httpMethod = "POST"
        
        let theJSONData  = try? JSONSerialization.data(withJSONObject: params , options: JSONSerialization.WritingOptions(rawValue: 0))
        let jsonString   = NSString(data: theJSONData!,encoding: String.Encoding.ascii.rawValue)
        
        request.httpBody = jsonString!.data(using: String.Encoding.utf8.rawValue, allowLossyConversion:true)
        request.allHTTPHeaderFields = self.getHeaderDic()
        
        let dataTask = session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
            
            if let errorResponse = error {
                println_debug(errorResponse)
                //                successBlock(response!)
            }else {
                if let httpStatus = response as? HTTPURLResponse {
                    if httpStatus.statusCode == 200 {
                        if let json = data {
                            do {
                                let dict = try JSONSerialization.jsonObject(with: json, options: .allowFragments)
                                if var dataDict = dict as? Dictionary<String,AnyObject> {
                                    
                                    println_debug(dataDict)
                                    
                                    if let success = dataDict["StatusCode"] as? NSNumber{
                                        if success == 200 {
                                            successBlock(dataDict)
                                        }
                                        else{
                                            successBlock(dataDict)
                                        }
                                    }
                                    
                                    
                                }
                            } catch {}
                        }
                    }else {
                        println_debug(httpStatus)
                    }
                }else {
                    println_debug("Fail")
                }
            }
        }
        dataTask.resume()
        
    }
    
    open func AddMedicineToCartRequest(requestId:NSInteger, shoppingCartTypeId:NSInteger, onSuccess successBlock: (() -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        
        let urlParameters = String(format: "%@/AddMedicineToCart/%d/%d", APIManagerClient.sharedInstance.base_url,requestId,shoppingCartTypeId) as String
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                let statusCode = json["StatusCode"].int
                if statusCode == 200 {
                    successBlock()
                } else {
                    successBlock()
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    // changes avaneesh
    func getmyMedicineRequest(_ userName:String,  onSuccess successBlock: ((GetMedicineModel) -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        
        let urlParameters = String(format: "%@/medicine/getmymedicinerequest", APIManagerClient.sharedInstance.base_url) as String
        
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { response in
            
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                let statusCode = json["StatusCode"].int
                if statusCode == 200 {
                    if let medicineObjectModel  = Mapper<GetMedicineModel>().map(JSONObject: json.dictionaryObject){
                        println_debug("\nPlay")
                        successBlock(medicineObjectModel)
                    }else{
                        errorBlock("Could not print JSON")
                    }
                }else{
                    errorBlock("\(String(describing: json["StatusCode"].int))")
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
            
        }
        
    }
    
    // changes avaneesh
    func getTheAppUpdationStatus(_ userName:String,  onSuccess successBlock: ((UpdationModel) -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        
        let urlParameters = String(format: "%@/appupgrade/getappupgradedetails", APIManagerClient.sharedInstance.base_url) as String
        
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                let statusCode = json["StatusCode"].int
                if statusCode == 200 {
                    if let updationModel  = Mapper<UpdationModel>().map(JSONObject: json.dictionaryObject){
                        println_debug("\nPlay")
                        successBlock(updationModel)
                    }else{
                        errorBlock("Could not print JSON")
                    }
                }else{
                    errorBlock("\(String(describing: json["StatusCode"].int))")
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
        
    }
    
    
    open func saveBillingOrShippingAddress(_ address:NSInteger, parameters: [AnyObject],  onSuccess successBlock: (() -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        //        var request = URLRequest(url: URL(string: NSString(format: "%@/checkout/checkoutsaveadress/%d", baseURL, address) as String)!)
        var request = URLRequest(url: URL(string: NSString(format: "%@/checkout/checkoutsaveadress/%d", APIManagerClient.sharedInstance.base_url, address) as String)!)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        //        request.allHTTPHeaderFields = self.getHeaderDic()
        request.allHTTPHeaderFields = self.getHeaderDic()
        request.httpBody = try! JSONSerialization.data(withJSONObject: parameters, options: [])
        Alamofire.request(request).responseJSON {
            response in  
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                let statusCode = json["StatusCode"].int
                if statusCode == 200 {
                    successBlock()
                } else {
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    // MARK: GetShippingMethod
    open func getShippingMethod( onSuccess successBlock: (([ShippingMethod]) -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        //        let urlParameters = String(format: "%@/checkout/checkoutgetshippingmethods",baseURL) as String
        //        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { //(response) -> Void in
        let urlParameters = String(format: "%@/checkout/checkoutgetshippingmethods",APIManagerClient.sharedInstance.base_url) as String
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                let statusCode = json["StatusCode"].int
                if statusCode == 200 {
                    if let data = Mapper<ShippingMethod>().mapArray(JSONObject: json["ShippingMethods"].arrayObject){
                        successBlock(data)
                    }
                } else {
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    // MARK: GetStores
    open func getStores( onSuccess successBlock: (([Store]) -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        //        let urlParameters = String(format: "%@/checkout/getcheckoutpickuppoints",baseURL) as String
        //        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { //(response) -> Void in
        //            response in
        let urlParameters = String(format: "%@/checkout/getcheckoutpickuppoints",APIManagerClient.sharedInstance.base_url) as String
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {  response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                println_debug(json)
                let statusCode = json["StatusCode"].int
                if statusCode == 200 {
                    var stores = [Store]()
                    if let resData = json["PickupPoints"].arrayObject {
                        for storeDictionary in resData {
                            let store = Store(JSON: storeDictionary as! [String : AnyObject])
                            stores.append(store)
                        }
                        successBlock(stores)
                    }
                } else {
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    // MARK: SetStore
    open func saveStoreAddress(_ store:NSInteger,  onSuccess successBlock: (() -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        let urlParameters = String(format: "%@/checkout/savecheckoutpickuppoint?pickupPointId=%d", APIManagerClient.sharedInstance.base_url, store) as String
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                let statusCode = json["StatusCode"].int
                if statusCode == 200 {
                    successBlock()
                } else {
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    //-------------------------------------------------------------------------------------------------------------------------------------------------
    // MARK: GetPaymentMethod
    open func getPaymentMethod( onSuccess successBlock: (([PaymentMethod]) -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        let urlParameters = String(format: "%@/checkout/checkoutgetpaymentmethod",APIManagerClient.sharedInstance.base_url) as String
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                println_debug(json)
                println_debug("\n")
                let statusCode = json["StatusCode"].int
                if statusCode == 200 {
                    if let data = Mapper<PaymentMethod>().mapArray(JSONObject: json["PaymentMethods"].arrayObject) {
                        successBlock(data)
                    }else{
                        errorBlock("JSON could not be parsed")
                    }
                } else {
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    //MARK: SetShippingMethods
    open  func setShippingMethods(_ couponText : NSString,  onSuccess successBlock: (() -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        //        let urlParameters = String(format: "%@/checkout/checkoutsetshippingmethod", baseURL) as String
        //        Alamofire.request(URL(string: urlParameters)!, method: .post, parameters: ["value":couponText], encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { response in
        let urlParameters = String(format: "%@/checkout/checkoutsetshippingmethod", APIManagerClient.sharedInstance.base_url) as String
        Alamofire.request(URL(string: urlParameters)!, method: .post, parameters: ["value":couponText], encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                if json["StatusCode"].int == 200 {
                    successBlock()
                } else {
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    //MARK: SetPaymentMethods
    open  func setPaymentMethods(_ couponText : NSString,  onSuccess successBlock: (() -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        //        let urlParameters = String(format: "%@/checkout/checkoutsavepaymentmethod", baseURL) as String
        //        Alamofire.request(URL(string: urlParameters)!, method: .post, parameters: ["value":couponText], encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { response in
        let urlParameters = String(format: "%@/checkout/checkoutsavepaymentmethod", APIManagerClient.sharedInstance.base_url) as String
        Alamofire.request(URL(string: urlParameters)!, method: .post, parameters: ["PaymentMethod":couponText,"VersionCode":APIManagerClient.sharedInstance.versioncode], encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                if json["StatusCode"].int == 200 {
                    successBlock()
                } else {
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
                
            }
        }
    }
    
    // MARK: loadCheckOutOrderInformation
    open func loadCheckOutOrderInformation( onSuccess successBlock: ((CheckOutOrderInfo) -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        let urlParameters = String(format: "%@/shoppingcart/checkoutorderinformation",APIManagerClient.sharedInstance.base_url) as String
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {   response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                println_debug(json)
                let statusCode = json["StatusCode"].int
                if statusCode == 200 {
                    
                    if let resData = json.dictionaryObject{
                        let orderInfo = CheckOutOrderInfo(JSON: resData as AnyObject)
                        successBlock(orderInfo)
                    }
                } else {
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    // MARK: CheckOutComplete
    open func checkOutComplete( onSuccess successBlock: ((JSON ) -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        //        let urlParameters = String(format: "%@/checkout/checkoutcomplete",baseURL) as String
        //        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { //(response) -> Void in
        //            response in
        
        let urlParameters = String(format: "%@/checkout/checkoutcomplete",APIManagerClient.sharedInstance.base_url) as String
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {  response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                let statusCode = json["StatusCode"].int
                if statusCode == 200 {
                    successBlock(json)
                } else {
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    // MARK: LoadTotalOrder
    
    open  func loadTotalOrder(onSuccess successBlock: ((TotalOrder) -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        //        let urlParameters = String(format: "%@/ShoppingCart/OrderTotal", baseURL) as String
        //        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { //(response) -> Void in
        let urlParameters = String(format: "%@/ShoppingCart/OrderTotal", APIManagerClient.sharedInstance.base_url) as String
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                if json["StatusCode"].int == 200 {
                    let totalOrder = TotalOrder(JSON: json.dictionaryObject! as [String : AnyObject])
                    successBlock(totalOrder!)
                } else {
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    // MARK: LoadTotalDoctorsAvailables
    
    open  func loadTotalDoctorsAvailables(onSuccess successBlock: (([Dictionary<String,AnyObject>]) -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        //        let urlParameters = String(format: "%@/ShoppingCart/OrderTotal", baseURL) as String
        //        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { //(response) -> Void in
        let urlParameters = String(format: "%@/okdollar/GetChatRoom", APIManagerClient.sharedInstance.baseURl_doctors) as String
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                if json["code"].int == 200 {
                    let dataResponse = json["data"].arrayObject as? [Dictionary<String,AnyObject>]
                    successBlock(dataResponse!)
                } else {
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    
    // MARK: Upload Base64 to AWS by SUBHASH
    open  func UploadImage(params : Dictionary<String,Any>,url: String , successBlock: @escaping (_ success: Bool, _ response: [AnyObject]) -> Void) {
        
        let url1 =  URL(string: url)
        println_debug(url1)
        let session             = URLSession.shared
        let request             = NSMutableURLRequest(url:url1!)
        request.timeoutInterval = 90
        request.cachePolicy     = NSURLRequest.CachePolicy.reloadIgnoringLocalCacheData
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.httpMethod = "POST"
        
        let theJSONData  = try? JSONSerialization.data(withJSONObject: params , options: JSONSerialization.WritingOptions(rawValue: 0))
        let jsonString   = NSString(data: theJSONData!,encoding: String.Encoding.ascii.rawValue)
        let postLength   = NSString(format:"%lu", jsonString!.length) as String
        request.setValue(postLength, forHTTPHeaderField:"Content-Length")
        request.httpBody = jsonString!.data(using: String.Encoding.utf8.rawValue, allowLossyConversion:true)
        request.setValue(nil, forHTTPHeaderField:"Authorization")
        
        let dataTask = session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
            if let errorResponse = error {
                println_debug(errorResponse)
                successBlock(false, ["failure" as AnyObject])
            }else {
                if let httpStatus = response as? HTTPURLResponse {
                    if httpStatus.statusCode == 200 {
                        if let json = data {
                            do {
                                let dict = try JSONSerialization.jsonObject(with: json, options: .allowFragments)
                                if var dataDict = dict as? Dictionary<String,AnyObject> {
                                    println_debug(dataDict)
                                    if dataDict["Code"] as! NSNumber == 200 {
                                        if let data = self.convertToArrDictionary(text: dataDict["Data"] as! String){
                                            successBlock(true,data)
                                        }
                                        //                                        if let firstUrl = data!.first as? String {
                                        //                                            successBlock(true, firstUrl)
                                        //                                        }
                                        
                                        
                                    }else if dataDict["Code"] as! NSNumber == 400 {
                                        if let data = dataDict["Msg"]{
                                            var arr = [AnyObject]()
                                            arr.append(data)
                                            successBlock(false,arr)
                                        }
                                    }
                                }
                            } catch {successBlock(false,[httpStatus.statusCode as AnyObject])}
                        }
                    }else {
                        successBlock(false,[httpStatus.statusCode as AnyObject])
                        println_debug(httpStatus)
                    }
                }
            }
        }
        dataTask.resume()
        
    }
    
    /*
     func uploadAudio(successBlock: @escaping (_ success: String) -> Void){
     let audioData: NSData = NSData()
     
     
     Alamofire.Manager.upload(.PUT,
     URL,
     headers: headers,
     multipartFormData: { multipartFormData in
     multipartFormData.appendBodyPart(data: "3".dataUsingEncoding(NSUTF8StringEncoding), name: "from_account_id")
     multipartFormData.appendBodyPart(data: "4".dataUsingEncoding(NSUTF8StringEncoding), name: "to_account_id")
     multipartFormData.appendBodyPart(data: audioData, name: "file", fileName: "file", mimeType: "application/octet-stream")
     },
     encodingCompletion: { encodingResult in
     switch encodingResult {
     
     case .Success(let upload, _, _):
     upload.responseJSON { response in
     
     }
     
     case .Failure(let encodingError): break
     // Error while encoding request:
     }
     })
     }
     */
    
    
    
    
    
    func convertToArrDictionary(text: String) -> [AnyObject]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [AnyObject]
            } catch {
                println_debug(error.localizedDescription)
            }
        }
        return nil
    }
    
    // MARK: RemoveGiftCard
    open  func RemoveGiftCard(_ ID : NSInteger ,  onSuccess successBlock: ((TotalOrder) -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        //        let urlParameters = String(format: "%@/ShoppingCart/RemoveGiftCard", baseURL) as String
        //        Alamofire.request(URL(string: urlParameters)!, method: .post, parameters: ["value":ID ], encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { //(response) -> Void in
        let urlParameters = String(format: "%@/ShoppingCart/RemoveGiftCard", APIManagerClient.sharedInstance.base_url) as String
        Alamofire.request(URL(string: urlParameters)!, method: .post, parameters: ["value":ID ], encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                if json["StatusCode"].int == 200 {
                    if let totalOrder = Mapper<TotalOrder>().map(JSONObject: json["Data"].arrayObject) {
                        successBlock(totalOrder)
                    }
                } else {
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    //MARK: DeleteAndUpdateCart
    open  func DeleteAndUpdate(_ parameters : NSMutableArray,  onSuccess successBlock: (([ShoppingCartInfo]) -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        //        var request = URLRequest(url: URL(string: NSString(format: "%@/ShoppingCart/UpdateCart", baseURL) as String)!)
        var request = URLRequest(url: URL(string: NSString(format: "%@/ShoppingCart/UpdateCart", APIManagerClient.sharedInstance.base_url) as String)!)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        //        request.allHTTPHeaderFields = self.getHeaderDic()
        request.allHTTPHeaderFields = self.getHeaderDic()
        request.httpBody = try! JSONSerialization.data(withJSONObject: parameters, options: [])
        
        Alamofire.request(request).responseJSON { response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                println_debug(json)
                println_debug("\n")
                if json["StatusCode"].int == 200 {
                    APIManagerClient.sharedInstance.shoppingCartCount = json["Count"].intValue
                    self.shoppingCartArray.removeAll()
                    if let resData = json.dictionaryObject{
                        let shoppingInfo = ShoppingCartInfo(JSON: resData as AnyObject)
                        self.shoppingCartArray.append(shoppingInfo)
                        successBlock(self.shoppingCartArray)
                    }
                }
                else  if json["StatusCode"].int == 400 && json["OrderTotalResponseModel"]["StatusCode"].int == 200{
                    APIManagerClient.sharedInstance.shoppingCartCount = json["Count"].intValue
                    self.shoppingCartArray.removeAll()
                    if let resData = json.dictionaryObject{
                        let shoppingInfo = ShoppingCartInfo(JSON: resData as AnyObject)
                        self.shoppingCartArray.append(shoppingInfo)
                        successBlock(self.shoppingCartArray)
                    }
                }
                    
                else {
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    
    
    //MARK: DeleteAndUpdateWishList
    open func deleteAndUpdateWishList(_ parameters : NSMutableArray, onSuccess successBlock: (() -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        //        var  request = URLRequest(url: URL(string: NSString(format: "%@/ShoppingCart/UpdateWishlist", baseURL) as String)!)
        var  request = URLRequest(url: URL(string: NSString(format: "%@/ShoppingCart/UpdateWishlist", APIManagerClient.sharedInstance.base_url) as String)!)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        //        request.allHTTPHeaderFields = self.getHeaderDic()
        request.allHTTPHeaderFields = self.getHeaderDic()
        request.httpBody = try! JSONSerialization.data(withJSONObject: parameters, options: [])
        
        Alamofire.request(request).responseJSON {  response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                if json["StatusCode"].int == 200 {
                    successBlock()
                } else {
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    //MARK: AddProductFromWishlistToShoppingCart
    
    open func addProductFromWishlistToShoppingCart(_ parameters : NSMutableArray, onSuccess successBlock: (() -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        //        var  request = URLRequest(url: URL(string: NSString(format: "%@/ShoppingCart/AddItemsToCartFromWishlist", baseURL) as String)!)
        var  request = URLRequest(url: URL(string: NSString(format: "%@/ShoppingCart/AddItemsToCartFromWishlist", APIManagerClient.sharedInstance.base_url) as String)!)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        //        request.allHTTPHeaderFields = self.getHeaderDic()
        request.allHTTPHeaderFields = self.getHeaderDic()
        request.httpBody = try! JSONSerialization.data(withJSONObject: parameters, options: [])
        Alamofire.request(request).responseJSON {
            response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                if json["StatusCode"].int == 200 {
                    APIManagerClient.sharedInstance.shoppingCartCount = json["Count"].intValue
                    successBlock()
                } else {
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------------------------------------------------
    //MARK: ApplyDiscountCoupon
    open  func applyDiscountCoupon(_ couponText : NSString,  onSuccess successBlock: ((TotalOrder) -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        //        let urlParameters = String(format: "%@/ShoppingCart/ApplyDiscountCoupon", baseURL) as String
        //        Alamofire.request(URL(string: urlParameters)!, method: .post, parameters: ["value":couponText], encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { //(response) -> Void in
        let urlParameters = String(format: "%@/ShoppingCart/ApplyDiscountCoupon", APIManagerClient.sharedInstance.base_url) as String
        Alamofire.request(URL(string: urlParameters)!, method: .post, parameters: ["value":couponText], encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                if json["StatusCode"].int == 200 {
                    if let totalOrder = Mapper<TotalOrder>().map(JSONObject: json["Data"].arrayObject) {
                        successBlock(totalOrder)
                    }
                } else {
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
                
            }
        }
    }
    
    //MARK: ApplyGiftCardCoupon
    open  func applyGiftCardCoupon(_ couponText : NSString,  onSuccess successBlock: ((TotalOrderGift) -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        //        let urlParameters = String(format: "%@/ShoppingCart/ApplyGiftCard", baseURL) as String
        //        Alamofire.request(URL(string: urlParameters)!, method: .post, parameters: ["value":couponText], encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { //(response) -> Void in
        let urlParameters = String(format: "%@/ShoppingCart/ApplyGiftCard", APIManagerClient.sharedInstance.base_url) as String
        Alamofire.request(URL(string: urlParameters)!, method: .post, parameters: ["value":couponText], encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                println_debug(json)
                println_debug("\n")
                if json["StatusCode"].int == 200 {
                    if let totalOrder =  Mapper<TotalOrderGift>().map(JSONObject: json["OrderTotalResponseModel"].dictionaryObject) {
                        successBlock(totalOrder)
                    }
                } else {
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    //MARK: RemoveDiscountCoupon
    open  func RemoveDiscountCoupon(  onSuccess successBlock: (() -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        //        let urlParameters = String(format: "%@/ShoppingCart/RemoveDiscountCoupon", baseURL) as String
        //        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { //(response) -> Void in
        let urlParameters = String(format: "%@/ShoppingCart/RemoveDiscountCoupon", APIManagerClient.sharedInstance.base_url) as String
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                if json["StatusCode"].int == 200 {
                    successBlock()
                } else {
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    //MARK: loadProceedToCheckout
    open  func loadProceedToCheckout(parameters : NSMutableArray,  onSuccess successBlock: (() -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        var  request = URLRequest(url: URL(string: NSString(format: "%@/ShoppingCart/applycheckoutattribute", baseURL) as String)!)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.allHTTPHeaderFields = self.getHeaderDic()
        request.httpBody = try! JSONSerialization.data(withJSONObject: parameters, options: [])
        println_debug("Nobody : \(parameters.removeAllObjects())")
        parameters.removeAllObjects()
        
        Alamofire.request(request).responseJSON { response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                if json["StatusCode"].int == 200 {
                    successBlock()
                } else {
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    //-------------------------------------------------------------------------------------------------------------------------------------------------
    //MARK: loadOrders
    open  func loadOrders(onSuccess successBlock: (([OrderInfo]) -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        //        let urlParameters = String(format: "%@/order/customerorders", baseURL) as String
        //        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { //(response) -> Void in
        let urlParameters = String(format: "%@/order/customerorders", APIManagerClient.sharedInstance.base_url) as String
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                println_debug(json)
                println_debug("\n")
                let statusCode = json["StatusCode"].int
                if statusCode == 200 {
                    if let data = Mapper<OrderInfo>().mapArray(JSONObject: json["Orders"].arrayObject) {
                        successBlock(data)
                    }
                } else {
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    //MARK: getOrderDetails
    open  func getOrderDetails(_ orderId:NSInteger, onSuccess successBlock: ((OrderDetailsInfo) -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        //        let urlParameters = String(format: "%@/order/details/%d", baseURL, orderId) as String
        //        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { //(response) -> Void in
        let urlParameters = String(format: "%@/order/details/%d", APIManagerClient.sharedInstance.base_url, orderId) as String
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                let statusCode = json["StatusCode"].int
                if statusCode == 200 {
                    let orderdetailsInfo = OrderDetailsInfo()
                    let  billingAddress = GenericBillingAddress(address: json["BillingAddress"].dictionaryObject! as [String : AnyObject])
                    let  shippingAddress = GenericBillingAddress(address: json["ShippingAddress"].dictionaryObject! as [String : AnyObject])
                    orderdetailsInfo.billingAddress       = billingAddress
                    orderdetailsInfo.shippingAddress      = shippingAddress
                    orderdetailsInfo.PaymentMethod        = json["PaymentMethod"].string
                    orderdetailsInfo.PaymentMethodStatus  = json["PaymentMethodStatus"].string
                    orderdetailsInfo.ShippingMethod       = json["ShippingMethod"].string
                    orderdetailsInfo.ShippingStatus       = json["ShippingStatus"].string
                    orderdetailsInfo.OrderSubtotal        = json["OrderSubtotal"].string
                    orderdetailsInfo.OrderTotal           = json["OrderTotal"].string
                    orderdetailsInfo.OrderSubTotalDiscount    = json["OrderSubTotalDiscount"].string
                    orderdetailsInfo.Tax                      = json["Tax"].string
                    orderdetailsInfo.OrderShipping            = json["OrderShipping"].string
                    orderdetailsInfo.CheckoutAttributeInfo    = json["CheckoutAttributeInfo"].string
                    
                    for address in json["Items"].arrayObject! {
                        let items = CheckOutItems(JSON: address as AnyObject)
                        orderdetailsInfo.items.append(items)
                    }
                    successBlock(orderdetailsInfo)
                } else {
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    //MARK: Register
    open func registerCustomer(_ params:[String: AnyObject], onSuccess successBlock:(() -> Void)!, onError errorBlock:((String?) -> Void)!) {
        let urlParameters = String(format: "%@/customer/register", baseURL) as String
        Alamofire.request(URL(string: urlParameters)!, method: .post, parameters: params, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { //(response) -> Void in
            response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                println_debug(json)
                println_debug("\n")
                if json["StatusCode"].int == 200 {
                    successBlock()
                } else {
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    //MARK: SignIn
    open func signInCustomer(_ params:[String: AnyObject], onSuccess successBlock:(() -> Void)!, onError errorBlock:((String?) -> Void)!) {
        
        
        let urlParameters = String(format: "%@/login", APIManagerClient.sharedInstance.base_url) as String
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                println_debug(json)
                println_debug("\n")
                if json["StatusCode"].int == 200 {
                    let token = json["Token"].string
                    let defaults = UserDefaults.standard
                    defaults.set(token, forKey: "accessToken")
                    defaults.synchronize()
                    successBlock()
                } else {
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    
    //MARK: SignIn
    open func LoginInUser(_ params:[String: AnyObject], onSuccess successBlock:(([String: AnyObject]) -> Void)!, onError errorBlock:((String?) -> Void)!) {
        
        let url1 =  URL(string: "\(APIManagerClient.sharedInstance.base_url)/customer/registerorlogin")!
        println_debug(params)
        println_debug(url1)
        
        // var request = URLRequest(url: URL(string: "http://69.160.4.149:1006/api/customer/registerorlogin")!,timeoutInterval: Double.infinity)
        let session             = URLSession.shared
        let request             = NSMutableURLRequest(url:url1)
        request.timeoutInterval = 90
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        //request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.httpMethod = "POST"
        
        let theJSONData  = try? JSONSerialization.data(withJSONObject: params , options: .prettyPrinted)
        let jsonString   = NSString(data: theJSONData!,encoding: String.Encoding.utf8.rawValue)
        
        request.httpBody = jsonString!.data(using: String.Encoding.utf8.rawValue)
        request.allHTTPHeaderFields = self.getHeaderDic()
        
        let dataTask = session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
            
            if let errorResponse = error {
                println_debug(errorResponse)
            }else {
                if let httpStatus = response as? HTTPURLResponse {
                    if httpStatus.statusCode == 200 {
                        if let json = data {
                            do {
                                let dict = try JSONSerialization.jsonObject(with: json, options: .allowFragments)
                                if var dataDict = dict as? Dictionary<String,AnyObject> {
                                    println_debug(dataDict)
                                    if let success = dataDict["StatusCode"] as? NSNumber{
                                        if success == 200 {
                                            successBlock(dataDict)
                                        }
                                        else{
                                            successBlock(dataDict)
                                        }
                                    }
                                    
                                    
                                }
                            } catch {}
                        }
                    }else {
                        println_debug(httpStatus)
                        errorBlock("\(error?.localizedDescription)")
                    }
                }else {
                    println_debug("Fail")
                    errorBlock("\(error?.localizedDescription)")
                }
            }
        }
        dataTask.resume()
    }
    
    
    
    
    //MARK: Forgor Password
    open func forgotPassword(_ params:[String: AnyObject], onSuccess successBlock:((String?) -> Void)!, onError errorBlock:((String?) -> Void)!) {
        let urlParameters = String(format: "%@/customer/passwordrecovery", baseURL) as String
        Alamofire.request(URL(string: urlParameters)!, method: .post, parameters: params, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { //(response) -> Void in
            response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                println_debug(json)
                if json["StatusCode"].int == 200 {
                    let message = json["SuccessMessage"].string
                    successBlock(message)
                } else {
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    //------------------------------------------------------------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------------------------------------------------
    //MARK: Customer Info
    open func loadCustomerInfo(onSuccess successBlock:(([String: AnyObject]) -> Void)!, onError errorBlock:((String?) -> Void)!) {
        //        let urlParameters = String(format: "%@/customer/info", baseURL) as String
        //        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { //(response) -> Void in
        let urlParameters = String(format: "%@/customer/info", APIManagerClient.sharedInstance.base_url) as String
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { //(response) -> Void in
            response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                if json["StatusCode"].int == 200 {
                    successBlock(json.dictionaryObject! as [String : AnyObject])
                } else {
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    open func saveCustomerInfo(_ params:[String: AnyObject], onSuccess successBlock:(() -> Void)!, onError errorBlock:((String?) -> Void)!) {
        //        let urlParameters = String(format: "%@/customer/info", baseURL) as String
        //        Alamofire.request(URL(string: urlParameters)!, method: .post, parameters: params, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { //(response) -> Void in
        let urlParameters = String(format: "%@/customer/info", APIManagerClient.sharedInstance.base_url) as String
        Alamofire.request(URL(string: urlParameters)!, method: .post, parameters: params, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                if json["StatusCode"].int == 200 {
                    successBlock()
                }  else {
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    //MARK: Customer Attributs
    func getCustomerAttributs(onSuccess successBlock:(([CustomerAttributs]) -> Void)!, onError errorBlock:((String?) -> Void)!) {
        let urlParameters = String(format: "%@/customer/attributes", baseURL) as String
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { //(response) -> Void in
            response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                println_debug(json)
                println_debug("\n")
                if json["StatusCode"].int == 200 {
                    if let data = Mapper<CustomerAttributs>().mapArray(JSONObject: json["Data"].arrayObject) {
                        successBlock(data)
                    }
                } else {
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    //MARK: Address
    open  func getAddress(onSuccess successBlock: (([GenericBillingAddress]) -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        //        let urlParameters = String(format: "%@/customer/addresses", baseURL) as String
        //        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { //(response) -> Void in
        let urlParameters = String(format: "%@/customer/addresses", APIManagerClient.sharedInstance.base_url) as String
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                let statusCode = json["StatusCode"].int
                if statusCode == 200 {
                    var existingAddress = [GenericBillingAddress]()
                    for address in json["ExistingAddresses"].arrayObject! {
                        let  existingAdd = GenericBillingAddress(address: address as! [String : AnyObject])
                        existingAddress.append(existingAdd)
                    }
                    successBlock(existingAddress)
                } else {
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    //MARK: DeleteAddress
    open  func deleteAddress(_ addressId:NSInteger, onSuccess successBlock: (() -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        //        let urlParameters = String(format: "%@/customer/address/remove/%d", baseURL, addressId) as String
        //        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { //(response) -> Void in
        let urlParameters = String(format: "%@/customer/address/remove/%d", APIManagerClient.sharedInstance.base_url, addressId) as String
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                let statusCode = json["StatusCode"].int
                if statusCode == 200 {
                    successBlock()
                } else {
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    //MARK: GetCustomerInfo
    open  func getCustomerInfo(_ addressId:NSInteger, onSuccess successBlock: ((GenericBillingAddress) -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        //        let urlParameters = String(format: "%@/customer/address/edit/%d", baseURL, addressId) as String
        //        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { //(response) -> Void in
        let urlParameters = String(format: "%@/customer/address/edit/%d", APIManagerClient.sharedInstance.base_url, addressId) as String
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                let statusCode = json["StatusCode"].int
                if statusCode == 200 {
                    let  address = GenericBillingAddress(address: json["Address"].dictionaryObject! as [String : AnyObject])
                    successBlock(address)
                } else {
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    //MARK: Saved Edite Info
    open  func savedAddress(_ addressId:NSInteger, parameters: [AnyObject], onSuccess successBlock: (() -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        var  request = URLRequest(url: URL(string: NSString(format: "%@/customer/address/edit/%d", baseURL, addressId) as String)!)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.allHTTPHeaderFields = self.getHeaderDic()
        request.httpBody = try! JSONSerialization.data(withJSONObject: parameters, options: [])
        Alamofire.request(request).responseJSON {
            response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                let statusCode = json["StatusCode"].int
                if statusCode == 200 {
                    successBlock()
                } else {
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    //MARK: Add New Address
    open  func addNewAddress(_ parameters: [AnyObject], onSuccess successBlock: (() -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        //        var request = URLRequest(url: URL(string: NSString(format: "%@/customer/address/add", baseURL) as String)!)
        var request = URLRequest(url: URL(string: NSString(format: "%@/customer/address/add", APIManagerClient.sharedInstance.base_url) as String)!)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        //        request.allHTTPHeaderFields = self.getHeaderDic()
        request.allHTTPHeaderFields = self.getHeaderDic()
        request.httpBody = try! JSONSerialization.data(withJSONObject: parameters, options: [])
        Alamofire.request(request).responseJSON {
            response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                let statusCode = json["StatusCode"].int
                if statusCode == 200 {
                    successBlock()
                } else {
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------------------------------------------------
    //MARK: Paypal Confirm
    open  func paypalConfirm(_ PaymentId : NSString, orderId: NSInteger ,  onSuccess successBlock: (() -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        let urlParameters = String(format: "%@/checkout/checkpaypalaccount", baseURL) as String
        Alamofire.request(URL(string: urlParameters)!, method: .post, parameters: ["PaymentId":PaymentId,"OrderId":orderId] , encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { //(response) -> Void in
            response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                if json["StatusCode"].int == 200 {
                    successBlock()
                } else {
                    let message = self.displayErrorMessage(json: json)
                    if (message != "") {
                        errorBlock(message)
                    } else {
                        errorBlock("Payment Failed.Please try again")
                    }
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
                
            }
        }
    }
    
    //MARK: Authorize.Net Confirm
    open func getAuthorizeDotNet(_ params:[String: AnyObject], onSuccess successBlock:(() -> Void)!, onError errorBlock:((String?) -> Void)!) {
        let urlParameters = String(format: "%@/checkout/checkauthorizepayment", baseURL) as String
        Alamofire.request(URL(string: urlParameters)!, method: .post, parameters: params, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { //(response) -> Void in
            response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                if json["StatusCode"].int == 200 {
                    successBlock()
                } else {
                    let message = self.displayErrorMessage(json: json)
                    if (message != "") {
                        errorBlock(message)
                    }else{
                        errorBlock("Payment Failed.Please try again")
                    }
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------------------------------------------------
    //MARK: Redirect Payment Confirm
    open func getRedirectPayment(_ orderId: NSInteger, onSuccess successBlock:(() -> Void)!, onError errorBlock:((String?) -> Void)!){
        let urlParameters = String(format: "%@/checkout/checkredirectpayment", baseURL) as String
        Alamofire.request(URL(string: urlParameters)!, method: .post, parameters: ["OrderId": orderId], encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { //(response) -> Void in
            response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                if json["StatusCode"].int == 200 {
                    successBlock()
                } else {
                    let message = self.displayErrorMessage(json: json)
                    if (message != "") {
                        errorBlock(message)
                    }else{
                        errorBlock("Checkout Failed.Please try again")
                    }
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------------------------------------------------
    // MARK: GetSearchResults
    open func getSearchResults(_ searchText: NSString, pageNumber:Int, onSuccess successBlock: ((CategoryDetailsInfo) -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        //        let urlParameters = String(format: "%@/catalog/search", baseURL) as String
        //        Alamofire.request(URL(string: urlParameters)!, method: .post, parameters: ["q":searchText], encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { //(response) -> Void in
        let urlParameters = "\(APIManagerClient.sharedInstance.base_url)/catalog/search?pageNumber=\(pageNumber)"
        Alamofire.request(URL(string: urlParameters)!, method: .post, parameters: ["q":searchText], encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                println_debug(json)
                println_debug("\n")
                
                if let arr = json["Products"].arrayObject, arr.count > 0 {
                    
                    
                    //                var productList = [ProductsInfo]()
                    //                if let resData = json["Products"].arrayObject {
                    //                    for productDictionary in resData {
                    //                        let products = ProductsInfo(JSON: productDictionary as! [String : AnyObject] as AnyObject)
                    //                        productList.append(products)
                    //                    }
                    //                }
                    //                successBlock(productList)
                    
                    let categoryDetailsInfo = CategoryDetailsInfo ()
                    //                categoryDetailsInfo.fromPrice = json["PriceRange"]["From"].double
                    //                categoryDetailsInfo.toPrice = json["PriceRange"]["To"].double
                    categoryDetailsInfo.TotalPages = json["TotalPages"].int!
                    
                    if let resData = json["Products"].arrayObject {
                        for productDictionary in resData {
                            let products = ProductsInfo(JSON: productDictionary as! [String : AnyObject] as AnyObject)
                            categoryDetailsInfo.productsInfo.append(products)
                        }
                    }
//                    if let resData = json["PriceRange"].dictionaryObject {
//                        categoryDetailsInfo.fromPrice = json["PriceRange"]["From"].double
//                        categoryDetailsInfo.toPrice = json["PriceRange"]["To"].double
//                        UserDefaults.standard.set(json["PriceRange"]["From"].stringValue, forKey: "min")
//                        UserDefaults.standard.set(json["PriceRange"]["To"].stringValue, forKey: "max")
//                        UserDefaults.standard.synchronize()
//                        let notFilteredItem = PriceRange(JSON: resData as [String : AnyObject] as AnyObject)
//                        categoryDetailsInfo.PriceRange.append(notFilteredItem)
//                    }
                    if let resData = json["PriceRange"].dictionaryObject {
                        if let defaultMinValue = UserDefaults.standard.value(forKey: "min") as? String , defaultMinValue != "" {
                            guard let minVal = String(format: "%@", UserDefaults.standard.value(forKey: "min") as! CVarArg).toDouble() as? Double else { return }
                            let minValueNew = json["PriceRange"]["From"].doubleValue
                            if minVal > minValueNew {
                                                        categoryDetailsInfo.fromPrice = json["PriceRange"]["From"].double
                                                        UserDefaults.standard.set(json["PriceRange"]["From"].stringValue, forKey: "min")
                                                        UserDefaults.standard.synchronize()
                                                        let notFilteredItem = PriceRange(JSON: resData as [String : AnyObject] as AnyObject)
                                                        categoryDetailsInfo.PriceRange.append(notFilteredItem)
                           }
                            else {
                              categoryDetailsInfo.fromPrice = minVal
                            }
                        }
                        if let defaultMaxValue = UserDefaults.standard.value(forKey: "max") as? String , defaultMaxValue != "" {
                            guard let maxVal = String(format: "%@", UserDefaults.standard.value(forKey: "max") as! CVarArg).toDouble() as? Double else { return }
                            let maxValueNew = json["PriceRange"]["To"].doubleValue
                                         if maxVal < maxValueNew {
                                                        categoryDetailsInfo.toPrice = json["PriceRange"]["To"].double
                                                        UserDefaults.standard.set(json["PriceRange"]["To"].stringValue, forKey: "max")
                                                        UserDefaults.standard.synchronize()
                                                        let notFilteredItem = PriceRange(JSON: resData as [String : AnyObject] as AnyObject)
                                                        categoryDetailsInfo.PriceRange.append(notFilteredItem)
                                                }
                            else {
                              categoryDetailsInfo.toPrice = maxVal
                            }

                        }
                        else {
                            categoryDetailsInfo.fromPrice = json["PriceRange"]["From"].double
                            categoryDetailsInfo.toPrice = json["PriceRange"]["To"].double
                            if categoryDetailsInfo.toPrice != nil {
                                UserDefaults.standard.set(json["PriceRange"]["From"].stringValue, forKey: "min")
                                UserDefaults.standard.set(json["PriceRange"]["To"].stringValue, forKey: "max")
                                UserDefaults.standard.synchronize()
                                let notFilteredItem = PriceRange(JSON: resData as [String : AnyObject] as AnyObject)
                                categoryDetailsInfo.PriceRange.append(notFilteredItem)
                            }
                        }
                    }
                    if let resData = json["NotFilteredItems"].arrayObject {
                        for notFilteredItemDictionary in resData {
                            let notFilteredItem = NotFilteredItems(JSON: notFilteredItemDictionary as! [String : AnyObject] as AnyObject)
                            categoryDetailsInfo.notFilteredItems.append(notFilteredItem)
                        }
                    }
                    if let resData = json["AlreadyFilteredItems"].arrayObject {
                        for alreadyFilteredItemDictionary in resData {
                            let alreadyFilteredItem = NotFilteredItems(JSON: alreadyFilteredItemDictionary as! [String : AnyObject] as AnyObject)
                            categoryDetailsInfo.alreadyFilteredItems.append(alreadyFilteredItem)
                        }
                    }
                    if let resData = json["AvailableSortOptions"].arrayObject {
                        for availableSortOptionDictionary in resData {
                            let availableSortOptionItem = AvailableSortOptions(JSON: availableSortOptionDictionary as! [String : AnyObject] as AnyObject)
                            categoryDetailsInfo.availableSortOptions.append(availableSortOptionItem)
                        }
                    }
                    
                    successBlock(categoryDetailsInfo)
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    //MARK: CheckOutForGuest
    open  func checkOutForGuest(onSuccess successBlock: ((Bool) -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        //        let urlParameters = String(format: "%@/checkout/opccheckoutforguest", baseURL) as String
        //        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {
        //
        let urlParameters = String(format: "%@/checkout/opccheckoutforguest", APIManagerClient.sharedInstance.base_url) as String
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                let statusCode = json["StatusCode"].int
                if statusCode == 200 {
                    if json["Data"].int == 1 {
                        successBlock(true)
                    } else {
                        successBlock(false)
                    }
                } else {
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    //MARK: RegisterDeviceId
    open  func registerDeviceId(_ deviceId: String, onSuccess successBlock: (() -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        let urlParameters = String(format: "%@/appstart", APIManagerClient.sharedInstance.base_url) as String
        //        Alamofire.request(URL(string: urlParameters)!, method: .post, parameters: ["DeviceTypeId":"5","SubscriptionId":deviceId,"EmailAddress":"mubassher@brainstation-23.com"], encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { //(response) -> Void in
        Alamofire.request(URL(string: urlParameters)!, method: .post, parameters: ["DeviceTypeId":"5","SubscriptionId":deviceId], encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { //(response) -> Void in
            response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                println_debug(json)
                println_debug("\n")
                if json["StatusCode"].int == 200 {
                    if let dic = Mapper<AppStartUpModel>().map(JSONObject: json["Data"].dictionaryObject){
                        APIManagerClient.sharedInstance.AppStartUp = dic
                    }
                } else {
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    //MARK: getOrderDetails
    open  func retryPaymentOrder(_ orderId:NSInteger, onSuccess successBlock: (() -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        //        let urlParameters = String(format: "%@/order/reorder/%d", baseURL, orderId) as String
        //        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { //(response) -> Void in
        let urlParameters = String(format: "%@/order/reorder/%d", APIManagerClient.sharedInstance.base_url, orderId) as String
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                let statusCode = json["StatusCode"].int
                if statusCode == 200 {
                    successBlock()
                } else {
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    //MARK: Facebook Sign In
    public func facebookLogin(params:[String: AnyObject], onSuccess successBlock:(() -> Void)!, onError errorBlock:((String?) -> Void)!) {
        let urlParameters = String(format: "%@/facebookLogin", baseURL) as String
        Alamofire.request(URL(string: urlParameters)!,method: .post, parameters:params, headers:self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                println_debug(json)
                if json["StatusCode"].int == 200 {
                    let token = json["Token"].string
                    let defaults = UserDefaults.standard
                    defaults.set(token, forKey: "accessToken")
                    defaults.synchronize()
                    successBlock()
                } else {
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure(let error):
                errorBlock(error.localizedDescription)
            }
        }
    }
    
    // crash detected (Avaneesh ) 
    public func GenericClassGet(url: String, successBlock: @escaping (_ isBool: Bool, _ response: JSON)-> Void) {
        let urlParameters = String(format: "%@/%@", APIManagerClient.sharedInstance.base_url,url) as String
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {   response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                successBlock(true,json)
            case .failure(let error):
                successBlock(false,error as! JSON)
            }
        }
    }
    
    public func GenericClassPost(url: String,params:[String: AnyObject]  ,successBlock: @escaping (_ isBool: Bool, _ response: JSON)-> Void) {
        
        let urlParameters = String(format: "%@/%@", APIManagerClient.sharedInstance.base_url,url) as String
        Alamofire.request(URL(string: urlParameters)!, method: .post, parameters: params, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {   response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                successBlock(true,json)
            case .failure(let error):
                if error is JSON {
                    successBlock(false,error as! JSON)
                }
            }
        }
    }
    
    
    //MARK:- Webapi manager
    public func genericClass(url: URL, param: AnyObject, httpMethod: String, header: Bool? = false, addAddress: String? = "", hbData: Data? = nil, handle :@escaping (_ result: Any, _ success: Bool, _ data: Data?) -> Void) {
        let session             = URLSession.shared
        let request             = NSMutableURLRequest(url:url)
        request.timeoutInterval = 60
        request.cachePolicy     = NSURLRequest.CachePolicy.reloadIgnoringLocalCacheData
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.httpMethod = httpMethod.capitalized
        
        if httpMethod == "POST" {
            if let stringParams = param as? String {
                let postLength = NSString(format:"%lu", stringParams.count) as String
                request.setValue(postLength, forHTTPHeaderField:"Content-Length")
                request.httpBody = stringParams.data(using: String.Encoding.utf8, allowLossyConversion:true)
            } else {
                let theJSONData = try? JSONSerialization.data(
                    withJSONObject: param ,
                    options: JSONSerialization.WritingOptions(rawValue: 0))
                let jsonString          = NSString(data: theJSONData!,encoding: String.Encoding.ascii.rawValue)
                let postLength = NSString(format:"%lu", jsonString!.length) as String
                request.setValue(postLength, forHTTPHeaderField:"Content-Length")
                request.httpBody = jsonString!.data(using: String.Encoding.utf8.rawValue, allowLossyConversion:false)
                if let httpBodyData = hbData {
                    request.httpBody = httpBodyData
                }
            }
        }
        if header ?? false {
            request.allHTTPHeaderFields = self.getHeaderDic()
        }
        
        let dataTask = session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
            if((error) != nil) {
                handle(error!.localizedDescription, false, nil)
            } else {
                do {
                    guard let dataResponse = data else {
                        handle("error", false, nil)
                        return }
                    let json = try JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments)
                    handle(json, true, dataResponse)
                } catch {
                    handle(error.localizedDescription, false, nil)
                }
            }
        }
        dataTask.resume()
    }
}


extension Alamofire.SessionManager{
    @discardableResult
    open func requestWithoutCache(
        _ url: URLConvertible,
        method: HTTPMethod = .post,
        parameters: Parameters? = nil,
        encoding: ParameterEncoding = JSONEncoding.default,
        headers: HTTPHeaders? = nil)// also you can add URLRequest.CachePolicy here as parameter
        -> DataRequest
    {
        do {
            var urlRequest = try URLRequest(url: url, method: method, headers: headers)
            urlRequest.cachePolicy = .reloadIgnoringCacheData // <<== Cache disabled
            let encodedURLRequest = try encoding.encode(urlRequest, with: parameters)
            return request(encodedURLRequest)
        } catch {
            // TODO: find a better way to handle error
            print(error)
            return request(URLRequest(url: URL(string: "http://example.com/wrong_request")!))
        }
    }
}
