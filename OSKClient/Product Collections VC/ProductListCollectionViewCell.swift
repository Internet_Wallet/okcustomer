//
//  ProductListCollectionViewCell.swift
//  VMart
//
//  Created by Shobhit Singhal on 10/18/18.
//  Copyright © 2018 Shobhit. All rights reserved.
//

import UIKit

@objc protocol AddRemoveWishListProtocol: class {
    func addToWishList(cell: ProductListCollectionViewCell?)
    func removeFromWishList(cell: ProductListCollectionViewCell?)
    func addToWishList(id: NSInteger)
    func removeToWishList(id: NSInteger)
}

class ProductListCollectionViewCell: UICollectionViewCell {
    
    var wishListSelected = false
    var productId:Int!
    var productName = ""
    var aPIManager = APIManager()
    weak var delegate: AddRemoveWishListProtocol?
    
    @IBOutlet var mainBackView: UIView!
    @IBOutlet var productImageView: UIImageView!
    
    
    @IBOutlet var gifImageView: UIImageView!{
        didSet{
            gifImageView.isHidden = true
            gifImageView.setGifImage(UIImage(gifName: "new.gif"), manager: SwiftyGifManager(memoryLimit:6))
        }
    }
    @IBOutlet var discountPercentageWidthConstraint: NSLayoutConstraint!
    @IBOutlet var discountPercentageLbl: UILabel!{
        didSet{
            self.discountPercentageLbl.font = UIFont(name: appFont, size: 15.0)
            self.discountPercentageLbl.text = self.discountPercentageLbl.text?.localized
        }
    }
    @IBOutlet var productNameLbl: UILabel!{
        didSet {
            self.productNameLbl.font = UIFont(name: appFont, size: 15.0)
            self.productNameLbl.text = self.productNameLbl.text?.localized
            self.productNameLbl.textColor = UIColor.red
            
        }
    }
    @IBOutlet var oldAmountLbl: UILabel!{
        didSet {
            self.oldAmountLbl.font = UIFont(name: appFont, size: 15.0)
            self.oldAmountLbl.text = self.oldAmountLbl.text?.localized
            
            
        }
    }
    @IBOutlet var discountAmountLbl: UILabel!{
        didSet {
            self.discountAmountLbl.font = UIFont(name: appFont, size: 15.0)
            self.discountAmountLbl.text = self.discountAmountLbl.text?.localized
            self.discountAmountLbl.textColor = UIColor.black
            
        }
    }
    @IBOutlet var amountLbl: UILabel!{
        didSet {
            self.amountLbl.font = UIFont(name: appFont, size: 15.0)
            self.amountLbl.text = self.amountLbl.text?.localized
            self.amountLbl.textColor = UIColor.black
            
        }
    }
    @IBOutlet weak var wishListBtn: UIButton! {
        didSet {
            wishListBtn.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        }
        
    }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //        self.mainBackView.shadowToColleectionView()
    }
    
    func updateCellWithData(productObject: ProductsInfo) {
        
        if productObject.MarkAsNew!{
            self.gifImageView.isHidden = false
        }
        else{
            self.gifImageView.isHidden = true
        }
        
        
        self.productId = productObject.ProductId
        self.productName = productObject.Name as String
        
        productImageView.sd_setImage(with: URL(string: productObject.ImageUrl as String))
        
        //        AppUtility.NKPlaceholderImage(image: UIImage(named: productObject.ImageUrl as String), imageView: self.productImageView, imgUrl: productObject.ImageUrl as String) { (image) in }
        
        
        
        
        //        AppUtility.getData(from: URL(string: productObject.ImageUrl as String)!) { data, response, error in
        //            guard let data = data, error == nil else { return }
        //            print("Download Finished")
        //            DispatchQueue.main.async() {
        //                self.productImageView.image = UIImage(data: data)
        //            }
        //        }
        
        
        productNameLbl.text = productObject.Name as String
        oldAmountLbl.isHidden = true
        discountAmountLbl.isHidden = true
        
        let (Price,_,_) = AppUtility.getPriceOldPriceDiscountPrice(price: productObject.Price ?? "", oldPrice: productObject.OldPrice ?? "", priceWithDiscount: productObject.PriceWithDiscount)
        
        if let productPrice = productObject.Price?.replacingOccurrences(of: "MMK", with: ""){
            
            let attrString = NSMutableAttributedString(string: productPrice + mmkText)
            let nsRange = NSString(string: productPrice + mmkText).range(of: mmkText, options: String.CompareOptions.caseInsensitive)
            attrString.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: mmkFont!, range: nsRange)
            amountLbl.attributedText = attrString
            discountAmountLbl.attributedText = attrString
            
        }
        
        
        
        if Price == "" {
            if productObject.DiscountPercentage != 0 {
                discountPercentageLbl.text = String(format: "%d%@", productObject.DiscountPercentage ?? "", "%")
                self.discountPercentageWidthConstraint.constant = discountPercentageLbl.intrinsicContentSize.width + 10
            } else {
                discountPercentageLbl.text = ""
                self.discountPercentageWidthConstraint.constant = 0
            }
            amountLbl.isHidden = true
            oldAmountLbl.isHidden = false
            discountAmountLbl.isHidden = false
            if productObject.PriceWithDiscount == "" {
                
                if let productPrice2 = productObject.Price?.replacingOccurrences(of: "MMK", with: ""){
                    let attrStr = NSMutableAttributedString(string: productPrice2 + mmkText)
                    let nsRange = NSString(string: productPrice2 + mmkText).range(of: mmkText, options: String.CompareOptions.caseInsensitive)
                    attrStr.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: mmkFont!, range: nsRange)
                    discountAmountLbl.attributedText = attrStr
                }
                
                
                
                
                //                let attrString = NSMutableAttributedString(string: "\(productObject.OldPrice ?? "")" , attributes: [NSAttributedString.Key.strikethroughStyle: NSUnderlineStyle.single.rawValue])
                //                let nsRange2 = NSString(string: "\(productPrice ?? "")" ).range(of: mmkText, options: String.CompareOptions.caseInsensitive)
                //                attrString.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: mmkFont!, range: nsRange2)
                //                self.oldAmountLbl.attributedText = attrString
                
                
                if  let oldAmount = productObject.OldPrice?.replacingOccurrences(of: "MMK", with: ""){
                    let attrString = NSMutableAttributedString(string: oldAmount + mmkText, attributes: [NSAttributedString.Key.strikethroughStyle: NSUnderlineStyle.single.rawValue])
                    let nsRange2 = NSString(string: oldAmount + mmkText).range(of: mmkText, options: String.CompareOptions.caseInsensitive)
                    attrString.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: mmkFont!, range: nsRange2)
                    self.oldAmountLbl.attributedText = attrString
                }
                
                
                
                
                
                
                
                
            } else {
                var productPrice2 = productObject.PriceWithDiscount.replacingOccurrences(of: "MMK", with: "")
                productPrice2 = productPrice2 + mmkText
                let attrStr = NSMutableAttributedString(string: productPrice2)
                let nsRange = NSString(string: productPrice2).range(of: mmkText, options: String.CompareOptions.caseInsensitive)
                attrStr.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: mmkFont!, range: nsRange)
                discountAmountLbl.attributedText = attrStr
                
                
                
                //                let attrString = NSMutableAttributedString(string: "\(productObject.Price ?? "")" , attributes: [NSAttributedString.Key.strikethroughStyle: NSUnderlineStyle.single.rawValue])
                //                let nsRange2 = NSString(string: "\(productPrice ?? "")" ).range(of: mmkText, options: String.CompareOptions.caseInsensitive)
                //                attrString.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: mmkFont!, range: nsRange2)
                //                self.oldAmountLbl.attributedText = attrString
                
                if productObject.OldPrice != nil {
                    var oldAmount = productObject.OldPrice?.replacingOccurrences(of: "MMK", with: "")
                    oldAmount = oldAmount! + mmkText
                    let attrString = NSMutableAttributedString(string: "\(oldAmount ?? "")", attributes: [NSAttributedString.Key.strikethroughStyle: NSUnderlineStyle.single.rawValue])
                    let nsRange2 = NSString(string: oldAmount ?? "").range(of: mmkText, options: String.CompareOptions.caseInsensitive)
                    attrString.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: mmkFont!, range: nsRange2)
                    attrString.addAttributes([NSAttributedString.Key.strikethroughStyle: NSUnderlineStyle.single.rawValue], range: nsRange2)
                    self.oldAmountLbl.attributedText = attrString
                }
                
                
                
            }
        }
        else {
            discountPercentageLbl.text = ""
            self.discountPercentageWidthConstraint.constant = 0
            amountLbl.isHidden = false
            oldAmountLbl.isHidden = true
            discountAmountLbl.isHidden = true
        }
        
        if productObject.DisableWishlistButton == true {
            self.wishListBtn.isHidden = true
        } else {
            self.wishListBtn.isHidden = false
        }
        
        if productObject.iswishList == true {
            wishListSelected = true
            self.wishListBtn.setImage(UIImage(named: "act_wishlist"), for: .normal)
        } else {
            wishListSelected = false
            self.wishListBtn.setImage(UIImage(named: "ic_wishList"), for: .normal)
        }
    }
    
    
    func updateCellWithFeatureProducts(productObject: GetHomePageProducts) {
        
        if productObject.MarkAsNew{
            self.gifImageView.isHidden = false
        }
        else{
            self.gifImageView.isHidden = true
        }
        self.productId = productObject.ProductId
        self.productName = productObject.Name as String
        productImageView.sd_setImage(with: URL(string: productObject.ImageUrl as String))
        
        
        //        AppUtility.NKPlaceholderImage(image: UIImage(named: productObject.ImageUrl as String), imageView: self.productImageView, imgUrl: productObject.ImageUrl as String) { (image) in }
        
        
        //        AppUtility.getData(from: URL(string: productObject.ImageUrl as String)!) { data, response, error in
        //            guard let data = data, error == nil else { return }
        //            print("Download Finished")
        //            DispatchQueue.main.async() {
        //                self.productImageView.image = UIImage(data: data)
        //            }
        //        }
        
        
        productNameLbl.text = productObject.Name as String
        oldAmountLbl.isHidden = true
        discountAmountLbl.isHidden = true
        
        let (Price,_,_) = AppUtility.getPriceOldPriceDiscountPrice(price: productObject.Price, oldPrice: productObject.OldPrice, priceWithDiscount: productObject.PriceWithDiscount)
        
        let mmkText = "MMK"
        let mmkFont = UIFont.init(name: appFont, size: 9)
        var productPrice = productObject.Price.replacingOccurrences(of: "MMK", with: "")
        productPrice = productPrice + mmkText
        let attrString = NSMutableAttributedString(string: productPrice)
        let nsRange = NSString(string: productPrice).range(of: mmkText, options: String.CompareOptions.caseInsensitive)
        attrString.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: mmkFont!, range: nsRange)
        amountLbl.attributedText = attrString
        discountAmountLbl.attributedText = attrString
        
        if Price == "" {
            if productObject.DiscountPercentage != 0 {
                discountPercentageLbl.text = String(format: "%d%@", productObject.DiscountPercentage ?? "", "%")
                self.discountPercentageWidthConstraint.constant = discountPercentageLbl.intrinsicContentSize.width + 10
            } else {
                discountPercentageLbl.text = ""
                self.discountPercentageWidthConstraint.constant = 0
            }
            amountLbl.isHidden = true
            oldAmountLbl.isHidden = false
            discountAmountLbl.isHidden = false
            if productObject.PriceWithDiscount == "" {
                var productPrice2 = productObject.Price.replacingOccurrences(of: "MMK", with: "")
                productPrice2 = productPrice2 + mmkText
                let attrStr = NSMutableAttributedString(string: productPrice2)
                let nsRange = NSString(string: productPrice2).range(of: mmkText, options: String.CompareOptions.caseInsensitive)
                attrStr.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: mmkFont!, range: nsRange)
                discountAmountLbl.attributedText = attrStr
                
                
                
                //                let attrString = NSMutableAttributedString(string: "\(productObject.OldPrice)" , attributes: [NSAttributedString.Key.strikethroughStyle: NSUnderlineStyle.single.rawValue])
                //                let nsRange2 = NSString(string: "\(productObject.OldPrice)" ).range(of: mmkText, options: String.CompareOptions.caseInsensitive)
                //                attrString.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: mmkFont!, range: nsRange2)
                //                self.oldAmountLbl.attributedText = attrString
                
                
                
                var oldAmount = productObject.OldPrice.replacingOccurrences(of: "MMK", with: "")
                oldAmount = oldAmount + mmkText
                let attrString = NSMutableAttributedString(string: "\(oldAmount)", attributes: [NSAttributedString.Key.strikethroughStyle: NSUnderlineStyle.single.rawValue])
                let nsRange2 = NSString(string: oldAmount).range(of: mmkText, options: String.CompareOptions.caseInsensitive)
                attrString.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: mmkFont!, range: nsRange2)
                attrString.addAttributes([NSAttributedString.Key.strikethroughStyle: NSUnderlineStyle.single.rawValue], range: nsRange2)
                self.oldAmountLbl.attributedText = attrString
                
                
                
                
                //                let attrString = NSAttributedString(string: "\(productObject.OldPrice)", attributes: [NSAttributedString.Key.strikethroughStyle: NSUnderlineStyle.single.rawValue])
                //                self.oldAmountLbl.attributedText = attrString
                
            } else {
                var productPrice2 = productObject.PriceWithDiscount.replacingOccurrences(of: "MMK", with: "")
                productPrice2 = productPrice2 + mmkText
                let attrStr = NSMutableAttributedString(string: productPrice2)
                let nsRange = NSString(string: productPrice2).range(of: mmkText, options: String.CompareOptions.caseInsensitive)
                attrStr.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: mmkFont!, range: nsRange)
                discountAmountLbl.attributedText = attrStr
                
                //                let attrString = NSMutableAttributedString(string: "\(productObject.Price)" , attributes: [NSAttributedString.Key.strikethroughStyle: NSUnderlineStyle.single.rawValue])
                //                let nsRange2 = NSString(string: "\(productObject.Price)" ).range(of: mmkText, options: String.CompareOptions.caseInsensitive)
                //                attrString.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: mmkFont!, range: nsRange2)
                //                self.oldAmountLbl.attributedText = attrString
                
                
                var oldAmount = productObject.Price.replacingOccurrences(of: "MMK", with: "")
                oldAmount = oldAmount + mmkText
                let attrString = NSMutableAttributedString(string: "\(oldAmount)", attributes: [NSAttributedString.Key.strikethroughStyle: NSUnderlineStyle.single.rawValue])
                let nsRange2 = NSString(string: oldAmount).range(of: mmkText, options: String.CompareOptions.caseInsensitive)
                attrString.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: mmkFont!, range: nsRange2)
                attrString.addAttributes([NSAttributedString.Key.strikethroughStyle: NSUnderlineStyle.single.rawValue], range: nsRange2)
                self.oldAmountLbl.attributedText = attrString
                
                
                
                //                let attrString = NSAttributedString(string: "\(productObject.Price)", attributes: [NSAttributedString.Key.strikethroughStyle: NSUnderlineStyle.single.rawValue])
                //                self.oldAmountLbl.attributedText = attrString
            }
        }
        else {
            discountPercentageLbl.text = ""
            self.discountPercentageWidthConstraint.constant = 0
            amountLbl.isHidden = false
            oldAmountLbl.isHidden = true
            discountAmountLbl.isHidden = true
        }
        
        if productObject.DisableWishlistButton == true {
            self.wishListBtn.isHidden = true
        } else {
            self.wishListBtn.isHidden = false
        }
        
        if productObject.isWishList == true {
            wishListSelected = true
            self.wishListBtn.setImage(UIImage(named: "act_wishlist"), for: .normal)
        } else {
            wishListSelected = false
            self.wishListBtn.setImage(UIImage(named: "ic_wishList"), for: .normal)
        }
    }
    
    func updateCellWithRelatedProductsData(productObject: ProductsInfo) {
        
    }
    
    private func addtoWishList() {
        let dictionaryArray = NSMutableArray()
        UIView.animate(withDuration: 0.4, animations: {
            self.wishListBtn.transform = CGAffineTransform(scaleX: 1.4, y: 1.4) }, completion: { (finish: Bool) in
                UIView.animate(withDuration: 0.4, animations: {
                    self.wishListBtn.transform = CGAffineTransform.identity
                }, completion: ({finished in
                    if (finished) {
                        self.wishListSelected = true
                        self.wishListBtn.setImage(UIImage(named: "act_wishlist"), for: .normal)
                        self.delegate?.addToWishList(cell: self)
                    }
                }))
        })
        self.aPIManager.loadAddProductToCart(2, productId: productId, parameters: dictionaryArray, onSuccess:{ [weak self] priceValue in
            }, onError: { message in
                
        })
    }
    
    private func removeFromWishList() {
        let dicArray = NSMutableArray()
        UIView.animate(withDuration: 0.4, animations: {
            self.wishListBtn.transform = CGAffineTransform(scaleX: 1.4, y: 1.4) }, completion: { (finish: Bool) in
                UIView.animate(withDuration: 0.4, animations: {
                    self.wishListBtn.transform = CGAffineTransform.identity
                }, completion: ({finished in
                    if (finished) {
                        self.wishListSelected = false
                        self.wishListBtn.setImage(UIImage(named: "ic_wishList"), for: .normal)
                        self.delegate?.removeFromWishList(cell: self)
                    }
                }))
        })
        self.aPIManager.removeProductFromWishList(2, productId: productId, parameters: dicArray, onSuccess: { [weak self] in
            
            }, onError: { message in
                
        })
    }
    
    @IBAction func wishListAction(_ sender : UIButton) {
        if !wishListSelected {
            self.addtoWishList()
        } else {
            self.removeFromWishList()
        }
    }
}
