//
//  ProductCollections.swift
//  VMart
//
//  Created by Shobhit Singhal on 10/16/18.
//  Copyright © 2018 Shobhit. All rights reserved.
//

import UIKit
import KRPullLoader

class ProductCollections: MartBaseViewController {
    
    
    //var tusharObj =  [NSInteger]()
    var aPIManager                  = APIManager()
    var apiManagerClient            : APIManagerClient!
    var subCategoriesArray          : [FeaturedProductsAndCategory] = []
    var PreviouSubCategoriesArray   : [FeaturedProductsAndCategory] = []
    var categoryDetailsInfo         = CategoryDetailsInfo()
    var homePageProductArray        = [GetHomePageProducts]()
    var filterProductArray          = [ProductsInfo]()
    var recentViewProductArray        = [GetHomePageProducts]()
    var pagingIndex                 = NSInteger ()
    var manufacturerFlag            = false
    var categoryId                  : NSInteger!
    var categoryName: String = ""
    var featuredProductsFlag = false
    var sortValue                   = -1
    var checkMark                   = -1
    var checked = [Bool]()
    var productId:Int!
    var notFilteredItemsCategories = [String]()
    var notFilteredItemsSubCategories = [[String]]()
    var notFilteredProductId = [NSInteger]()
    var notFilteredSubProductId = [[NSInteger]]()
    var finalFilterProductsId = [NSInteger]()
    var checkedRightTable = [[Bool]]()
    var selectedFilterProductsId = [NSInteger]()
    var minMaxAmountAfterApply = true
    var minAmount = ""
    var maxAmount = ""
    var minRange = ""
    var maxRange = ""
    var productName = ""
    var isListShow = false
    var visibleIndexPath:IndexPath!
    var CategoaryOrNoCategoary: Bool?
    
    @IBOutlet weak var headerStack: UIStackView!{
        didSet{
            self.headerStack.isHidden = false
        }
    }
    
    @IBOutlet weak var ProductCollectionView: UICollectionView!
    @IBOutlet weak var listGridImageView: UIImageView!{
        didSet{
            listGridImageView.isHidden = false
        }
    }
    @IBOutlet var recentHeaderLabel: UILabel! {
        didSet {
            self.recentHeaderLabel.font = UIFont(name: appFont, size: 15.0)
            self.recentHeaderLabel.text = self.recentHeaderLabel.text?.localized
            self.recentHeaderLabel.isHidden = true
            
        }
    }
    @IBOutlet weak var sortFilterView: UIView!
    @IBOutlet weak var sortBGView: UIView!
    @IBOutlet weak var filterBGView: UIView!
    @IBOutlet var cartBarButton: UIBarButtonItem!
    @IBOutlet var sortLabel: UILabel! {
        didSet {
            self.sortLabel.font = UIFont(name: appFont, size: 15.0)
            self.sortLabel.text = self.sortLabel.text?.localized
            
        }
    }
    @IBOutlet var filterLabel: UILabel!{
        didSet {
            self.filterLabel.font = UIFont(name: appFont, size: 15.0)
            self.filterLabel.text = self.filterLabel.text?.localized
            
        }
    }
    
    @IBOutlet var sortViewHeightConstraint: NSLayoutConstraint!
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //        self.cartBarButton = UIBarButtonItem.init(badge: UserDefaults.standard.value(forKey: "badge") as? String, title: "", target: self, action: #selector(self.cartAction(_:)))
        self.cartBarButton = UIBarButtonItem.init(badge: UserDefaults.standard.string(forKey: "badge") ?? "", title: "", target: self, action: #selector(self.cartAction(_:)))
        self.navigationItem.rightBarButtonItem  = self.cartBarButton
        self.configureIntialData()
    }
    
    func configureIntialData(){
        
        self.navigationItem.title = categoryName.localized
        self.navigationItem.setMarqueLabelInNavigation(TextStr: self.navigationItem.title!, count: self.navigationItem.title?.count ?? 40)
        ProductCollectionView.delegate = self
        ProductCollectionView.dataSource = self
        self.ProductCollectionView.register(UINib(nibName: "ProductListCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ProductListCell")
        self.apiManagerClient = APIManagerClient.sharedInstance
        sortFilterView.layer.shadowOpacity = 0.20
        sortFilterView.layer.shadowOffset = CGSize(width: 0, height: 3)
        sortFilterView.layer.shadowRadius = 3
        sortFilterView.layer.shadowColor = UIColor.black.cgColor
        sortFilterView.layer.masksToBounds = false
        NotificationCenter.default.addObserver(self, selector:#selector(ProductCollections.reloadCategoryTableView(_:)), name:NSNotification.Name(rawValue: "com.notification.reloadCategoryTableView"), object: nil)
        if self.categoryId != nil{
            if AppUtility.isConnectedToNetwork() {
                self.categoryDetailsInfo = self.apiManagerClient.getProductCategoryDetailsInfo()
                if self.categoryDetailsInfo.productsInfo.count == 0 {
                    self.subCategoriesArray.removeAll()
                    self.loadCategoryDetails()
                }
            } else {
                AppUtility.showInternetErrorToast(AppConstants.InternetErrorText.message, view: self.view)
            }
        } else {
            if AppUtility.isConnectedToNetwork() {
                if self.categoryName == "Recently viewed"{
                    self.headerStack.isHidden = true
                    self.listGridImageView.isHidden = true
                    self.recentHeaderLabel.isHidden = false
                    self.loadRecentViewProduct()
                }
                else{
                    self.loadHomePageProducts()
                }
            } else {
                AppUtility.showInternetErrorToast(AppConstants.InternetErrorText.message, view: self.view)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let loadMoreView = KRPullLoadView()
        loadMoreView.delegate = self
        self.ProductCollectionView.addPullLoadableView(loadMoreView, type: .loadMore)
        self.ProductCollectionView.contentInset.bottom = 50
        UserDefaults.standard.set(0.0, forKey: "min")
        UserDefaults.standard.set(0.0, forKey: "max")
        UserDefaults.standard.synchronize()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    @IBAction func cartAction(_ sender: UIBarButtonItem) {
        let viewController = UIStoryboard(name: "Cart", bundle: Bundle.main).instantiateViewController(withIdentifier: "CartRoot")
        self.present(viewController, animated: true, completion: nil)
    }
    
    
    @IBAction func searchAction(_ sender: UIBarButtonItem) {
        
        let viewController = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SearchProducts")
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    func loadHomePageProducts() {
        self.aPIManager.loadHomePageProducts( onSuccess:{ [weak self] getHomeProducts in
            self?.homePageProductArray = getHomeProducts
            self?.ProductCollectionView.reloadData()
            if let viewLoc = self?.view {
                AppUtility.hideLoading(viewLoc)
            }
            }, onError: { [weak self] message in
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                }
        })
    }
    
    
    func loadRecentViewProduct() {
        AppUtility.showLoading(self.view)
        self.aPIManager.loadHomePageRecentlyViewedProduct(onSuccess:{ [weak self] getHomeProducts in
            self?.recentViewProductArray = getHomeProducts
            if (self?.recentViewProductArray.count)! > 0{
                self?.ProductCollectionView.reloadData()
            }
            else{
                if let viewLoc = self?.view {
                    AppUtility.showToastlocal(message: "Data Not Found", view: viewLoc)
                }
            }
            if let viewLoc = self?.view {
                AppUtility.hideLoading(viewLoc)
            }
            },  onError: { [weak self] message in
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                }
        })
    }
    
    
    
    func loadCategoryDetails() {
        AppUtility.showLoading(self.view)
        pagingIndex             = 1
        var paramDic            = [String:AnyObject]()
        paramDic["pagenumber"]  = 1 as AnyObject
        self.aPIManager.loadCategoryDetails(self.manufacturerFlag, categoryId: self.categoryId, params: paramDic, onSuccess:{ [weak self] getProducts in
            self?.categoryDetailsInfo = getProducts
            self?.apiManagerClient.categoryDetailsInfo = getProducts
            self?.ProductCollectionView.reloadData()
            self?.filterCategories()
            if self?.manufacturerFlag == false{
                self?.loadSubCategories()
            } else {
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                }
            }
            }, onError: { [weak self] message in
                if let viewLoc = self?.view {
                    //                    AppUtility.hideLoading(viewLoc)
                }
        })
    }
    
    func loadSubCategories() {
        self.aPIManager.loadFeaturedProductsAndCategory(self.categoryId, onSuccess:{ [weak self] getSubCategories in
            self?.subCategoriesArray = getSubCategories
            if self?.subCategoriesArray.count == 0 {
                
                // self.subCategoryDropDownButton.isHidden = true
            } else {
                // self.subCategoryDropDownButton.isHidden = false
            }
            //self.subCategoryTableView.reloadData()
            if let viewLoc = self?.view {
                AppUtility.hideLoading(viewLoc)
            }
            }, onError: { [weak self] message in
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                }
        })
    }
    
    func filterCategories() {
        for _ in self.apiManagerClient.categoryDetailsInfo.PriceRange {
            if !notFilteredItemsCategories.contains("Price") {
                notFilteredItemsCategories.append("Price")
                notFilteredProductId.append(1000)
            }
        }
        
        for notFilteredItem in self.apiManagerClient.categoryDetailsInfo.notFilteredItems {
            let spAttributeName = notFilteredItem.SpecificationAttributeName!
            let productId = notFilteredItem.ProductId
            if !notFilteredItemsCategories.contains(spAttributeName as String) {
                notFilteredItemsCategories.append(spAttributeName as String)
                notFilteredProductId.append(productId)
            }
        }
        
        var notFilteredItemsSubCategoriesArray = [[String]]()
        var notFilteredSubProductIdArray = [[NSInteger]]()
        for categoryName in notFilteredItemsCategories {
            var arr = [String]()
            var idArr = [NSInteger]()
            for i in 0  ..< self.apiManagerClient.categoryDetailsInfo.notFilteredItems.count {
                let spAttributeName:NSString = categoryName as NSString
                if spAttributeName == self.apiManagerClient.categoryDetailsInfo.notFilteredItems[i].SpecificationAttributeName {
                    if let subCategoryName = self.apiManagerClient.categoryDetailsInfo.notFilteredItems[i].SpecificationAttributeOptionName as String? {
                        arr.append(subCategoryName)
                    }
                    let productId = self.apiManagerClient.categoryDetailsInfo.notFilteredItems[i].ProductId
                    idArr.append(productId)
                }
            }
            notFilteredItemsSubCategoriesArray.append(arr)
            notFilteredSubProductIdArray.append(idArr)
        }
        for (i,subCategoryNameArr) in notFilteredItemsSubCategoriesArray.enumerated() {
            var arr = [String]()
            var idArr = [NSInteger]()
            for (j,name) in subCategoryNameArr.enumerated() {
                if !arr.contains(name) {
                    arr.append(name)
                    idArr.append(notFilteredSubProductIdArray[i][j])
                }
            }
            notFilteredItemsSubCategories.append(arr)
            notFilteredSubProductId.append(idArr)
        }
        
    }
    
    @objc func reloadCategoryTableView(_ notification: Notification) {
        pagingIndex             = 1
        var paramDic            = [String:AnyObject]()
        paramDic["pagenumber"] = self.pagingIndex as AnyObject
        if let minPrice = self.apiManagerClient.categoryDetailsInfo.fromPrice, let maxPrice = self.apiManagerClient.categoryDetailsInfo.toPrice {
            paramDic["price"]   = String(format: "%f-%f", minPrice, maxPrice) as AnyObject
        }
        if (self.apiManagerClient.categoryDetailsInfo.alreadyFilteredItems.count > 0) {
            var specsString     = "\(self.apiManagerClient.categoryDetailsInfo.alreadyFilteredItems[0].FilterId ?? 0)"
            for i in 1 ..< self.apiManagerClient.categoryDetailsInfo.alreadyFilteredItems.count{
                specsString     += ",\(self.apiManagerClient.categoryDetailsInfo.alreadyFilteredItems[i].FilterId ?? 0)"
            }
            paramDic["specs"]   = specsString as AnyObject
        }
        
        if (self.sortValue > -1) {
            paramDic["orderby"] = sortValue as AnyObject
        }
        
        self.aPIManager.loadCategoryDetails(self.manufacturerFlag, categoryId: self.categoryId, params: paramDic, onSuccess:{ [weak self] getProducts in
            self?.categoryDetailsInfo = getProducts
            self?.apiManagerClient.categoryDetailsInfo = getProducts
            
            //            if self?.tusharObj.count ?? 0 > 0 {
            //                for object in self!.apiManagerClient.categoryDetailsInfo.productsInfo {
            //                    if self!.tusharObj.contains(object.ProductId) {
            //                        self!.filterProductArray.append(object)
            //                    }
            //                }
            //            }
            
            
            self?.ProductCollectionView.reloadData()
            if let viewLoc = self?.view {
                AppUtility.hideLoading(viewLoc)
            }
            }, onError: { [weak self] message in
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                }
        })
    }
    
    @IBAction func backAction(_ sender: UIBarButtonItem) {
        apiManagerClient.categoryDetailsInfo = CategoryDetailsInfo()
        self.dismiss(animated: false, completion: nil)
    }
    
    
    
    
    @IBAction func sortByAction(_ sender: UIButton) {
        self.performSegue(withIdentifier: "ProuctListToSortSegue", sender: self)
    }
    
    @IBAction func filterAction(_ sender: UIButton) {
        self.performSegue(withIdentifier: "ProductListToFilterSegue", sender: self)
    }
    
    @IBAction func listTypeAction(_ sender: UIButton) {
        if isListShow {
            isListShow = false
            listGridImageView.image = UIImage(named: "listGallery")
            ProductCollectionView.reloadData()
        }
        else {
            isListShow = true
            listGridImageView.image = UIImage(named: "grid")
            ProductCollectionView.reloadData()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ProuctListToSortSegue" {
            if let vc = segue.destination as? SortBy {
                vc.SDelegate = self
                vc.checked = self.checked
                vc.categoryDetailsInfo = categoryDetailsInfo
            }
        } else if segue.identifier == "ProductListToFilterSegue" {
            if let vc = segue.destination as? UINavigationController {
                if let rootViewController = vc.viewControllers.first as? Filter {
                    rootViewController.FDelegate = self
                    rootViewController.notFilteredItemsCategories = self.notFilteredItemsCategories
                    rootViewController.notFilteredItemsSubCategories = self.notFilteredItemsSubCategories
                    rootViewController.allFilterProductsId = self.notFilteredSubProductId
                    rootViewController.homePageProductArray = self.homePageProductArray
                    rootViewController.checkedRightTable = self.checkedRightTable
                    rootViewController.selectedFilterProductsId = self.selectedFilterProductsId
                    rootViewController.notFilteredItems = categoryDetailsInfo.notFilteredItems
                    rootViewController.productsInfo = categoryDetailsInfo.productsInfo
                    if minMaxAmountAfterApply {
                        rootViewController.minAmt = minAmount
                        rootViewController.maxAmt = maxAmount
                        rootViewController.min = minRange
                        rootViewController.max = maxRange
                        // minMaxAmountAfterApply = false
                    }else {
                        rootViewController.minAmt = ""
                        rootViewController.maxAmt = ""
                    }
                    // Make Sort check array empty when Filter button pressed
                    //self.checked = []
                    
                }
            }
        } else if segue.identifier == "ProductCollectionsToProductDetailsSegue" {
            if let vc = segue.destination as? ProductDetails {
                vc.delegate = self
                vc.productId = self.productId
                vc.productName = self.productName
            }
        } else if segue.identifier == "CustomSubCategoarySegue" {
            let vc = segue.destination as! CustomMenuforSubCategoary
            vc.subCategoriesArray = self.PreviouSubCategoriesArray
            vc.CMSubCatDelegate = self
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "com.notification.reloadCategoryTableView"), object: nil)
    }
    
    @IBAction func swipeRight(_ sender: UISwipeGestureRecognizer) {
        if sender.direction == UISwipeGestureRecognizer.Direction.right {
            
            if self.PreviouSubCategoriesArray.count != 0{
                
                DispatchQueue.main.async {
                    
                    let controller:CustomMenuforSubCategoary = (self.storyboard!.instantiateViewController(withIdentifier: "CustomMenuforSubCategoary") as? CustomMenuforSubCategoary)!
                    
                    controller.view.animateTo(frame: CGRect(x: 0, y: controller.view.frame.origin.y, width: controller.view.frame.width, height: controller.view.frame.height), withDuration: 0.7)
                    
                    controller.CMSubCatDelegate = self
                    controller.subCategoriesArray=self.PreviouSubCategoriesArray // If you want to pass value
                    controller.SubCategoryhideAutomatically()
                    controller.view.frame = self.view.bounds;
                    controller.willMove(toParent: self)
                    self.view.addSubview(controller.view)
                    self.addChild(controller)
                    controller.didMove(toParent: self)
                    
                    
                }
            }
        }
    }
}

extension ProductCollections: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.categoryName == "Recently viewed"{
            return recentViewProductArray.count
        }
        if featuredProductsFlag {
            return homePageProductArray.count
        }
        if filterProductArray.count > 0 {
            return filterProductArray.count
        }
        return self.categoryDetailsInfo.productsInfo.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = ProductCollectionView.dequeueReusableCell(withReuseIdentifier: "ProductListCell", for: indexPath) as! ProductListCollectionViewCell
        cell.roundCell(cell)
        cell.delegate = self
        
        if self.categoryName == "Recently viewed"{
            let object = self.recentViewProductArray[indexPath.row]
            cell.updateCellWithFeatureProducts(productObject: object)
            return cell
        }
        
        
        
        if featuredProductsFlag {
            let object = self.homePageProductArray[indexPath.row]
            cell.updateCellWithFeatureProducts(productObject: object)
            return cell
        }
        if filterProductArray.count > 0 {
            let object = self.filterProductArray[indexPath.row]
            cell.updateCellWithData(productObject: object)
            return cell
        }
        let object = self.categoryDetailsInfo.productsInfo[indexPath.row]
        cell.updateCellWithData(productObject: object)
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if isListShow {
            return CGSize(width: collectionView.bounds.size.width, height: collectionView.bounds.size.width/2 + 60)
        }
        return CGSize(width: collectionView.bounds.size.width/2 - 2, height: collectionView.bounds.size.width/2 + 30)
    }
    
    // MARK: UICollectionViewDelegate
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        if self.categoryName == "Recently viewed"{
            if recentViewProductArray.count > 0 {
                self.productId = self.recentViewProductArray[indexPath.item].Id
                self.productName = self.recentViewProductArray[indexPath.item].Name as String
                self.performSegue(withIdentifier: "ProductCollectionsToProductDetailsSegue", sender: self)
                return
            }
        }
        
        
        if featuredProductsFlag {
            if self.homePageProductArray[indexPath.item].Id != 0 {
                self.productId = self.homePageProductArray[indexPath.item].Id
                self.productName = self.homePageProductArray[indexPath.item].Name
                self.performSegue(withIdentifier: "ProductCollectionsToProductDetailsSegue", sender: self)
                return
            }
        }
        
        if filterProductArray.count > 0 {
            self.productId = self.filterProductArray[indexPath.item].id
            self.productName = self.filterProductArray[indexPath.item].Name as String
            self.performSegue(withIdentifier: "ProductCollectionsToProductDetailsSegue", sender: self)
            return
        }
        
        if self.categoryDetailsInfo.productsInfo[indexPath.item].id != 0 {
            //            self.productDetailsInfo?.productReviewOverviewModel?.RatingSum ?? ""
            self.productId = self.categoryDetailsInfo.productsInfo[indexPath.item].id
            self.productName = self.categoryDetailsInfo.productsInfo[indexPath.item].Name as String
            self.performSegue(withIdentifier: "ProductCollectionsToProductDetailsSegue", sender: self)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        var visibleRect = CGRect()
        visibleRect.origin = ProductCollectionView.contentOffset
        visibleRect.size = ProductCollectionView.bounds.size
        let visiblePoint = CGPoint(x: CGFloat(visibleRect.minX), y: CGFloat(visibleRect.minY))
        let visibleIndexPath: IndexPath? = ProductCollectionView.indexPathForItem(at: visiblePoint)
        if visibleIndexPath != nil {
            self.visibleIndexPath = visibleIndexPath
            print("Visible cell's index is : \(String(describing: self.visibleIndexPath))!")
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        var visibleRect = CGRect()
        visibleRect.origin = ProductCollectionView.contentOffset
        visibleRect.size = ProductCollectionView.bounds.size
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        guard let indexPath = ProductCollectionView.indexPathForItem(at: visiblePoint) else { return }
        println_debug(indexPath)
        
        if self.pagingIndex < categoryDetailsInfo.TotalPages {
            var paramDic = [String:AnyObject]()
            self.pagingIndex += 1
            paramDic["pagenumber"] = self.pagingIndex as AnyObject
            
            if let minPrice = self.apiManagerClient.categoryDetailsInfo.fromPrice, let maxPrice = self.apiManagerClient.categoryDetailsInfo.toPrice{
                paramDic["price"] = String(format: "%f-%f", minPrice, maxPrice) as AnyObject
            }
            if (self.apiManagerClient.categoryDetailsInfo.alreadyFilteredItems.count > 0){
                var specsString = "\(String(describing: self.apiManagerClient.categoryDetailsInfo.alreadyFilteredItems[0].FilterId))"
                for i in 1 ..< self.apiManagerClient.categoryDetailsInfo.alreadyFilteredItems.count{
                    specsString += ",\(String(describing: self.apiManagerClient.categoryDetailsInfo.alreadyFilteredItems[i].FilterId))"
                }
                paramDic["specs"] = specsString as AnyObject
            }
            if(sortValue > -1){
                paramDic["orderby"] = sortValue as AnyObject
            }
            self.aPIManager.loadCategoryDetails(self.manufacturerFlag, categoryId: self.categoryId, params: paramDic, onSuccess:{ [weak self] getProducts in
                self?.categoryDetailsInfo.productsInfo.append(contentsOf: getProducts.productsInfo)
                if let prodtuctInfoArr = self?.categoryDetailsInfo.productsInfo {
                    self?.apiManagerClient.categoryDetailsInfo.productsInfo = prodtuctInfoArr
                }
                self?.ProductCollectionView.reloadData()
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                }
                }, onError: { [weak self] message in
                    if let viewLoc = self?.view {
                        AppUtility.hideLoading(viewLoc)
                    }
            })
        }
    }
}

extension ProductCollections: SortByDelegate {
    
    func SortByTestMethod(sortByTableView: UITableView, indexPath: IndexPath, sortingType: String) {
        sortByTableView.reloadData()
        switch sortingType {
        case "Position".localized:
            if filterProductArray.count > 0 {
                filterProductArray = filterProductArray.sorted(by: { $0.DisplayOrder < $1.DisplayOrder})
            }else {
                self.categoryDetailsInfo.productsInfo = self.categoryDetailsInfo.productsInfo.sorted(by: { $0.DisplayOrder < $1.DisplayOrder })
            }
            break
        case "Name : A to Z".localized:
            if filterProductArray.count > 0 {
                filterProductArray = filterProductArray.sorted(by: { $0.Name.localizedLowercase < $1.Name.localizedLowercase })
            }else {
                self.categoryDetailsInfo.productsInfo = self.categoryDetailsInfo.productsInfo.sorted(by: { $0.Name.localizedLowercase < $1.Name.localizedLowercase })
            }
            break
        case "Name : Z to A".localized:
            if filterProductArray.count > 0 {
                filterProductArray = filterProductArray.sorted(by: { $0.Name.localizedLowercase > $1.Name.localizedLowercase })
            }else {
                self.categoryDetailsInfo.productsInfo = self.categoryDetailsInfo.productsInfo.sorted(by: { $0.Name.localizedLowercase > $1.Name.localizedLowercase })
            }
            break
        case "Price : Low to High".localized:
            if filterProductArray.count > 0 {
                filterProductArray = filterProductArray.sorted(by: { $0.PriceValue < $1.PriceValue })
            }else {
                self.categoryDetailsInfo.productsInfo =  self.categoryDetailsInfo.productsInfo.sorted(by: { $0.PriceValue < $1.PriceValue })
            }
            break
        case "Price : High to Low".localized:
            if filterProductArray.count > 0 {
                filterProductArray = filterProductArray.sorted(by: { $0.PriceValue > $1.PriceValue })
            }else {
                self.categoryDetailsInfo.productsInfo =  self.categoryDetailsInfo.productsInfo.sorted(by: { $0.PriceValue > $1.PriceValue })
            }
            break
        case "Created on".localized:
            if filterProductArray.count > 0 {
                filterProductArray = filterProductArray.sorted(by: { $0.CreatedOn > $1.CreatedOn })
            }else {
                self.categoryDetailsInfo.productsInfo =  self.categoryDetailsInfo.productsInfo.sorted(by: { $0.CreatedOn > $1.CreatedOn })
            }
            break
        default:
            break
        }
        ProductCollectionView.reloadData()
    }
    
    func sortedCheckMarkArray(checked: [Bool]) {
        self.checked = checked
        if self.checked.count > 0 {
            self.sortBGView.backgroundColor = UIColor.colorWithRedValue(redValue: 230, greenValue: 230, blueValue: 230, alpha: 1)
        } else {
            self.sortBGView.backgroundColor = UIColor.white
        }
    }
    
}

extension ProductCollections: FilterDelegate {
    func filterProducts(finalFilterProductsId: [NSInteger], selectedFilterProductsId: [NSInteger], checkedRightTable: [[Bool]], isPriceFilter: Bool) {
        if !isPriceFilter {
            filterProductArray = []
        }
        //self.tusharObj = finalFilterProductsId
        self.selectedFilterProductsId = selectedFilterProductsId
        self.checkedRightTable = checkedRightTable
        for object in self.categoryDetailsInfo.productsInfo {
            if finalFilterProductsId.contains(object.ProductId) {
                filterProductArray.append(object)
            }
        }
        //self.sortBGView.backgroundColor = UIColor.white
        ProductCollectionView.reloadData()
        //Filter BG Update-Gauri
        if filterProductArray.count > 0 {
            self.filterBGView.backgroundColor = UIColor.colorWithRedValue(redValue: 230, greenValue: 230, blueValue: 230, alpha: 1)
        } else {
            //self.filterBGView.backgroundColor = UIColor.white
            self.filterBGView.backgroundColor = UIColor.colorWithRedValue(redValue: 230, greenValue: 230, blueValue: 230, alpha: 1)
        }
    }
    
    func filterProducts(min: String, max: String, minSliderValue: Double, maxSliderValue: Double) {
        minAmount = String(minSliderValue)
        maxAmount = String(maxSliderValue)
        minRange = min
        maxRange = max
        minMaxAmountAfterApply = true
        filterProductArray = []
        let intMin = Float(min) ?? 0
        let intMax = Float(max) ?? 0
        filterProductArray = self.categoryDetailsInfo.productsInfo.filter({ $0.PriceValue <= intMax && $0.PriceValue >= intMin })
        //self.sortBGView.backgroundColor = UIColor.white
        ProductCollectionView.reloadData()
        //Filter BG Update-Gauri
        if filterProductArray.count > 0 {
            self.filterBGView.backgroundColor = UIColor.colorWithRedValue(redValue: 230, greenValue: 230, blueValue: 230, alpha: 1)
        } else {
            //self.filterBGView.backgroundColor = UIColor.white
            self.filterBGView.backgroundColor = UIColor.colorWithRedValue(redValue: 230, greenValue: 230, blueValue: 230, alpha: 1)
        }
    }
    
    func resetAllData() {
        filterProductArray = []
        ProductCollectionView.reloadData()
        self.sortBGView.backgroundColor = UIColor.white
        self.filterBGView.backgroundColor = UIColor.white
    }
    
    func applyActionCall(isReset: Bool) {
        if isReset {
            self.filterBGView.backgroundColor = UIColor.white
        } else {
            
            if filterProductArray.count > 0 {
                self.filterBGView.backgroundColor = UIColor.colorWithRedValue(redValue: 230, greenValue: 230, blueValue: 230, alpha: 1)
            } else {
                self.filterBGView.backgroundColor = UIColor.white
            }
        }
    }
}

extension ProductCollections : KRPullLoadViewDelegate {
    
    func pullLoadView(_ pullLoadView: KRPullLoadView, didChangeState state: KRPullLoaderState, viewType type: KRPullLoaderType) {
        if type == .loadMore {
            switch state {
            case let .loading(completionHandler):
                if self.pagingIndex < self.categoryDetailsInfo.TotalPages {
                    var paramDic = [String:AnyObject]()
                    self.pagingIndex += 1
                    paramDic["pagenumber"] = self.pagingIndex as AnyObject
                    
//                                        if let minPrice = self.apiManagerClient.categoryDetailsInfo.fromPrice, let maxPrice = self.apiManagerClient.categoryDetailsInfo.toPrice{
//                                            paramDic["price"] = String(format: "%f-%f", minPrice, maxPrice) as AnyObject
//                                        }
                    if (self.apiManagerClient.categoryDetailsInfo.alreadyFilteredItems.count > 0){
                        var specsString = "\(String(describing: self.apiManagerClient.categoryDetailsInfo.alreadyFilteredItems[0].FilterId))"
                        for i in 1 ..< self.apiManagerClient.categoryDetailsInfo.alreadyFilteredItems.count{
                            specsString += ",\(String(describing: self.apiManagerClient.categoryDetailsInfo.alreadyFilteredItems[i].FilterId))"
                        }
                        paramDic["specs"] = specsString as AnyObject
                    }
                    if(sortValue > -1){
                        paramDic["orderby"] = sortValue as AnyObject
                    }
                    self.aPIManager.loadCategoryDetails(self.manufacturerFlag, categoryId: self.categoryId, params: paramDic, onSuccess:{ [weak self] getProducts in
                        self?.categoryDetailsInfo.productsInfo.append(contentsOf: getProducts.productsInfo)
                        //self!.pagingIndex += 1
                        if let prodtuctInfoArr = self?.categoryDetailsInfo.productsInfo {
                            self?.apiManagerClient.categoryDetailsInfo.productsInfo = prodtuctInfoArr
                        }
                        self?.ProductCollectionView.reloadData()
                        if let viewLoc = self?.view {
                            AppUtility.hideLoading(viewLoc)
                        }
                        }, onError: { [weak self] message in
                            if let viewLoc = self?.view {
                                AppUtility.hideLoading(viewLoc)
                            }
                    })
                }
                else {
                    AppUtility.showToastCustomBlackInView(message: "No More Products!".localized , controller: self)
                }
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+1) {
                    completionHandler()
                    //                    self.sortBGView.backgroundColor = UIColor.white
                    //                    self.filterBGView.backgroundColor = UIColor.white;
                    self.ProductCollectionView.reloadData()
                }
            default:
                break
            }
            return
        }
        
        switch state {
        case .none:
            pullLoadView.messageLabel.text = ""
            
        case let .pulling(offset, threshould):
            if offset.y > threshould {
                pullLoadView.messageLabel.text = "Pull more. offset: \(Int(offset.y)), threshould: \(Int(threshould)))"
            } else {
                pullLoadView.messageLabel.text = "Release to refresh. offset: \(Int(offset.y)), threshould: \(Int(threshould)))"
            }
            
        case let .loading(completionHandler):
            pullLoadView.messageLabel.text = "Updating..."
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+1) {
                completionHandler()
                self.sortBGView.backgroundColor = UIColor.white
                self.filterBGView.backgroundColor = UIColor.white
                self.ProductCollectionView.reloadData()
            }
        }
    }
}

extension ProductCollections: CustomMenuforSubCategoaryDelegate{
    func TapSubcategoary(indexPath: IndexPath, CategoaryId: NSInteger, CatgoaryName: String) {
        self.navigationItem.title = CatgoaryName.localized
        self.navigationItem.setMarqueLabelInNavigation(TextStr: self.navigationItem.title!, count: self.navigationItem.title?.count ?? 40)
        self.categoryId = CategoaryId
        if self.categoryId != nil{
            if AppUtility.isConnectedToNetwork() {
                self.subCategoriesArray.removeAll()
                self.loadCategoryDetails()
            } else {
                AppUtility.showInternetErrorToast(AppConstants.InternetErrorText.message, view: self.view)
            }
        } else {
            self.loadHomePageProducts()
        }
    }
    
    func TapSubcategoary(indexPath: IndexPath, CategoaryId: NSInteger) {
        println_debug(indexPath)
        println_debug(CategoaryId)
    }
    
    func SubTapSubcategoary(indexPath: IndexPath) {
        println_debug(indexPath)
    }
    
    func backAction(_ sender: UIButton) {
        println_debug("back Pressed")
        self.dismiss(animated: true, completion: nil)
    }
}

extension ProductCollections: AddRemoveWishListProtocol {
    func addToWishList(cell: ProductListCollectionViewCell?) {
        if let index = self.ProductCollectionView.indexPath(for: cell!) {
            if self.categoryName == "Recently viewed"{
                let model = self.recentViewProductArray[index.row]
                model.isWishList = true
                return
            }
            if featuredProductsFlag {
                let model = self.homePageProductArray[index.row]
                model.isWishList = true
            } else if filterProductArray.count > 0 {
                let model = self.filterProductArray[index.row]
                model.iswishList = true
            } else {
                let model = self.categoryDetailsInfo.productsInfo[index.row]
                model.iswishList = true
            }
        }
    }
    
    func removeFromWishList(cell: ProductListCollectionViewCell?) {
        if let index = self.ProductCollectionView.indexPath(for: cell!) {
            if self.categoryName == "Recently viewed"{
                let model = self.recentViewProductArray[index.row]
                model.isWishList = false
                return
            }
            if featuredProductsFlag {
                let model = self.homePageProductArray[index.row]
                model.isWishList = false
            } else if filterProductArray.count > 0 {
                let model = self.filterProductArray[index.row]
                model.iswishList = false
            } else {
                let model = self.categoryDetailsInfo.productsInfo[index.row]
                model.iswishList = false
            }
        }
    }
    
    private func updateModel(filterArray: [GetHomePageProducts], status: Bool) {
        if filterArray.count > 0 {
            let prdct = filterArray.first
            prdct?.isWishList = status
            DispatchQueue.main.async {
                self.ProductCollectionView.reloadData()
            }
        }
    }
    
    private func updateModel(filterArray: [ProductsInfo], status: Bool) {
        if filterArray.count > 0 {
            let prdct = filterArray.first
            prdct?.iswishList = status
            DispatchQueue.main.async {
                self.ProductCollectionView.reloadData()
            }
        }
    }
    
    func addToWishList(id: NSInteger) {
        if featuredProductsFlag {
            let filter = self.homePageProductArray.filter { (product) -> Bool in
                return product.Id == id
            }
            self.updateModel(filterArray: filter, status: true)
        } else if filterProductArray.count > 0 {
            let filter = self.filterProductArray.filter { (product) -> Bool in
                return product.id == id
            }
            self.updateModel(filterArray: filter, status: true)
        } else {
            let filter = self.categoryDetailsInfo.productsInfo.filter { (product) -> Bool in
                return product.id == id
            }
            self.updateModel(filterArray: filter, status: true)
        }
    }
    
    func removeToWishList(id: NSInteger) {
        if featuredProductsFlag {
            let filter = self.homePageProductArray.filter { (product) -> Bool in
                return product.Id == id
            }
            self.updateModel(filterArray: filter, status: false)
        } else if filterProductArray.count > 0 {
            let filter = self.filterProductArray.filter { (product) -> Bool in
                return product.id == id
            }
            self.updateModel(filterArray: filter, status: false)
        } else {
            let filter = self.categoryDetailsInfo.productsInfo.filter { (product) -> Bool in
                return product.id == id
            }
            self.updateModel(filterArray: filter, status: false)
        }
    }
}
