//
//  Township.swift
//  OneStopMart
//
//  Created by iMac on 02/05/2020.
//  Copyright © 2020 Shobhit. All rights reserved.
//

import UIKit


protocol TownshipDelegate: class {
    func selectedTownshipMethod(type: tapGestureType, selectedText: String, MinOrderValue: String)
}

class Township: MartBaseViewController {
    
    let appDel = UIApplication.shared.delegate as! AppDelegate
    let aPIManager = APIManager()
    var apiManagerClient: APIManagerClient!
    weak var delegate: TownshipDelegate?
    
    var selectedType: tapGestureType?
    var ResponseArray = [TownShip]()
    var localBundle : Bundle?
    
    @IBOutlet var maynamarView: UIView!
    @IBOutlet var unicodeView: UIView!
    @IBOutlet var englishView: UIView!
    
    
    @IBOutlet var HeaderLbl: UILabel!{
        didSet {
            self.HeaderLbl.font = UIFont(name: appFont, size: 15.0)
            HeaderLbl?.text = HeaderLbl?.text?.localized
            
        }
    }
    
    @IBOutlet var HeaderOneLbl: UILabel!{
        didSet {
            self.HeaderOneLbl.font = UIFont(name: appFont, size: 15.0)
            HeaderOneLbl?.text = HeaderOneLbl?.text?.localized
            
        }
    }
    
    @IBOutlet var HeaderTwoLbl: UILabel!{
        didSet {
            self.HeaderTwoLbl.font = UIFont(name: appFont, size: 15.0)
            HeaderTwoLbl?.text = HeaderTwoLbl?.text?.localized
            
        }
    }
    
    @IBOutlet var HeaderThreeLbl: UILabel!{
        didSet {
            self.HeaderThreeLbl.font = UIFont(name: appFont, size: 15.0)
            HeaderThreeLbl?.text = HeaderThreeLbl?.text?.localized
            
        }
    }
    
    @IBOutlet weak var listTableView: UITableView!{
        didSet{
            listTableView.tableFooterView = UIView()
        }
    }
    @IBOutlet weak var mainBackgroundView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mainBackgroundView.layer.cornerRadius = 6
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.loadTownship()
        let lang = UserDefaults.standard.value(forKey: "currentLanguage") as! String
        appDel.currentLanguage = lang
        if lang == "my" {
            appLang = "Myanmar"
            appFont = "Zawgyi-One"
            maynamarView.backgroundColor = UIColor.colorWithRedValue(redValue: 13, greenValue: 177, blueValue: 75, alpha: 1)
            unicodeView.backgroundColor = .lightGray
            englishView.backgroundColor = .lightGray
        } else if lang == "en" {
            appLang = "English"
            appFont = "Zawgyi-One"
            maynamarView.backgroundColor = .lightGray
            unicodeView.backgroundColor = .lightGray
            englishView.backgroundColor = UIColor.colorWithRedValue(redValue: 13, greenValue: 177, blueValue: 75, alpha: 1)
        } else if lang == "uni" {
            appLang = "Unicode"
            appFont = "Myanmar3"
            maynamarView.backgroundColor = .lightGray
            unicodeView.backgroundColor = UIColor.colorWithRedValue(redValue: 13, greenValue: 177, blueValue: 75, alpha: 1)
            englishView.backgroundColor = .lightGray
        }
        else {
            print("error")
        }
         
    }
    
    func loadTownship(){
        AppUtility.showLoading(self.view)
        self.aPIManager.loadenableTownship( onSuccess:{ [weak self] getEnableTownshipr in
            self?.ResponseArray = getEnableTownshipr
            self?.UpdateHeadersData()
            self?.animate()
            },  onError: { message in
                print(message as Any)
                AppUtility.hideLoading(self.view)
        })
    }
    
    
    func animate () {
        AppUtility.hideLoading(self.view)
        self.listTableView.reloadData()
        let cells = self.listTableView.visibleCells
        let tableHeight: CGFloat = self.listTableView.bounds.size.height
        for i in cells {
            let cell: UITableViewCell = i as UITableViewCell
            cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
        }
        var index = 0
        for a in cells {
            let cell: UITableViewCell = a as UITableViewCell
            UIView.animate(withDuration: 1.3, delay: 0.05 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: [], animations: {
                cell.transform = CGAffineTransform(translationX: 0, y: 0);
            }, completion: nil)
            index += 1
        }
    }
    
    
    
    open func showInView(_ aView: UIView!) {
        self.view.frame = aView.frame
        aView.addSubview(self.view)
        self.showAnimate()
    }
    
    func showAnimate() {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate(completionHandler: @escaping (_ isCompleted: Bool) -> Void) {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished) {
                self.dismiss(animated: true, completion: nil)
                completionHandler(true)
            }
        })
    }
    
    @IBAction func dismissViewAction(_ sender: UITapGestureRecognizer) {
        self.removeAnimate() { isCompleted in
            NotificationCenter.default.post(name: NSNotification.Name("AppLanguageChange"), object: nil, userInfo: nil)
        }
    }
    
    @IBAction func languageSetUpChange(sender:UIButton){
        
        
        switch sender.tag {
        case 1:
            appDel.currentLanguage = "en"
            appLang = "English"
            appFont = "Zawgyi-One"
            
            appDel.currentLanguage = "en"
            appDel.currentFont = "Zawgyi-One"
            
            if let path = Bundle.main.path(forResource: "en", ofType: "lproj") {
                localBundle = Bundle(path: path)
            }
            appDel.setSeletedlocaLizationLanguage(language: "en")
            englishView.backgroundColor = UIColor.colorWithRedValue(redValue: 13, greenValue: 177, blueValue: 75, alpha: 1)
            unicodeView.backgroundColor = .lightGray
            maynamarView.backgroundColor = .lightGray
            
            
            break
        case 2:
            appDel.currentLanguage = "my"
            appLang = "Myanmar"
            appFont = "Zawgyi-One"
            
            appDel.currentLanguage = "my"
            appDel.currentFont = "Zawgyi-One"
            
            if let path = Bundle.main.path(forResource: "my", ofType: "lproj") {
                localBundle = Bundle(path: path)
            }
            appDel.setSeletedlocaLizationLanguage(language: "my")
            maynamarView.backgroundColor = UIColor.colorWithRedValue(redValue: 13, greenValue: 177, blueValue: 75, alpha: 1)
            unicodeView.backgroundColor = .lightGray
            englishView.backgroundColor = .lightGray
            break
            
        default:
            appDel.currentLanguage = "uni"
            appLang = "Unicode"
            appFont = "Myanmar3"
            
            appDel.currentLanguage = "uni"
            appDel.currentFont = "Myanmar3"
            
            if let path = Bundle.main.path(forResource: "uni", ofType: "lproj") {
                localBundle = Bundle(path: path)
            }
            appDel.setSeletedlocaLizationLanguage(language: "uni")
            unicodeView.backgroundColor = UIColor.colorWithRedValue(redValue: 13, greenValue: 177, blueValue: 75, alpha: 1)
            maynamarView.backgroundColor = .lightGray
            englishView.backgroundColor = .lightGray
            break
        }
        self.changeAppLanguage(index: sender.tag)    }
    
    
    func changeAppLanguage(index: Int) {
        self.changeAppLanguageApiCall(index: index)
        self.UpdateHeadersData()
    }
    
    func UpdateHeadersData() {
        self.HeaderLbl.font = UIFont(name: appFont, size: 15.0)
        self.HeaderOneLbl.font = UIFont(name: appFont, size: 15.0)
        self.HeaderTwoLbl.font = UIFont(name: appFont, size: 15.0)
        self.HeaderThreeLbl.font = UIFont(name: appFont, size: 15.0)
           HeaderLbl?.text = "Yangon".localized
           HeaderOneLbl?.text = "Please Select Township Where We Do Delivery".localized
           HeaderTwoLbl?.text = "Select State / Division".localized
           HeaderThreeLbl?.text = "Select Township".localized

    }
    
    
    func changeAppLanguageApiCall(index: Int) {
        AppUtility.showLoading(self.view)
        let type = index //(index == 0) ? 1 : 2
        let urlString = String(format: "%@/SetLanguage/%d", APIManagerClient.sharedInstance.base_url, type)
        guard let url = URL(string: urlString) else { return }
        let params = Dictionary<String, Any>()
        aPIManager.genericClass(url: url, param: params as AnyObject, httpMethod: "POST", header: true) { [weak self](response, success, _) in
            if success {
                if let json = response as? Dictionary<String,Any> {
                    if let code = json["StatusCode"] as? Int, code == 200 {
                        var selectedLanguage = ""
                        if type == 1 {
                            UserDefaults.standard.set("Zawgyi-One", forKey: "appFont")
                            UserDefaults.standard.synchronize()
                            appFont = "Zawgyi-One"
                            appLang = "English"
                            selectedLanguage = "en"
                            
                        } else if type == 2 {
                            UserDefaults.standard.set("Zawgyi-One", forKey: "appFont")
                            UserDefaults.standard.synchronize()
                            appFont = "Zawgyi-One"
                            appLang = "Myanmar"
                            selectedLanguage = "my"
                            
                        } else{
                            UserDefaults.standard.set("Myanmar3", forKey: "appFont")
                            UserDefaults.standard.synchronize()
                            appFont = "Myanmar3"
                            appLang = "Unicode"
                            selectedLanguage = "uni"
                            
                        }
                        UserDefaults.standard.set(selectedLanguage, forKey: "currentLanguage")
                        appDelegate.setSeletedlocaLizationLanguage(language: selectedLanguage)
                        UserDefaults.standard.synchronize()
                        DispatchQueue.main.async {
                            AppUtility.hideLoading(self!.view)
                            self?.loadTownship()
                        }
                    } else {
                        DispatchQueue.main.async {
                            AppUtility.hideLoading(self!.view)
                            AppUtility.showToastlocal(message: "Failure", view: self!.view) }
                    }
                }
            }
        }
    }
    
    
    
}

extension Township: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.ResponseArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TownshipCell", for: indexPath) as! TownshipCell
        cell.wrapData(value: self.ResponseArray[indexPath.row].CityName ?? "")
//        if indexPath.row != 0{
//            cell.imgIcon.isHidden = true
//        }
         cell.imgIcon.isHidden = true
        return cell
    }
    
    
    
    func selectTownship(type: tapGestureType, selectedText: String, MinOrderValue: String){
        DispatchQueue.main.async {
                   var minOrder = "0"
                   let largeNumber = Int(MinOrderValue)
                   let numberFormatter = NumberFormatter()
                   numberFormatter.numberStyle = .decimal
                   if let valueOne = largeNumber{
                       let formattedNumber = numberFormatter.string(from: NSNumber(value:valueOne))
                       //print("*****************")
                       //  print(formattedNumber)
                       if let hasValue = formattedNumber{
                           minOrder = "\(hasValue) MMK"
                       }else{
                           minOrder = "0.0 MMK"
                       }
                       // text = "\(String(describing: formattedNumber)) MMK"
                       //print("*****************")
                   }
            
            var message = ""
            if selectedText == "Others" {
                message = "We are coming soon for Other township.".localized
            }
            else {
                message = "Congratulation Your township fall into Free Deliver of Minimum buying of".localized
                message = message.replacingOccurrences(of: "%s", with: "\(minOrder)")
            }
            let alertVC = SAlertController()
            alertVC.ShowSAlert(title: "".localized, withDescription: message, onController: self)
            let YesAction = SAlertAction()
            YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                if selectedText != "Others" {
                    self.dismissViewAction(UITapGestureRecognizer())
                }
            })
            
            alertVC.addAction(action: [YesAction])
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        UserDefaults.standard.set(true, forKey: "OneTime")
        UserDefaults.standard.synchronize()
        //        self.delegate?.selectedTownshipMethod(type: self.selectedType ?? tapGestureType.none, selectedText: self.ResponseArray[indexPath.row].CityName ?? "", MinOrderValue: self.ResponseArray[indexPath.row].MinOrderValue ?? "")
        
        self.selectTownship(type: self.selectedType ?? tapGestureType.none, selectedText: self.ResponseArray[indexPath.row].CityName ?? "", MinOrderValue: self.ResponseArray[indexPath.row].MinOrderValue ?? "")
        /*
         stateObject.name = self.stateTownShipArray[indexPath.row]?["name"] as? String ?? ""
         stateObject.code = "\(self.stateTownShipArray[indexPath.row]?["id"] as? Int ?? 0)"
         if self.stateObject.name == "Others" && self.stateObject.code == "5690" {
         DispatchQueue.main.async {
         let alertVC = SAlertController()
         alertVC.ShowSAlert(title: "".localized, withDescription: "We are coming soon.".localized, onController: self)
         let cancelAction = SAlertAction()
         cancelAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {  })
         alertVC.addAction(action: [cancelAction])
         }
         }
         else {
         self.removeAnimate() { isCompleted in
         self.delegate?.selectedStateTownshipMethod(type: self.selectedType ?? tapGestureType.none, selectedText: self.stateObject)
         }
         }
         */
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

class TownshipCell: UITableViewCell {
    @IBOutlet var nameLbl: UILabel!{
        didSet {
            self.nameLbl.font = UIFont(name: appFont, size: 15.0)
            nameLbl?.text = nameLbl?.text?.localized
            
        }
    }
    
    
    @IBOutlet var imgIcon: UIImageView!{
        didSet {
            imgIcon.isHidden = false
        }
    }
    
    
    func wrapData(value: String){
        
        self.nameLbl.font = UIFont(name: appFont, size: 15.0)
        self.nameLbl.text = value
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}


