//
//  MyOrderDetailViewController.swift
//  VMart
//
//  Created by Kethan on 12/15/18.
//  Copyright © 2018 Shobhit. All rights reserved.
//

import UIKit
import PDFKit

class MyOrderDetailViewController: MartBaseViewController {
    
    var responseModel: Dictionary<String, Any>?
    var itemsArray = [Dictionary<String, Any>]()
    var noteDictionary: [Dictionary<String,Any>] = []
    var PaymentMethodStatus : String = ""
    var aPIManager = APIManager()
    var selectedProductId = Int()
    var orderDetail: VMMyOrderDetail? = nil
    var shareBarButton: UIBarButtonItem!
    
    //    @IBOutlet var PDFViewContainer:UIView?
    @IBOutlet weak var myOrderDetailTbl: UITableView!
    @IBOutlet weak var PdfTbl: UITableView!
    @IBOutlet var PDFViewContainer:PDFView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.topItem?.title = ""
        myOrderDetailTbl.estimatedRowHeight = 50
        myOrderDetailTbl.rowHeight = UITableView.automaticDimension
        getProductDetails()
        myOrderDetailTbl.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //        super.viewWillAppear(animated)
        //        self.shareBarButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.action, target: self, action:  #selector(self.ShareAction(_:)))
        //        self.shareBarButton.tintColor = UIColor.white
        //        self.navigationItem.rightBarButtonItem  = self.shareBarButton
         self.navigationItem.title = "Order Details".localized
        let button = UIButton()
        button.setImage(UIImage(named: "shareProductwhite"), for: .normal)
        button.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        button.widthAnchor.constraint(equalToConstant: 25).isActive = true
        button.heightAnchor.constraint(equalToConstant: 25).isActive = true
        button.addTarget(self, action: #selector(self.ShareAction(_:)), for: .touchUpInside)
        let item = UIBarButtonItem(customView: button)
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        self.navigationItem.rightBarButtonItem = item
    }
    
    @IBAction func ShareAction(_ sender: UIBarButtonItem) {
        self.PDFViewContainer?.isHidden = false
        self.PDFViewContainer?.autoScales = true
        self.PDFViewContainer?.backgroundColor = UIColor.lightGray
        var pdfDocument: PDFDocument?
        
        DispatchQueue.main.async {
            
            //1StopKitchen-2203202023824-2020-03-22T16_42_04.3896484.pdf
            
            
            let formatingDate = AppUtility.getFormattedDate(date: Date(), format: "yyyy-MM-dd'T'HH:mm:ssZZZZZ")
            print(formatingDate)
            
            let strPdfName = "1StopMart-" + "\(Date().toMillis() ?? 232333333323223)" + "-" + formatingDate
            
            
            let pdfUrl = self.pdfDataWithTableView(tableView: self.PdfTbl, saveToDocumentsWithFileName: strPdfName)
            
            
            if let document = PDFDocument(url: pdfUrl!) {
                document.delegate = self
                pdfDocument = document
            }
            
            guard let data = pdfDocument!.dataRepresentation() else { return }
            
            let documentDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            
            let docURL = documentDirectory.appendingPathComponent(strPdfName + ".pdf")
            
            do{
                
                try data.write(to: docURL)
                
            }catch(let error){
                print("error is \(error.localizedDescription)")
            }
            
            let docURL1 = documentDirectory.appendingPathComponent(strPdfName + ".pdf")
            
            let message = MessageWithSubject(subject: "1 Stop Mart Invoice", message: "This is an 1 Stop Mart Invoice to share with you.")
            var activityItems = [Any]()
            activityItems.append(message)
            activityItems.append(docURL1 as Any)
            let activityViewController = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
            
            self.present(activityViewController, animated: true, completion: {
                self.PDFViewContainer?.isHidden = true
            })
        }
    }
    
    class MessageWithSubject: NSObject, UIActivityItemSource {
        
        let subject:String
        let message:String
        
        init(subject: String, message: String) {
            self.subject = subject
            self.message = message
            
            super.init()
        }
        
        func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
            return message
        }
        
        func activityViewController(_ activityViewController: UIActivityViewController, itemForActivityType activityType: UIActivity.ActivityType?) -> Any? {
            return message
        }
        
        func activityViewController(_ activityViewController: UIActivityViewController,
                                    subjectForActivityType activityType: UIActivity.ActivityType?) -> String {
            return subject
        }
    }
    
    // 2. Return your custom PDFPage class
    /// - Tag: ClassForPage
    @available(iOS 11.0, *)
    func classForPage() -> AnyClass {
        return WatermarkPage.self
    }
    
    
    
    
    
    func pdfDataWithTableView(tableView: UITableView,saveToDocumentsWithFileName pdfFileName: String)  -> URL?{
        let priorBounds = tableView.bounds
        let fittedSize = tableView.sizeThatFits(CGSize(width:priorBounds.size.width, height:tableView.contentSize.height + 300))
        tableView.bounds = CGRect(x:0, y:0, width:fittedSize.width, height:fittedSize.height + 300)
        let pdfPageBounds = CGRect(x:0, y:0, width:tableView.frame.width, height:tableView.contentSize.height + 300)
        let pdfData = NSMutableData()
        UIGraphicsBeginPDFContextToData(pdfData, pdfPageBounds,nil)
        var pageOriginY: CGFloat = 0
        while pageOriginY < fittedSize.height + 300 {
            UIGraphicsBeginPDFPageWithInfo(pdfPageBounds, nil)
            UIGraphicsGetCurrentContext()!.saveGState()
            UIGraphicsGetCurrentContext()!.translateBy(x: 0, y: -pageOriginY)
            tableView.layer.render(in: UIGraphicsGetCurrentContext()!)
            UIGraphicsGetCurrentContext()!.restoreGState()
            pageOriginY += pdfPageBounds.size.height
        }
        UIGraphicsEndPDFContext()
        tableView.bounds = priorBounds
        
        guard let documentDirectories = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first else  { return nil}
        let properPDFFileName = pdfFileName.replacingOccurrences(of: ".pdf", with: "") + ".pdf"
        let pdfUrl = documentDirectories + "/" + properPDFFileName
        
        
        //Delete file if alreay exists
        do {
            try FileManager.default.removeItem(atPath: pdfUrl)
        } catch let error as NSError {
            println_debug("Error: \(error.domain)")
        }
        //Write file
        if pdfData.write(toFile: pdfUrl, atomically: true) {
            println_debug("Successfully wrote to \(pdfUrl)")
            let pathUrl = URL(fileURLWithPath: documentDirectories)
            let fileUrl = pathUrl.appendingPathComponent(properPDFFileName)
            return fileUrl
        }
        
        return nil
        
    }
    
    
    
    
    
    
    
    
    
    private func parseItems(dict: [Any], completionHandler: @escaping(Bool) -> Void) {
        if dict.count > 0 {
            for item in dict {
                if let itm = item as? Dictionary<String, Any> {
                    itemsArray.append(itm)
                }
            }
        }
        completionHandler(true)
    }
    
    private func getProductDetails() {
        let urlString = String(format: "%@/order/details/%d", APIManagerClient.sharedInstance.base_url, selectedProductId)
        guard let url = URL(string: urlString) else { return }
        let params = Dictionary<String, Any>()
        AppUtility.showLoading(self.view)
        aPIManager.genericClass(url: url, param: params as AnyObject, httpMethod: "GET", header: true) { [weak self](response, success, data) in
            DispatchQueue.main.async {
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                }
            }
            if success {
                do {
                    guard let dataVal = data else { return }
                    if let dict = try JSONSerialization.jsonObject(with: dataVal, options: .allowFragments) as? Dictionary<String, Any> {
                        self?.responseModel = dict
                        print(dict)
                    }
                    if let dict = self?.responseModel, let orders = dict["Items"] as? [Any] {
                        let d = dict["OrderNotes"] as? [Dictionary<String,Any>]
                        let paystatus = dict["PaymentMethodStatus"] as? String
                        self?.PaymentMethodStatus = paystatus ?? ""
                        if let nt = d {
                            self?.noteDictionary = nt
                            self?.noteDictionary.reverse()
                        }
                        
                        self?.parseItems(dict: orders) { (status) in
                            DispatchQueue.main.async {
                                self?.myOrderDetailTbl.delegate = self
                                self?.myOrderDetailTbl.dataSource = self
                                self?.myOrderDetailTbl.reloadData()
                                
                                
                                
                                self?.PdfTbl.delegate = self
                                self?.PdfTbl.dataSource = self
                                self?.PdfTbl.reloadData()
                                
                                
                            }
                        }
                        
                    }
                } catch _ {
                    println_debug("Parse error")
                }
            }
        }
    }
}

@available(iOS 11.0, *)
extension MyOrderDetailViewController : PDFDocumentDelegate {
    
}

extension MyOrderDetailViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == PdfTbl{
            return 6
        }else {
            return 6
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == PdfTbl {
            if section == 4 {
                let TotalObject = responseModel!["Items"] as? [Dictionary<String,Any>]
                return (TotalObject?.count)!
            }else {
                return 1
            }
        }else {
            if section == 0 {
                return itemsArray.count
            }
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == PdfTbl{
            println_debug(indexPath)
            if indexPath.section == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "PDFHeaderCell", for: indexPath) as! PDFTableCell
                return cell
            }else if indexPath.section == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "PDFinvoiceCell", for: indexPath) as! PDFTableCell
                cell.PDFInvoice(responseModel: responseModel!)
                return cell
            }else if indexPath.section == 2 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "PDFAddressCell", for: indexPath) as! PDFTableCell
                cell.PDFAddress(responseModel: responseModel!)
                return cell
            }else if indexPath.section == 3 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "PDFOrderHeaderCell", for: indexPath) as! PDFTableCell
                return cell
            }else if indexPath.section == 4 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "PDFOrderDetailCell", for: indexPath) as! PDFTableCell
                let currentObject = responseModel!["Items"] as? [Dictionary<String,Any>]
                cell.PDFOrderDetail(responseModel: currentObject![indexPath.row],completeDict: responseModel)
                return cell
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "PDFTotalCell", for: indexPath) as! PDFTableCell
                cell.PDFwrapTotalData(responseModel: responseModel!)
                return cell
            }
        }
        else{
            if indexPath.section == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "myOrderItemCell", for: indexPath) as! MyOrderDetailTableCell
                cell.wrapData(responseModel: itemsArray[indexPath.row])
                return cell
            }else if indexPath.section == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "shipStatusCell", for: indexPath) as! MyOrderDetailTableCell
                cell.wrapNotecell(notes: noteDictionary, completeDict: responseModel)
                return cell
            }else if indexPath.section == 2 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "orderAddressCell", for: indexPath) as! MyOrderDetailTableCell
                cell.wrapAddressData(responseModel: responseModel)
                return cell
            }
            else if indexPath.section == 3 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "paymentStatusCell", for: indexPath) as! MyOrderDetailTableCell
                cell.wraPaymentStatusCell(paymentStatus: PaymentMethodStatus)
                return cell
            }
                
            else  if indexPath.section == 4 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "orderPaymentCell", for: indexPath) as! MyOrderDetailTableCell
                cell.wrapPaymentData(orders: responseModel)
                return cell
            }
            else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "OrderNoteCell", for: indexPath) as! OrderNoteCell
                cell.remarkLable.text = "Remarks: Final order Value / amount will be as per Qty / Weight of the products.".localized
                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == PdfTbl {
            if indexPath.section == 0 || indexPath.section == 2 || indexPath.section == 1{
                return 130
            }else if indexPath.section == 5{
                return 80
            }else if indexPath.section == 3{
                return 50
            }else {
                return 120
            }
        }else {
            if indexPath.section == 0 {
                return 175
            }else if indexPath.section == 1 {
                if noteDictionary.count == 0 {
                    return 0
                }else if noteDictionary.count == 1  {
                    //                    return 100
                    return 80
                }
                else if noteDictionary.count == 2  {
                    return 180
                }
                    
                else if noteDictionary.count == 3  {
                    return 225
                }else  {
                    return 0
                }
            }else if indexPath.section == 2 {
                return  225
            }
                
            else if indexPath.section == 3  {
                return 80
            }
            else if indexPath.section == 4  {
                return 320
            }
                
            else {
                return  80
            }
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    private func showProductDetailVC(index: Int) {
        let productId =  self.itemsArray[index]["ProductId"] as? Int
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        if let vc = storyBoard.instantiateViewController(withIdentifier: "ProductDetails") as? ProductDetails {
            vc.productId = productId ?? 0
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

class MyOrderDetailTableCell: UITableViewCell {
    
    
    @IBOutlet var DiscountLabel: UILabel!{
        didSet {
             self.DiscountLabel.font = UIFont(name: appFont, size: 12.0)
            self.DiscountLabel.isHidden = true
            self.DiscountLabel.text = self.DiscountLabel.text?.localized
           
        }
    }
    
    @IBOutlet var DiscounValtLabel: UILabel!{
        didSet {
             self.DiscounValtLabel.font = UIFont(name: appFont, size: 12.0)
            self.DiscounValtLabel.text = self.DiscounValtLabel.text?.localized
            
        }
    }
    
    
    @IBOutlet weak var orderNumLable: UILabel!{
        didSet {
            self.orderNumLable.font = UIFont(name: appFont, size: 15.0)
            self.orderNumLable.text = self.orderNumLable.text?.localized
            
        }
    }
    @IBOutlet weak var orderNumValueLable: UILabel!{
        didSet {
            self.orderNumValueLable.font = UIFont(name: appFont, size: 15.0)
            self.orderNumValueLable.text = self.orderNumValueLable.text?.localized
            
        }
    }
    
    @IBOutlet weak var RefNumLable: UILabel!{
        didSet {
            self.RefNumLable.font = UIFont(name: appFont, size: 15.0)
            self.RefNumLable.text = self.RefNumLable.text?.localized
            
        }
    }
    @IBOutlet weak var RefNumValueLable: UILabel!{
        didSet {
            self.RefNumValueLable.font = UIFont(name: appFont, size: 15.0)
            self.RefNumValueLable.text = self.RefNumValueLable.text?.localized
            
        }
    }
    
    @IBOutlet weak var TaxLable: UILabel!{
        didSet {
            self.TaxLable.font = UIFont(name: appFont, size: 15.0)
            self.TaxLable.text = "Discount".localized
            
        }
    }
    @IBOutlet weak var TaxValueLable: UILabel!{
        didSet {
            self.TaxValueLable.font = UIFont(name: appFont, size: 15.0)
            self.TaxValueLable.text = self.TaxValueLable.text?.localized
            
        }
    }
    
    
    @IBOutlet weak var colorLable: UILabel!{
        didSet {
             self.colorLable.font = UIFont(name: appFont, size: 15.0)
            self.colorLable.text = self.colorLable.text?.localized
           
        }
    }
    
    @IBOutlet weak var ratingView: UIView!
    @IBOutlet weak var ratingWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var ratingHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!{
        didSet {
             self.nameLabel.font = UIFont(name: appFont, size: 15.0)
            self.nameLabel.text = self.nameLabel.text?.localized
           
        }
    }
    @IBOutlet weak var starLabel: UILabel!{
        didSet {
            self.starLabel.font = UIFont(name: appFont, size: 15.0)
            self.starLabel.text = self.starLabel.text?.localized
            
        }
    }
    @IBOutlet weak var ratRevLabel: UILabel!{
        didSet {
              self.ratRevLabel.font = UIFont(name: appFont, size: 15.0)
            self.ratRevLabel.text = self.ratRevLabel.text?.localized
          
        }
    }
    @IBOutlet weak var priceAfterDisc: UILabel!{
        didSet {
            self.priceAfterDisc.font = UIFont(name: appFont, size: 15.0)
            self.priceAfterDisc.text = self.priceAfterDisc.text?.localized
            
        }
    }
    @IBOutlet weak var priceBefDisc: UILabel!{
        didSet {
            self.priceBefDisc.font = UIFont(name: appFont, size: 15.0)
            self.priceBefDisc.text = self.priceBefDisc.text?.localized
            
        }
    }
    @IBOutlet weak var perOffLabel: UILabel!{
        didSet {
            self.perOffLabel.font = UIFont(name: appFont, size: 15.0)
            self.perOffLabel.text = self.perOffLabel.text?.localized
            
        }
    }
    @IBOutlet weak var qtyLabel: UILabel!{
        didSet {
            self.qtyLabel.font = UIFont(name: appFont, size: 15.0)
            self.qtyLabel.text = self.qtyLabel.text?.localized
            
        }
    }
    
    @IBOutlet weak var blueLable1: UILabel!{
        didSet {
            self.blueLable1.font = UIFont(name: appFont, size: 15.0)
            self.blueLable1.text = self.blueLable1.text?.localized
            
        }
    }
    @IBOutlet weak var blueLable2: UILabel!{
        didSet {
             self.blueLable2.font = UIFont(name: appFont, size: 15.0)
            self.blueLable2.text = self.blueLable2.text?.localized
           
        }
    }
    @IBOutlet weak var blueLable3: UILabel!{
        didSet {
            self.blueLable3.font = UIFont(name: appFont, size: 15.0)
            self.blueLable3.text = self.blueLable3.text?.localized
            
        }
    }
    @IBOutlet weak var blueLable4: UILabel!{
        didSet {
             self.blueLable4.font = UIFont(name: appFont, size: 15.0)
            self.blueLable4.text = self.blueLable4.text?.localized
           
        }
    }
    @IBOutlet weak var blueLable5: UILabel!{
        didSet {
              self.blueLable5.font = UIFont(name: appFont, size: 15.0)
            self.blueLable5.text = self.blueLable5.text?.localized
          
        }
    }
    
    @IBOutlet weak var approveStatusLabel: UILabel!{
        didSet {
             self.approveStatusLabel.font = UIFont(name: appFont, size: 15.0)
            self.approveStatusLabel.text = self.approveStatusLabel.text?.localized
           
        }
    }
    @IBOutlet weak var approveStatusDateLabel: UILabel!{
        didSet {
            self.approveStatusDateLabel.font = UIFont(name: appFont, size: 15.0)
            self.approveStatusDateLabel.text = self.approveStatusDateLabel.text?.localized
            
        }
    }
    @IBOutlet weak var shippingStatusLabel: UILabel!{
        didSet {
             self.shippingStatusLabel.font = UIFont(name: appFont, size: 15.0)
            self.shippingStatusLabel.text = self.shippingStatusLabel.text?.localized
           
        }
    }
    @IBOutlet weak var shippingStatusDateLabel: UILabel!{
        didSet {
             self.shippingStatusDateLabel.font = UIFont(name: appFont, size: 15.0)
            self.shippingStatusDateLabel.text = self.shippingStatusDateLabel.text?.localized
           
        }
    }
    @IBOutlet weak var deliverStatusLabel: UILabel!{
        didSet {
             self.deliverStatusLabel.font = UIFont(name: appFont, size: 15.0)
            self.deliverStatusLabel.text = self.deliverStatusLabel.text?.localized
           
        }
    }
    @IBOutlet weak var deliverStatusDateLabel: UILabel!{
        didSet {
             self.deliverStatusDateLabel.font = UIFont(name: appFont, size: 15.0)
            self.deliverStatusDateLabel.text = self.deliverStatusDateLabel.text?.localized
           
        }
    }
    
    @IBOutlet weak var addressNameLabel: UILabel!{
        didSet {
            self.addressNameLabel.font = UIFont(name: appFont, size: 20.0)
            self.addressNameLabel.text = self.addressNameLabel.text?.localized
            
        }
    }
    @IBOutlet weak var addressLabel: UILabel!{
        didSet {
            self.addressLabel.font = UIFont(name: appFont, size: 15.0)
            self.addressLabel.text = self.addressLabel.text?.localized
            
        }
    }
    @IBOutlet weak var addressPhoneLabel: UILabel!{
        didSet {
             self.addressPhoneLabel.font = UIFont(name: appFont, size: 15.0)
            self.addressPhoneLabel.text = self.addressPhoneLabel.text?.localized
           
        }
    }
    @IBOutlet weak var productAmtLabel: UILabel!{
        didSet {
            self.productAmtLabel.font = UIFont(name: appFont, size: 15.0)
            self.productAmtLabel.text = self.productAmtLabel.text?.localized
            
        }
    }
    @IBOutlet weak var shippingChargesLabel: UILabel!{
        didSet {
            self.shippingChargesLabel.font = UIFont(name: appFont, size: 15.0)
            self.shippingChargesLabel.text = self.shippingChargesLabel.text?.localized
            
        }
    }
    @IBOutlet weak var totalPayableAmtLabel: UILabel!{
        didSet {
            self.totalPayableAmtLabel.font = UIFont(name: appFont, size: 15.0)
            self.totalPayableAmtLabel.text = self.totalPayableAmtLabel.text?.localized
            
        }
    }
    @IBOutlet weak var paymentModeLabel: UILabel!{
        didSet {
            self.paymentModeLabel.font = UIFont(name: appFont, size: 15.0)
            self.paymentModeLabel.text = self.paymentModeLabel.text?.localized
            
        }
    }
    @IBOutlet weak var paymentModeLbl: UILabel!{
        didSet {
             self.paymentModeLbl.font = UIFont(name: appFont, size: 15.0)
            self.paymentModeLbl.text = self.paymentModeLbl.text?.localized
           
        }
    }
    @IBOutlet weak var totalPayAmtLbl: UILabel!{
        didSet {
            self.totalPayAmtLbl.font = UIFont(name: appFont, size: 15.0)
            self.totalPayAmtLbl.text = self.totalPayAmtLbl.text?.localized
            
        }
    }
    @IBOutlet weak var shippingChrgLbl: UILabel!{
        didSet {
            self.shippingChrgLbl.font = UIFont(name: appFont, size: 15.0)
            self.shippingChrgLbl.text = self.shippingChrgLbl.text?.localized
            
        }
    }
    @IBOutlet weak var prdtAmtLbl: UILabel!{
        didSet {
            self.prdtAmtLbl.font = UIFont(name: appFont, size: 15.0)
            self.prdtAmtLbl.text = self.prdtAmtLbl.text?.localized
            
        }
    }
    @IBOutlet weak var paymntDetailLbl: UILabel!{
        didSet {
            self.paymntDetailLbl.font = UIFont(name: appFont, size: 15.0)
            self.paymntDetailLbl.text = self.paymntDetailLbl.text?.localized
            
        }
    }
    @IBOutlet weak var addDetailLbl: UILabel!{
        didSet {
            self.addDetailLbl.font = UIFont(name: appFont, size: 15.0)
            self.addDetailLbl.text = "Address Details".localized
            
        }
    }
    
    @IBOutlet weak var deliverylabel: UILabel!{
        didSet {
            self.deliverylabel.font = UIFont(name: appFont, size: 15.0)
            self.deliverylabel.text = self.deliverylabel.text?.localized
            
        }
    }
    
    
    @IBOutlet weak var paymentStatusLbl: UILabel!{
        didSet {
            self.paymentStatusLbl.font = UIFont(name: appFont, size: 15.0)
            self.paymentStatusLbl.text = "Payment Status :".localized
            
        }
    }
    
    @IBOutlet weak var paymentLbl: UILabel!{
        didSet {
            self.paymentLbl.font = UIFont(name: appFont, size: 15.0)
            self.paymentLbl.text = self.paymentLbl.text?.localized
            
        }
    }
    
    
    func wrapData(responseModel: Dictionary<String, Any>) {
        //        if let item = items {
        //            if let imageUrl = item.picture?.imageURL {
        //                self.itemImageView.setImageUsingUrl(imageUrl)
        //            }
        //            self.nameLabel.text = item.productName ?? ""
        //            self.starLabel.text = "\(Float(item.productReviewOverview?.ratingSum ?? 0))"
        //            self.ratRevLabel.text = "Ratings \(item.productReviewOverview?.ratingSum ?? 0) and Reviews \(item.productReviewOverview?.totalReviews ?? 0)"
        //            self.priceAfterDisc.text = item.priceAfterDiscount ?? ""
        //            self.priceBefDisc.text = item.unitPrice ?? ""
        //            if let disc = item.discountPercentage, disc > 0 {
        //                self.perOffLabel.text = "\(disc)% off"
        //            } else {
        //                self.perOffLabel.isHidden = true
        //            }
        //            if let qty = item.quantity, qty > 1 {
        //                self.qtyLabel.text = "Quantity: \(qty)"
        //            } else {
        //                self.qtyLabel.isHidden = true
        //            }
        //        }
        
        if let imageDict = responseModel["Picture"] as? Dictionary<String, Any> {
            itemImageView.setImageUsingUrl(imageDict["ImageUrl"] as? String ?? "")
        }
        self.nameLabel.text = responseModel["ProductName"] as? String ?? ""
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: responseModel["OldPrice"] as? String ?? "")
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 1, range: NSMakeRange(0, attributeString.length))
        if responseModel["OldPrice"] as? String == "0 MMK"{
            self.priceBefDisc.attributedText = nil
        }else{
            self.priceBefDisc.attributedText = attributeString
        }
        self.colorLable.text = (responseModel["AttributeInfo"] as? String)?.replacingOccurrences(of: "<br />", with: "\n") ?? ""
        qtyLabel.text = "\("Quantity".localized) \(responseModel["Quantity"] as? Int ?? 0)"
        if let disc = responseModel["DiscountPercentage"] as? Int, disc > 0 {
            //self.priceBefDisc.amountAttributedString()
            
            //             self.priceBefDisc.isHidden = false
            self.priceAfterDisc.text = responseModel["PriceAfterDiscount"] as? String ?? ""
            self.priceAfterDisc.amountAttributedString()
            self.perOffLabel.text = "\(disc)%"
        } else {
            //            self.priceBefDisc.isHidden = true
            self.priceAfterDisc.text = responseModel["UnitPrice"] as? String ?? ""
            self.priceAfterDisc.amountAttributedString()
            self.perOffLabel.isHidden = true
            
        }
        
        if let qty = responseModel["Quantity"] as? Int{ //, qty > 1 {
            self.qtyLabel.text = "\("Quantity".localized): \(qty)"
        } else {
            self.qtyLabel.isHidden = true
        }
        
        if let productReviewDict = responseModel["ProductReviewOverview"] as? Dictionary<String, Any> {
            //            if productReviewDict["RatingSum"] as? Double == 0.0 {
            //               // ratingView.isHidden = true
            //                ratingWidthConstraint.constant = 70
            //                ratingHeightConstraint.constant = 25
            //                self.ratRevLabel.text = "Reviews \(productReviewDict["TotalReviews"] as? Int ?? 0)"
            //            } else {
            ratingWidthConstraint.constant = 70
            ratingHeightConstraint.constant = 25
            //ratingView.isHidden = false
            self.starLabel.text = "\(Double(productReviewDict["RatingSum"] as? Double ?? 0))"
            self.ratRevLabel.text = "\(productReviewDict["TotalReviews"] as? Int ?? 0) Reviews"
            //}
        }
    }
    
    func wrapAddressData(responseModel: Dictionary<String, Any>?) {
        
        if let model = responseModel {
            var addressDic: Dictionary<String, Any>?
            // println_debug( model["PickUpInStore"])
            if let pickuplocation = model["PickUpInStore"] as? Bool {
                if pickuplocation {
                    addressDic =  model["PickupAddress"] as? Dictionary<String, Any>
                }else {
                    addressDic =  model["BillingAddress"] as? Dictionary<String, Any>
                }
            }
            
            if let address = addressDic {
                self.addressNameLabel.text = address["FirstName"] as? String ?? ""
                
                if var number = address["PhoneNumber"] as? String, number.hasPrefix("00950") {
                    number = (number as NSString).replacingCharacters(in: NSRange(location: 0, length: 5), with: "+95 ")
                    self.addressPhoneLabel.text = number
                } else {
                    self.addressPhoneLabel.text = address["PhoneNumber"] as? String ?? ""
                }
                let houseNo = address["HouseNo"] as? String ?? ""
                let floorNo = address["FloorNo"] as? String ?? ""
                let roomNo = address["RoomNo"] as? String ?? ""
                let address1 = address["Address1"] as? String ?? ""
                let address2 = address["Address2"] as? String ?? ""
                let city = address["City"] as? String ?? ""
                let state = address["StateProvinceName"] as? String ?? ""
                let country = address["CountryName"] as? String ?? ""
                
                var fullAddres = ""
                var house = ""
                var floor = ""
                var room = ""
                
                if houseNo == "House No.".localized {
                    house = ""
                    //  fullAddres += house
                }
                else if houseNo != "House No.".localized
                {
                    house = houseNo + ", "
                    fullAddres += house
                }
                
                if floorNo == "Floor No.".localized {
                    floor = ""
                }
                else if floorNo != "Floor No.".localized {
                    floor = floorNo + ", "
                    fullAddres += floor
                }
                
                if roomNo == "Room No.".localized {
                    room = ""
                }
                else if roomNo != "Room No.".localized
                {
                    room = roomNo + ", "
                    fullAddres += room
                }
                if address1 != "" {
                    fullAddres += address1 + ", "
                }
                if address2 != "" {
                    fullAddres += address2 + ", "
                }
                if city != "" {
                    fullAddres += city + ", "
                }
                if state != "" {
                    fullAddres += state + ", "
                }
                if country != "" {
                    fullAddres += country
                }
                
                self.addressLabel.text = fullAddres
            }
        }
    }
    
    func wraPaymentStatusCell(paymentStatus :String) {
        //let payStatus = paymentStatus as? String ?? ""
        if paymentStatus == "Paid"{
            self.paymentLbl.textColor = UIColor(red: 53.0/255.0, green: 165.0/255.0, blue: 49.0/255.0, alpha: 1.0)
        }
        else{
            //self.paymentLbl.text = paymentStatus
            self.paymentLbl.textColor = UIColor.red
        }
        self.paymentLbl.text = paymentStatus
    }
    
    func wrapNotecell(notes : [Dictionary<String, Any>],completeDict : Dictionary<String, Any>?) {
        
        blueLable1.isHidden = true
        blueLable2.isHidden = true
        blueLable3.isHidden = true
        blueLable4.isHidden = true
        blueLable5.isHidden = true
        println_debug(notes)
        
        approveStatusLabel.text = ""
        approveStatusDateLabel.text = ""
        shippingStatusLabel.text = ""
        shippingStatusDateLabel.text = ""
        deliverStatusLabel.text = ""
        deliverStatusDateLabel.text = ""
        
        if notes.count == 1 {
            
            let dateString = AppUtility.dateAndTimeOrderPlaced(timeStr: (completeDict?["CreatedOn"]as? String ?? ""))
            blueLable1.isHidden = false
            let note = notes[0]
            approveStatusLabel.text = "Order Placed".localized
            approveStatusDateLabel.text = dateString
            shippingStatusLabel.text = note["Note"] as? String ?? ""
            shippingStatusDateLabel.text =  "\(completeDict?["ExpectedDeliveryDate"]as? String ?? "") \(note["CreatedOn"] as? String ?? "")"
            shippingStatusDateLabel.text = dateString
            
        }else if notes.count == 2 {
            blueLable1.isHidden = false
            blueLable2.isHidden = false
            blueLable3.isHidden = false
            let note = notes[1]
            approveStatusLabel.text = note["Note"] as? String ?? ""
            approveStatusDateLabel.text = AppUtility.dateAndTimeOrderPlaced(timeStr: (note["CreatedOn"] as? String ?? ""))
            //note["CreatedOn"] as? String ?? ""
            let note1 = notes[0]
            shippingStatusLabel.text = note1["Note"] as? String ?? ""
            //shippingStatusDateLabel.text = note1["CreatedOn"] as? String ?? ""
            shippingStatusDateLabel.text = AppUtility.dateAndTimeOrderPlaced(timeStr: (note1["CreatedOn"] as? String ?? ""))
            
        }else if notes.count == 3 {
            blueLable1.isHidden = false
            blueLable2.isHidden = false
            blueLable3.isHidden = false
            blueLable4.isHidden = false
            blueLable5.isHidden = false
            let note = notes[2]
            approveStatusLabel.text = note["Note"] as? String ?? ""
            approveStatusDateLabel.text =  AppUtility.dateAndTimeOrderPlaced(timeStr: (note["CreatedOn"] as? String ?? ""))//note["CreatedOn"] as? String ?? ""
            
            let note1 = notes[1]
            shippingStatusLabel.text = note1["Note"] as? String ?? ""
            //shippingStatusDateLabel.text = note1["CreatedOn"] as? String ?? ""
            shippingStatusDateLabel.text = AppUtility.dateAndTimeOrderPlaced(timeStr: (note1["CreatedOn"] as? String ?? ""))
            
            let note2 = notes[0]
            deliverStatusLabel.text = note2["Note"] as? String ?? ""
            deliverStatusDateLabel.text =  AppUtility.dateAndTimeOrderPlaced(timeStr: (note2["CreatedOn"] as? String ?? ""))//note2["CreatedOn"] as? String ?? ""
        }
        
    }
    
    
    func wrapPaymentData(orders: Dictionary<String, Any>?) {
        
        
        
        
        if let order = orders {
            self.orderNumValueLable.text = order["CustomOrderNumber"].safelyWrappingString()
            self.RefNumValueLable.text = order["ReferenceNumber"].safelyWrappingString()
            // self.TaxValueLable.text = order["Tax"].safelyWrappingString()
            let ordertax = order["OrderTotalDiscount"].safelyWrappingString()
            
            if  ordertax == "0 MMK" || ordertax == "<null>"{
                self.TaxValueLable.text = ""
                self.TaxValueLable.isHidden = true
                self.TaxLable.isHidden = true
            }
            else{
                self.TaxLable.isHidden = false
                self.TaxValueLable.isHidden = false
                self.TaxValueLable.text = order["OrderTotalDiscount"].safelyWrappingString()
                self.TaxValueLable.amountAttributedString()
            }
            
            
            
            
            
            self.productAmtLabel.text = order["OrderSubtotal"].safelyWrappingString()
            self.productAmtLabel.amountAttributedString()
            //  self.shippingChargesLabel.text = order["OrderShipping"].safelyWrappingString()
            //  self.shippingChargesLabel.amountAttributedString()
            let orderShipping = order["OrderShipping"].safelyWrappingString()
            if  orderShipping == "0 MMK" {
                self.shippingChargesLabel.text = "Free".localized
                
            }
            else{
                
                self.shippingChargesLabel.text = order["OrderShipping"].safelyWrappingString()
                self.shippingChargesLabel.amountAttributedString()
            }
            
            
            self.totalPayableAmtLabel.text = order["OrderTotal"].safelyWrappingString()
            self.totalPayableAmtLabel.amountAttributedString()
            self.paymentModeLabel.text = order["PaymentMethod"].safelyWrappingString()
            
        }
    }
}

class OrderNoteCell: UITableViewCell {
    
    @IBOutlet weak var remarkLable: UILabel!{
        didSet{
            self.remarkLable.font = UIFont(name: appFont, size: 15.0)
            self.remarkLable.text = "Remarks: Final order Value / amount will be as per Qty / Weight of the products.".localized
        }
    }
}

class PDFTableCell: UITableViewCell{
    // pdf invoice outlet
    
    @IBOutlet var PDFheaderLabel: UILabel!{
        didSet {
            self.PDFheaderLabel.font = UIFont(name: appFont, size: 12.0)
            self.PDFheaderLabel.text = self.PDFheaderLabel.text?.localized
            
        }
    }
    
    @IBOutlet var PickUpAddheaderLabel: UILabel!{
        didSet {
            self.PickUpAddheaderLabel.font = UIFont(name: appFont, size: 12.0)
            self.PickUpAddheaderLabel.text = "Shipping Address" //self.PickUpAddheaderLabel.text?.localized
            
        }
    }
    
    @IBOutlet var headerLabel: UILabel!{
        didSet {
            self.headerLabel.font = UIFont(name: appFont, size: 12.0)
            self.headerLabel.text = self.headerLabel.text?.localized
            
        }
    }
    
    @IBOutlet var invoiceLabel: UILabel!{
        didSet {
            self.invoiceLabel.font = UIFont(name: appFont, size: 12.0)
            self.invoiceLabel.text = self.invoiceLabel.text?.localized
            
        }
    }
    
    @IBOutlet var orderDateLabel: UILabel!{
        didSet {
            self.orderDateLabel.font = UIFont(name: appFont, size: 12.0)
            self.orderDateLabel.text = self.orderDateLabel.text?.localized
            
        }
    }
    
    @IBOutlet var InvoiceNumLabel: UILabel!{
        didSet {
            self.InvoiceNumLabel.font = UIFont(name: appFont, size: 12.0)
            self.InvoiceNumLabel.text = self.InvoiceNumLabel.text?.localized
            
        }
    }
    
    @IBOutlet var SoldByLabel: UILabel!{
        didSet {
            self.SoldByLabel.font = UIFont(name: appFont, size: 12.0)
            self.SoldByLabel.text = self.SoldByLabel.text?.localized
            
        }
    }
    
    @IBOutlet var PickupAddLabel: UILabel!{
        didSet {
             self.PickupAddLabel.font = UIFont(name: appFont, size: 12.0)
            self.PickupAddLabel.text = self.PickupAddLabel.text?.localized
           
        }
    }
    
    
    @IBOutlet var orderNOLabel: UILabel!{
        didSet {
            self.orderNOLabel.font = UIFont(name: appFont, size: 12.0)
            self.orderNOLabel.text = self.orderNOLabel.text?.localized
            
        }
    }
    
    @IBOutlet var ProductLabel: UILabel!{
        didSet {
            self.ProductLabel.font = UIFont(name: appFont, size: 12.0)
            self.ProductLabel.text = self.ProductLabel.text?.localized
            
        }
    }
    
    @IBOutlet var QuantityLabel: UILabel!{
        didSet {
            self.QuantityLabel.font = UIFont(name: appFont, size: 12.0)
            self.QuantityLabel.text = self.QuantityLabel.text?.localized
            
        }
    }
    
    @IBOutlet var GrossAmtLabel: UILabel!{
        didSet {
            self.GrossAmtLabel.font = UIFont(name: appFont, size: 12.0)
            self.GrossAmtLabel.text = "Product Amount".localized//self.GrossAmtLabel.text?.localized
            
        }
    }
    
    @IBOutlet var DiscountLabel: UILabel!{
        didSet {
            self.DiscountLabel.font = UIFont(name: appFont, size: 12.0)
            self.DiscountLabel.isHidden = true
            self.DiscountLabel.text = self.DiscountLabel.text?.localized
            
        }
    }
    
    @IBOutlet var TotalLabel: UILabel!{
        didSet {
            
            self.TotalLabel.font = UIFont(name: appFont, size: 12.0)
            self.TotalLabel.text = self.TotalLabel.text?.localized
        }
    }
    
    @IBOutlet var orderNOValLabel: UILabel!{
        didSet {
            self.orderNOValLabel.font = UIFont(name: appFont, size: 10.0)
            self.orderNOValLabel.text = self.orderNOValLabel.text?.localized
            
        }
        
    }
    
    @IBOutlet var ProductValLabel: UILabel!{
        didSet {
            self.ProductValLabel.font = UIFont(name: appFont, size: 12.0)
            self.ProductValLabel.text = self.ProductValLabel.text?.localized
            
        }
    }
    
    @IBOutlet var QuantityValLabel: UILabel!{
        didSet {
            self.QuantityValLabel.font = UIFont(name: appFont, size: 12.0)
            self.QuantityValLabel.text = self.QuantityValLabel.text?.localized
            
        }
    }
    
    @IBOutlet var GrossAmtValLabel: UILabel!{
        didSet {
            self.GrossAmtValLabel.font = UIFont(name: appFont, size: 12.0)
            self.GrossAmtValLabel.text = self.GrossAmtValLabel.text?.localized
            
        }
    }
    
    @IBOutlet var DiscounValtLabel: UILabel!{
        didSet {
            self.DiscounValtLabel.font = UIFont(name: appFont, size: 12.0)
            self.DiscounValtLabel.text = self.DiscounValtLabel.text?.localized
            
        }
    }
    
    @IBOutlet var TotalValLabel: UILabel!{
        didSet {
            self.TotalValLabel.font = UIFont(name: appFont, size: 12.0)
            self.TotalValLabel.text = self.TotalValLabel.text?.localized
            
        }
    }
    
    @IBOutlet var TotalQuantityLabel: UILabel!{
        didSet {
            self.TotalQuantityLabel.font = UIFont(name: appFont, size: 12.0)
            self.TotalQuantityLabel.text = self.TotalQuantityLabel.text?.localized
            
        }
    }
    
    @IBOutlet var TaxLabel: UILabel!{
        didSet {
            self.TaxLabel.font = UIFont(name: appFont, size: 12.0)
            self.TaxLabel.text = self.TaxLabel.text?.localized
            
        }
    }
    
    @IBOutlet var ShippingLabel: UILabel!{
        didSet {
            self.ShippingLabel.font = UIFont(name: appFont, size: 12.0)
            self.ShippingLabel.text = self.ShippingLabel.text?.localized
            
        }
    }
    
    @IBOutlet var TotalPriceLabel: UILabel!{
        didSet {
            self.TotalPriceLabel.font = UIFont(name: appFont, size: 12.0)
            self.TotalPriceLabel.text = self.TotalPriceLabel.text?.localized
            
        }
    }
    
    @IBOutlet var Add1Label: UILabel!{
        didSet {
            self.Add1Label.font = UIFont(name: appFont, size: 12.0)
            self.Add1Label.text = self.Add1Label.text?.localized
            
        }
    }
    @IBOutlet var Add2Label: UILabel!{
        didSet {
            self.Add2Label.font = UIFont(name: appFont, size: 12.0)
            self.Add2Label.text = self.Add2Label.text?.localized
            
        }
    }
    @IBOutlet var Add3Label: UILabel!{
        didSet {
            self.Add3Label.font = UIFont(name: appFont, size: 12.0)
            self.Add3Label.text = self.Add3Label.text?.localized
            
        }
    }
    @IBOutlet var Add4Label: UILabel!{
        didSet {
            self.Add4Label.font = UIFont(name: appFont, size: 12.0)
            self.Add4Label.text = self.Add4Label.text?.localized
            
        }
    }
    @IBOutlet var Add5Label: UILabel!{
        didSet {
            self.Add5Label.font = UIFont(name: appFont, size: 12.0)
            self.Add5Label.text = self.Add5Label.text?.localized
            
        }
    }
    @IBOutlet var Add6Label: UILabel!{
        didSet {
            self.Add6Label.font = UIFont(name: appFont, size: 12.0)
            self.Add6Label.text = self.Add6Label.text?.localized
            
        }
    }
    @IBOutlet var Add7Label: UILabel!{
        didSet {
            self.Add7Label.font = UIFont(name: appFont, size: 12.0)
            self.Add7Label.text = self.Add7Label.text?.localized
            
        }
    }
    @IBOutlet var Add8Label: UILabel!{
        didSet {
             self.Add8Label.font = UIFont(name: appFont, size: 12.0)
            self.Add8Label.text = self.Add8Label.text?.localized
           
        }
    }
    
    
    func PDFInvoice(responseModel: Dictionary<String,Any>?){
        if let order = responseModel {
            //self.InvoiceNumLabel.text = "Order NO: \(order["CustomOrderNumber"].safelyWrappingString())" + "\n Invoice NO:\( order["ReferenceNumber"].safelyWrappingString())"
            self.InvoiceNumLabel.text = "Invoice NO : \(order["ReferenceNumber"].safelyWrappingString())"
            
            //CreatedOn
            //self.orderDateLabel.text = "Order Date : \(order["ExpectedDeliveryDate"].safelyWrappingString())"
            self.orderDateLabel.text = "Order Date : " + AppUtility.dateAndTimeOrderPlaced(timeStr: (order["CreatedOn"].safelyWrappingString()))
            
        }
    }
    
    
    func PDFAddress(responseModel: Dictionary<String,Any>?){
        if let order = responseModel {
             
            var address: Dictionary<String, Any>?
            
            if let pickuplocation = order["PickUpInStore"] as? Bool {
                if pickuplocation {
                    address =  order["PickupAddress"] as? Dictionary<String, Any>
                }else {
                    address =  order["BillingAddress"] as? Dictionary<String, Any>
                }
            }
             
            self.Add1Label.text =  "\("No.(265)"), \("Bogyoke Aung San Rd,"), \("Kyauktada") \("Township"), \("Yangon"), \("Myanmmar")"
             
            var address1 = (address?["Address1"] as? String ?? "")
            var address2 = (address?["Address2"] as? String ?? "")
            var city = (address?["City"] as? String ?? "")
            var state = (address?["StateProvinceName"] as? String ?? "")
            var country = (address?["CountryName"] as? String ?? "")
             
            if ((address?["HouseNo"] as? String ?? "") != "" && (address?["HouseNo"] as? String ?? "") != nil) && ((address?["RoomNo"] as? String ?? "") != "" && (address?["RoomNo"] as? String ?? "") != nil) && ((address?["FloorNo"] as? String ?? "") != "" && (address?["FloorNo"] as? String ?? "") != nil){
                
                var houseNo = (address?["HouseNo"] as? String ?? "")
                var floorNo = (address?["FloorNo"] as? String ?? "")
                var roomNo = (address?["RoomNo"] as? String ?? "")
                
                
                if houseNo != "" {
                    houseNo = ""
                    houseNo.append("House No ".localized + ": \((address?["HouseNo"] as? String ?? "").replacingOccurrences(of: "House No ", with: "").replacingOccurrences(of: "အိမ္ယာနံပါတ္", with: "").replacingOccurrences(of: "အိမ်အမှတ်", with: "") )")
                    houseNo.append(", ")
                }
                
                if roomNo != "" {
                    roomNo = ""
                    roomNo.append("Room No ".localized+": \((address?["RoomNo"] as? String ?? "").replacingOccurrences(of: "Room No ", with: "").replacingOccurrences(of: "အခန္းနံပါတ္", with: "").replacingOccurrences(of: "အခန်းအမှတ်", with: "") )")
                    roomNo.append(", ")
                }
                
                
                if floorNo != "" {
                    floorNo = ""
                    floorNo.append("Floor No ".localized + ": \((address?["FloorNo"] as? String ?? "").replacingOccurrences(of: "Floor No ", with: "").replacingOccurrences(of: "အထပ္နံပါတ္", with: "").replacingOccurrences(of: "အထပ်နံပါတ်", with: ""))")
                    floorNo.append(", ")
                    
                }
                 
                
                if address1 != "" {
                    address1 = ""
                    address1.append("\(address?["Address1"] as? String ?? "")")
                    address1.append(", ")
                }
                if address2 != "" {
                    address2 = ""
                    address2.append("\(address?["Address2"] as? String ?? "")")
                    address2.append(", ")
                }
                if city != "" {
                    city = ""
                    city.append("\(address?["City"] as? String ?? "")")
                    city.append(", ")
                }
                if state != "" {
                    state = ""
                    state.append("\(address?["StateProvinceName"] as? String ?? "")")
                }
                
                if country != "" {
                    country = ""
                    country.append("\(address?["CountryName"] as? String ?? "")")
                }
                
                
                self.Add5Label.text = "\(houseNo)\(roomNo)\(floorNo)\n\(address1)\(address2)\n\(city)\(state), \(country)"
                
                
            }else{
                self.Add5Label.text = "\(address1)\(address2)\n\(city)\(state), \(country)"
            }
             
        }
    }
    
    func PDFOrderDetail(responseModel: Dictionary<String,Any>?, completeDict: Dictionary<String,Any>?){
        self.orderNOValLabel.text = (completeDict!["CustomOrderNumber"].safelyWrappingString())
        if let order = responseModel {
            
            var qty: String?
            var grossamount: String?
            //            if let obj = order["Items"] as? [Dictionary<String,Any>]{
            //                self.TotalQuantityLabel.text = "Total QUANTITY : \((obj[0]["Quantity"].safelyWrappingString()))"
            self.QuantityValLabel.text = order["Quantity"].safelyWrappingString()
            
            self.GrossAmtValLabel.text = order["UnitPrice"].safelyWrappingString()
            qty = order["Quantity"].safelyWrappingString()
            grossamount = self.GrossAmtValLabel.text?.replacingOccurrences(of: " MMK", with: "")
            self.GrossAmtValLabel.amountAttributedString()
            //  self.DiscounValtLabel.text = "\(order["DiscountAmount"] ?? "0")"
            //  self.DiscounValtLabel.amountAttributedString()
            self.ProductValLabel.text = order["ProductName"].safelyWrappingString()
            //            }
            //  let discountamount = ((order["DiscountAmount"].safelyWrappingString().replacingOccurrences(of: " MMK", with: "").replacingOccurrences(of: ",", with: "")as NSString).integerValue)
            let qtyNum = qty?.numberValue?.intValue
            let grossamt = grossamount?.numberValue?.intValue
            let multi = (qtyNum! * grossamt!) //- discountamount
            
            //            self.TotalValLabel.text =  "\(String(multi)) MMK"
            //            self.TotalValLabel.amountAttributedString()
            
            
            
            
            let totVal = multi
            let largeNumber = Int(String(format: "\(totVal)"))
            let numberFormatter = NumberFormatter()
            numberFormatter.numberStyle = .decimal
            if let valueOne = largeNumber{
                let formattedNumber = numberFormatter.string(from: NSNumber(value:valueOne))
                //print("*****************")
                //  print(formattedNumber)
                if let hasValue = formattedNumber{
                    
                    print("\(hasValue) MMK")
                    
                    self.TotalValLabel.text = "\(hasValue) MMK"
                    self.TotalValLabel.amountAttributedString()
                    
                }else{
                    print("0.0 MMK")
                }
            }
            
            
            
            
            
        }
    }
    
    
    func PDFwrapTotalData(responseModel: Dictionary<String,Any>?){
        if let order = responseModel {
            //            let taxData = order["Tax"].safelyWrappingString()
            let taxData = order["OrderTotalDiscount"].safelyWrappingString()
            
            if  taxData == "0 MMK" || taxData == "<null>"{
                
                self.TaxLabel.text = ""
                self.TaxLabel.isHidden = true
            }
            else{
                self.TaxLabel.isHidden = false
                if taxData.count > 0 {
                    //                    self.TaxLabel.text = "Tax: \(order["Tax"].safelyWrappingString())"
                    
                    self.TaxLabel.text = "Discount: \(order["OrderTotalDiscount"].safelyWrappingString())"
                    
                }
                self.TaxLabel.amountAttributedString()
            }
            //self.TaxLabel.text = "Tax: \(order["Tax"].safelyWrappingString())"
            
            //            self.TaxLabel.text = "Discount: \(order["OrderSubTotalDiscount"].safelyWrappingString())"
            
            self.ShippingLabel.text = "Shipping Charges : \(order["OrderShipping"].safelyWrappingString())"
            self.ShippingLabel.amountAttributedString()
            
            if let obj = order["Items"] as? [Dictionary<String,Any>]{
                var totalQuantity = 0
                var discountAmt = 0
                for k in obj{
                    totalQuantity += Int(k["Quantity"].safelyWrappingString())!
                    discountAmt += ((k["DiscountAmount"].safelyWrappingString().replacingOccurrences(of: " MMK", with: "").replacingOccurrences(of: ",", with: "")as NSString).integerValue)
                }
                //                self.TotalQuantityLabel.text = "Total QUANTITY : \((obj[0]["Quantity"].safelyWrappingString()))"
                self.TotalQuantityLabel.text = "TOTAL QUANTITY : \((totalQuantity))"
                
                
                
                
                let totVal = ((((order["OrderTotal"].safelyWrappingString().replacingOccurrences(of: " MMK", with: "").replacingOccurrences(of: ",", with: "")as NSString).integerValue)+((order["OrderShipping"].safelyWrappingString().replacingOccurrences(of: " MMK", with: "").replacingOccurrences(of: ",", with: "")as NSString).integerValue)+((order["Tax"].safelyWrappingString().replacingOccurrences(of: " MMK", with: "").replacingOccurrences(of: ",", with: "")as NSString).integerValue))-discountAmt)
                let largeNumber = Int(String(format: "\(totVal)"))
                let numberFormatter = NumberFormatter()
                numberFormatter.numberStyle = .decimal
                if let valueOne = largeNumber{
                    let formattedNumber = numberFormatter.string(from: NSNumber(value:valueOne))
                    //print("*****************")
                    //  print(formattedNumber)
                    if let hasValue = formattedNumber{
                        
                        print("\(hasValue) MMK")
                        
                        self.TotalPriceLabel.text = "TOTAL PRICE : \(hasValue) MMK"
                        self.TotalPriceLabel.amountAttributedString()
                        
                    }else{
                        print("0.0 MMK")
                    }
                }
            }
        }
    }
    
    func PDFwrapPaymentTotalData(responseModel: [Dictionary<String,Any>], allData: Dictionary<String,Any>){
        //if let order = responseModel {
        
        //        let taxData = allData["TotalTax"].safelyWrappingString()
        
        let taxData = allData["DiscountAmount"].safelyWrappingString()
        
        if  taxData == "0 MMK" || taxData == "<null>"{
            
            self.TaxLabel.text = ""
            self.TaxLabel.isHidden = true
        }
        else{
            self.TaxLabel.isHidden = false
            if taxData.count > 0 {
                //                self.TaxLabel.text = "Tax: \(allData["TotalTax"].safelyWrappingString())"
                self.TaxLabel.text = "Discount: \(allData["DiscountAmount"].safelyWrappingString())"
            }
            self.TaxLabel.amountAttributedString()
        }
        //self.TaxLabel.text = "Tax: \(order["Tax"].safelyWrappingString())"
        
        //         self.TaxLabel.text = "Discount: \(allData["DiscountAmount"].safelyWrappingString())"
        
        self.ShippingLabel.text = "Shipping Charges : \(allData["ShippingCharges"].safelyWrappingString())"
        self.ShippingLabel.amountAttributedString()
        
        self.TotalPriceLabel.text = "TOTAL PRICE : \(allData["TotalPaidAmount"].safelyWrappingString())"
        
        var totalQuantity = 0
        
        for allOrderObj in responseModel {
            if let obj = allOrderObj["Items"] as? [Dictionary<String,Any>]{
                //var discountAmt = 0
                for k in obj{
                    totalQuantity += Int(k["Quantity"].safelyWrappingString())!
                    //discountAmt += ((k["DiscountAmount"].safelyWrappingString().replacingOccurrences(of: " MMK", with: "").replacingOccurrences(of: ",", with: "")as NSString).integerValue)
                }
            }
        }
        //                self.TotalQuantityLabel.text = "Total QUANTITY : \((obj[0]["Quantity"].safelyWrappingString()))"
        self.TotalQuantityLabel.text = "TOTAL QUANTITY : \((totalQuantity))"
        //self.TotalPriceLabel.text = "TOTAL PRICE : \((((order["OrderSubtotal"].safelyWrappingString().replacingOccurrences(of: " MMK", with: "").replacingOccurrences(of: ",", with: "")as NSString).integerValue)+((order["OrderShipping"].safelyWrappingString().replacingOccurrences(of: " MMK", with: "").replacingOccurrences(of: ",", with: "")as NSString).integerValue)+((order["Tax"].safelyWrappingString().replacingOccurrences(of: " MMK", with: "").replacingOccurrences(of: ",", with: "")as NSString).integerValue))-discountAmt) MMK"
        //self.TotalPriceLabel.amountAttributedString()
        //}
        //}
    }
    
    
    /*
     func loadPDFViewContainer(responseModel: Dictionary<String, Any>?){
     
     if let order = responseModel {
     self.InvoiceNumLabel.text = "Order NO: \(order["CustomOrderNumber"].safelyWrappingString())" + "\n Invoice NO:\( order["ReferenceNumber"].safelyWrappingString())"
     self.TaxLabel.text = "Tax: \(order["Tax"].safelyWrappingString())"
     self.TaxLabel.amountAttributedString()
     self.ShippingLabel.text = "Shipping Charges : \(order["OrderShipping"].safelyWrappingString())"
     self.ShippingLabel.amountAttributedString()
     self.TotalPriceLabel.text = "TOTAL PRICE : \(order["OrderTotal"].safelyWrappingString())"
     self.TotalPriceLabel.amountAttributedString()
     
     self.orderNOValLabel.text = "\(order["CustomOrderNumber"].safelyWrappingString())"
     self.orderDateLabel.text = "Order Date :\(order["ExpectedDeliveryDate"].safelyWrappingString())"
     
     
     var qty: String?
     var grossamount: String?
     if let obj = order["Items"] as? [Dictionary<String,Any>]{
     self.TotalQuantityLabel.text = "Total QUANTITY : \((obj[0]["Quantity"].safelyWrappingString()))"
     self.QuantityValLabel.text = obj[0]["Quantity"].safelyWrappingString()
     self.GrossAmtValLabel.text = obj[0]["UnitPrice"].safelyWrappingString()
     qty = obj[0]["Quantity"].safelyWrappingString()
     grossamount = self.GrossAmtValLabel.text?.replacingOccurrences(of: " MMK", with: "")
     self.DiscounValtLabel.text = "\(obj[0]["DiscountAmount"] ?? "0")"
     self.ProductValLabel.text = obj[0]["ProductName"].safelyWrappingString()
     }
     
     
     let qtyNum = qty?.numberValue?.intValue
     let grossamt = grossamount?.numberValue?.intValue
     let multi = qtyNum! * grossamt!
     
     
     self.TotalValLabel.text =  "\(String(multi))MMK"
     self.TotalValLabel.amountAttributedString()
     
     if let dict = order["PickupAddress"] as? Dictionary<String, Any>{
     self.Add5Label.text =  "\(dict["HouseNo"] as? String ?? ""),\(dict["FloorNo"] as? String ?? ""),\(dict["RoomNo"] as? String ?? ""),\(dict["Address1"] as? String ?? ""),\(dict["Address2"] as? String ?? ""),\(dict["City"] as? String ?? ""),\(dict["StateProvinceName"] as? String ?? ""),\(dict["CountryName"] as? String ?? "")"
     }
     if let dictObj = order["ShippingAddress"] as? Dictionary<String,Any>{
     self.Add1Label.text =  "\(dictObj["HouseNo"] as? String ?? ""),\(dictObj["FloorNo"] as? String ?? ""),\(dictObj["RoomNo"] as? String ?? ""),\(dictObj["Address1"] as? String ?? ""),\(dictObj["Address2"] as? String ?? ""),\(dictObj["City"] as? String ?? ""),\(dictObj["StateProvinceName"] as? String ?? ""),\(dictObj["CountryName"] as? String ?? "")"
     }
     }
     }
     */
    
}
