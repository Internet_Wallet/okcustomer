//
//  OKPaymentWebViewController.swift
//  OneStopMart
//
//  Created by iMac on 08/05/2020.
//  Copyright © 2020 Shobhit. All rights reserved.
//

import UIKit

import WebKit
import CryptoSwift
//import SwiftSoup



//MARK: step 1 Add Protocol here.
protocol OKPaymentWebViewDelegate: class {
    typealias Item = (text: String, html: String)
    func success(response:[String: String])
}


class OKPaymentWebViewController: MartBaseViewController {
    
    typealias Item = (text: String, html: String)
    // current document
//    var document: Document = Document.init("")
    // item founds
    var items: [Item] = []
    var ResponseDict = [String: String]()
    weak var OKPaymentDelegate: OKPaymentWebViewDelegate?
    var dict = [String: String]()
    
    @IBOutlet var paymentWebView: WKWebView!{
        didSet{
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configPaymentWebView()
        // Do any additional setup after loading the view.
    }
    
}


extension OKPaymentWebViewController: WKNavigationDelegate, WKUIDelegate{
    
    
    func configPaymentWebView(){
        
        paymentWebView.navigationDelegate = self
        paymentWebView.uiDelegate = self
        paymentWebView.scrollView.bounces = false
        //Adding observer for show loading indicator
        self.paymentWebView.addObserver(self, forKeyPath:#keyPath(WKWebView.isLoading), options: .new, context: nil)
        
        
        var request = URLRequest(url: URL(string: APIManagerClient.sharedInstance.OKPayment)!)
        request.httpMethod = "POST"
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        
        let jsonData = try! JSONSerialization.data(withJSONObject: postString(), options: .prettyPrinted)
        let decoded = String(data: jsonData, encoding: .utf8)!
        let encryptedValue = try! decoded.aesEncrypt(key: APIManagerClient.sharedInstance.paymentSecretKey)
        guard let postString = (encryptedValue + "," + APIManagerClient.sharedInstance.paymentIValue + "," + dict.safeValueForKey("Destination")!) as? String else { return }
        let idNew =  "requestToJson=\(postString)".replacingOccurrences(of: "+", with: "%2B")
        request.httpBody = idNew.data(using: .utf8)
        paymentWebView.load(request)
        
    }
    
    fileprivate func postString()-> NSMutableDictionary{
        let dic = NSMutableDictionary()
        dic.setValue(dict.safeValueForKey("Destination"), forKey: "Destination")
        dic.setValue((dict.safeValueForKey("totalAmount")?.replacingOccurrences(of: " MMK", with: "").replacingOccurrences(of: ",", with: "")), forKey: "Amount")
        //self.getTheCorrectFormatedMobile()
        //dic.setValue(1.0, forKey: "Amount")
        dic.setValue(VMLoginModel.shared.mobileNumber, forKey: "Source")
        dic.setValue(APIManagerClient.sharedInstance.paymentAPIKey, forKey: "ApiKey")
        dic.setValue("Vmart", forKey: "MerchantName")
        dic.setValue("", forKey: "Comments")
        dic.setValue("", forKey: "TelcotopupNumber")
        dic.setValue(gettransID() as? String, forKey: "RefNumber")
        return dic
    }
    
    func gettransID() -> String {
        let tenDigit = Date().toMillis()
        return "\(tenDigit ?? 323232232323232)"
    }
    
    func getTheCorrectFormatedMobile()-> String {
        var modified = ""
        let mobile = VMLoginModel.shared.mobileNumber as? String
        let indexChar = mobile?[4]
        if indexChar == "0" {
           // mobile?.replacingOccurrences(of: <#T##StringProtocol#>, with: <#T##StringProtocol#>, options: <#T##String.CompareOptions#>, range: <#T##Range<String.Index>?#>)
        }
        else {
            modified = mobile!
        }
        return modified
    }
    
    
    
    @IBAction func refereshAction(_ sender: Any) {
        paymentWebView.reload()
    }
    
    @IBAction func backAction(_ sender: Any) {
        
         self.dismiss(animated: true, completion: nil)
//        if paymentWebView.canGoBack {
//            paymentWebView.goBack()
//        } else {
//            self.dismiss(animated: true, completion: nil)
//        }
        
    }
    
    
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        print("Started to load")
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("Finished loading")
        if self.ResponseDict.count != 0{
            self.dismiss(animated: true, completion: {
                if let del = self.OKPaymentDelegate {
                    del.success(response: self.ResponseDict)
                }
            })
        }
        
        
    }
    
    
    //MARK: - ActivityIndicator StartAnimate And StopAnimate
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "loading"{
            if paymentWebView.isLoading{
                AppUtility.showLoading(self.view)
            }else{
                AppUtility.hideLoading(self.view)
            }
        }
    }
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        
        AppUtility.showToastlocal(message: error.localizedDescription, view: self.view)
        print(error.localizedDescription)
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: (WKNavigationActionPolicy) -> Void) {
        if let urlStr = navigationAction.request.url?.absoluteString{
            //urlStr is what you want, I guess.
            print(urlStr)
            if urlStr.contains(find: "OKDollarResponse/OKDollarResponse") {
                if let url = URL(string: urlStr){
                    print(url.queryDictionary ?? "NONE")
                    if let response = url.queryDictionary{
                        self.ResponseDict = response
                    }
                }
            }
           else if urlStr.contains(find: "/Plugins/PaymentOKDollar/PDTHandler"){
                print("payment page after the complition of payment with status.")
            }
            else{
                if urlStr.contains("http://www.okdollar.com/contact_us.php") {
                    print("try again")
                    AppUtility.showToastlocal(message: AppConstants.PaymentFailed.message, view: self.view)
                }
                if urlStr.contains("PayMentReceive.aspx") {
                    self.dismiss(animated: true, completion: {
                        //                    if let del = self.OKPaymentDelegate {
                        // start first request
                        //                        self.downloadHTML(url: urlStr)
                        //                        del.success(response: self.items)
                        //                    }
                    })
                }
                else {
//                    AppUtility.showToastlocal(message: AppConstants.PaymentFailed.message, view: self.view)
                }
            }
        }
        decisionHandler(.allow)
    }
}

//extension OKPaymentWebViewController{
//    //Download HTML
//    func downloadHTML(url:String) {
//        // url string to URL
//        guard let url = URL(string: url) else {
//            // an error occurred
//            AppUtility.showToastlocal(message: "Error: \("url") doesn't seem to be a valid URL", view: self.view)
//            return
//        }
//
//        do {
//            // content of url
//            let html = try String.init(contentsOf: url)
//            // parse it into a Document
//            document = try SwiftSoup.parse(html)
//            // parse css query
//            parse()
//        } catch let error {
//            // an error occurred
//
//            AppUtility.showToastlocal(message: "Error: \(error)", view: self.view)
//        }
//
//    }
//
//
//    //Parse CSS selector
//    func parse() {
//        do {
//            //empty old items
//            items = []
//            // firn css selector
//            let elements: Elements = try document.select("div")
//            //transform it into a local object (Item)
//            for element in elements {
//                let text = try element.text()
//                let html = try element.outerHtml()
//                items.append(Item(text: text, html: html))
//            }
//
//        } catch let error {
//            AppUtility.showToastlocal(message: "Error: \(error)", view: self.view)
//        }
//    }
//}
