//
//  FeatureCategoryTableViewController.swift
//  Merchant_Demo
//
//  Created by yan feng liu on 3/10/18.
//  Copyright © 2018 2C2P. All rights reserved.
//

import UIKit

class FeatureCategoryTableViewController: UITableViewController {
    
    var sdkBuildType:SDKBuildType = SDKBuildType.CORE_UI_SDK
    private var featureList:[Feature] = [Feature]()
    private var featureCategoryHelper:FeatureCategoryHelper!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = self.sdkBuildType.rawValue
        
        self.featureCategoryHelper = FeatureCategoryHelper()
        
        self.initData()
    }
    
    private func initData() {
        
        self.featureList.append(contentsOf: self.featureCategoryHelper.getList(sdkBuildType: self.sdkBuildType))
    }

}

//MARK: - UITableViewDataSource & UITableViewDelegate
extension FeatureCategoryTableViewController {
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.featureList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:FeatureTableViewCell = tableView.dequeueReusableCell(withIdentifier: "FeatureTableViewCell") as! FeatureTableViewCell
        
        cell.delegate = self
        
        cell.icon.image = UIImage(named: self.featureList[indexPath.row].icon)
        cell.name.text = self.featureList[indexPath.row].name
        cell.desc.text = self.featureList[indexPath.row].description
        
        return cell
    }
}

//MARK: - FeatureTableViewCellDelegate
extension FeatureCategoryTableViewController:FeatureTableViewCellDelegate {
    func didSelectFeatureTableViewCell(cell: FeatureTableViewCell) {
        
        guard let itemPosition:Int = self.tableView.indexPath(for: cell)?.row else { return }
        
        self.featureCategoryHelper.redirect(viewController: self, feature: self.featureList[itemPosition])
    }
}
