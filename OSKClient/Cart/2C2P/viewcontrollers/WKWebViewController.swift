//
//  WKWebViewController.swift
//  Merchant_Demo
//
//  Created by yan feng liu on 5/10/18.
//  Copyright © 2018 2C2P. All rights reserved.
//

import UIKit
import WebKit
import PGW

class WKWebViewController: UIViewController {
    
    /**
     * Reference : https://developer.2c2p.com/docs/mobile-v4-3ds-or-non-3ds#step-5
     */
    
    var webView:WKWebView!
    var pgwWebViewDelegate:PGWWKWebViewDelegate!
    var redirectUrl:String?
    
    private var merchantServerSimulator:MerchantServerSimulator!
    private var progressDialog:ProgressDialog!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.merchantServerSimulator = MerchantServerSimulator()
        self.progressDialog = ProgressDialog.shared
        
        //Step 5: Authentication handling for 3DS payment.
        let requestUrl:URL = URL.init(string: self.redirectUrl!)!
        let request:URLRequest = URLRequest.init(url: requestUrl)
        
        let webConfiguration = WKWebViewConfiguration()
        self.webView = WKWebView(frame: UIScreen.main.bounds, configuration: webConfiguration)
        self.webView.navigationDelegate = self.transactionResultCallback()
        self.webView.load(request)
        
        self.view.addSubview(self.webView)
    }
    
    func transactionResultCallback() -> PGWWKWebViewDelegate {
        
        self.pgwWebViewDelegate = PGWWKWebViewDelegate(success: { (response: TransactionResultResponse) in
            
            if response.responseCode == APIResponseCode.TRANSACTION_COMPLETED {
                
                //Inquiry payment result by using transaction id.
                self.inquiry(transactionID: response.transactionID!)
            } else {
                
                //Get error response and display error
                print("\(type(of: self)) \(String(describing: response.responseCode)) :: \(String(describing: response.responseDescription))")
            }
        }, failure: { (error: NSError) in
            
            //Get error response and display error
            print("\(type(of: self)) \(error.domain)")
        })
        
        return self.pgwWebViewDelegate
    }
    
    /**
     * Build for payment inquiry request
     *
     * @param transactionID
     * @return
     */
    func buildPaymentInquiry(transactionID:String) -> PaymentInquiryRequest {
        
        //Construct payment inquiry request
        let request:PaymentInquiryRequest = PaymentInquiryRequest()
        request.transactionID = transactionID
        
        return request
    }
    
    /**
     * For Non-3DS
     * @param transactionID
     */
    func inquiry(transactionID: String) {
        
        self.progressDialog.message = Constants.common_message_payment_result_message
        self.progressDialog.showLoadingIndicator()
        
        //Step 6: Get payment result.
        self.merchantServerSimulator.inquiryPaymentResult(paymentInquiryRequest: self.buildPaymentInquiry(transactionID: transactionID), success: { (response:String) in
            
            self.progressDialog.hideLoadingIndicator()
            
            self.displayResult(response)
        }) { (error:NSError) in
            
            self.progressDialog.hideLoadingIndicator()
            
            print("\(type(of: self)) \(error.domain)")
        }
    }
    
    func displayResult(_ response:String) {
        
        let storyboard = UIStoryboard(name: "Cart", bundle: nil)
        let transactionResultVC:PaymentSuccess =
            storyboard.instantiateViewController(withIdentifier: "PaymentSuccess_ID") as! PaymentSuccess
        //        transactionResultVC.paymentResponse
        transactionResultVC.paymentInquiryResponse = response
        
        self.navigationController?.pushViewController(transactionResultVC, animated: true)
    }
}
