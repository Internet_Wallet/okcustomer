//
//  BasePayment.swift
//  Merchant_Demo
//
//  Created by yan feng liu on 3/10/18.
//  Copyright © 2018 2C2P. All rights reserved.
//

import UIKit

class BasePayment:NSObject {
        
    func buildPaymentToken() -> PaymentTokenRequest {
        return PaymentTokenRequest()
    }

    func execute() {}
    
    func submit(paymentToken:String) {}
    
    func inquiry(transactionID:String) {}
    
    var merchantServerSimulator:MerchantServerSimulator
    var stringHelper:StringHelper
    var viewController:UIViewController
    var progressDialog:ProgressDialog
    
    init(viewController vc:UIViewController) {
        
        viewController = vc
        merchantServerSimulator = MerchantServerSimulator()
        stringHelper = StringHelper()
        
        progressDialog = ProgressDialog.shared
    }
    
    /**
     * Build for payment inquiry request
     *
     * @param transactionID
     * @return
     */
    func buildPaymentInquiry(transactionID:String) -> PaymentInquiryRequest {
        
        //Construct payment inquiry request
        let request:PaymentInquiryRequest = PaymentInquiryRequest()
        request.transactionID = transactionID
        
        return request
    }
    
    func displayResult(_ response:String) {
        
        let storyboard = UIStoryboard(name: "Cart", bundle: nil)
        let transactionResultVC:PaymentSuccess =
            storyboard.instantiateViewController(withIdentifier: "PaymentSuccess_ID") as! PaymentSuccess
        //        transactionResultVC.paymentResponse
        transactionResultVC.paymentInquiryResponse = response
        
        viewController.navigationController?.pushViewController(transactionResultVC, animated: true)
    }
}
