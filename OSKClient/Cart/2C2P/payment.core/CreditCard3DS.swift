//
//  CreditCard3DS.swift
//  Merchant_Demo
//
//  Created by yan feng liu on 5/10/18.
//  Copyright © 2018 2C2P. All rights reserved.
//

import Foundation
import UIKit
import PGW

class CreditCard3DS: BasePayment {
    
    /**
     * Reference : https://developer.2c2p.com/docs/mobile-v4-3ds-or-non-3ds
     *             https://developer.2c2p.com/docs/mobile-v4-api-parameters#credit-card-payment-builder
     */
    
    override func buildPaymentToken() -> PaymentTokenRequest {
        
        //Construct payment token request
        let request:PaymentTokenRequest = PaymentTokenRequest()
        request.paymentChannel = "CC"
        
        //Enable / Disable / Force 3DS
        request.request3DS = "Y"
        
        return request
    }
    
    override func execute() {
        
        self.progressDialog.message = Constants.common_message_payment_token_message
        self.progressDialog.showLoadingIndicator()
        
        //Step 1 : Get payment token
        self.merchantServerSimulator.getPaymentToken(paymentTokenRequest: self.buildPaymentToken(), success: { (response:String) in
            
            self.progressDialog.hideLoadingIndicator()
            
            //Submit payment
            self.submit(paymentToken: self.stringHelper.extractPaymentToken(response: response))
        }) { (error:NSError) in
            
            self.progressDialog.hideLoadingIndicator()
            
            print("\(type(of: self)) \(error.domain)")
        }
    }
    
    func getSubView() -> String{
        for view in self.viewController.view.subviews  {
            print("view \(view)")
            for stv in  view.subviews  {
                print("stv \(stv)")
                for viewObject in stv.subviews{
                    if let tf = viewObject.viewWithTag(2) as? UITextField{
                        //                        return (tf.text!)
                        print(tf.text!)
                    }
                    else if let stack = viewObject.viewWithTag(1) as? UIStackView{
                        for viewobj in stack.subviews{
                            
                            if let tf = viewobj.viewWithTag(1) as? UITextField{
                                //                                    return (tf.text!)
                                print(tf.text!)
                            }
                            else  if let tf = viewobj.viewWithTag(2) as? UITextField{
                                //                                    return (tf.text!)
                                print(tf.text!)
                            }
                        }
                    }
                    else if let stack = viewObject.viewWithTag(2) as? UIStackView{
                        for viewobj in stack.subviews{
                            
                            if let tf = viewobj.viewWithTag(3) as? UITextField{
                                //                                    return (tf.text!)
                                print(tf.text!)
                            }
                            
                        }
                    }
                }
            }
        }
        return ""
    }
    
    
    
    
    
    override func submit(paymentToken: String) {
        
        self.progressDialog.message = Constants.common_message_payment_processing_message
        self.progressDialog.showLoadingIndicator()
        print(self.viewController)
        self.getSubView()
        //Step 2: Construct credit card request.   card Number : "4111111111111111" self.getSubView().replacingOccurrences(of: " ", with: "")
        let creditCardPayment:CreditCardPayment = CreditCardPaymentBuilder(pan: "4111111111111111")
            .expiryMonth(12)
            .expiryYear(2019)
            .securityCode("123")
            .build()
        
        //Step 3: Construct transaction request.
        let transactionRequest:TransactionRequest = TransactionRequestBuilder(paymentToken: paymentToken)
            .withCreditCardPayment(creditCardPayment)
            .build()
        
        //Step 4: Execute payment request.
        PGWSDK.shared.proceedTransaction(transactionRequest: transactionRequest, success: { (response:TransactionResultResponse) in
            
            self.progressDialog.hideLoadingIndicator()
            
            //For 3DS
            if response.responseCode == APIResponseCode.TRANSACTION_AUTHENTICATE {
                
                guard let redirectUrl:String = response.redirectUrl else { return }
                self.openAuthentication(redirectUrl: redirectUrl) //Open WebView for 3DS
            } else if response.responseCode == APIResponseCode.TRANSACTION_COMPLETED {
                
                //Inquiry payment result by using transaction id.
                self.inquiry(transactionID: response.transactionID!)
            } else {
                
                //Get error response and display error
                print("\(type(of: self)) \(String(describing: response.responseCode)) :: \(String(describing: response.responseDescription))")
            }
        }) { (error:NSError) in
            
            self.progressDialog.hideLoadingIndicator()
            
            //Get error response and display error
            print("\(type(of: self)) \(error.domain)")
        }
    }
    
    /**
     * For Non-3DS
     * @param transactionID
     */
    override func inquiry(transactionID: String) {
        
        self.progressDialog.message = Constants.common_message_payment_result_message
        self.progressDialog.showLoadingIndicator()
        
        //Step 6: Get payment result.
        self.merchantServerSimulator.inquiryPaymentResult(paymentInquiryRequest: self.buildPaymentInquiry(transactionID: transactionID), success: { (response:String) in
            
            self.progressDialog.hideLoadingIndicator()
            
            self.displayResult(response)
        }) { (error:NSError) in
            
            self.progressDialog.hideLoadingIndicator()
            
            print("\(type(of: self)) \(error.domain)")
        }
    }
    
    /**
     * For Open 3DS authentication
     * @param redirectUrl
     * SDK support UIWebView & WKWebView
     * Reference : https://developer.2c2p.com/docs/mobile-v4-3ds-or-non-3ds#step-5
     */
    private func openAuthentication(redirectUrl:String) {
        
        let wkWebVC:WKWebViewController = WKWebViewController()
        wkWebVC.redirectUrl = redirectUrl
        wkWebVC.navigationItem.title = self.viewController.navigationItem.title
        self.viewController.navigationController?.pushViewController(wkWebVC, animated: true)
    }
}
