//
//  Feature.swift
//  Merchant_Demo
//
//  Created by yan feng liu on 1/10/18.
//  Copyright © 2018 2C2P. All rights reserved.
//

import Foundation

class Feature {
    
    var id:Int
    var icon:String
    var name:String
    var description:String
    
    init(id:Int, icon:String, name:String, desc:String) {
        
        self.id = id
        self.icon = icon
        self.name = name
        self.description = desc
    }
}
