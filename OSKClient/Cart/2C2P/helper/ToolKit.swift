//
//  ToolKit.swift
//  PGWSDK
//
//  Created by yan feng liu on 8/6/18.
//  Copyright © 2018 My2c2p. All rights reserved.
//

import Foundation

class ToolKit {

}

//JSON <--> Dictionary
extension ToolKit {
    
    class func base64DecodedString(encodedString:String) -> String? {
        
        guard let encodedData:Data = encodedString.data(using: String.Encoding.utf8) else { return nil }
        
        guard let decodedData = Data.init(base64Encoded: encodedData, options: Data.Base64DecodingOptions(rawValue: 0)) else { return nil }
        
        guard let decodedStr:String = String.init(data: decodedData, encoding: String.Encoding.utf8) else { return nil }
                
        return decodedStr
    }
    
    class func dicFrom(jsonString:String) -> [String:Any]? {
        
        do {
            guard let jsonData:Data = jsonString.data(using: String.Encoding.utf8) else {return nil}
            
            let dic:[String:Any] = try JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as! [String : Any]
            return dic
        } catch {
            return nil
        }
    }
    
    class func jsonStringFrom(dic:[String:Any]) -> String? {
        do {
            let jsonData:Data = try JSONSerialization.data(withJSONObject: dic, options: .init(rawValue: 0))
            
            guard let jsonStr:String = String.init(data: jsonData, encoding: String.Encoding.utf8) else { return nil }
                        
            return jsonStr
        } catch {
            return nil
        }
    }
    
    class func prettyPrintJsonString(jsonStr:String) -> String {
        var newStr:String = ""
        //add head as "{"
        newStr.append("{")
        newStr.append("\n")
        
        //separate string by ","
        var stripedStr:String = jsonStr
        stripedStr.removeFirst()
        stripedStr.removeLast()
        let components:[String] = stripedStr.components(separatedBy: ",")
        for item in components {
            newStr.append("\t")
            newStr.append(item)
            newStr.append(",")
            newStr.append("\n")
        }
        
        //remove redundant string
        newStr.removeLast(2)
        
        //add tail as "}"
        newStr.append("\n")
        newStr.append("}")
        
        return newStr
    }
}
