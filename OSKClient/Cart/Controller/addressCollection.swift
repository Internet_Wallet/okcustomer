//
//  addressCollection.swift
//  VMart
//
//  Created by ANTONY on 26/10/2018.
//  Copyright © 2018 Shobhit. All rights reserved.
//

import UIKit

protocol AddBillingAddressDelegate: class {
    func AddressTapped(indexPath: IndexPath)
    func addBillingAddressTapped(indexPath: IndexPath,count:Int)
}

class addressCollection: UIViewController {
    @IBOutlet weak var addressCollectionView: UICollectionView!
    @IBOutlet var selectAddLbl: UILabel!{
        didSet {
            self.selectAddLbl.font = UIFont(name: appFont, size: 15.0)
            self.selectAddLbl.text = self.selectAddLbl.text?.localized
            
        }
    }
    
    var allBillingAddresses: [GenericBillingAddress]?
    weak var delegate: AddBillingAddressDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.applyNavigationGradient(colors: [UIColor.colorWithRedValue(redValue: 13, greenValue: 177, blueValue: 75, alpha: 1), UIColor.init(red: 20.0/255.0, green: 185.0/255.0, blue: 85.0/255.0, alpha: 1.0)])
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.addressCollectionView.delegate = self
            self.addressCollectionView.dataSource = self
            self.addressCollectionView.animate()
        }
    }
    
    @IBAction func backAction(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func dismissTapAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        UIView.animate(withDuration: 0.20, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0
        }, completion:{(finished : Bool)  in
            if (finished) {
                self.dismiss(animated: true, completion: nil)
            }
        })
    }
}


class AddressCollectionViewCell: UICollectionViewCell {
    @IBOutlet var nameLabel: UILabel! {
        didSet {
            self.nameLabel.font = UIFont(name: appFont, size: 15.0)
            self.nameLabel.text = self.nameLabel.text?.localized
            
        }
    }
    @IBOutlet var address1Label: UILabel! {
        didSet {
            self.address1Label.font = UIFont(name: appFont, size: 15.0)
            self.address1Label.text = self.address1Label.text?.localized
            
        }
    }
    @IBOutlet var address2Label: UILabel! {
        didSet {
            self.address2Label.font = UIFont(name: appFont, size: 15.0)
            self.address2Label.text = self.address2Label.text?.localized
            
        }
    }
    @IBOutlet var cityLabel: UILabel! {
        didSet {
            self.cityLabel.font = UIFont(name: appFont, size: 15.0)
            self.cityLabel.text = self.cityLabel.text?.localized
            
        }
    }
    @IBOutlet var countryLabel: UILabel! {
        didSet {
            self.countryLabel.font = UIFont(name: appFont, size: 15.0)
            self.countryLabel.text = self.countryLabel.text?.localized
            
        }
    }
    @IBOutlet var phNumLabel: UILabel! {
        didSet {
            self.phNumLabel.font = UIFont(name: appFont, size: 15.0)
            self.phNumLabel.text = self.phNumLabel.text?.localized
            
        }
    }
    @IBOutlet var addAddressLbl: UILabel! {
        didSet {
            self.addAddressLbl.font = UIFont(name: appFont, size: 15.0)
            self.addAddressLbl.text = self.addAddressLbl.text?.localized
            
        }
    }
    
    @IBOutlet var addAddressBorderView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    private func clearAll() {
        nameLabel.text = ""
        countryLabel.text = ""
        phNumLabel.text = ""
    }
    
    func wrapdata(addressObject: GenericBillingAddress?) {
        clearAll()
        if let address = addressObject {
            nameLabel.text = address.firstName
            
            var houseNo = "\(address.houseNo?.replacingOccurrences(of: "House No ", with: "") ?? "")"
            var roomNo = "\(address.roomNo?.replacingOccurrences(of: "Room No ", with: "") ?? "")"
            var floorNo = "\(address.floorNo?.replacingOccurrences(of: "Floor No ", with: "") ?? "")"
            var address1 = "\(address.address1 ?? "")"
            var address2 = "\(address.address2 ?? "")"
            var city = "\(address.cityName ?? "")"
            var state = "\(address.stateProvinceName ?? "")"
            
            if (address.houseNo != "" && address.houseNo != nil) && (address.roomNo != "" && address.roomNo != nil) && (address.floorNo != "" && address.floorNo != nil) {
                if houseNo != "" {
                    houseNo = ""
                    houseNo.append("House No ".localized + ": \(address.houseNo?.replacingOccurrences(of: "House No ", with: "") ?? "")")
                    houseNo.append(", ")
                }
                if roomNo != "" {
                    roomNo = ""
                    roomNo.append("Room No ".localized + ": \(address.roomNo?.replacingOccurrences(of: "Room No ", with: "") ?? "")")
                    roomNo.append(", ")
                }
                if floorNo != "" {
                    floorNo = ""
                    floorNo.append("Floor No ".localized + ": \(address.floorNo?.replacingOccurrences(of: "Floor No ", with: "") ?? "")")
                    floorNo.append(", ")
                }
                if address1 != "" {
                    address1 = ""
                    address1.append("\(address.address1 ?? "")")
                    address1.append(", ")
                }
                if address2 != "" {
                    address2 = ""
                    address2.append("\(address.address2 ?? "")")
                    address2.append(", ")
                }
                if city != "" {
                    city = ""
                    city.append("\(address.cityName ?? "")")
                    city.append(", ")
                }
                if state != "" {
                    state = ""
                    state.append("\(address.stateProvinceName ?? "")")
                }
                
                self.countryLabel.text = "\(houseNo)\(roomNo)\(floorNo)\n\(address1)\(address2)\n\(city)\(state)"
            }
            else{
                self.countryLabel.text = "\(address1)\(address2)\n\(city)\(state)"
            }
            if let number = address.phoneNumber {
                if number.hasPrefix("0095") {
                    phNumLabel.text = (number as NSString).replacingCharacters(in: NSRange(location: 0, length: 4), with: "0")
                } else {
                    phNumLabel.text = number
                }
            }
        }
    }
}

extension addressCollection: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (self.allBillingAddresses?.count ?? 0) + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell : AddressCollectionViewCell?
        if indexPath.row < (self.allBillingAddresses?.count)! {
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddressCollectionViewCell", for: indexPath) as? AddressCollectionViewCell
            cell?.wrapdata(addressObject: self.allBillingAddresses?[indexPath.row])
            self.roundCell(cell!)
        }else {
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "addAddressCell", for: indexPath) as? AddressCollectionViewCell
            cell?.addAddressBorderView.layer.borderColor = UIColor.black.cgColor
            cell?.addAddressBorderView.layer.borderWidth = 0.6
            cell?.addAddressBorderView.layer.cornerRadius = 6
            if (self.allBillingAddresses?.count)! < 5 {
                cell?.addAddressBorderView.isHidden = false
            }else {
                cell?.addAddressBorderView.isHidden = true
                cell?.isUserInteractionEnabled = false
            }
        }
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.size.width/2 - 2, height: 230)
    }
    
    // MARK: UICollectionViewDelegate
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.dismiss(animated: true, completion: {
            if indexPath.row == (self.allBillingAddresses?.count)! {
                self.delegate?.addBillingAddressTapped(indexPath: indexPath,count: (self.allBillingAddresses?.count)!)
            }else {
                self.delegate?.AddressTapped(indexPath: indexPath)
            }
        })
        
    }
    
    func roundCell(_ cell:UIView) {
        cell.layer.cornerRadius = 8
        cell.layer.masksToBounds = true
        cell.backgroundColor = UIColor.white
        cell.layer.shadowColor = UIColor.lightGray.cgColor
        cell.layer.shadowOffset = CGSize(width: 1.50, height: 1.50)
        cell.layer.shadowOpacity = 0.30
        cell.layer.shadowRadius = 1
        cell.layer.masksToBounds = true
        cell.clipsToBounds = false
        cell.layer.borderWidth = 0.5
        cell.layer.borderColor = UIColor(red: 218/255, green: 218/255, blue: 218/255, alpha: 1.0).cgColor
    }
}

