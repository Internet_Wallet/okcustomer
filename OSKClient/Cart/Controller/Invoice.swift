//
//  Invoice.swift
//  VMart
//
//  Created by ANTONY on 29/10/2018.
//  Copyright © 2018 Shobhit. All rights reserved.
//

import UIKit

class Invoice: UIViewController {
    
    var responseModel: Dictionary<String, Any>?
    
    @IBOutlet var InvoiceTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.applyNavigationGradient(colors: [UIColor.colorWithRedValue(redValue: 32, greenValue: 89, blueValue: 235, alpha: 1), UIColor.magenta])
        
        self.InvoiceTableView.reloadData()
        // Do any additional setup after loading the view.
        
    }
    
    func pdfDataWithTableView(tableView: UITableView) -> URL? {
        
        let priorBounds = tableView.bounds
        let fittedSize = tableView.sizeThatFits(CGSize(width:priorBounds.size.width, height:tableView.contentSize.height))
        tableView.bounds = CGRect(x:0, y:0, width:fittedSize.width, height:fittedSize.height)
        let pdfPageBounds = CGRect(x:0, y:0, width:tableView.frame.width, height:self.view.frame.height)
        let pdfData = NSMutableData()
        UIGraphicsBeginPDFContextToData(pdfData, pdfPageBounds,nil)
        var pageOriginY: CGFloat = 0
        while pageOriginY < fittedSize.height {
            UIGraphicsBeginPDFPageWithInfo(pdfPageBounds, nil)
            UIGraphicsGetCurrentContext()!.saveGState()
            UIGraphicsGetCurrentContext()!.translateBy(x: 0, y: -pageOriginY)
            tableView.layer.render(in: UIGraphicsGetCurrentContext()!)
            UIGraphicsGetCurrentContext()!.restoreGState()
            pageOriginY += pdfPageBounds.size.height + 350
        }
        UIGraphicsEndPDFContext()
        tableView.bounds = priorBounds
        
        var docURL = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last! as URL
        docURL = docURL.appendingPathComponent("receipt.pdf")
        if pdfData.write(to: docURL as URL, atomically: true){
            return docURL
        }
        
        return nil
    }
    
    
    
    @IBAction func shareClick(_ sender: UIButton) {
        
        let pdfUrl = self.pdfDataWithTableView(tableView: self.InvoiceTableView)
        
        let activityViewController = UIActivityViewController(activityItems: [pdfUrl!], applicationActivities: nil)
        self.present(activityViewController, animated: true, completion: nil)
        
    }
    
}

extension Invoice: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 10
        }
        else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell : InvoiceTableViewCell?
        switch indexPath.section {
        case 0:  cell = tableView.dequeueReusableCell(withIdentifier: "itemCell", for: indexPath) as? InvoiceTableViewCell
            break
        case 1:  cell = tableView.dequeueReusableCell(withIdentifier: "addressCell", for: indexPath) as? InvoiceTableViewCell
            break
        default: cell = tableView.dequeueReusableCell(withIdentifier: "paymentCell", for: indexPath) as? InvoiceTableViewCell
            break
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 2: return 300
        default: return 120
        }
    }
}

class InvoiceTableViewCell: UITableViewCell {
    @IBOutlet var subCategoriesLbl: UILabel!{
        didSet {
            self.subCategoriesLbl.text = self.subCategoriesLbl.text?.localized
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
