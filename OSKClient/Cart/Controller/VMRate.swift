//
//  VMRate.swift
//  VMart
//
//  Created by Mohit on 1/29/19.
//  Copyright © 2019 Shobhit. All rights reserved.
//

import UIKit

class VMRate: UIViewController {
    
    var aPIManager = APIManager()
    var dictionaryArray =               NSMutableArray ()
    var orderNumber:NSNumber?
    
    var KeyboardSize : CGFloat = 0.0
    
    @IBOutlet weak var bottomGetBillBtnConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightGetBillBtnConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    
    @IBOutlet var txtTitle:UITextField!{
        didSet{
            self.txtTitle.text = self.txtTitle?.text?.localized
            self.txtTitle.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var txtComment: UITextView!{ 
        didSet{
            self.txtComment.text = self.txtComment.text.localized
            self.txtComment.font = UIFont(name: appFont, size: 15.0)
            self.txtComment.textColor = .black
        }
    }
    @IBOutlet var cartBarButton: UIBarButtonItem!
    
    @IBOutlet weak var okayBtn: UIButton! {
        didSet {
            okayBtn.titleLabel?.font = UIFont(name: appFont, size: 15.0)
            //okayBtn.applyButtonGradient(colors: [UIColor.colorWithRedValue(redValue: 32, greenValue: 89, blueValue: 235, alpha: 1), UIColor.magenta])
            //            okayBtn.applyButtonGradient(colors: [UIColor.colorWithRedValue(redValue: 13, greenValue: 177, blueValue: 75, alpha: 1), UIColor.init(red: 20.0/255.0, green: 185.0/255.0, blue: 85.0/255.0, alpha: 1.0)])
            self.okayBtn.setTitle(self.okayBtn.titleLabel?.text?.localized, for: .normal)
            
        }
    }
    
    @IBOutlet weak var submitBtn: UIButton! {
        didSet {
            //            submitBtn.applyButtonGradient(colors: [UIColor.colorWithRedValue(redValue: 32, greenValue: 89, blueValue: 235, alpha: 1), UIColor.magenta])
            submitBtn.applyButtonGradient(colors: [UIColor.colorWithRedValue(redValue: 13, greenValue: 177, blueValue: 75, alpha: 1), UIColor.init(red: 20.0/255.0, green: 185.0/255.0, blue: 85.0/255.0, alpha: 1.0)])
            self.submitBtn.setTitle("Submit".localized, for: .normal)
            submitBtn.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    @IBOutlet weak var Btn0: UIButton!
    @IBOutlet weak var Btn1: UIButton!
    @IBOutlet weak var Btn2: UIButton!
    @IBOutlet weak var Btn3: UIButton!
    @IBOutlet weak var Btn4: UIButton!
    
    @IBOutlet var lblheader: UILabel!{
        didSet {
            self.lblheader.font = UIFont(name: appFont, size: 15.0)
            self.lblheader.text = self.lblheader.text?.localized
            
        }
    }
    
    @IBOutlet var lblheader1: UILabel!{
        didSet {
            self.lblheader1.font = UIFont(name: appFont, size: 15.0)
            self.lblheader1.text = self.lblheader1.text?.localized
            
        }
    }
    
    
    
    @IBOutlet var viewOne: UIView!{
        didSet {
            //self.viewOne.applyViewnGradient(colors: [UIColor.colorWithRedValue(redValue: 13, greenValue: 177, blueValue: 75, alpha: 1), UIColor.init(red: 20.0/255.0, green: 185.0/255.0, blue: 85.0/255.0, alpha: 1.0)])
            //self.viewOne.frame = CGRect(x: 0.0, y: 0.0, width: self.view.frame.width, height: 230.0)
        }
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        let button = UIButton()
        button.setImage(UIImage(named: "back"), for: .normal)
        button.addTarget(self, action: #selector(self.backClick), for: .touchUpInside)
        button.imageEdgeInsets.left = -35
        let item = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = item
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWasShown(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillBeHidden(notification:)) , name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillChangeHeight(notification:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        
        // self.navigationController?.setNavigationBarHidden(true, animated: animated)
        //        self.navigationItem.setHidesBackButton(true, animated: true)
    }
    
    @objc func backClick(){
        self.dismiss(animated: true, completion: nil)
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillChangeFrameNotification,object : nil)
        self.view.endEditing(true)
        
        
        //        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Rate Our Service".localized
        txtComment.text = "Comments".localized
        txtComment.textColor = UIColor.lightGray
        
        subscribeToShowKeyboardNotifications()
        
        self.orderNumber=5
        //        self.navigationController?.navigationBar.applyNavigationGradient(colors: [UIColor.colorWithRedValue(redValue: 32, greenValue: 89, blueValue: 235, alpha: 1), UIColor.magenta])
        self.navigationController?.navigationBar.applyNavigationGradient(colors: [UIColor.colorWithRedValue(redValue: 13, greenValue: 177, blueValue: 75, alpha: 1), UIColor.init(red: 20.0/255.0, green: 185.0/255.0, blue: 85.0/255.0, alpha: 1.0)])
        //        self.cartBarButton = UIBarButtonItem.init(badge: UserDefaults.standard.value(forKey: "badge") as? String, title: "", target: self, action: #selector(self.cartAction(_:)))
        //        self.cartBarButton = UIBarButtonItem.init(badge: UserDefaults.standard.string(forKey: "badge") ?? "", title: "", target: self, action: #selector(self.cartAction(_:)))
        //        self.navigationItem.rightBarButtonItem  = self.cartBarButton
        
        
        self.myButtonTapped(sender: Btn3)
    }
    
    
    @IBAction func cartAction(_ sender: UIBarButtonItem) {
        let viewController = UIStoryboard(name: "Cart", bundle: Bundle.main).instantiateViewController(withIdentifier: "CartRoot")
        self.present(viewController, animated: true, completion: nil)
    }
    
    
    @IBAction func backTohomeAction(_ sender: UIButton) {
        self.performSegue(withIdentifier: "unwindBackToHome", sender: self)
    }
    
    
    
    
    func getShoppingCartDetails(dict: [String:Any]) {
        AppUtility.showLoading(self.view)
        
        self.aPIManager.RateAndReview(dict, onSuccess:{ getProducts in
            println_debug(getProducts)
            if let viewLoc = self.view {
                AppUtility.hideLoading(viewLoc)
                AppUtility.showToastCustomBlackInView(message: "Thanks for review", controller: self)
                self.dismiss(animated: true, completion: nil)
                //                self.navigationController?.popToRootViewController(animated: true)
                
            }
        }, onError: { [weak self] message in
            if let viewLoc = self?.view {
                AppUtility.hideLoading(viewLoc)
                //                    AppUtility.showToast(message,view: viewLoc)
                AppUtility.showToastCustomBlackInView(message: message!, controller: self!)
            }
        })
    }
    
    
    @IBAction func myButtonTapped(sender: UIButton){
        
        switch sender.tag {
        case 0: self.okayBtn.setTitle("Terrible".localized,for: .normal); self.IconChange(sender: sender);  break
        case 1:  self.okayBtn.setTitle("Bad".localized,for: .normal); self.IconChange(sender: sender);  break
        case 2: self.okayBtn.setTitle("Okay".localized,for: .normal); self.IconChange(sender: sender);  break
        case 3: self.okayBtn.setTitle("Good".localized,for: .normal);  self.IconChange(sender: sender); break
        case 4: self.okayBtn.setTitle("Great".localized,for: .normal);  self.IconChange(sender: sender); break
        default: break
        }
    }
    
    func IconChange(sender: UIButton){
        switch sender.tag {
            
        case 0: self.Btn0.isSelected = true ; self.Btn0.removeContentInset();   self.Btn1.isSelected = false ; self.Btn1.addContentInset(); self.Btn2.isSelected = false; self.Btn2.addContentInset(); self.Btn3.isSelected = false; self.Btn3.addContentInset(); self.Btn4.isSelected = false; self.Btn4.addContentInset(); break
        case 1: self.Btn1.isSelected = true ; self.Btn1.removeContentInset(); self.Btn0.isSelected = false ;self.Btn0.addContentInset();  self.Btn2.isSelected = false; self.Btn2.addContentInset();  self.Btn3.isSelected = false; self.Btn3.addContentInset();  self.Btn4.isSelected = false; self.Btn4.addContentInset();  break
        case 2: self.Btn2.isSelected = true ;  self.Btn2.removeContentInset();  self.Btn1.isSelected = false ; self.Btn1.addContentInset(); self.Btn0.isSelected = false; self.Btn0.addContentInset(); self.Btn3.isSelected = false; self.Btn3.addContentInset(); self.Btn4.isSelected = false; self.Btn4.addContentInset(); break
        case 3: self.Btn3.isSelected = true ; self.Btn3.removeContentInset();   self.Btn1.isSelected = false ; self.Btn1.addContentInset(); self.Btn2.isSelected = false; self.Btn2.addContentInset(); self.Btn0.isSelected = false; self.Btn0.addContentInset(); self.Btn4.isSelected = false; self.Btn4.addContentInset(); break
        case 4: self.Btn4.isSelected = true ;  self.Btn4.removeContentInset();  self.Btn0.isSelected = false ; self.Btn0.addContentInset(); self.Btn1.isSelected = false; self.Btn1.addContentInset(); self.Btn2.isSelected = false; self.Btn2.addContentInset(); self.Btn3.isSelected = false; self.Btn3.addContentInset(); break
        default:break
        }
    }
    
    
    
    
    @IBAction func SubmitAction(_ sender: UIButton) {
        
        if AppUtility.isConnectedToNetwork() {
            
            if txtComment.text != "" && txtComment.text != nil{
                self.dictionaryArray = []
                let dictData : [String:Any] = ["ReviewText":(self.okayBtn.titleLabel?.text)!, "OrderNo":self.orderNumber!,"Rating":sender.tag+1, "ReviewType":1]
                self.getShoppingCartDetails(dict: dictData)
            }else{
                AppUtility.showToastlocal(message: "Service feed back".localized, view: self.view)
                
            }
            
        } else {
            AppUtility.showToastlocal(message: AppConstants.InternetErrorText.message, view: self.view)
            //AppUtility.showInternetErrorToast(AppConstants.InternetErrorText.message, view: self.view)
        }
    }
}

extension VMRate{
    func subscribeToShowKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    
    @objc func keyboardWillShow(_ notification: Notification) {
        let userInfo = notification.userInfo
        let keyboardSize = userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue
        let keyboardHeight = keyboardSize.cgRectValue.height
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            //            self.adImageConstraint.constant = 0 - keyboardHeight
            self.bottomGetBillBtnConstraint.constant = keyboardHeight
            if self.submitBtn.isHidden {
                self.heightGetBillBtnConstraint.constant = 0
            } else {
                self.heightGetBillBtnConstraint.constant = 50
            }
            
        })
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        //        self.adImageConstraint.constant = 0
        self.bottomGetBillBtnConstraint.constant = 0
        if self.submitBtn.isHidden {
            self.heightGetBillBtnConstraint.constant = 0
        } else {
            self.heightGetBillBtnConstraint.constant = 50
        }
    }
    
    
    @objc func keyboardWasShown(notification: NSNotification){
        //Need to calculate keyboard exact size due to Apple suggestions
        let info : NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
        self.KeyboardSize = (keyboardSize?.height)!
        let contentInsets : UIEdgeInsets = UIEdgeInsets.init(top: 0.0, left: 0.0, bottom: keyboardSize!.height + 45, right: 0.0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if txtComment != nil{
            if (!aRect.contains(txtComment!.frame.origin)){
                self.scrollView.scrollRectToVisible(txtComment!.frame, animated: true)
            }
        }
    }
    @objc func keyboardWillChangeHeight(notification: NSNotification){
        //Need to calculate keyboard exact size due to Apple suggestions
        let info : NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
        self.KeyboardSize = (keyboardSize?.height)!
        
        //        classActionButton?.frame.origin.y = self.view.frame.height - KeyboardSize - 54
        bottomGetBillBtnConstraint.constant = KeyboardSize + 54
        self.view.layoutIfNeeded()
    }
    
    
    @objc func keyboardWillBeHidden(notification: NSNotification){
        let contentInsets : UIEdgeInsets = UIEdgeInsets.init(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        //        classActionButton?.frame.origin.y = self.view.frame.height - 54
        bottomGetBillBtnConstraint.constant = 0
    }
    
    
    
    
    
}

extension VMRate: UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Comments"
            textView.textColor = UIColor.lightGray
        }
    }
}


extension UIButton{
    func addContentInset(){
        self.contentEdgeInsets = UIEdgeInsets(top: 8.0, left: 15.0, bottom: 8.0, right: 15.0)
    }
    func removeContentInset()  {
        self.contentEdgeInsets = UIEdgeInsets(top: 0.0, left: 8.0, bottom: 0.0, right: 8.0)
    }
}
