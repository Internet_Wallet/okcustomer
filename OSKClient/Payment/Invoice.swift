//
//  Invoice.swift
//  VMart
//
//  Created by ANTONY on 29/10/2018.
//  Copyright © 2018 Shobhit. All rights reserved.
//

import UIKit

class Invoice: MartBaseViewController {
    
    var responseModel: Dictionary<String, Any>?
    var itemsArray = [Dictionary<String, Any>]()
    var ordersCount = 0
    
    @IBOutlet var InvoiceTableView: UITableView!
    @IBOutlet var ShareBtn: UIButton!{
        didSet {
            ShareBtn.setTitle((ShareBtn.titleLabel?.text ?? "").localized, for: .normal)
            ShareBtn.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let button = UIButton()
        button.setImage(UIImage(named: "back"), for: .normal)
        button.addTarget(self, action: #selector(dismissScreen), for: .touchUpInside)
        button.imageEdgeInsets.left = -35
        let item = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = item
        if let dict = responseModel, let orders = dict["Orders"] as? [Any] {
            self.parseItems(dict: dict) { (status) in
                self.InvoiceTableView.reloadData()
            }
            ordersCount = orders.count
        }
        
        let rightBarButtonItem = UIBarButtonItem.init(image: UIImage(named: "reportShareWhite"), style: .done, target: self, action: #selector(self.share))
        self.navigationItem.rightBarButtonItem = rightBarButtonItem
    }
    
    @objc func dismissScreen() {
        self.dismiss(animated: true, completion: nil)
    }
    
    private func parseItems(dict: Dictionary<String, Any>, completionHandler: @escaping(Bool) -> Void) {
        if let ordersArray = dict["Orders"] as? [Any], ordersArray.count > 0 {
            for orderLoc in ordersArray {
                if let order = orderLoc as? Dictionary<String, Any> {
                    if let itemArray = order["Items"] as? [Any], itemArray.count > 0 {
                        for item in itemArray {
                            if let itm = item as? Dictionary<String, Any> {
                                itemsArray.append(itm)
                            }
                        }
                    }
                }
            }
        }
        completionHandler(true)
    }
    
    func pdfDataWithTableView(tableView: UITableView) -> URL? {
        let priorBounds = tableView.bounds
        let fittedSize = tableView.sizeThatFits(CGSize(width:priorBounds.size.width, height:tableView.contentSize.height))
        tableView.bounds = CGRect(x:0, y:0, width:fittedSize.width, height:fittedSize.height)
        let pdfPageBounds = CGRect(x:0, y:0, width:tableView.frame.width, height:self.view.frame.height)
        let pdfData = NSMutableData()
        UIGraphicsBeginPDFContextToData(pdfData, pdfPageBounds,nil)
        var pageOriginY: CGFloat = 0
        while pageOriginY < fittedSize.height {
            UIGraphicsBeginPDFPageWithInfo(pdfPageBounds, nil)
            UIGraphicsGetCurrentContext()!.saveGState()
            UIGraphicsGetCurrentContext()!.translateBy(x: 0, y: -pageOriginY)
            tableView.layer.render(in: UIGraphicsGetCurrentContext()!)
            UIGraphicsGetCurrentContext()!.restoreGState()
            pageOriginY += pdfPageBounds.size.height + 400
        }
        UIGraphicsEndPDFContext()
        tableView.bounds = priorBounds
        var docURL = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last! as URL
        docURL = docURL.appendingPathComponent("receipt.pdf")
        if pdfData.write(to: docURL as URL, atomically: true) {
            return docURL
        }
        return nil
    }
    
    
    @objc func share() {
        let pdfUrl = self.pdfDataWithTableView(tableView: self.InvoiceTableView)
        let activityViewController = UIActivityViewController(activityItems: [pdfUrl!], applicationActivities: nil)
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    
    @IBAction func shareClick(_ sender: UIButton) {
        let pdfUrl = self.pdfDataWithTableView(tableView: self.InvoiceTableView)
        let activityViewController = UIActivityViewController(activityItems: [pdfUrl!], applicationActivities: nil)
        self.present(activityViewController, animated: true, completion: nil)
    }
}

extension Invoice: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return itemsArray.count
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell : InvoiceTableViewCell?
        switch indexPath.section {
        case 0:  cell = tableView.dequeueReusableCell(withIdentifier: "itemCell", for: indexPath) as? InvoiceTableViewCell
        cell?.wrapItemCell(responseModel: itemsArray[indexPath.row], index: indexPath.row)
        case 1:  cell = tableView.dequeueReusableCell(withIdentifier: "addressCell", for: indexPath) as? InvoiceTableViewCell
        if let response = responseModel {
            cell?.wrapAddressCell(responseModel: response)
            }
        default: cell = tableView.dequeueReusableCell(withIdentifier: "paymentCell", for: indexPath) as? InvoiceTableViewCell
        if let response = responseModel {
            cell?.wrapPaymentCell(responseModel: response)
            }
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 1: return 150
        case 2: return 300
        default: return 125
        }
    }
}

class InvoiceTableViewCell: UITableViewCell {
    //Item
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var itemName: UILabel!{
        didSet {
            self.itemName.font = UIFont(name: appFont, size: 15.0)
            self.itemName.text = self.itemName.text?.localized
            
        }
    }
    @IBOutlet weak var itemDescription: UILabel!{
        didSet {
            self.itemDescription.font = UIFont(name: appFont, size: 15.0)
            self.itemDescription.text = self.itemDescription.text?.localized
            
        }
    }
    @IBOutlet weak var itemPrice: UILabel!{
        didSet {
            self.itemPrice.font = UIFont(name: appFont, size: 15.0)
            self.itemPrice.text = self.itemPrice.text?.localized
            
        }
    }
    @IBOutlet weak var qtyLabel: UILabel!{
        didSet {
            self.qtyLabel.font = UIFont(name: appFont, size: 15.0)
            self.qtyLabel.text = self.qtyLabel.text?.localized
            
        }
    }
    //Address
    @IBOutlet weak var addressName: UILabel!{
        didSet {
            self.addressName.font = UIFont(name: appFont, size: 15.0)
            self.addressName.text = self.addressName.text?.localized
            
        }
    }
    @IBOutlet weak var addressMobile: UILabel!{
        didSet {
            self.addressMobile.font = UIFont(name: appFont, size: 15.0)
            self.addressMobile.text = self.addressMobile.text?.localized
            
        }
    }
    @IBOutlet weak var shipAddress: UILabel!{
        didSet {
            self.shipAddress.font = UIFont(name: appFont, size: 15.0)
            self.shipAddress.text = self.shipAddress.text?.localized
            
        }
    }
    //Payment
    @IBOutlet weak var orderNumber: UILabel!{
        didSet {
            self.orderNumber.font = UIFont(name: appFont, size: 15.0)
            self.orderNumber.text = self.orderNumber.text?.localized
            
        }
    }
    @IBOutlet weak var referenceNumber: UILabel!{
        didSet {
            self.referenceNumber.font = UIFont(name: appFont, size: 15.0)
            self.referenceNumber.text = self.referenceNumber.text?.localized
            
        }
    }
    @IBOutlet weak var payPhoneNum: UILabel!{
        didSet {
            self.payPhoneNum.font = UIFont(name: appFont, size: 15.0)
            self.payPhoneNum.text = self.payPhoneNum.text?.localized
            
        }
    }
    @IBOutlet weak var productAmount: UILabel!{
        didSet {
            self.productAmount.font = UIFont(name: appFont, size: 15.0)
            self.productAmount.text = self.productAmount.text?.localized
            
        }
    }
    @IBOutlet weak var shipCharges: UILabel!{
        didSet {
            self.shipCharges.font = UIFont(name: appFont, size: 15.0)
            self.shipCharges.text = self.shipCharges.text?.localized
            
        }
    }
    @IBOutlet weak var totalAmount: UILabel!{
        didSet {
            self.totalAmount.font = UIFont(name: appFont, size: 15.0)
            self.totalAmount.text = self.totalAmount.text?.localized
            
        }
    }
    @IBOutlet var AddressDetailLabel: UILabel!{
        didSet {
            self.AddressDetailLabel.font = UIFont(name: appFont, size: 15.0)
            self.AddressDetailLabel.text = self.AddressDetailLabel.text?.localized
            
        }
    }
    @IBOutlet var PaymentDetailLabel: UILabel!{
        didSet {
            self.PaymentDetailLabel.font = UIFont(name: appFont, size: 15.0)
            self.PaymentDetailLabel.text = self.PaymentDetailLabel.text?.localized
            
        }
    }
    @IBOutlet var orderNumbLabel: UILabel!{
        didSet {
            self.orderNumbLabel.font = UIFont(name: appFont, size: 15.0)
            self.orderNumbLabel.text = self.orderNumbLabel.text?.localized
            
        }
    }
    @IBOutlet var RefNumbLabel: UILabel!{
        didSet {
            self.RefNumbLabel.font = UIFont(name: appFont, size: 15.0)
            self.RefNumbLabel.text = self.RefNumbLabel.text?.localized
            
        }
    }
    @IBOutlet var PhnNumbLabel: UILabel!{
        didSet {
            self.PhnNumbLabel.font = UIFont(name: appFont, size: 15.0)
            self.PhnNumbLabel.text = self.PhnNumbLabel.text?.localized
            
        }
    }
    @IBOutlet var ProdAmtLabel: UILabel!{
        didSet {
            self.ProdAmtLabel.font = UIFont(name: appFont, size: 15.0)
            self.ProdAmtLabel.text = self.ProdAmtLabel.text?.localized
            
        }
    }
    @IBOutlet var ShipChrgeLabel: UILabel!{
        didSet {
            self.ShipChrgeLabel.font = UIFont(name: appFont, size: 15.0)
            self.ShipChrgeLabel.text = self.ShipChrgeLabel.text?.localized
            
        }
    }
    @IBOutlet var totPayLabel: UILabel!{
        didSet {
            self.totPayLabel.font = UIFont(name: appFont, size: 15.0)
            self.totPayLabel.text = self.totPayLabel.text?.localized
            
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func wrapItemCell(responseModel: Dictionary<String, Any>, index: Int) {
        if let imageDict = responseModel["Picture"] as? Dictionary<String, Any> {
            itemImageView.setImageUsingUrl(imageDict["ImageUrl"] as? String ?? "")
        }
        itemName.text = responseModel["Sku"] as? String ?? ""
        itemDescription.text = responseModel["ProductName"] as? String ?? ""
        itemPrice.text = responseModel["UnitPrice"] as? String ?? ""
        qtyLabel.text = "Quantity \(responseModel["Quantity"] as? Int ?? 0)"
    }
    
    func wrapAddressCell(responseModel: Dictionary<String, Any>) {
        if let ordersArray = responseModel["Orders"] as? [Any], ordersArray.count > 0 {
            if let order = ordersArray.first as? Dictionary<String, Any> {
                if let address = order["BillingAddress"] as? Dictionary<String, Any> {
                    self.addressName.text = address["FirstName"] as? String ?? ""
                    self.addressMobile.text = address["PhoneNumber"] as? String ?? ""
                }
            }
        }
        self.shipAddress.text = responseModel["ShippingAddress"] as? String ?? ""
    }
    
    func wrapPaymentCell(responseModel: Dictionary<String, Any>) {
        if let ordersArray = responseModel["Orders"] as? [Any], ordersArray.count > 0 {
            if let order = ordersArray.first as? Dictionary<String, Any> {
                self.orderNumber.text = "#\(order["CustomOrderNumber"] as? String ?? "")"
            }
        } else {
            self.orderNumber.text = ""
        }
        self.referenceNumber.text = responseModel["ReferenceNumber"] as? String ?? ""
        self.payPhoneNum.text = responseModel["PhoneNumber"] as? String ?? ""
        
        if let number = payPhoneNum.text {
            if number.hasPrefix("0095") {
                payPhoneNum.text = (number as NSString).replacingCharacters(in: NSRange(location: 0, length: 4), with: "+95")
            }
        }
        self.productAmount.text = responseModel["ProductAmount"] as? String ?? "0.0 MMK"
        self.shipCharges.text = responseModel["ShippingCharges"] as? String ?? "0.0 MMK"
        self.totalAmount.text = responseModel["TotalPaidAmount"] as? String ?? "0.0 MMK"
    }
}
