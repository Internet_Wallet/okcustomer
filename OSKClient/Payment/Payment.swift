
//
//  Payment.swift
//  VMart
//
//  Created by Shobhit Singhal on 11/6/18.
//  Copyright © 2018 Shobhit. All rights reserved.
//

import UIKit

class Payment: MartBaseViewController {
    @IBOutlet weak var paymentTableView: UITableView!
    @IBOutlet weak var continueBtn: UIButton!{
        didSet {
            self.continueBtn.setTitle("CONTINUE".localized, for: .normal)
            //            self.continueBtn.applyButtonGradient(colors: [UIColor.colorWithRedValue(redValue: 32, greenValue: 89, blueValue: 235, alpha: 1), UIColor.magenta])
            self.continueBtn.applyButtonGradient(colors: [UIColor.colorWithRedValue(redValue: 13, greenValue: 177, blueValue: 75, alpha: 1), UIColor.init(red: 20.0/255.0, green: 185.0/255.0, blue: 85.0/255.0, alpha: 1.0)])
            continueBtn.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    var aPIManager         = APIManager()
    var paymentMethodArray = [PaymentMethod]()
    var selectedPaymentMethod = ""
    var screenFrom: String? = "Other"
    var errorFromServer: Bool?
    
    var checkedLeftTable = [Bool]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        paymentTableView.estimatedRowHeight = 50
        paymentTableView.rowHeight = UITableView.automaticDimension
        self.getPaymentMethod()
        
        self.navigationController?.navigationBar.tintColor = .white
        
        if let screenFrm = screenFrom, screenFrm == "Cart" {
            let button = UIButton()
            button.setImage(UIImage(named: "back"), for: .normal)
            button.addTarget(self, action: #selector(dismissScreen), for: .touchUpInside)
            button.imageEdgeInsets.left = -35
            let item = UIBarButtonItem(customView: button)
            self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
            self.navigationItem.leftBarButtonItem = item
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = "Payment".localized
        
        
        paymentTableView.reloadData()
    }
    
    func makeLeftCheckArrayToFalse() {
        checkedLeftTable = Array(repeating: false, count: self.paymentMethodArray.count)
        if self.paymentMethodArray.count > 0 {
            // checkedLeftTable[0] = true
        }
    }
    
    @objc func dismissScreen() {
        if let screenFrm = screenFrom {
            switch screenFrm {
            case "Cart":
                DispatchQueue.main.async {
                    self.navigationController?.popToRootViewController(animated: true)
                }                
            default:
                DispatchQueue.main.async {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    
    func getPaymentMethod() {
        self.aPIManager.getPaymentMethod(onSuccess: { [weak self] gotShippingMethod in
            self?.paymentMethodArray = gotShippingMethod
            self?.errorFromServer = false
            self?.paymentTableView.delegate = self
            self?.paymentTableView.dataSource = self
            self?.makeLeftCheckArrayToFalse()
            self?.paymentTableView.reloadData()
            if (self?.paymentMethodArray.count ?? 0) > 0 {
                self?.selectedPaymentMethod = self?.paymentMethodArray[0].Name as String? ?? ""
            }
            if let viewLoc = self?.view {
                AppUtility.hideLoading(viewLoc)
            }
            }, onError: { [weak self] message in
                self?.errorFromServer = true
                if let viewLoc = self?.view {
                    AppUtility.showToastlocal(message: message!, view: viewLoc)
                    AppUtility.hideLoading(viewLoc)
                }
        })
    }
    
    @IBAction func continueBtnAction(_ sender: UIButton) {
        
        if self.errorFromServer!{
            AppUtility.showToastlocal(message: "Payment method not coming from server side", view: self.view)
        }
        else{
            
            // changes for cash on delivery option
            var text = ""
            switch selectedPaymentMethod {
            case "2C2P":
                text =  "Payments.2C2P"
            case "Cash On Delivery (COD)":
                text = "Payments.CashOnDelivery"
            case "COD":
                text = "Payments.COD"
            default:
                text = "Payments.OKDollar"
            }
            // let text = (selectedPaymentMethod.contains("2C2P")) ? "Payments.2C2P" : "Payments.OKDollar"
            self.aPIManager.setPaymentMethods(text as NSString, onSuccess: {
                
                self.performSegue(withIdentifier: "PaymentMethodsToOrderConfirmation", sender: self)
                
                //                if self.selectedPaymentMethod.contains("Money Order") {
                //                    self.performSegue(withIdentifier: "PaymentMethodsToCardDetails", sender: self)
                //                }
                //                else {
                //                    self.performSegue(withIdentifier: "PaymentMethodsToOrderConfirmation", sender: self)
                //                }
                
            }) { (error) in
                
            }
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "PaymentMethodsToOrderConfirmation" {
            if let rootViewController = segue.destination as? OrderConfirmation {
                rootViewController.paymenType = self.selectedPaymentMethod
            }
        }
    }
}

extension Payment: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //        return paymentMethodArray.count + 1
        return paymentMethodArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentCell", for: indexPath) as! paymentTableViewCell
        cell.radioImg.tag = indexPath.row
        cell.paymentOptionData(str: paymentMethodArray[indexPath.row].Name as String)
        cell.radioImg.image = UIImage(named: "radio")
        if checkedLeftTable[indexPath.row] {
            cell.radioImg.image = UIImage(named: "act_radio")
        } else {
            cell.radioImg.image = UIImage(named: "radio")
        }
        
        
        //        if indexPath.row == 0{
        //            cell.payOtionLbl.text = paymentMethodArray[indexPath.row].Name as String
        //            if paymentMethodArray[indexPath.row].Selected {
        //                cell.radioImg.image = UIImage(named: "act_radio")
        //            } else {
        //                cell.radioImg.image = UIImage(named: "radio")
        //            }
        //            cell.nearByBtn.isHidden = false
        //            cell.underlineLbl.isHidden = false
        //        }
        //        else{
        //            cell.payOtionLbl.text = "Credit Card"
        //            cell.nearByBtn.isHidden = true
        //            cell.underlineLbl.isHidden = true
        //        }
        
        cell.Delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        paymentTableView.deselectRow(at: indexPath, animated: true)
        
        self.selectedPaymentMethod = paymentMethodArray[indexPath.row].Name as String
        for (index,_) in checkedLeftTable.enumerated() {
            checkedLeftTable[index] = false
        }
        checkedLeftTable[indexPath.row] = true
        paymentTableView.reloadData()
        
        // changes for cash on delivery option
        var text = ""
        switch selectedPaymentMethod {
        case "2C2P":
            text =  "Payments.2C2P"
        case "Cash On Delivery (COD)":
            text = "Payments.CashOnDelivery"
        case "COD":
            text = "Payments.COD"
        default:
            text = "Payments.OKDollar"
        }
        // let text = (selectedPaymentMethod.contains("2C2P")) ? "Payments.2C2P" : "Payments.OKDollar"
        self.aPIManager.setPaymentMethods(text as NSString, onSuccess: {
            
            self.performSegue(withIdentifier: "PaymentMethodsToOrderConfirmation", sender: self)
            
            //                if self.selectedPaymentMethod.contains("Money Order") {
            //                    self.performSegue(withIdentifier: "PaymentMethodsToCardDetails", sender: self)
            //                }
            //                else {
            //                    self.performSegue(withIdentifier: "PaymentMethodsToOrderConfirmation", sender: self)
            //                }
            
            self.checkedLeftTable[indexPath.row] = false
            self.paymentTableView.reloadData()
            
        }) { (error) in
            
        }
        
        //                let cell = tableView.cellForRow(at: indexPath) as? paymentTableViewCell
        //                cell?.radioImg.image = UIImage(named: "act_radio")
        
        //        if indexPath.row == 0{
        //            for checkSelectedIndex in self.paymentMethodArray{
        //                checkSelectedIndex.Selected = false
        //            }
        //            self.paymentMethodArray[indexPath.row].Selected = true
        //            self.paymentTableView.reloadData()
        //            self.selectedPaymentMethod = paymentMethodArray[indexPath.row].Name as String
        //        }
        //        else{
        //            self.performSegue(withIdentifier: "PaymentMethodsToCardDetails", sender: self)
        //        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //        if indexPath.row == 0{
        //            return 60
        //        }
        //        else{
        //            return UITableView.automaticDimension
        //        }
        
        return 70
    }
}

extension Payment:paymentTableViewCellDelegate{
    func NearOkServiceBtnActionClick(sender: UIButton) {
        self.performSegue(withIdentifier: "NearBySeque", sender: self)
    }
}

protocol paymentTableViewCellDelegate: class {
    func NearOkServiceBtnActionClick(sender: UIButton)
}

class paymentTableViewCell: UITableViewCell {
    weak var Delegate: paymentTableViewCellDelegate?
    @IBOutlet var imageType : UIStackView!
    @IBOutlet var nearByBtn: UIButton!{
        didSet {
            self.nearByBtn.setTitle("Nearby OK$ Service".localized, for: .normal)
            nearByBtn.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    @IBOutlet var underlineLbl: UILabel!
    @IBOutlet var payOtionLbl: UILabel!{
        didSet {
            self.payOtionLbl.font = UIFont(name: appFont, size: 15.0)
            self.payOtionLbl.text = self.payOtionLbl.text?.localized
            
        }
    }
    @IBOutlet var radioImg: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func paymentOptionData(str:String){
        
        self.payOtionLbl.font = UIFont(name: appFont, size: 15.0)
        if str == "OK Dollar"{
            self.nearByBtn.isHidden = false
            self.imageType.isHidden = true
            self.payOtionLbl.text = "OK Dollar Payment".localized
        }
        else if (str == "Cash On Delivery (COD)") {
            
            self.nearByBtn.isHidden = true
            self.imageType.isHidden = true
            self.payOtionLbl.text = "Cash On Delivery (COD)".localized
        }
        else if (str == "COD") {
            
            self.nearByBtn.isHidden = true
            self.imageType.isHidden = true
            self.payOtionLbl.text = "Cash On Delivery (COD)".localized
        }
        else{
            self.nearByBtn.isHidden = true
            self.imageType.isHidden = false
            self.payOtionLbl.text = "Select Payment Type".localized
        }
    }
    
    @IBAction func NearOkServiceBtnAction(_ sender: UIButton) {
        self.Delegate?.NearOkServiceBtnActionClick(sender: sender)
    }
}
