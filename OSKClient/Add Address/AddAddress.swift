//
//  AddAddress.swift
//  VMart
//
//  Created by Shobhit Singhal on 11/8/18.
//  Copyright © 2018 Shobhit. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import IQKeyboardManagerSwift

enum tapGestureType {
    case townShip
    case state
    case country
    case none
}

enum LiftOptions {
    case yesLift
    case noLift
    case none
}

protocol VMAddNewAddressProtocol: class {
    func addNewAddress()
}

class AddAddress: VMLoginBaseViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var fullNameTF: SubSkyFloatTextField! {
        didSet {
            self.fullNameTF.font = UIFont(name: appFont, size: 15.0)
            self.fullNameTF.titleFont = UIFont(name: appFont, size: 15.0)!
            self.fullNameTF.placeholder = self.fullNameTF.placeholder?.localized
            
        }
    }
    @IBOutlet weak var address1TF: SubSkyFloatTextField! {
        didSet {
            self.address1TF.font = UIFont(name: appFont, size: 15.0)
            self.address1TF.titleFont = UIFont(name: appFont, size: 15.0)!
            self.address1TF.placeholder = self.address1TF.placeholder?.localized
        }
    }
    @IBOutlet weak var address2TF: SubSkyFloatTextField! {
        didSet {
            self.address2TF.font = UIFont(name: appFont, size: 15.0)
            self.address2TF.titleFont = UIFont(name: appFont, size: 15.0)!
            self.address2TF.placeholder = self.address2TF.placeholder?.localized
        }
    }
    @IBOutlet weak var houseNoTF: SubSkyFloatTextField! {
        didSet {
            self.houseNoTF.font = UIFont(name: appFont, size: 15.0)
            self.houseNoTF.titleFont = UIFont(name: appFont, size: 15.0)!
            self.houseNoTF.placeholder = self.houseNoTF.placeholder?.localized
        }
    }
    @IBOutlet weak var floorTF: SubSkyFloatTextField! {
        didSet {
            self.floorTF.font = UIFont(name: appFont, size: 15.0)
            self.floorTF.titleFont = UIFont(name: appFont, size: 15.0)!
            self.floorTF.placeholder = self.floorTF.placeholder?.localized
        }
    }
    @IBOutlet weak var roomNoTF: SubSkyFloatTextField! {
        didSet {
            self.roomNoTF.font = UIFont(name: appFont, size: 15.0)
            self.roomNoTF.titleFont = UIFont(name: appFont, size: 15.0)!
            self.roomNoTF.placeholder = self.roomNoTF.placeholder?.localized
        }
    }
    @IBOutlet weak var phNumberTF: UneditableTextField! {
        didSet {
            self.phNumberTF.font = UIFont(name: appFont, size: 15.0)
            self.phNumberTF.titleFont = UIFont(name: appFont, size: 15.0)!
            self.phNumberTF.text = "+95 09"
            self.phNumberTF.placeholder = self.phNumberTF.placeholder?.localized
        }
    }
    @IBOutlet weak var stateTitleLabel: UILabel! {
        didSet {
            self.stateTitleLabel.font = UIFont(name: appFont, size: 15.0)
            self.stateTitleLabel.text = self.stateTitleLabel.text?.localized
        }
    }
    @IBOutlet weak var liftoptionLabel: UILabel!{
        didSet {
            self.liftoptionLabel.font = UIFont(name: appFont, size: 15.0)
            self.liftoptionLabel.text = self.liftoptionLabel.text?.localized
        }
    }
    @IBOutlet weak var townshipTitleLabel: UILabel! {
        didSet {
            self.townshipTitleLabel.font = UIFont(name: appFont, size: 15.0)
            self.townshipTitleLabel.text = self.townshipTitleLabel.text?.localized
        }
    }
    @IBOutlet weak var stateValueLabel: UILabel! {
        didSet {
            self.stateValueLabel.font = UIFont(name: appFont, size: 15.0)
            self.stateValueLabel.text = self.stateValueLabel.text?.localized
        }
    }
    @IBOutlet weak var townshipValueLabel: UILabel! {
        didSet {
            self.townshipValueLabel.font = UIFont(name: appFont, size: 15.0)
            self.townshipValueLabel.text = self.townshipValueLabel.text?.localized
        }
    }
    @IBOutlet weak var liftYesBtn: UIButton! {
        didSet {
            liftYesBtn.titleLabel?.font = UIFont(name: appFont, size: 15.0)
            liftYesBtn.setTitle((liftYesBtn.titleLabel?.text ?? "").localized, for: .normal)
        }
    }
    @IBOutlet weak var liftNoBtn: UIButton! {
        didSet {
            liftNoBtn.titleLabel?.font = UIFont(name: appFont, size: 15.0)
            liftNoBtn.setImage(UIImage(named: "act_radio"), for: .normal)
            liftNoBtn.setTitle((liftNoBtn.titleLabel?.text ?? "").localized, for: .normal)
        }
    }
    @IBOutlet weak var continueBtn: UIButton! {
        didSet {
            continueBtn.titleLabel?.font = UIFont(name: appFont, size: 15.0)
            continueBtn.setTitle((continueBtn.titleLabel?.text ?? "").localized, for: .normal)
        }
    }
    //Color
    @IBOutlet weak var colorSelectButton: UIButton! {
        didSet {
            colorSelectButton.titleLabel?.font = UIFont(name: appFont, size: 15.0)
            colorSelectButton.setTitle((colorSelectButton.titleLabel?.text ?? "").localized, for: .normal)
        }
    }
    @IBOutlet weak var colorSelectLabel: UILabel! {
        didSet {
            self.colorSelectLabel.font = UIFont(name: appFont, size: 15.0)
            colorSelectLabel.text = "Select rope color".localized
        }
    }
    @IBOutlet weak var colorSelectValueLabel: UILabel! {
        didSet {
            self.colorSelectValueLabel.font = UIFont(name: appFont, size: 15.0)
            colorSelectValueLabel.text = colorSelectValueLabel.text?.localized
        }
    }
    @IBOutlet weak var selectColorTableView: UITableView! {
        didSet {
            self.selectColorTableView.isHidden = true
            self.selectColorTableView.layer.borderWidth = 1.0
            self.selectColorTableView.layer.borderColor = UIColor.gray.cgColor
        }
    }
    @IBOutlet weak var ropeStack: UIStackView!
    @IBOutlet weak var constraintTopMobileNumber: NSLayoutConstraint!
    
    //MARK: - Properties
    var aPIManager = APIManager()
    var screenFrom: String?
    var defaultAddress: GenericBillingAddress?
    var liftOption: LiftOptions = .none
    var countryObject = (code: "60", name: "Myanmar")
    var selectedState = (code: "", name: "")
    var townshipObject = (code: "", name: "")
    weak var delegate: VMAddNewAddressProtocol?
    let colorArray = ["Black", "Red", "Blue", "Green", "Yellow"]
    var result: (min: Int, max: Int, operator: String, isRejected: Bool, color: String)?
    
    private var continueView: UIView {
        let view = UIView.init(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 50))
        let btn = UIButton.init(frame: view.frame)
        btn.backgroundColor = UIColor.init(red: 13/255.0, green: 177/255.0, blue: 75/255.0, alpha: 1)
        if let myFont = UIFont(name: appFont, size: 18) {
            btn.titleLabel?.font =  myFont
        }
        btn.setTitle("CONTINUE".localized, for: .normal)
        btn.addTarget(self, action: #selector(submitFromToolbar), for: UIControl.Event.touchUpInside)
        view.addSubview(btn)
        return view
    }
    
    var pressCount = 0
    //MARK: - View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        IQKeyboardManager.shared.enable = true
        self.navigationItem.title = "Add Address".localized
        self.navigationController?.navigationBar.applyNavigationGradient(colors: [UIColor.colorWithRedValue(redValue: 13, greenValue: 177, blueValue: 75, alpha: 1), UIColor.init(red: 20.0/255.0, green: 185.0/255.0, blue: 85.0/255.0, alpha: 1.0)])
        initializeView()
        getBillform()
        self.selectedYesLiftBtn()
        
        println_debug("screenFrom -----> \(String(describing: screenFrom))")
        if let screenFrm = screenFrom {
            if screenFrm == "DashBoard" || screenFrm == "Cart"  || screenFrm == "Regi_Cart" {
                self.addBack()
            } else if screenFrm == "Edit Address" {
                self.navigationItem.title = "Edit Address".localized
                self.addBack()
                self.updateUI()
            } else if screenFrm == "Other" {
                self.addBack()
                loadFreshScreen()
            }else if screenFrm == "DashBoard / " {
                self.addBack()
                loadFreshScreen()
            }else if screenFrom == "Add Address" {
                self.addBack()
            }
        }
    }
    
    
    func addBack(){
        let button = UIButton()
        button.setImage(UIImage(named: "back"), for: .normal)
        button.addTarget(self, action: #selector(dismissScreen), for: .touchUpInside)
        button.imageEdgeInsets.left = -35
        let item = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = item
    }
    
    
    override func hideKeyboard() {
        self.view.endEditing(true)
        self.selectColorTableView.isHidden = true
    }
    
    //MARK: - Target Methods
    @objc func dismissScreen() {
        if let screenFrm = screenFrom {
            switch screenFrm {
            case "DashBoard":
                DispatchQueue.main.async {
                    self.navigationController?.popToRootViewController(animated: true)
                }
            case "Cart","Other","Edit Address","addBack","Regi_Cart":
                if UserDefaults.standard.bool(forKey: "GuestLogin") {
                    UserDefaults.standard.set(false, forKey: "GuestLogin")
                    VMLoginModel.shared.addressAdded = false
                    UserDefaults.standard.synchronize()
                }
                DispatchQueue.main.async {
                    self.navigationController?.popToRootViewController(animated: true)
                }
            case "DashBoard / ":
                DispatchQueue.main.async {
                    self.performSegue(withIdentifier: "unwindAddToHome", sender: self)
                }
            case "Add Address":
                DispatchQueue.main.async {
                    self.navigationController?.popViewController(animated: true)
                }
            default:
                DispatchQueue.main.async {
                    self.performSegue(withIdentifier: "unwindAddToHome", sender: self)
                }
            }
        }
    }
    
    @objc func submitFromToolbar() {
        addNewAddressAction(UIButton())
    }
    
    //MARK: - Methods
    private func updateUI() {
        if let defaultAddr = defaultAddress {
            fullNameTF.text = defaultAddr.firstName
            address1TF.text = defaultAddr.address1
            address2TF.text = defaultAddr.address2
            if let houseNo = defaultAddr.houseNo {
                houseNoTF.text = houseNo.replacingOccurrences(of: "House No ".localized, with: "").replacingOccurrences(of: "အိမ္ယာနံပါတ္", with: "").replacingOccurrences(of: "အိမ်အမှတ်", with: "")
            }
            if let floorNo = defaultAddr.floorNo {
                floorTF.text = floorNo.replacingOccurrences(of: "Floor No ".localized, with: "").replacingOccurrences(of: "အထပ္နံပါတ္", with: "").replacingOccurrences(of: "အထပ်နံပါတ်", with: "")
            }
            if let roomNo = defaultAddr.roomNo {
                roomNoTF.text = roomNo.replacingOccurrences(of: "Room No ".localized, with: "").replacingOccurrences(of: "အခန္းနံပါတ္", with: "").replacingOccurrences(of: "အခန်းအမှတ်", with: "")
            }
            townshipValueLabel.text = defaultAddr.cityName
            stateValueLabel.text = defaultAddr.stateProvinceName
            if let lift = defaultAddr.isLift {
                if lift == 1 {
                    liftYesBtn.setImage(UIImage(named: "act_radio"), for: .normal)
                    liftNoBtn.setImage(UIImage(named: "radio"), for: .normal)
                    liftOption = .yesLift
                    ropeStack.isHidden = true
                    constraintTopMobileNumber.constant = -66
                    self.view.layoutIfNeeded()
                } else {
                    liftYesBtn.setImage(UIImage(named: "radio"), for: .normal)
                    liftNoBtn.setImage(UIImage(named: "act_radio"), for: .normal)
                    liftOption = .noLift
                    ropeStack.isHidden = false
                    constraintTopMobileNumber.constant = 19
                    self.view.layoutIfNeeded()
                }
            } else {
                liftYesBtn.setImage(UIImage(named: "radio"), for: .normal)
                liftNoBtn.setImage(UIImage(named: "radio"), for: .normal)
                liftOption = .none
                self.selectedYesLiftBtn()
            }
            if let phNumber = defaultAddr.phoneNumber, phNumber.count > 0 {
                if phNumber.hasPrefix("009509") {
                    phNumberTF.text = (phNumber as NSString).replacingCharacters(in: NSRange(location: 0, length: 6), with: "+95 09")
                }else if  phNumber.hasPrefix("00959"){
                    phNumberTF.text = (phNumber as NSString).replacingCharacters(in: NSRange(location: 0, length: 5), with: "+95 09")
                }else if phNumber.hasPrefix("+95") {
                    phNumberTF.text = (phNumber as NSString).replacingCharacters(in: NSRange(location: 0, length: 4 ), with: "+95 09")
                }else {
                    phNumberTF.text = (phNumber as NSString).replacingCharacters(in: NSRange(location: 0, length: 1), with: "+95 ")
                }
            } else {
                phNumberTF.text = "+95 09"
            }
            
            self.colorSelectValueLabel.text = defaultAddr.ropeColor?.localized
            self.townshipObject = (code:"\(defaultAddr.cityId ?? 0)", name: (defaultAddr.cityName ?? ""))
            self.selectedState = (code: "\(defaultAddr.stateProvinceId ?? 0)", name: (defaultAddr.stateProvinceName ?? ""))
        }
    }
    
    private func phNoToUIPhNo(phStr: String) -> String {
        if phStr.hasPrefix("0095") {
            return (phStr as NSString).replacingCharacters(in: NSRange(location: 0, length: 5), with: "+95 09")
        } else if phStr.hasPrefix("+95") {
            return (phStr as NSString).replacingCharacters(in: NSRange(location: 0, length: 4), with: "+95 09")
        } else if phStr.hasPrefix("09"){
            return (phStr as NSString).replacingCharacters(in: NSRange(location: 0, length: 0), with: "+95 ")
        } else if phStr.hasPrefix("9") {
            return (phStr as NSString).replacingCharacters(in: NSRange(location: 0, length: 0), with: "+95 0")
        } else {
            return "+95 09"
        }
    }
    
    private func initializeView() {
        if let firstName = VMLoginModel.shared.firstName {
            self.fullNameTF.text = firstName
        }
    }
    
    private func setupToolbar() {
        fullNameTF.inputAccessoryView = continueView
        address1TF.inputAccessoryView = continueView
        address2TF.inputAccessoryView = continueView
        houseNoTF.inputAccessoryView = continueView
        floorTF.inputAccessoryView = continueView
        roomNoTF.inputAccessoryView = continueView
        phNumberTF.inputAccessoryView = continueView
    }
    
    func showViewController() {
        if let screenFrm = screenFrom {
            switch screenFrm {
            case "DashBoard":
                DispatchQueue.main.async {
                    self.dismiss(animated: true, completion: nil)
                }
                break
            case "Cart":
                if UserDefaults.standard.bool(forKey: "GuestLogin") {
                    UserDefaults.standard.set(false, forKey: "GuestLogin")
                    VMLoginModel.shared.addressAdded = false
                    UserDefaults.standard.synchronize()
                }
                self.showPaymentScreen()
                break
            case "Regi_Cart" :
                DispatchQueue.main.async {
                    NotificationCenter.default.post(name: NSNotification.Name("ReloadAddressAfterAdd"), object: nil, userInfo: nil)
                    self.navigationController?.popToRootViewController(animated: true)
                }
                break
            case "Edit Address","Add Address":
                DispatchQueue.main.async {
                    self.delegate?.addNewAddress()
                    self.navigationController?.popViewController(animated: true)
                }
                break
            case "Other":
                self.navigateToDashBoard()
                break
            case "DashBoard / ":
                self.navigateToDashBoard()
                break
            default:
                break
            }
        }else {
            self.navigateToDashBoard()
        }
    }
    
    private func navigateToDashBoard() {
        DispatchQueue.main.async {
            self.delegate?.addNewAddress()
            self.performSegue(withIdentifier: "unwindAddToHome", sender: self)
        }
    }
    
    private func isMandatoryFilled() -> Bool {
        var isFilled = true
        if fullNameTF.text == "" {
            AppUtility.showToastlocal(message: "First Name is required".localized, view: self.view)
            isFilled = false
        } else if let fName = fullNameTF.text, fName.count < 3 {
            AppUtility.showToastlocal(message: "First Name is required".localized, view: self.view)
            isFilled = false
        } else if address1TF.text == "" {
            AppUtility.showToastlocal(message: "Address Line 1 is required.".localized, view: self.view)
            isFilled = false
        } else if let address = address1TF.text, address.count > 0 && address.count < 4 {
            AppUtility.showToastlocal(message: "Address Line 1 is required.".localized, view: self.view)
            isFilled = false
        } else if stateValueLabel.text == "" || stateValueLabel.text == "Select State/ Division" {
            AppUtility.showToastlocal(message: "State/ Division is Required".localized, view: self.view)
            isFilled = false
        } else if townshipValueLabel.text == "" || townshipValueLabel.text == "Select Township" {
            AppUtility.showToastlocal(message: "Township is Required".localized, view: self.view)
            isFilled = false
        } else if let ph = phNumberTF.text, ph.count > 5 {
            let phno = String(ph.dropFirst().dropFirst().dropFirst().dropFirst())
            if let safeResult = result {
                if safeResult.isRejected || phno.count < (safeResult.min) || phno.count > (safeResult.max) {
                    AppUtility.showToastlocal(message: "Invalid phone number".localized, view: self.view)
                    isFilled = false
                }
            } else {
                let result = myanmarValidation(phno)
                if result.isRejected || phno.count < (result.min) || phno.count > (result.max) {
                    AppUtility.showToastlocal(message: "Invalid phone number".localized, view: self.view)
                    isFilled = false
                }
            }
        }
        return isFilled
    }
    
    private func showPaymentScreen() {
        if let peyment = UIStoryboard(name: "Cart", bundle: Bundle.main).instantiateViewController(withIdentifier: "Payment_ID") as? Payment {
            peyment.screenFrom = "Cart"
            self.navigationController?.pushViewController(peyment, animated: true)
        }
    }
    
    private func showTapGestureVC(type: tapGestureType) {
        self.view.endEditing(true)
        if let viewController = UIStoryboard(name: "Cart", bundle: Bundle.main).instantiateViewController(withIdentifier: "StateTownshipListing") as? StateTownshipListing {
            viewController.delegate = self
            viewController.selectedType = type
            if type == .state {
                viewController.countryObject = self.countryObject
            } else if type == .townShip {
                viewController.countryObject = self.selectedState
            }
            self.present(viewController, animated: true, completion: nil)
        }
    }
    
    private func isStateSelected() -> Bool {
        if self.selectedState.name == "" {
            return false
        }
        return true
    }
    
    private func loadFreshScreen() {
        fullNameTF.text = VMLoginModel.shared.firstName ?? ""
        phNumberTF.text = phNoToUIPhNo(phStr: defaultAddress?.phoneNumber ?? "")
        self.ropeStack.isHidden = true
        self.liftYesBtn.setImage(UIImage(named: "radio"), for: .normal)
        self.liftNoBtn.setImage(UIImage(named: "radio"), for: .normal)
        self.constraintTopMobileNumber.constant = -66
        self.view.layoutIfNeeded()
    }
    
    //MARK: - Button Action Methods
    @IBAction func selectColorButtonAction(_ sender: UIButton) {
        self.performSegue(withIdentifier: "AddAddressToRopeColorVCSegue", sender: self)
    }
    
    @IBAction func addNewAddressAction(_ sender: UIButton) {
        self.continueBtn.isUserInteractionEnabled = false
        if isMandatoryFilled() {
            if pressCount == 0 {
                pressCount = 1
            }else {
                self.continueBtn.isUserInteractionEnabled = true
                self.showViewController()
            }
            updateUserProfile()
            
            self.continueBtn.isUserInteractionEnabled = true
            self.phNumberTF.resignFirstResponder()
        } else {
            self.continueBtn.isUserInteractionEnabled = true
        }
    }
    
    @IBAction func stateTapAction(_ sender: UITapGestureRecognizer) {
        showTapGestureVC(type: tapGestureType.state)
    }
    
    @IBAction func townshipTapAction(_ sender: UITapGestureRecognizer) {
        if !isStateSelected() {
            let alert  = UIAlertController(title: "", message: "Please select State/ Division".localized, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }
        showTapGestureVC(type: tapGestureType.townShip)
    }
    
    @IBAction func liftYesButtonAction(_sender: UIButton) {
        liftYesBtn.setImage(UIImage(named: "act_radio"), for: .normal)
        liftNoBtn.setImage(UIImage(named: "radio"), for: .normal)
        liftOption = .yesLift
        ropeStack.isHidden = true
        constraintTopMobileNumber.constant = -66
        self.view.layoutIfNeeded()
    }
    
    func selectedYesLiftBtn() {
        liftYesBtn.setImage(UIImage(named: "act_radio"), for: .normal)
        liftNoBtn.setImage(UIImage(named: "radio"), for: .normal)
        liftOption = .yesLift
        ropeStack.isHidden = true
        constraintTopMobileNumber.constant = -66
        self.view.layoutIfNeeded()
    }
    
    @IBAction func liftNoButtonAction(_sender: UIButton) {
        liftNoBtn.setImage(UIImage(named: "act_radio"), for: .normal)
        liftYesBtn.setImage(UIImage(named: "radio"), for: .normal)
        liftOption = .noLift
        ropeStack.isHidden = false
        constraintTopMobileNumber.constant = 19
        self.view.layoutIfNeeded()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "AddAddressToRopeColorVCSegue" {
            if let vc = segue.destination as? RopeColorVC {
                vc.delegate = self
            }
        }
    }
}

extension AddAddress: RopeColorDelegate {
    func updateRopeColor(data: String) {
        colorSelectValueLabel.text = data.localized
    }
}

//MARK: - StateTownshipDelegate
extension AddAddress: StateTownshipDelegate {
    func selectedStateTownshipMethod(type: tapGestureType, selectedText: (String, String)) {
        switch type {
        case .state:
            if self.selectedState.name != selectedText.0 {
                self.selectedState = selectedText
                self.stateValueLabel.text = self.selectedState.name
                self.townshipValueLabel.text = "Select Township"
                self.townshipObject = (code: "", name: "")
                self.showTapGestureVC(type: .townShip)
            }
        case .townShip:
            self.townshipObject = selectedText
            self.townshipValueLabel.text = selectedText.1
            self.houseNoTF.becomeFirstResponder()
        case .none, .country:
            break
        }
    }
}

//MARK: - Api
extension AddAddress {
    private func updateUserProfile() {
        
        let urlString = String(format: "%@/checkout/checkoutsaveadress/1", APIManagerClient.sharedInstance.base_url)
        var params = ""
        guard let url = URL(string: urlString) else { return }
        if UserDefaults.standard.bool(forKey: "GuestLogin") {
            params = AppUtility.JSONStringFromAnyObject(value: self.addNewAddrParamsForGuestLogin() as AnyObject)
        }
        else {
            params = AppUtility.JSONStringFromAnyObject(value: self.addNewAddrParams() as AnyObject)
        }
        
        AppUtility.showLoading(self.view)
        println_debug("params to be add\(params)")
        aPIManager.genericClass(url: url, param: params as AnyObject, httpMethod: "POST", header: true, addAddress: "Add Address") { [weak self](response, success, _) in
            DispatchQueue.main.async {
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                }
                if success {
                    if let json = response as? Dictionary<String,Any> {
                        if let code = json["StatusCode"] as? Int, code == 200 {
                            self?.showAlert(status: "Success".localized)
                        } else {
                            self?.pressCount = 0
                            self?.showAlert(status: "Failure".localized, messageText: "Try again".localized)
                        }
                    }
                }
            }
        }
    }
    
    func addNewAddrParams() -> [[String:Any]] {
        var paramDicArray = [[String: Any]]()
        var paramDic            = [String:Any]()
        paramDic["key"] = "BillingNewAddress.FirstName"
        paramDic["value"] = fullNameTF.text ?? ""
        paramDicArray.append(paramDic)
        paramDic.removeAll()
        paramDic["key"] = "BillingNewAddress.LastName"
        paramDic["value"] = fullNameTF.text ?? ""
        paramDicArray.append(paramDic)
        paramDic.removeAll()
        paramDic["key"] = "BillingNewAddress.Email"
        paramDic["value"] = VMLoginModel.shared.email ?? ""
        paramDicArray.append(paramDic)
        paramDic.removeAll()
        paramDic["key"] = "BillingNewAddress.Company"
        paramDic["value"] = ""
        paramDicArray.append(paramDic)
        paramDic.removeAll()
        paramDic["key"] = "BillingNewAddress.CountryId"
        paramDic["value"] = countryObject.code
        paramDicArray.append(paramDic)
        paramDic.removeAll()
        paramDic["key"] = "BillingNewAddress.StateProvinceId"
        paramDic["value"] = selectedState.code
        paramDicArray.append(paramDic)
        paramDic.removeAll()
        paramDic["key"] = "BillingNewAddress.CityId"
        paramDic["value"] = townshipObject.code
        paramDicArray.append(paramDic)
        paramDic.removeAll()
        paramDic["key"] = "BillingNewAddress.Address1"
        paramDic["value"] = address1TF.text ?? ""
        paramDicArray.append(paramDic)
        paramDic.removeAll()
        paramDic["key"] = "BillingNewAddress.Address2"
        paramDic["value"] = address2TF.text ?? ""
        paramDicArray.append(paramDic)
        paramDic.removeAll()
        paramDic["key"] = "BillingNewAddress.PhoneNumber"
        if let mobNo = phNumberTF.text {
            paramDic["value"] = (mobNo as NSString).replacingCharacters(in: NSRange(location: 0, length: 4), with: "0095")
            paramDicArray.append(paramDic)
        }
        paramDic.removeAll()
        paramDic["key"] = "BillingNewAddress.FaxNumber"
        paramDic["value"] = ""
        paramDicArray.append(paramDic)
        paramDic.removeAll()
        paramDic["key"] = "BillingNewAddress.HouseNo"
        paramDic["value"] = "House No ".localized + "\(houseNoTF.text?.replacingOccurrences(of: "အိမ္ယာနံပါတ္", with: "").replacingOccurrences(of: "အိမ်အမှတ်", with: "") ?? "")"
        paramDicArray.append(paramDic)
        paramDic.removeAll()
        paramDic["key"] = "BillingNewAddress.FloorNo"
        paramDic["value"] = "Floor No ".localized + "\(floorTF.text?.replacingOccurrences(of: "အထပ္နံပါတ္", with: "").replacingOccurrences(of: "အထပ်နံပါတ်", with: "") ?? "")"
        paramDicArray.append(paramDic)
        paramDic.removeAll()
        paramDic["key"] = "BillingNewAddress.RoomNo"
        paramDic["value"] = "Room No ".localized + "\(roomNoTF.text?.replacingOccurrences(of: "အခန္းနံပါတ္", with: "").replacingOccurrences(of: "အခန်းအမှတ်", with: "") ?? "")"
        paramDicArray.append(paramDic)
        paramDic.removeAll()
        paramDic["key"] = "BillingNewAddress.RopeColor"
        paramDic["value"] = colorSelectValueLabel.text
        paramDicArray.append(paramDic)
        paramDic.removeAll()
        paramDic["key"] = "BillingNewAddress.IsLiftOption"
        paramDic["value"] = (liftOption == .yesLift) ? true : ((liftOption == .noLift) ? false : nil)
        paramDicArray.append(paramDic)
        paramDic.removeAll()
        paramDic["key"] = "BillingNewAddress.Longitude"
        paramDic["value"] = VMGeoLocationManager.shared.currentLongitude
        paramDicArray.append(paramDic)
        paramDic.removeAll()
        paramDic["key"] = "BillingNewAddress.Latitude"
        paramDic["value"] = VMGeoLocationManager.shared.currentLatitude
        paramDicArray.append(paramDic)
        paramDic.removeAll()
        paramDic["key"] = "BillingNewAddress.Id"
        if let screenFrm = self.screenFrom, screenFrm == "Edit Address", let defaultAdd = defaultAddress{
            paramDic["value"] = "\(defaultAdd.id ?? 0)"
        } else {
            
        }
        paramDicArray.append(paramDic)
        paramDic.removeAll()
        return paramDicArray
    }
    
    func addNewAddrParamsForGuestLogin() -> [[String:Any]] {
        var paramDicArray = [[String: Any]]()
        var paramDic            = [String:Any]()
        paramDic["key"] = "BillingNewAddress.FirstName"
        paramDic["value"] = fullNameTF.text ?? ""
        paramDicArray.append(paramDic)
        paramDic.removeAll()
        paramDic["key"] = "BillingNewAddress.LastName"
        paramDic["value"] = fullNameTF.text ?? ""
        paramDicArray.append(paramDic)
        paramDic.removeAll()
        paramDic["key"] = "BillingNewAddress.Email"
        paramDic["value"] = "no-reply@onestopmart.com"
        paramDicArray.append(paramDic)
        paramDic.removeAll()
        paramDic["key"] = "BillingNewAddress.Company"
        paramDic["value"] = ""
        paramDicArray.append(paramDic)
        paramDic.removeAll()
        paramDic["key"] = "BillingNewAddress.CountryId"
        paramDic["value"] = countryObject.code
        paramDicArray.append(paramDic)
        paramDic.removeAll()
        paramDic["key"] = "BillingNewAddress.StateProvinceId"
        paramDic["value"] = selectedState.code
        paramDicArray.append(paramDic)
        paramDic.removeAll()
        paramDic["key"] = "BillingNewAddress.CityId"
        paramDic["value"] = townshipObject.code
        paramDicArray.append(paramDic)
        paramDic.removeAll()
        paramDic["key"] = "BillingNewAddress.Address1"
        paramDic["value"] = address1TF.text ?? ""
        paramDicArray.append(paramDic)
        paramDic.removeAll()
        paramDic["key"] = "BillingNewAddress.Address2"
        paramDic["value"] = address2TF.text ?? ""
        paramDicArray.append(paramDic)
        paramDic.removeAll()
        paramDic["key"] = "BillingNewAddress.PhoneNumber"
        if let mobNo = phNumberTF.text {
            paramDic["value"] = (mobNo as NSString).replacingCharacters(in: NSRange(location: 0, length: 4), with: "0095")
            paramDicArray.append(paramDic)
        }
        paramDic.removeAll()
        paramDic["key"] = "BillingNewAddress.FaxNumber"
        paramDic["value"] = ""
        paramDicArray.append(paramDic)
        paramDic.removeAll()
        paramDic["key"] = "BillingNewAddress.HouseNo"
        paramDic["value"] = "House No ".localized + "\(houseNoTF.text?.replacingOccurrences(of: "အိမ္ယာနံပါတ္", with: "").replacingOccurrences(of: "အိမ်အမှတ်", with: "") ?? "")"
        paramDicArray.append(paramDic)
        paramDic.removeAll()
        paramDic["key"] = "BillingNewAddress.FloorNo"
        paramDic["value"] = "Floor No ".localized + "\(floorTF.text?.replacingOccurrences(of: "အထပ္နံပါတ္", with: "").replacingOccurrences(of: "အထပ်နံပါတ်", with: "") ?? "")"
        paramDicArray.append(paramDic)
        paramDic.removeAll()
        paramDic["key"] = "BillingNewAddress.RoomNo"
        paramDic["value"] = "Room No ".localized + "\(roomNoTF.text?.replacingOccurrences(of: "အခန္းနံပါတ္", with: "").replacingOccurrences(of: "အခန်းအမှတ်", with: "") ?? "")"
        paramDicArray.append(paramDic)
        paramDic.removeAll()
        paramDic["key"] = "BillingNewAddress.RopeColor"
        paramDic["value"] = colorSelectValueLabel.text
        paramDicArray.append(paramDic)
        paramDic.removeAll()
        paramDic["key"] = "BillingNewAddress.IsLiftOption"
        paramDic["value"] = (liftOption == .yesLift) ? true : ((liftOption == .noLift) ? false : nil)
        paramDicArray.append(paramDic)
        paramDic.removeAll()
        paramDic["key"] = "BillingNewAddress.Longitude"
        paramDic["value"] = VMGeoLocationManager.shared.currentLongitude
        paramDicArray.append(paramDic)
        paramDic.removeAll()
        paramDic["key"] = "BillingNewAddress.Latitude"
        paramDic["value"] = VMGeoLocationManager.shared.currentLatitude
        paramDicArray.append(paramDic)
        paramDic.removeAll()
        paramDic["key"] = "BillingNewAddress.Id"
        if let screenFrm = self.screenFrom, screenFrm == "Edit Address", let defaultAdd = defaultAddress{
            paramDic["value"] = "\(defaultAdd.id ?? 0)"
        } else {
            
        }
        paramDicArray.append(paramDic)
        paramDic.removeAll()
        return paramDicArray
    }
    
    private func showAlert(message: String) {
        if message == "Your details updated successfully".localized {
            DispatchQueue.main.async {
                
                let alertVC = SAlertController()
                alertVC.ShowSAlert(title: message, withDescription: "Updated successfully".localized, onController: self)
                let ok = SAlertAction()
                ok.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                    self.pressCount = 0
                    self.showViewController()
                })
                alertVC.addAction(action: [ok])
            }
        } else {
            self.pressCount = 0
            self.showAlert(alertTitle: "Warning!".localized, description: message)
        }
    }
    
    
    private func showAlert(status: String, messageText: String? = "") {
        DispatchQueue.main.async {
            self.view.endEditing(true)
            if status == "Success" {
                var message = ""
                if let screenFrm = self.screenFrom, screenFrm == "Edit Address" {
                    message = "Delivery Address Successfully Updated.".localized
                } else {
                    message = "Address added successfully".localized
                }
                VMLoginModel.shared.addressAdded = true
                
                let alertVC = SAlertController()
                alertVC.ShowSAlert(title: "Success", withDescription: message.localized, onController: self)
                let ok = SAlertAction()
                ok.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                    self.pressCount = 0
                    self.showViewController()
                })
                alertVC.addAction(action: [ok])
            } else {
                self.pressCount = 0
                let alertVC = SAlertController()
                alertVC.ShowSAlert(title: "", withDescription: messageText?.localized ?? "", onController: self)
                let ok = SAlertAction()
                ok.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                    
                })
                alertVC.addAction(action: [ok])
                
            }
        }
    }
    
    private func getBillform() {
        let urlString = String(format: "%@/checkout/billingform", APIManagerClient.sharedInstance.base_url)
        guard let url = URL(string: urlString) else { return }
        let params = Dictionary<String, Any>()
        AppUtility.showLoading(self.view)
        aPIManager.genericClass(url: url, param: params as AnyObject, httpMethod: "GET", header: true) { [weak self] (response, success, _) in
            
            DispatchQueue.main.async {
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                }
            }
            if success {
                if let json = response as? Dictionary<String,Any> {
                    if let code = json["StatusCode"] as? Int, code == 200 {
                        if let newAddress = json["NewAddress"] as? Dictionary<String, Any> {
                            if let firstObj = newAddress["AvailableCountries"] as? NSArray, firstObj.count > 1 {
                                if let countryObj = firstObj.lastObject as? Dictionary<String, Any> {
                                    self?.countryObject.code = countryObj["Value"].safelyWrappingString()
                                    self?.countryObject.name = countryObj["Text"].safelyWrappingString()
                                }
                            }
                        }
                    } else {
                        self?.showAlert(status: "Failure".localized, messageText: "No Records found".localized)
                    }
                }
            }
        }
    }
}

//MARK: - UITextFieldDelegate
extension AddAddress: UITextFieldDelegate, PhValidationProtocol {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == fullNameTF || textField == address1TF || textField == address2TF {
            textField.autocapitalizationType = .allCharacters
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        switch textField {
        case self.fullNameTF:
            //            let onlyAlphabetsSpace = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ ")
            //            let invertedSet = onlyAlphabetsSpace.inverted
            //            let filteredSet = string.components(separatedBy: invertedSet).joined(separator: "")
            //            if string != filteredSet {
            //                return false
            //            }
            if text.count == 1 {
                if string == " " {
                    return false
                }
            }
            if text.count > 40 {
                return false
            }
            if string == "" || string == " " {
                let _ = Validations.trimSuccessiveSpaces(textField: textField, range: range, string: string)
                return false
            }
            break
        case self.phNumberTF:
            if !text.hasPrefix("+95 09") {
                return false
            }
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: "0123456789").inverted).joined(separator: "")) {
                return false
            }
            if text.count > 17 {
                self.view.endEditing(true)
                return false
            }
            let phno = String(text.dropFirst().dropFirst().dropFirst().dropFirst())
            result = myanmarValidation(phno)
            if result!.isRejected {
                if string == "" {
                    textField.text = text
                }else {
                    AppUtility.showToastlocal(message: "Invalid mobile number".localized, view: self.view)
                }
                return false
            }
            if text.count  > (result!.max + 3) {
                if (text.count - (result!.max + 3)) == 1 {
                    textField.text = text
                }else {
                    
                }
                self.view.endEditing(true)
                return false
            }
        case address1TF, address2TF:
            if text.count > 80 {
                return false
            }
        case houseNoTF, floorTF, roomNoTF:
            if text.count > 6 {
                return false
            }
        default:
            break
        }
        return true
    }
}
//MARK: - UneditableTextField
class UneditableTextField: SubSkyFloatTextField {
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        return false
    }
    
    override func closestPosition(to point: CGPoint) -> UITextPosition? {
        let beginning = self.beginningOfDocument
        let end = self.position(from: beginning, offset: self.text?.count ?? 0)
        return end
    }
    
    override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: 0, y: 0, width: 90 , height: bounds.height)
    }
    
    
}

class SubSkyFloatTextField: SkyFloatingLabelTextField {
    
}

class Validations {
    class func trimSuccessiveSpaces( textField: UITextField, range: NSRange, string: String) -> Bool {
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        var commaRestrStr = text
        
        if string == " " {
            var boundValue = range.upperBound + 1
            if text.contains("  ") {
                commaRestrStr = commaRestrStr.replacingOccurrences(of: "  ", with: " ")
                boundValue = range.lowerBound
            }
            if text.contains("  ") {
                boundValue = range.lowerBound
                commaRestrStr = commaRestrStr.replacingOccurrences(of: "  ", with: " ")
            }
            textField.text = commaRestrStr
            if range.upperBound == range.lowerBound {
                if let newPosition = textField.position(from: textField.beginningOfDocument, offset: boundValue) {
                    textField.selectedTextRange = textField.textRange(from: newPosition, to: newPosition)
                }
            } else {
                if let newPosition = textField.position(from: textField.beginningOfDocument, offset: range.lowerBound) {
                    textField.selectedTextRange = textField.textRange(from: newPosition, to: newPosition)
                }
            }
        } else if string == "" {
            let boundValue = range.lowerBound
            if text.contains("  ") {
                commaRestrStr = commaRestrStr.replacingOccurrences(of: "  ", with: " ")
            }
            if text.contains("  ") {
                commaRestrStr = commaRestrStr.replacingOccurrences(of: "  ", with: " ")
            }
            textField.text = commaRestrStr
            if let newPosition = textField.position(from: textField.beginningOfDocument, offset: boundValue) {
                textField.selectedTextRange = textField.textRange(from: newPosition, to: newPosition)
            }
        }
        return false
    }
}



