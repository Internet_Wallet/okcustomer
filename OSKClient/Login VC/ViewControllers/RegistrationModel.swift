//
//  RegistrationModel.swift
//  VMart
//
//  Created by Imac on 2/4/19.
//  Copyright © 2019 Shobhit. All rights reserved.
//

import UIKit
import AVFoundation

class RegistrationModel: NSObject {
    
    static var share = RegistrationModel()
    
    var Address1 : String = ""
    var Address2 : String = ""
    var City : String = ""
    var State : String = ""
    var Country : String = ""
    var DateOfBirthDay : String = ""
    var DateOfBirthYear : String = ""
    var DateofBirthMonth : String = ""
    var ProfilePictureUrl : String = ""
    var Email : String = ""
    var Firstname : String = ""
    var Lastname : String = ""
    var Gender : String = ""
    var MobileNumber : String = ""
    var Username : String = ""
    var Password : String = String(UUID2.suffix(6))
    var Mode  = "register"
    var OtherEmail : String = ""
    var ViberNumber : String = ""
    var MaritalStatus : Int = 0
    var DisplayAvatar : Bool = true
    var DeviceID : String = UUID2
    var Simid : String = String(UUID2.suffix(6))
    var Token : String = ""
    var DeviceInfo = Dictionary<String,Any>()
    var InternetCheckin = true
    var BSSID : String = ""
    var BluetoothAddress : String = ""
    var BluetoothName : String = ""
    var Cellid : String = ""
    var ConnectedNetworkType : String = "WIFI"
    var DeviceSoftwareVersion : String = UIDevice.current.systemVersion
    var HiddenSSID = false
    var IPAddress : String = ""
    var LinkSpeed = 1
    var MACAddress : String = ""
    var Msid1 : String = ""
    var Msid2 : String = ""
    var NetworkCountryIso : String = "mm"
    var NetworkID : String = ""
    var NetworkOperator : String = ""
    var NetworkOperatorName : String = ""
    var NetworkSignal : String = ""
    var NetworkType =  "3G"
    var PhoneType : String = "GSM"
    var SIMCountryIso =  "mm"
    var SIMOperator = ""
    var SIMOperatorName = ""
    var SSID : String = ""
    var Simid1 : String = UUID2
    var Simid2 : String = ""
    var VoiceMailNo : String = ""
    var isNetworkRoaming = false
    var Latitude : String = ""
    var Longitude : String = ""
    
    func wrapDeviceInfoData() -> Dictionary<String,Any> {
        let dic = [ "InternetCheckin": InternetCheckin,"BSSID": BSSID,"BluetoothAddress": BluetoothAddress,"BluetoothName": BluetoothName,"Cellid": Cellid,"ConnectedNetworkType": ConnectedNetworkType,"DeviceSoftwareVersion": DeviceSoftwareVersion,"HiddenSSID": HiddenSSID,"IPAddress": IPAddress,"LinkSpeed": LinkSpeed,"MACAddress": MACAddress,"Msid1": Msid1,"Msid2": Msid2,"NetworkCountryIso": NetworkCountryIso,"NetworkID": NetworkID,"NetworkOperator": NetworkOperator,"NetworkOperatorName": NetworkOperatorName,"NetworkSignal": NetworkSignal,"NetworkType": NetworkType,"PhoneType": PhoneType,"SIMCountryIso": SIMCountryIso,"SIMOperator": SIMOperator,"SIMOperatorName": SIMOperatorName,"SSID": SSID,"Simid1": Simid1,"Simid2": Simid2,"VoiceMailNo": VoiceMailNo,"isNetworkRoaming": isNetworkRoaming,"Latitude": Latitude,"Longitude": Longitude] as [String : Any]
        return dic
    }
    
    func wrapData() -> Dictionary<String,Any> {
        let dic = ["Address1": Address1,
                   "Address2": Address1,
                   "City": City,
                   "State": State,
                   "Country": Country,
                   "DateOfBirthDay": DateOfBirthDay,
                   "DateOfBirthYear": DateOfBirthYear,
                   "DateofBirthMonth": DateofBirthMonth,
                   "ProfilePictureUrl" : ProfilePictureUrl,
                   "Email": Email,
                   "Firstname": Firstname,
                   "Lastname": Lastname,
                   "Gender": Gender,
                   "MobileNumber":MobileNumber,
                   "Username": Username,
                   "Password": Password,
                   "Mode": Mode,
                   "OtherEmail": OtherEmail,
                   "ViberNumber": ViberNumber,
                   "MaritalStatus": MaritalStatus,
                   "DisplayAvatar": DisplayAvatar,
                   "DeviceID": DeviceID,
                   "Simid": Simid,
                   "Token": Token,
                   "DeviceInfo": DeviceInfo,
                   "VersionCode": APIManagerClient.sharedInstance.versioncode
            ] as [String : Any]
        return dic
    }
    
    func clearData() {
        
        RegistrationModel.share.DateOfBirthDay = ""
        RegistrationModel.share.DateofBirthMonth = ""
        RegistrationModel.share.DateOfBirthYear = ""
        
        RegistrationModel.share.Firstname = ""
        RegistrationModel.share.Lastname = ""
        RegistrationModel.share.Email = ""
        RegistrationModel.share.Gender = ""
        
        RegistrationModel.share.ProfilePictureUrl = ""
        
    }
    
}
