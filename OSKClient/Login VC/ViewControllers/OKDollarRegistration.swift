//
//  OKDollarRegistration.swift
//  VMart
//
//  Created by Mohit on 13/12/2019.
//  Copyright © 2019 Shobhit. All rights reserved.
//

import UIKit

class OKDollarRegistration: UIViewController {
    
    
    @IBOutlet weak var btnConRegistration: UIButton!{
        didSet {
            //self.btnConRegistration.setTitle(("Register" ?? "").localized, for: .normal)
            btnConRegistration.titleLabel?.font = UIFont(name: appFont, size: 15.0)
            btnConRegistration.applyButtonGradient(colors: [UIColor.colorWithRedValue(redValue: 13, greenValue: 177, blueValue: 75, alpha: 1), UIColor.init(red: 20.0/255.0, green: 185.0/255.0, blue: 85.0/255.0, alpha: 1.0)])
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func continueTapAction(_ sender: Any){
        AppUtility.showToastlocal(message: "Registration by OK Dollar".localized, view: self.view)
    }
    
    
    @IBAction func dismissTapAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
