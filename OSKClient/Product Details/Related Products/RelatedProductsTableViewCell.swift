//
//  RelatedProductsTableViewCell.swift
//  VMart
//
//  Created by Shobhit Singhal on 11/3/18.
//  Copyright © 2018 Shobhit. All rights reserved.
//

import UIKit

protocol RelatedProductsCollectionCellDelegate:class {
    func tapOnRelatedProductCollectionView(collectioncell:ProductListCollectionViewCell?)
}


class RelatedProductsTableViewCell: UITableViewCell {
    
    var relatedProductsInfo = [ProductsInfo]()
    var parentVC:ProductDetails?
    weak var cellDelegate:RelatedProductsCollectionCellDelegate?
    
    
    @IBOutlet weak var relatedProductCollectionView: UICollectionView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.relatedProductCollectionView.register(UINib(nibName: "ProductListCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ProductListCell")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func updateRelatedProducts(relatedProductsInfo: [ProductsInfo]) {
        self.relatedProductsInfo = relatedProductsInfo
        self.relatedProductCollectionView?.reloadData()
    }
}

extension RelatedProductsTableViewCell: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.relatedProductsInfo.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = relatedProductCollectionView.dequeueReusableCell(withReuseIdentifier: "ProductListCell", for: indexPath) as! ProductListCollectionViewCell
        cell.layer.borderWidth = 0.5
        cell.layer.borderColor = UIColor(red: 210/255, green: 210/255, blue: 210/255, alpha: 1.0).cgColor
        cell.delegate = self
        cell.updateCellWithData(productObject: self.relatedProductsInfo[indexPath.row])
        cell.wishListBtn.isHidden = true
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.size.width/2 - 25, height: collectionView.bounds.size.width/2 + 40)
    }
    
    // MARK: UICollectionViewDelegate
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        let cell = collectionView.cellForItem(at: indexPath) as? ProductListCollectionViewCell
        self.cellDelegate?.tapOnRelatedProductCollectionView(collectioncell: cell)
    }
}

extension RelatedProductsTableViewCell : AddRemoveWishListProtocol {
    func addToWishList(cell: ProductListCollectionViewCell?) {
        if let index = self.relatedProductCollectionView.indexPath(for: cell!) {
            let model = self.relatedProductsInfo[index.row]
            model.iswishList = true
        }
    }
    
    func removeFromWishList(cell: ProductListCollectionViewCell?) {
        if let index = self.relatedProductCollectionView.indexPath(for: cell!) {
            let model = self.relatedProductsInfo[index.row]
            model.iswishList = false
        }
    }
    
    private func updateModel(filterArray: [GetHomePageProducts], status: Bool) {
    }
    
    private func updateModel(filterArray: [ProductsInfo], status: Bool) {
    }
    
    func addToWishList(id: NSInteger) {
    }
    
    func removeToWishList(id: NSInteger) {
    }
}
