//
//  WebViewAboutUS.swift
//  NopCommerce
//
//  Created by BS-136 on 3/30/18.
//  Copyright © 2018 Jahid Hassan. All rights reserved.
//

import Foundation
import ObjectMapper
import SwiftyJSON

class WebViewAboutUS: Mappable {
    var TopicTitle: String = ""
    var TopicBody: String  = ""
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        self.TopicTitle  <- map["Title"]
        self.TopicBody   <- map["Body"]
    }
}
