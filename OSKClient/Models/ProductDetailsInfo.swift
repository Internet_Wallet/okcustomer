//
//  ProductDetailsInfo.swift
//  NopCommerce
//
//  Created by BS-125 on 11/23/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit
import ObjectMapper
import SwiftyJSON

class ProductDetailsInfo: Mappable {
    
    var addToCartModel :                    AddToCartModel?
    var breadcrumb :                        [Breadcrumb]?
    var giftCardModel :                     [GiftCardModel]?
    var vendorModel :                       [VendorModel]?
    var productReviewOverviewModel :        ProductReviewOverviewModel?
    //var productPriceModel :                 [ProductPriceModel]?
    var productPriceModel:                  ProductPriceModel?
    var productAttributes :                 [ProductAttributesInfo]?
    var productSpecifications :             [ProductSpecifications]?
    var quantityModel:                      QuantityModel?
    
    var AssociatedProducts :                  NSArray?
    var CompareProductsEnabled :              Bool = false
    var CustomProperties :                    NSString?
    var defaultPictureModelModel :            DefaultPictureModel?
    var DefaultPictureZoomEnabled :           Bool = false
    
    var DeliveryDate :                        NSString?
    var DisplayBackInStockSubscription :      Int = 0
    var EmailAFriendEnabled :                 Bool = false
    
    var FreeShippingNotificationEnabled :     Bool = false
    
    var FullDescription :                     String = ""
    var HasSampleDownload :                   Bool = false
    
    var Id :                                  Int = 0
    var IsFreeShipping :                     Bool = false
    var GiftCard_IsRental :                   Bool = false
    
    var IsShipEnabled :                       Bool = false
    
    var ManufacturerPartNumber :              NSString?
    var Name :                                NSString?
    var PictureModels :                       NSArray?
    var ProductManufacturers :                NSArray?
    var ProductTemplateViewPath :             NSString?
    var RentalEndDate :                       NSString?
    var RentalStartDate :                     NSString?
    var ShortDescription :                    String = ""
    var ShowManufacturerPartNumber :          NSInteger?
    var ShowVendor :                          Bool = false
    
    var StockAvailability : String            = ""
    var TierPrices  :                         NSArray?
    
    
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        
        self.addToCartModel                 <- map["AddToCart"]
        self.breadcrumb                     <- map["Breadcrumb"]
        self.giftCardModel                  <- map["GiftCard"]
        self.vendorModel                    <- map["VendorModel"]
        self.productReviewOverviewModel     <- map["ProductReviewOverview"]
        self.productPriceModel              <- map["ProductPrice"]
        self.productAttributes              <- map["ProductAttributes"]
        self.productSpecifications          <- map["ProductSpecifications"]
        
        self.AssociatedProducts                 <- map["AssociatedProducts"]
        self.CompareProductsEnabled             <- map["CompareProductsEnabled"]
        self.CustomProperties                   <- map["CustomProperties"]
        self.DefaultPictureZoomEnabled          <- map["DefaultPictureZoomEnabled"]
        self.DeliveryDate                       <- map["DeliveryDate"]
        self.DisplayBackInStockSubscription     <- map["DisplayBackInStockSubscription"]
        self.EmailAFriendEnabled                <- map["EmailAFriendEnabled"]
        self.FreeShippingNotificationEnabled    <- map["FreeShippingNotificationEnabled"]
        self.FullDescription                    <- map["FullDescription"]
        self.HasSampleDownload                  <- map["HasSampleDownload"]
        self.Id                                 <- map["Id"]
        self.IsFreeShipping                     <- map["IsFreeShipping"]
        self.GiftCard_IsRental                  <- map["IsFreeShipping"]
        self.IsShipEnabled                      <- map["IsShipEnabled"]
        self.ManufacturerPartNumber             <- map["ManufacturerPartNumber"]
        self.Name                               <- map["Name"]
        self.PictureModels                      <- map["PictureModels"]
        self.defaultPictureModelModel           <- map["DefaultPictureModel"]
        self.ProductManufacturers               <- map["ProductManufacturers"]
        self.ProductTemplateViewPath            <- map["ProductTemplateViewPath"]
        self.RentalEndDate                      <- map["RentalEndDate"]
        self.RentalStartDate                    <- map["RentalStartDate"]
        self.ShortDescription                   <- map["ShortDescription"]
        self.ShowManufacturerPartNumber         <- map["ShowManufacturerPartNumber"]
        self.ShowVendor                         <- map["ShowVendor"]
        self.StockAvailability                  <- map["StockAvailability"]
        self.TierPrices                         <- map["StockAvailability"]
        self.quantityModel                      <- map["Quantity"]
    }
}

class QuantityModel: Mappable{
    var OrderMaximumQuantity: Int = 0
    var StockQuantity: Int = 0
    var OrderMinimumQuantity: Int = 0
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        self.OrderMaximumQuantity   <- map["OrderMaximumQuantity"]
        self.StockQuantity          <- map["StockQuantity"]
        self.OrderMinimumQuantity   <- map["OrderMinimumQuantity"]
    }
}


class AddToCartModel: Mappable {
    
    var AllowedQuantities :                   NSArray?
    var AvailableForPreOrder :                NSInteger?
    var CustomerEnteredPrice :                NSInteger?
    var CustomerEnteredPriceRange :           NSInteger?
    var CustomerEntersPrice :                 NSInteger?
    var DisableBuyButton :                    Bool = false
    var DisableWishlistButton :               Bool = false
    var IsWishList :                          Bool = false
    var EnteredQuantity :                     Int = 0
    var IsRental :                            Bool = false
    var PreOrderAvailabilityStartDateTimeUtc :String?
    var Cart_ProductId :                      NSInteger?
    var UpdatedShoppingCartItemId :           NSInteger?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        self.AllowedQuantities                      <- map["AllowedQuantities"]
        self.AllowedQuantities                      <- map["AllowedQuantities"]
        self.AvailableForPreOrder                   <- map["AvailableForPreOrder"]
        self.CustomerEnteredPrice                   <- map["CustomerEnteredPrice"]
        self.CustomerEnteredPriceRange              <- map["CustomerEnteredPriceRange"]
        self.CustomerEntersPrice                    <- map["CustomerEntersPrice"]
        self.DisableBuyButton                       <- map["DisableBuyButton"]
        self.DisableWishlistButton                  <- map["DisableWishlistButton"]
        self.IsWishList                             <- map["IsWishList"]
        self.EnteredQuantity                        <- map["EnteredQuantity"]
        self.IsRental                               <- map["EnteredQuantity"]
        self.PreOrderAvailabilityStartDateTimeUtc   <- map["PreOrderAvailabilityStartDateTimeUtc"]
        self.Cart_ProductId                         <- map["ProductId"]
        self.UpdatedShoppingCartItemId              <- map["UpdatedShoppingCartItemId"]
    }
}

class GiftCardModel: Mappable {
    
    var GiftCard_CustomProperties :           NSString?
    var GiftCardType :                        NSInteger?
    var IsGiftCard :                          Bool = false
    var Message :                             NSString?
    var RecipientEmail :                      NSString?
    var RecipientName :                       NSString?
    var SenderEmail :                         NSString?
    var SenderName :                          NSString?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        self.GiftCard_CustomProperties     <- map["CustomProperties"]
        self.GiftCardType                  <- map["GiftCardType"]
        self.IsGiftCard                    <- map["IsGiftCard"]
        self.Message                       <- map["Message"]
        self.RecipientEmail                <- map["RecipientEmail"]
        self.RecipientName                 <- map["RecipientName"]
        self.SenderEmail                   <- map["SenderEmail"]
        self.SenderName                    <- map["SenderName"]
    }
}

class VendorModel: Mappable {
    
    var VendorModel_CustomProperties :        NSString?
    var VendorModel_Id  :                     Int = 0
    var VendorModel_Name :                    NSString?
    var SeName :                              NSString?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        self.VendorModel_CustomProperties   <- map["CustomProperties"]
        self.VendorModel_Id                 <- map["Id"]
        self.VendorModel_Name               <- map["Name"]
        self.SeName                         <- map["SeName"]
    }
}


class ProductReviewOverviewModel: Mappable {
    
    var AllowCustomerReviews :                Bool = false
    var ProductReviewOverview_ProductId  :    NSInteger?
    var RatingSum :                           Float?
    var TotalReviews :                        NSInteger?
    var RatingUrlFromGSMArena :                        NSString?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        self.AllowCustomerReviews               <- map["AllowCustomerReviews"]
        self.ProductReviewOverview_ProductId    <- map["ProductId"]
        self.RatingSum                          <- map["RatingSum"]
        self.TotalReviews                       <- map["TotalReviews"]
        self.RatingUrlFromGSMArena                       <- map["RatingUrlFromGSMArena"]
    }
}

class ProductPriceModel: Mappable {
    
    var BasePricePAngV :                      NSString?
    var CallForPrice   :                      NSInteger?
    var CurrencyCode  :                       NSString?
    var ProductPrice_CustomProperties :       NSString?
    var ProductPrice_CustomerEntersPrice :    NSInteger?
    var DisplayTaxShippingInfo :              Bool = false
    var HidePrices :                          Bool = false
    var ProductPrice_IsRental :               Bool = false
    
    var OldPrice :                            String = ""
    var oldPriceValue :                       Float?
    
    var Price :                               String = ""
    var PriceValue :                          Float?
    var DiscountPercentage :                  Int?
    
    var PriceWithDiscount :                   String = ""
    var PriceWithDiscountValue :              Float?
    
    var ProductPrice_ProductId :              NSInteger?
    var RentalPrice  :                        NSString?
    
    var DiscountOfferName :                   String = ""
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        self.BasePricePAngV                         <- map["BasePricePAngV"]
        self.CallForPrice                           <- map["CallForPrice"]
        self.CurrencyCode                           <- map["CurrencyCode"]
        self.ProductPrice_CustomProperties          <- map["CustomProperties"]
        self.ProductPrice_CustomerEntersPrice       <- map["CustomerEntersPrice"]
        
        self.DisplayTaxShippingInfo                 <- map["DisplayTaxShippingInfo"]
        self.HidePrices                             <- map["HidePrices"]
        self.ProductPrice_IsRental                  <- map["IsRental"]
        
        self.OldPrice                               <- map["OldPrice"]
        self.oldPriceValue                          <- map["OldPriceValue"]
        
        self.Price                                  <- map["Price"]
        self.PriceValue                             <- map["PriceValue"]
        
        self.PriceWithDiscount                      <- map["PriceWithDiscount"]
        self.PriceWithDiscountValue                 <- map["PriceWithDiscountValue"]
        self.ProductPrice_ProductId                 <- map["ProductId"]
        self.RentalPrice                            <- map["RentalPrice"]
        self.DiscountPercentage                     <- map["DiscountPercentage"]
        self.DiscountOfferName                      <- map["DiscountOfferName"]
    }
}

class DefaultPictureModel: Mappable {
    var ImageUrl     : NSString?
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        self.ImageUrl <- map["ImageUrl"]
    }
}

