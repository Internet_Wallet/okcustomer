//
//  medicineRequest.swift
//  VMart
//
//  Created by Kethan on 7/23/19.
//  Copyright © 2019 Shobhit. All rights reserved.
//

import Foundation
import ObjectMapper
import SwiftyJSON

class GetMedicineModel: Mappable {
    
    var medicineData: [medicineRequest]?
    
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        self.medicineData  <- map["Data"]
    }
}


class medicineRequest: Mappable {
    
    var iD: Int = -100
    var requestStatusId : Int = -110
    var requestStatusMessage : String?
    var orderTotal : Int = -120
    var orderTotalValue : Int = -130
    var rejectedReason : String?
    var medicineItems : [[String : Any]]?
    var patientName : String?
    var doctorName : String?
    var hospitalName : String?
    var mobileNumber : String?
    var remarks : String?
    var prescriptionImageUrl: [String]?
    
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        
        self.iD                     <- map["Id"]
        self.requestStatusId        <- map["RequestStatusId"]
        self.requestStatusMessage   <- map["RequestStatusMessage"]
        self.orderTotal             <- map["OrderTotal"]
        self.orderTotalValue        <- map["OrderTotalValue"]
        self.rejectedReason         <- map["RejectedReason"]
        self.medicineItems          <- map["MedicineItems"]
        self.patientName            <- map["PatientName"]
        self.doctorName            <- map["DoctorName"]
        self.hospitalName            <- map["HospitalName"]
        self.mobileNumber           <- map["MobileNumber"]
        self.remarks                <- map["Remarks"]
        self.prescriptionImageUrl   <- map["PrescriptionImageUrl"]
    }
}
