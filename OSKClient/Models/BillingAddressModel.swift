//
//  BillingAddressModel.swift
//  NopCommerce
//
//  Created by Masudur Rahman on 12/15/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit

open class BillingAddressModel: NSObject {
    
    var newBillingAddress: GenericBillingAddress?
    var existingBillingAddresses: [GenericBillingAddress] = []
    
}


open class GenericBillingAddress: NSObject {
    
    var id: Int?
    var firstName: String?
    var lastName: String?
    var email: String?
    var companyEnabled: Bool
    var companyRequired: Bool
    var companyName: String?
    var countryEnabled: Bool
    var countryId: Int?
    var countryName: String?
    var stateProvinceEnabled: Bool
    var stateProvinceId: Int?
    var stateProvinceName: String?
    var cityEnabled: Bool
    var cityRequired: Bool
    var cityId: Int?
    var cityName: String?
    var streetAddressEnabled: Bool
    var streetAddressRequired: Bool
    var address1: String?
    var streetAddress2Enabled: Bool
    var streetAddress2Required: Bool
    var address2: String?
    var zipPostalCodeEnabled: Bool
    var zipPostalCodeRequired: Bool
    var zipPostalCode: String?
    var phoneEnabled: Bool
    var phoneRequired: Bool
    var phoneNumber: String?
    var roomNo: String?
    var floorNo: String?
    var houseNo: String?
    var faxEnabled: Bool
    var faxRequired: Bool
    var faxNumber: String?
    var isLift: Int?
    var ropeColor: String?
    var availableCountries: [BillingCountry]
    var availableStates: [BillingState]
    
    init(address: [String: AnyObject]) {
        
        self.id = address["Id"] as? Int
        
        self.firstName = address["FirstName"] as? String
        self.lastName = address["LastName"] as? String
        self.email = address["Email"] as? String
        
        self.companyEnabled = address["CompanyEnabled"] as! Bool
        self.companyRequired = address["CompanyRequired"] as! Bool
        self.companyName = address["Company"] as? String
        
        self.countryEnabled = address["CountryEnabled"] as! Bool
        self.countryId = address["CountryId"] as? Int
        self.countryName = address["CountryName"] as? String
        
        self.stateProvinceEnabled = address["StateProvinceEnabled"] as! Bool
        self.stateProvinceId = address["StateProvinceId"] as? Int
        self.stateProvinceName = address["StateProvinceName"] as? String
        
        self.cityEnabled = address["CityEnabled"] as! Bool
        self.cityRequired = address["CityRequired"] as! Bool
        self.cityName = address["City"] as? String
        self.cityId = address["CityId"] as? Int
        
        self.streetAddressEnabled = address["StreetAddressEnabled"] as! Bool
        self.streetAddressRequired = address["StreetAddressRequired"] as! Bool
        self.address1 = address["Address1"] as? String
        
        self.streetAddress2Enabled = address["StreetAddress2Enabled"] as! Bool
        self.streetAddress2Required = address["StreetAddress2Required"] as! Bool
        self.address2 = address["Address2"] as? String
        
        self.zipPostalCodeEnabled = address["ZipPostalCodeEnabled"] as! Bool
        self.zipPostalCodeRequired = address["ZipPostalCodeRequired"] as! Bool
        self.zipPostalCode = address["ZipPostalCode"] as? String
        
        self.phoneEnabled = address["PhoneEnabled"] as! Bool
        self.phoneRequired = address["PhoneRequired"] as! Bool
        self.phoneNumber = address["PhoneNumber"] as? String
        
        self.faxEnabled = address["FaxEnabled"] as! Bool
        self.faxRequired = address["FaxRequired"] as! Bool
        self.faxNumber = address["FaxNumber"] as? String
        
        self.roomNo = address["RoomNo"] as? String
        self.floorNo = address["FloorNo"] as? String
        self.houseNo = address["HouseNo"] as? String
        self.isLift = address["IsLiftOption"] as? Int
        self.ropeColor = address["RopeColor"] as? String
        
        self.availableCountries = [BillingCountry]()
        for country in address["AvailableCountries"] as! NSArray {
            let billingCountry = BillingCountry(country: country as! [String : AnyObject])
            self.availableCountries.append(billingCountry)
        }
        
        self.availableStates = [BillingState]()
        for state in address["AvailableStates"] as! NSArray {
            let billingState = BillingState(state: state as! [String : AnyObject])
            self.availableStates.append(billingState)
        }
    }
    
}

open class BillingCountry: NSObject {
    
    var countryName: String?
    var countryId: String?
    
    init(country: [String : AnyObject]) {
        self.countryName = country["Text"] as? String
        self.countryId = country["Value"] as? String
    }
}

open class BillingState: NSObject {
    
    var stateName: String?
    var stateId: String?
    
    init(state: [String : AnyObject]) {
        self.stateName =  state["Text"] as? String
        self.stateId =  state["Value"] as? String
    }
    
    init(stateName:String, stateId:String) {
        self.stateName = stateName
        self.stateId = stateId
    }
}
