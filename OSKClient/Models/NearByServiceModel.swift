//
//  NearByServiceModel.swift
//  VMart
//
//  Created by Mohit on 1/30/19.
//  Copyright © 2019 Shobhit. All rights reserved.
//

import UIKit



open class NearByServiceModel: NSObject {
    
    var ClosingTime: String?
    var CashOut: Bool?
    var Country: String?
    var DivisionName: String?
    var BusinessName: String?
    var HomeLocation: String?
    var AppVersion: String?
    var UserName: String?
    var Line1: String?
    var Category: String?
    var State: String?
    var Rating: String?
    var Password: String?
    var FirstName: String?
    var PhoneNumber: String?
    var CellId: String?
    var Email: String?
    var SubCategory: String?
    var IsDeleted: Bool?
    var IsOpenAlways: Bool?
    var LastName: String?
    var ModifiedDate: String?
    var CreatedBy: String?
    var IsActive: Bool?
    var Address: String?
    var Id: String?
    var DistanceInMiles: String?
    var TownShipName: String?
    var MessageRequestRejectedTime: String?
    var CashIn: Bool?
    var OpeningTime: String?
    var CountryCode: String?
    var TypeID: String?
    var CityName: String?
    var IsOpen: String?
    var DistanceInKm: String?
    var CreatedDate: String?
    var Gender: String?
    var TransactionId: String?
    var TotalRateCount: String?
    var Line2: String?
    var OkDollarBalance: String?
    var AgentType: String?
    
    
    //    var BusinessName: String?
    //    var PhoneNumber: String?
    //    var Country: String?
    //    var TownShipName: String?
    //
    //
    //    init(BusinessName : String, PhoneNumber : String  , Country : String , TownShipName : String ) {
    //
    //        self.BusinessName = BusinessName
    //        self.PhoneNumber = PhoneNumber
    //        self.Country = Country
    //        self.TownShipName = TownShipName
    //
    //    }
    
    
    
    
    init(ClosingTime: String,CashOut: Bool,Country: String,DivisionName: String,BusinessName: String,HomeLocation: String,AppVersion: String,UserName: String,Line1: String,Category: String,State: String,Rating: String,Password: String,FirstName: String,PhoneNumber: String,CellId: String,Email: String,SubCategory: String,IsDeleted: Bool,IsOpenAlways: Bool,LastName: String,ModifiedDate: String,CreatedBy: String,IsActive: Bool,Address: String,Id: String,DistanceInMiles: String,TownShipName: String,MessageRequestRejectedTime: String,CashIn: Bool,OpeningTime: String,CountryCode: String,TypeID: String,CityName: String,IsOpen: String,DistanceInKm: String,CreatedDate: String,Gender: String,TransactionId: String,TotalRateCount: String,Line2: String,OkDollarBalance: String,AgentType: String){
        
        
        self.ClosingTime = ClosingTime
        self.CashOut = CashOut
        self.Country = Country
        self.DivisionName = DivisionName
        self.BusinessName = BusinessName
        self.HomeLocation = HomeLocation
        self.AppVersion = AppVersion
        self.UserName = UserName
        self.Line1 = Line1
        self.Category = Category
        self.State = State
        self.Rating = Rating
        self.Password = Password
        self.FirstName = FirstName
        self.PhoneNumber = PhoneNumber
        self.CellId = CellId
        self.Email = Email
        self.SubCategory = SubCategory
        self.IsDeleted = IsDeleted
        self.IsOpenAlways = IsOpenAlways
        self.LastName = LastName
        self.ModifiedDate = ModifiedDate
        self.CreatedBy = CreatedBy
        self.IsActive = IsActive
        self.Address = Address
        self.Id = Id
        self.DistanceInMiles = DistanceInMiles
        self.TownShipName = TownShipName
        self.MessageRequestRejectedTime = MessageRequestRejectedTime
        self.CashIn = CashIn
        self.OpeningTime = OpeningTime
        self.CountryCode = CountryCode
        self.TypeID = TypeID
        self.CityName = CityName
        self.IsOpen = IsOpen
        self.DistanceInKm = DistanceInKm
        self.CreatedDate = CreatedDate
        self.Gender = Gender
        self.TransactionId = TransactionId
        self.TotalRateCount = TotalRateCount
        self.Line2 = Line2
        self.OkDollarBalance = OkDollarBalance
        self.AgentType = AgentType
    }
    
    
    
    
    
    
    
}
