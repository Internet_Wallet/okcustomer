//
//  TotalOrder.swift
//  NopCommerce
//
//  Created by BS-125 on 12/17/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit
import ObjectMapper
open class TotalOrder: Mappable {
    
    var subTotal    : String?
    var tax         : String?
    var orderTotal  : String?
    var shipping    : String?
    var discount    : String?
    
    required public init?(map: Map) {
    }
    
    // Mappable
    public func mapping(map: Map) {
        self.subTotal           <- map["SubTotal"]
        self.tax                <- map["Tax"]
        self.orderTotal         <- map["OrderTotal"]
        self.shipping           <- map["Shipping"]
        self.discount           <- map["OrderTotalDiscount"]
    }
}
