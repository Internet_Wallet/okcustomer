//
//  CheckOutItems.swift
//  NopCommerce
//
//  Created by BS-125 on 12/14/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit

class CheckOutItems: NSObject {
    
    var Sku                        : NSString?
    var ImageUrl                   : String?
    var ProductId                  : NSInteger
    var ProductName                : NSString
    var ProductSeName              : NSString
    var UnitPrice                  : NSString
    var OldPrice                   : NSString
    var SubTotal                   : NSString
    var Discount                   : NSInteger?
    var Quantity                   : NSInteger
    var AvailableQuantity                   : NSInteger
    var AllowedQuantities          : NSArray?
    var AttributeInfo              : NSString
    var RecurringInfo              : NSString?
    var RentalInfo                 : NSString?
    var Warnings                   : NSArray?
    var Id                         : NSInteger
    var CustomProperties           : NSArray?
    var DeliveryDays               : String?
    var DiscountPercentage         : Int?
    var DiscountOfferName          : String?
    
    
    
    init(JSON : AnyObject) {
        
        self.Sku                          = (JSON.value(forKeyPath: "Sku") as? NSString)
        self.ImageUrl                     = (JSON.value(forKeyPath: "Picture.ImageUrl") as? String)
        self.ProductId                    = (JSON.value(forKeyPath: "ProductId") as! NSInteger)
        self.ProductName                  = (JSON.value(forKeyPath: "ProductName") as! NSString)
        self.ProductSeName                = (JSON.value(forKeyPath: "ProductSeName") as! NSString)
        self.UnitPrice                    = (JSON.value(forKeyPath: "UnitPrice") as! NSString)
        self.OldPrice                     = (JSON.value(forKeyPath: "OldPrice") as! NSString)
        self.SubTotal                     = (JSON.value(forKeyPath: "SubTotal") as! NSString)
        self.Discount                     = (JSON.value(forKeyPath: "Discount") as? NSInteger)
        self.Quantity                     = (JSON.value(forKeyPath: "Quantity") as! NSInteger)
        self.AvailableQuantity                     = (JSON.value(forKeyPath: "AvailableQuantity") as! NSInteger)
        self.AllowedQuantities            = (JSON.value(forKeyPath: "AllowedQuantities") as? NSArray)
        self.AttributeInfo                = (JSON.value(forKeyPath: "AttributeInfo") as! NSString)
        self.RecurringInfo                = (JSON.value(forKeyPath: "RecurringInfo") as? NSString)
        self.RentalInfo                   = (JSON.value(forKeyPath: "RentalInfo") as? NSString)
        self.Warnings                     = (JSON.value(forKeyPath: "Warnings") as? NSArray)
        self.Id                           = (JSON.value(forKeyPath: "Id") as! NSInteger)
        self.CustomProperties             = (JSON.value(forKeyPath: "CustomProperties") as? NSArray)
        self.DeliveryDays                 = (JSON.value(forKeyPath: "DeliveryDays") as? String)
        self.DiscountPercentage           = (JSON.value(forKeyPath: "DiscountPercentage") as? Int)
        self.DiscountOfferName           = (JSON.value(forKeyPath: "DiscountOfferName")) as? String
    }
    
}
