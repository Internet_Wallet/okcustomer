//
//  HomePageCategoryModel.swift
//  NopCommerce
//
//  Created by Masudur Rahman on 1/6/16.
//  Copyright © 2016 Jahid Hassan. All rights reserved.
//

import UIKit

open class HomePageCategoryModel: NSObject {
    
    var name :              String?
    var imageUrl :          String?
    var idd :                NSInteger?
    
    var products : [GetHomePageProducts] = []
    var subCategory : [HomePageCategories] = []
    
    init (JSON:AnyObject){
        
        self.name = JSON.value(forKeyPath: "Category.Name") as? String
        self.imageUrl = JSON.value(forKeyPath: "Category.DefaultPictureModel.ImageUrl") as? String
        self.idd = JSON.value(forKeyPath: "Category.Id") as? NSInteger
        //        let productArray = JSON["Product"] as! [AnyObject]
        let productArray = JSON.value(forKeyPath: "Product" ) as? [AnyObject]
        
        for data in productArray! {
            var customProperties = data.value(forKeyPath: "CustomProperties") as? String
            let imageUrl = data.value(forKeyPath: "DefaultPictureModel.ImageUrl") as? String
            let idd = data.value(forKeyPath: "Id") as? NSInteger
            let name = data.value(forKeyPath: "Name") as? String
            var oldPrice = data.value(forKeyPath: "ProductPrice.OldPrice") as? String
            let price = data.value(forKeyPath: "ProductPrice.Price") as? String
            let priceValue = data.value(forKeyPath: "ProductPrice.PriceValue") as? Float
            let priceWithDiscount = data.value(forKeyPath: "ProductPrice.PriceWithDiscount") as? String
            let discountPercentage = data.value(forKeyPath: "ProductPrice.DiscountPercentage") as? NSInteger
            let allowCustomerReviews = data.value(forKeyPath: "ReviewOverviewModel.AllowCustomerReviews") as? Bool
            let productId = data.value(forKeyPath: "ReviewOverviewModel.ProductId") as? NSInteger
            let ratingSum = data.value(forKeyPath: "ReviewOverviewModel.RatingSum") as? NSInteger ?? 0
            let totalReviews = data.value(forKeyPath: "ReviewOverviewModel.TotalReviews") as? NSInteger
            var shortDescription = data.value(forKeyPath: "ShortDescription") as? String
            let isWishList = data.value(forKeyPath: "IsWishList") as? Bool
            let DisableWishlistButton = data.value(forKeyPath: "DisableWishlistButton") as? Bool
            let MarkAsNew = data.value(forKeyPath: "MarkAsNew") as? Bool
            if customProperties==nil{
                customProperties = ""
            }
            if shortDescription==nil{
                shortDescription = ""
            }
            if oldPrice==nil{
                oldPrice = ""
            }
            let getHomePageProducts = GetHomePageProducts(CustomProperties: customProperties ?? "", ImageUrl: imageUrl ?? "", Id: idd!, Name: name ?? "", OldPrice: oldPrice ?? "", Price: price ?? "", AllowCustomerReviews: allowCustomerReviews!, MarkAsNew: MarkAsNew ?? false, ProductId: productId!, RatingSum: ratingSum, TotalReviews: totalReviews!, ShortDescription: shortDescription ?? "", PriceWithDiscount: priceWithDiscount ?? "", DiscountPercentage: discountPercentage, PriceValue: priceValue ?? 0, IsWishList : isWishList, DisableWishlistButton: DisableWishlistButton ?? false)
            self.products.append(getHomePageProducts)
        }
        
        for data in (JSON.value(forKeyPath: "SubCategory") as? NSArray)! {
            
            let name = (data as AnyObject).value(forKeyPath: "Name") as? String
            let id = (data as AnyObject).value(forKeyPath: "Id") as? NSInteger
            let imageUrl = (data as AnyObject).value(forKeyPath: "IconPath") as? String
            let homePageSubCategories = HomePageCategories(name: name!, imageUrl: imageUrl!, idd: id!)
            
            self.subCategory.append(homePageSubCategories)
        }
        
    }
}


