//
//  ShippingMethod.swift
//  NopCommerce
//
//  Created by BS-125 on 12/21/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit
import ObjectMapper

open class ShippingMethod: Mappable {
    
    var Name             : String = ""
    var Selected         : Bool = false
    var Description      : String = ""
    var ShippingRateComputationMethodSystemName         : String = ""
    
    required public init?(map: Map) {
    }
    public func mapping(map: Map) {
        
        self.Name               <- map["Name"]
        self.Selected           <- map["Selected"]
        self.Description        <- map["Description"]
        self.ShippingRateComputationMethodSystemName <- map["ShippingRateComputationMethodSystemName"]
    }
}
