//
//  HomeCategoriesHeaderView.swift
//  CollectionInTable
//
//  Created by Shobhit Singhal on 10/11/18.
//  Copyright © 2018 Shobhit. All rights reserved.
//

import UIKit

class HomeCategoriesHeaderView: UIView {
    @IBOutlet weak var headerNameLabel: UILabel!{
        didSet {
            self.headerNameLabel.font = UIFont(name: appFont, size: 15.0)
            self.headerNameLabel.text = self.headerNameLabel.text?.localized
            
        }
    }
    @IBOutlet weak var viewAllButton: UIButton!{
        didSet {
            //            viewAllButton.applyButtonGradient(colors: [UIColor.colorWithRedValue(redValue: 32, greenValue: 89, blueValue: 235, alpha: 1), UIColor.magenta])
            viewAllButton.applyButtonGradient(colors: [UIColor.colorWithRedValue(redValue: 13, greenValue: 177, blueValue: 75, alpha: 1), UIColor.init(red: 20.0/255.0, green: 185.0/255.0, blue: 85.0/255.0, alpha: 1.0)])
            viewAllButton.setTitle((viewAllButton.titleLabel?.text ?? "").localized, for: .normal)
            viewAllButton.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var viewAllButtonWidth: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if let lang = UserDefaults.standard.value(forKey: "currentLanguage") as? String, lang == "en" {
            viewAllButtonWidth.constant = 80.0
        } else {
            viewAllButtonWidth.constant = 105.0
        }
        viewAllButton.layer.cornerRadius = 4
    }
}


class CustomDetailView: UIView {
    
    
    @IBOutlet weak var leftNameLabel: UILabel!{
        didSet {
            self.leftNameLabel.font = UIFont(name: appFont, size: 15.0)
            self.leftNameLabel.text = self.leftNameLabel.text?.localized
            
        }
    }
    
    @IBOutlet weak var rightNameLabel: UILabel!{
        didSet {
            self.rightNameLabel.text = self.rightNameLabel.text?.localized
            self.rightNameLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    func UpdatedLabelData(leftStr:String,rightStr:String)  {
        self.leftNameLabel.text = leftStr
        self.rightNameLabel.text = rightStr
    }
    
    
    //    @IBOutlet weak var viewAllButtonHeight: NSLayoutConstraint!
    
    //    override func awakeFromNib() {
    //        super.awakeFromNib()
    //
    //
    //
    //    }
    
}
