//
//  qrScanner.swift
//  VMart
//
//  Created by ANTONY on 03/12/2018.
//  Copyright © 2018 Shobhit. All rights reserved.
//

import UIKit
import AVFoundation

protocol qrScannerDelegate:class {
    func navigateToProductDetails(product_ID: Int)
}


class qrScanner: MartBaseViewController {
    
    var dictionaryArray =               NSMutableArray ()
    var aPIManager = APIManager()
    var captureSession: AVCaptureSession?
    var videoPreviewLayer:AVCaptureVideoPreviewLayer?
    var scanBarCodeStr = ""
    var productId = 44444
    let scanSize = CGSize(width: 200.0, height: 200.0)
    var contentW: CGFloat = 0.0
    var contentH: CGFloat = 0.0
    var DynView = UIView()
    var scanImg = UIImageView()
    var cartBarButton: UIBarButtonItem!
    var searchButton: UIBarButtonItem!
    var delegate: qrScannerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.topItem?.title = "";
        let screenSize = UIScreen.main.bounds.size
        self.navigationItem.title = "Scan Barcode".localized
        contentW = screenSize.width
        contentH = screenSize.height
        self.checkCameraAccessAndOpen()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //        self.cartBarButton = UIBarButtonItem.init(badge: UserDefaults.standard.value(forKey: "badge") as? String, title: "", target: self, action: #selector(self.cartAction(_:)))
        self.navigationController?.navigationBar.applyNavigationGradient(colors: [UIColor.colorWithRedValue(redValue: 13, greenValue: 177, blueValue: 75, alpha: 1), UIColor.init(red: 20.0/255.0, green: 185.0/255.0, blue: 85.0/255.0, alpha: 1.0)])
        
        self.cartBarButton = UIBarButtonItem.init(badge: UserDefaults.standard.string(forKey: "badge") ?? "", title: "", target: self, action: #selector(self.cartAction(_:)))
        self.searchButton = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonItem.SystemItem.search, target: self, action: #selector(self.searchAction(_:)))
        self.navigationItem.rightBarButtonItems = [self.cartBarButton, self.searchButton]
        DispatchQueue.global(qos: .background).async {
            self.captureSession?.startRunning()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        DispatchQueue.main.async {
            if (self.captureSession?.isRunning) != nil {
                self.captureSession?.stopRunning()
            }
        }
    }
    
    @IBAction func searchAction(_ sender: UIBarButtonItem) {
        
        let viewController = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SearchProducts")
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    @IBAction func cartAction(_ sender: UIBarButtonItem) {
        let viewController = UIStoryboard(name: "Cart", bundle: Bundle.main).instantiateViewController(withIdentifier: "CartRoot")
        self.present(viewController, animated: true, completion: nil)
    }
}

extension qrScanner{
    
    func setUpQRMask() {
        let centerRect = CGRect(x: (contentW-scanSize.width)/2.0, y: (contentH-scanSize.height)/2.0, width: scanSize.width, height: scanSize.height)
        let path = UIBezierPath(rect: view.bounds)
        let centerPath = UIBezierPath(rect: centerRect)
        path.append(centerPath)
        path.usesEvenOddFillRule = true
        let fillLayer = CAShapeLayer()
        fillLayer.path = path.cgPath
        fillLayer.fillRule = CAShapeLayerFillRule.evenOdd
        fillLayer.fillColor = UIColor.black.withAlphaComponent(0.3).cgColor
        
        scanImg = UIImageView(frame: CGRect(x: (contentW-scanSize.width - 2)/2.0, y: (contentH-scanSize.height - 4)/2.0, width: scanSize.width + 2 , height: scanSize.height + 5 ))
        //        DynView = UIView(frame: CGRect(x: (contentW-scanSize.width + 20)/2.0, y: scanImg.frame.minY + 20, width: self.scanSize.width - 20, height: 3))
        DynView = UIView(frame: CGRect(x: (contentW-scanSize.width + 20)/2.0, y: scanImg.frame.minY + 100, width: self.scanSize.width - 20, height: 1))
        DynView.backgroundColor = UIColor.red
        scanImg.image =  #imageLiteral(resourceName: "scan_bg")
        view.addSubview(scanImg)
        view.addSubview(DynView)
        view.layer.addSublayer(fillLayer)
        //        self.moveTopToBottom()
    }
    
    private func moveTopToBottom() {
        DynView.frame = CGRect(x: (self.contentW - self.scanSize.width + 20)/2.0, y: scanImg.frame.minY + 20, width: self.scanSize.width - 20, height: 3)
        let mediumInterval: TimeInterval = 4.0
        UIView.animate(withDuration: mediumInterval, animations: {
            self.DynView.frame = CGRect(x: (self.contentW - self.scanSize.width + 20)/2.0 , y: self.scanImg.frame.maxY - 20, width: self.scanSize.width - 20, height: 3)
        }, completion: { (_) in
            self.moveBottomToTop()
        })
    }
    
    private func moveBottomToTop() {
        DynView.frame =  CGRect(x: (self.contentW - self.scanSize.width + 20)/2.0 , y: scanImg.frame.maxY - 20, width: self.scanSize.width - 20 , height: 3)
        let mediumInterval: TimeInterval = 4.0
        UIView.animate(withDuration: mediumInterval, animations: {
            self.DynView.frame = CGRect(x: (self.contentW - self.scanSize.width + 20)/2.0 , y: self.scanImg.frame.minY + 20, width: self.scanSize.width - 20 , height: 3)
        }, completion: { (_) in
            self.moveTopToBottom()
        } )
    }
    
    
    
    
    //MARK: - Methods
    func checkCameraAccessAndOpen() {
        if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
            self.addCameraLayer()
        } else {
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
                if granted {
                    self.addCameraLayer()
                } else {
                    DispatchQueue.main.async {
                        self.showAlert()
                    }
                }
            })
        }
    }
    
    private func showAlert() {
        let settingsAppURL = URL(string: UIApplication.openSettingsURLString)!
        let alertVC = SAlertController()
        alertVC.ShowSAlert(title: "Camera".localized, withDescription: "Camera access required to scan Bar Code".localized, onController: self)
        let cancel = SAlertAction()
        cancel.action(name: "Cancel".localized, AlertType: .defualt, withComplition: {
            // go for dashboard
            DispatchQueue.main.async {
                self.navigationController?.popViewController(animated: true)
            }
        })
        let allow = SAlertAction()
        allow.action(name: "Allow Camera".localized, AlertType: .defualt, withComplition: {
            UIApplication.shared.open(settingsAppURL, options: [:], completionHandler: nil)
        })
        alertVC.addAction(action: [cancel,allow])
    }
    
    func addCameraLayer() {
        DispatchQueue.main.async  {
            //AVCaptureDevice allows us to reference a physical capture device (video in our case)
            let captureDevice = AVCaptureDevice.default(for: AVMediaType.video)
            if let captureDevice = captureDevice {
                do {
                    AppUtility.showLoading(self.view)
                    // CaptureSession needs an input to capture Data from
                    let input = try AVCaptureDeviceInput(device: captureDevice)
                    self.captureSession = AVCaptureSession()
                    self.captureSession?.addInput(input)
                    // CaptureSession needs and output to transfer Data to
                    let captureMetadataOutput = AVCaptureMetadataOutput()
                    self.captureSession?.addOutput(captureMetadataOutput)
                    //We tell our Output the expected Meta-data type
                    captureMetadataOutput.setMetadataObjectsDelegate(self , queue: DispatchQueue.main)
                    captureMetadataOutput.metadataObjectTypes = [.code128, .qr,.ean13, .ean8, .code39, .upce, .aztec, .pdf417] //AVMetadataObject.ObjectType
                    
                    DispatchQueue.main.async {
                        self.captureSession?.startRunning()
                    }
                    //The videoPreviewLayer displays video in conjunction with the captureSession
                    self.videoPreviewLayer = AVCaptureVideoPreviewLayer(session: self.captureSession!)
                    self.videoPreviewLayer?.videoGravity = .resizeAspectFill
                    AppUtility.hideLoading(self.view)
                    DispatchQueue.main.async {
                        self.videoPreviewLayer?.frame = self.view.layer.bounds
                        if let vpLayer = self.videoPreviewLayer {
                            self.view.layer.addSublayer(vpLayer)
                            self.setUpQRMask()
                        }
                    }
                } catch {
                    println_debug(error)
                }
            }
            DispatchQueue.main.async {
                self.scanBarCodeStr = ""
                self.captureSession?.startRunning()
            }
        }
    }
    
}


//MARK: - AVCaptureMetadataOutputObjectsDelegate
extension qrScanner: AVCaptureMetadataOutputObjectsDelegate {
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        captureSession?.stopRunning()
        self.DynView.isHidden = true
        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            found(code: stringValue)
        }
    }
    
    func found(code: String) {
        scanBarCodeStr = code
        if AppUtility.isConnectedToNetwork() {
            self.dictionaryArray = []
            self.ScanBarcodeApi(strBarCode: code)
        } else {
            AppUtility.showInternetErrorToast(AppConstants.InternetErrorText.message, view: self.view)
        }
    }
}


extension qrScanner {
    
    func ScanBarcodeApi(strBarCode : String) {
        AppUtility.showLoading(self.view)
        let barCodeNumber = (strBarCode as NSString).integerValue
        self.aPIManager.ScanBarcodeId(barCodeNumber, onSuccess: { res in
            AppUtility.hideLoading(self.view)
            println_debug(res)
            if res["Message"].stringValue == "Barcode not found" ||  res["ProductId"].number == 0 {
                DispatchQueue.main.async {
                    if (self.captureSession?.isRunning) != nil {
                        self.captureSession?.stopRunning()
                    }
                    //AppUtility.showToastlocal(message: "There are no product for this barcode", view: self.view)
                    
                    
                    
                    let msg = "Product will come soon".localized
                    
                    
                    let sAlertController = SAlertController()
                    sAlertController.ShowSAlert(title: "".localized, withDescription: msg , onController: self)
                    let okAlertAction = SAlertAction()
                    okAlertAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                        self.captureSession?.startRunning()
                    })
                    
                    sAlertController.addAction(action: [okAlertAction])
                    self.captureSession?.stopRunning()
                    
                    
                    
                    self.DynView.isHidden = false
                    //
                }
                
                //                if let del = self.delegate {
                //                    self.navigationController?.popViewController(animated: false)
                //                    del.navigateToProductDetails(product_ID: 2)
                //                }
                //                let alertVC = SAlertController()
                //                alertVC.ShowSAlert(title: "Bar Code".localized, withDescription: "This Bar code not found".localized, onController: self)
                //                let cancel = SAlertAction()
                //                cancel.action(name: "Back".localized, AlertType: .defualt, withComplition: {
                //                    self.navigationController?.popViewController(animated: false)
                //                })
                //                let tryAgain = SAlertAction()
                //                tryAgain.action(name: "Try Again".localized, AlertType: .defualt, withComplition: {
                //                    self.DynView.isHidden = false
                //                    self.captureSession?.startRunning()
                //                })
                //
                //                alertVC.addAction(action: [cancel,tryAgain])
                
            }else {
                DispatchQueue.main.async {
                    if (self.captureSession?.isRunning) != nil {
                        self.captureSession?.stopRunning()
                    }
                }
                if let del = self.delegate {
                    self.navigationController?.popViewController(animated: false)
                    del.navigateToProductDetails(product_ID: res["ProductId"].number as! Int)
                }
            }
        }, onError: { message in
            AppUtility.hideLoading(self.view)
            self.showAlert(alertTitle: "Error!", description: "Scaning bar code is failed. Please Try again".localized)
        })
    }
}
