//
//  DoctorListViewController.swift
//  VMart
//
//  Created by Kethan on 8/3/19.
//  Copyright © 2019 Shobhit. All rights reserved.
//

import UIKit
import MobileRTC

class DoctorListViewController: ZAYOKBaseViewController {
    
    @IBOutlet weak var doctorListTableView: UITableView!{
        didSet {
            doctorListTableView.tableFooterView = UIView(frame: .zero)
        }
    }
    
    var aPIManager = APIManager()
    var responseObject :[Dictionary<String,AnyObject>] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Available Doctors"
        
        self.doctorListTableView.delegate = self
        self.doctorListTableView.dataSource = self
        self.addBack()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.applyNavigationGradient(colors: [UIColor.colorWithRedValue(redValue: 0, greenValue: 176, blueValue: 255, alpha: 1), UIColor.init(red: 40.0/255.0, green: 116.0/255.0, blue: 239.0/255.0, alpha: 1.0)])
        
        if AppUtility.isConnectedToNetwork() {
            self.getMedicineRequest()
        } else {
            AppUtility.showInternetErrorToast(AppConstants.InternetErrorText.message, view: self.view)
        }
    }
    
    
    func getMedicineRequest() {
        AppUtility.showLoading(self.view)
        aPIManager.loadTotalDoctorsAvailables( onSuccess: { [weak self] responseData in
            if let dataModel = responseData  as? [Dictionary<String,AnyObject>] {
                //print(dataDict["Data"])
                self?.responseObject = dataModel
                self?.doctorListTableView.reloadData()
            }
            if let viewLoc = self?.view {
                AppUtility.hideLoading(viewLoc)
            }
            
            }, onError: { [weak self] message in
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                    AppUtility.showToast(message,view: viewLoc)
                }})
    }
    
    func addBack(){
        let button = UIButton()
        button.setImage(UIImage(named: "back"), for: .normal)
        button.addTarget(self, action: #selector(dismissScreen), for: .touchUpInside)
        button.imageEdgeInsets.left = -35
        let item = UIBarButtonItem(customView: button)
        item.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = item
    }
    
    @objc func dismissScreen(){
        self.navigationController?.popViewController(animated: true)
    }
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    @objc func JoinMeetingAction(_ sender: UIButton) {
        
        let meetingData = self.responseObject[sender.tag]
        // Step 1: get meeting number
        let meetingID = meetingData["ChatId"] as? String
        let userName = VMLoginModel.shared.firstName
        print("join meetingID is: \(String(describing: meetingID))")
        
        // Step 3: Call joinMeeting method.
        if (meetingID!.count > 0) {
            // If the meeting number is not empty.
            let service = MobileRTC.shared()?.getMeetingService()
            if ((service) != nil) {
                service?.delegate = self;
                // initialize a parameter dictionary to store parameters.
                let paramDict = [kMeetingParam_Username: userName,
                                 kMeetingParam_MeetingNumber:meetingID!]
                let response = service?.joinMeeting(with: paramDict as [AnyHashable : Any])
                print("onJoinMeeting, response: \(String(describing: response))")
            }
        }
        else {
            print("please enter meeting id : ")
        }
    }
}

extension DoctorListViewController : UITableViewDataSource, UITableViewDelegate {
    // MARK: - TableView Delegate and DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.responseObject.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : CUstomDoctorCell = tableView.dequeueReusableCell(withIdentifier: "customDoctorCell") as! CUstomDoctorCell
        // cell.deleteBtn.tag = indexPath.row
        //        cell.Delegate = self
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        cell.updateCellData(object: self.responseObject[indexPath.row], indexPath: indexPath)
        cell.btnCall.addTarget(self, action: #selector(JoinMeetingAction(_:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 66.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        if let viewController = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "RequestDetails") as? RequestDetails{
        //            viewController.responseObject = self.responseObject[indexPath.row]
        //            self.navigationController?.pushViewController(viewController, animated: true)
        //        }
    }
    
}

extension DoctorListViewController: MobileRTCMeetingServiceDelegate{
    func onMeetingError(_ error: MobileRTCMeetError, message: String!) {
        print(message!)
    }
    
    func onMeetingReady() {
        
    }
}



class CUstomDoctorCell: UITableViewCell {
    
    @IBOutlet weak var imgCall: UIImageView!
    @IBOutlet weak var lblDoctorName: UILabel!{
        didSet {
            self.lblDoctorName.text = self.lblDoctorName.text?.localized
            self.lblDoctorName.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var lblDoctorType: UILabel!{
        didSet {
            self.lblDoctorType.text = self.lblDoctorType.text?.localized
            self.lblDoctorType.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var btnCall: UIButton!{
        didSet {
            btnCall.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    func updateCellData(object: Dictionary<String,AnyObject> ,indexPath: IndexPath) {
        
        imgCall.image = UIImage.init(named: "AppIcon")
        lblDoctorName.text = object["Name"] as? String
        lblDoctorType.text = object["Category"] as? String
        
    }
    
}
