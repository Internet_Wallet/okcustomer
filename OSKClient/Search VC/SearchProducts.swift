//
//  SearchProducts.swift
//  VMart
//
//  Created by ANTONY on 19/10/2018.
//  Copyright © 2018 Shobhit. All rights reserved.
//

import UIKit
import Alamofire

class SearchProducts: MartBaseViewController {
    
    var aPIManager                  = APIManager()
    var apiManagerClient            : APIManagerClient!
    var searchedProductList = [ProductsInfo]()
    var categoryDetailsInfo         = CategoryDetailsInfo()
    var notFilteredItemsCategories = [String]()
    var notFilteredItemsSubCategories = [[String]]()
    var notFilteredSubProductId = [[NSInteger]]()
    var homePageProductArray        = [GetHomePageProducts]()
    var selectedFilterProductsId = [NSInteger]()
    var checkedRightTable = [[Bool]]()
    var filterProductArray          = [ProductsInfo]()
    var productId:Int!
    var searchStr = ""
    var isListShow = false
    var checked = [Bool]()
    var checkMark                   = -1
    var sortValue                   = -1
    var minMaxAmountAfterApply = true
    var minAmount = ""
    var maxAmount = ""
    var minRange = ""
    var maxRange = ""
    var productName = ""
    var notFilteredProductId = [NSInteger]()
    var searchHistoryArray = [String]()
    var block: DispatchWorkItem?
    var pagingIndex:Int = 1
    var visibleIndexPath:IndexPath!
    var manufacturerFlag            = false
    var categoryId                  : NSInteger!
    var loadingView: LoadingReusableView?
    var isLoading = false
    
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var listGridImageView: UIImageView!
    @IBOutlet weak var sortBGView: UIView!
    @IBOutlet weak var filterBGView: UIView!
    var isHideOptions: Bool {
        get {
            return self.optionView.isHidden
        }
        set {
            if newValue {
                self.optionView.isHidden = true
                self.constraintOptionViewH.constant = 0
                self.view.layoutIfNeeded()
            } else {
                self.optionView.isHidden = false
                self.constraintOptionViewH.constant = 50
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var ProductCollectionView: UICollectionView!
    private lazy var searchBar = UISearchBar()
    @IBOutlet var searchTableView: UITableView!{
        didSet{
            self.searchTableView.tableFooterView = UIView()
        }
    }
    @IBOutlet weak var recentHistory: UIView!
    @IBOutlet weak var optionView: UIView!
    @IBOutlet weak var constraintOptionViewH: NSLayoutConstraint!
    @IBOutlet var sortLabel: UILabel! {
        didSet {
            self.sortLabel.font = UIFont(name: appFont, size: 15.0)
            self.sortLabel.text = self.sortLabel.text?.localized
            
        }
    }
    @IBOutlet var filterLabel: UILabel!{
        didSet {
            self.filterLabel.font = UIFont(name: appFont, size: 15.0)
            self.filterLabel.text = self.filterLabel.text?.localized
            
        }
    }
    
    @IBOutlet var recentlySearchLabel: UILabel!{
        didSet {
            self.recentlySearchLabel.font = UIFont(name: appFont, size: 15.0)
            self.recentlySearchLabel.text = self.recentlySearchLabel.text?.localized
            
        }
    }
    
    
    override func viewDidLoad(){
        super.viewDidLoad()
        self.recentlySearchLabel.text = "Recently searched".localized
        //Register Loading Reuseable View
        let loadingReusableNib = UINib(nibName: "LoadingReusableView", bundle: nil)
        ProductCollectionView.register(loadingReusableNib, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: "loadingresuableviewid")
        self.ProductCollectionView.contentInset.bottom = 55
        self.ProductCollectionView.collectionViewLayout.invalidateLayout()
        UserDefaults.standard.set(0.0, forKey: "min")
        UserDefaults.standard.set(0.0, forKey: "max")
        UserDefaults.standard.synchronize()
    }
    
    func configureInitialData() {
        
        self.recentHistory.isHidden = true
        self.isHideOptions = true
        let leftNavBarButton = UIBarButtonItem(customView:searchBar)
        self.navigationItem.leftBarButtonItem = leftNavBarButton
        // searchBar.backgroundColor = UIColor.white
        self.activityIndicator.isHidden = true
        searchBar.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 30)
        searchBar.showsCancelButton = true
        searchBar.delegate = self
        searchBar.tintColor = UIColor.white
        searchBar.placeholder = "Search for product".localized
        searchBar.searchBarStyle = .prominent
        if let searchTextField = searchBar.value(forKey: "searchField") as? UITextField, let myFont = UIFont(name: appFont, size: 16) {
            if searchTextField.responds(to: #selector(getter: UITextField.attributedPlaceholder)) {
                searchTextField.font = myFont
                searchTextField.font = myFont
                searchTextField.keyboardType = .asciiCapable
                // var color = UIColor.black
                //                let attributeDict = [NSAttributedString.Key.foregroundColor: UIColor.black]
                //                searchTextField.attributedPlaceholder = NSAttributedString(string: "Search for product".localized, attributes: attributeDict)
                //    searchTextField.placeholder = "Search for product".localized
                //
                //    searchTextField.textColor = UIColor.black
                
                //    searchTextField.text = "Search for product".localized
                
                //   searchTextField.attributedPlaceholder = NSAttributedString(string: "Search for product".localized,
                //         attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
                
            }
        }
        
        let view = self.searchBar.subviews[0] as UIView
        let subViewsArray = view.subviews
        for subView: UIView in subViewsArray {
            if subView.isKind(of: UITextField.self) {
                subView.tintColor = UIColor.darkGray
            }
        }
        self.apiManagerClient = APIManagerClient.sharedInstance
        searchBar.becomeFirstResponder()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        tapGesture.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapGesture)
        self.ProductCollectionView.delegate = self
        self.ProductCollectionView.dataSource = self
        self.ProductCollectionView.register(UINib(nibName: "ProductListCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ProductListCell")
        self.aPIManager = APIManager()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.configureInitialData()
        if AppUtility.isConnectedToNetwork() {
            self.recentSearcHistoryAPI()
        } else {
            AppUtility.showInternetErrorToast(AppConstants.InternetErrorText.message, view: self.view)
        }
        
        
        //        if UserDefaults.standard.object(forKey: "recentHistory") != nil {
        //            self.searchHistoryArray.removeAll()
        //            self.searchHistoryArray = (UserDefaults.standard.object(forKey: "recentHistory") as? [String])!
        //            self.searchHistoryArray.reverse()
        //            self.recentSearchHistory()
        //        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.recentHistory.isHidden = true
        }
        
    }
    
    
    @objc func hideKeyboard() {
        self.view.endEditing(true)
    }
    
    @IBAction func sortByAction(_ sender: UIButton) {
        self.performSegue(withIdentifier: "SearchProuctListToSortSegue", sender: self)
    }
    
    @IBAction func filterAction(_ sender: UIButton) {
        self.performSegue(withIdentifier: "SearchProductListToFilterSegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SearchProuctListToSortSegue" {
            if let vc = segue.destination as? SortBy {
                vc.SDelegate = self
                vc.checked = self.checked
                vc.categoryDetailsInfo = categoryDetailsInfo
            }
        }
        else if segue.identifier == "ProductCollectionsToProductDetailsSegue" {
            if let vc = segue.destination as? ProductDetails {
                vc.productId = self.productId
                vc.productName = self.productName
            }
        }
        else if segue.identifier == "SearchProductListToFilterSegue" {
            if let vc = segue.destination as? UINavigationController {
                if let rootViewController = vc.viewControllers.first as? Filter {
                    rootViewController.FDelegate = self
                    rootViewController.notFilteredItemsCategories = self.notFilteredItemsCategories
                    rootViewController.notFilteredItemsSubCategories = self.notFilteredItemsSubCategories
                    rootViewController.allFilterProductsId = self.notFilteredSubProductId
                    rootViewController.homePageProductArray = self.homePageProductArray
                    rootViewController.checkedRightTable = self.checkedRightTable
                    rootViewController.selectedFilterProductsId = self.selectedFilterProductsId
                    rootViewController.notFilteredItems = categoryDetailsInfo.notFilteredItems
                    if minMaxAmountAfterApply {
                        rootViewController.minAmt = minAmount
                        rootViewController.maxAmt = maxAmount
                        rootViewController.min = minRange
                        rootViewController.max = maxRange
                        // minMaxAmountAfterApply = false
                    }else {
                        rootViewController.minAmt = ""
                        rootViewController.maxAmt = ""
                    }
                    
                    
                    // Make Sort check array empty when Filter button pressed
                    self.checked = []
                    
                }
            }
        }
    }
    
    @IBAction func listTypeAction(_ sender: UIButton) {
        if isListShow {
            isListShow = false
            listGridImageView.image = UIImage(named: "listGallery")
            ProductCollectionView.reloadData()
            //            ProductCollectionView.reloadItems(at: [self.visibleIndexPath])
        }
        else {
            isListShow = true
            listGridImageView.image = UIImage(named: "grid")
            ProductCollectionView.reloadData()
            //            ProductCollectionView.reloadItems(at: [self.visibleIndexPath])
        }
    }
    
    func clearAllDataForFilter() {
        notFilteredItemsCategories = []
        notFilteredItemsSubCategories = []
        notFilteredSubProductId = []
        homePageProductArray = []
        checkedRightTable = []
        selectedFilterProductsId = []
        minAmount = ""
        maxAmount = ""
        minRange = ""
        maxRange = ""
    }
}

extension SearchProducts: UISearchBarDelegate {
    // MARK: UISearchBarDelegate
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        if let uiButton = searchBar.value(forKey: "cancelButton") as? UIButton {
            if appLang == "Myanmar" {
                uiButton.setTitle("မလုပ္ပါ", for:.normal)
            } else if appLang == "Unicode" {
                uiButton.setTitle("မလုပ်ပါ", for:.normal)
            } else {
                uiButton.setTitle("Cancel".localized, for:.normal)
            }
            uiButton.titleLabel?.font = UIFont(name: appFont, size: 14.0)
        }
        self.recentSearchHistory()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.recentHistory.isHidden = true
        self.searchBar.resignFirstResponder()
        apiManagerClient.categoryDetailsInfo = CategoryDetailsInfo()
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
        
    }
    
    func searchBarSearchButtonClicked( _ searchBar: UISearchBar) {
        
        if (searchBar.text?.count ?? 0) >= 2 {
            self.searchBar.endEditing(true)
            UserDefaults.standard.set(0.0, forKey: "min")
            UserDefaults.standard.set(0.0, forKey: "max")
            UserDefaults.standard.synchronize()
            self.searchApiCall(searchBar.text ?? "")
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText.isEmpty {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                if self.searchHistoryArray.count > 0 {
                    self.recentHistory.isHidden = false
                    searchBar.resignFirstResponder()
                }
            }
        }
        
        
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        self.recentHistory.isHidden = true
        var searchText = (searchBar.text! as NSString).replacingCharacters(in: range, with: text)
        
        if let char = searchText.cString(using: String.Encoding.utf8), searchText == "" {
            let isBackSpace = strcmp(char, "\\b")
            if (isBackSpace == -92) {
                self.recentSearchHistory()
            }
        }
        
        
        if searchText == " " {
            return false
        }
        if searchText.count > 1 && text == "\n" {
            searchText = String(searchText.dropLast())
        }
        searchStr = searchText
        if searchText.count >= 2 {
            
            clearAllDataForFilter()
            //            if !self.searchHistoryArray.contains(searchText){
            //                self.searchHistoryArray.append(searchText)
            //            }
            
            self.block?.cancel()
            self.block = DispatchWorkItem  {
                self.pagingIndex = 1
                self.isLoading = false
                UserDefaults.standard.set(0.0, forKey: "min")
                UserDefaults.standard.set(0.0, forKey: "max")
                UserDefaults.standard.synchronize()
                self.searchApiCall(searchText)
            }
            // execute task in 2 seconds
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: self.block!)
            
        } else {
            self.isHideOptions = true
            //AppUtility.showToast("No result found".localized, view: self.view)
            self.categoryDetailsInfo.productsInfo.removeAll()
            DispatchQueue.main.async {
                self.ProductCollectionView.reloadData()
            }
        }
        return true
    }
    
    
    func recentSearchHistory(){
        //        self.searchTableView.isHidden = false
        self.optionView.isUserInteractionEnabled = true
        if self.searchHistoryArray.count > 0{
            self.recentHistory.isHidden = true
            self.searchTableView.delegate = self
            self.searchTableView.dataSource = self
            self.searchTableView.rowHeight = UITableView.automaticDimension
            self.searchTableView.estimatedRowHeight = 44.0
            self.searchTableView.reloadData()
        }
        
    }
    
    
    func recentSearcHistoryAPI(){
        //        AppUtility.showLoading(self.view)
        self.optionView.isUserInteractionEnabled = false
        self.activityIndicator.isHidden = false
        
        self.aPIManager.loadRecentSearchHistory(onSuccess: { [weak self] gotSearcheResults in
            
            
            //            UserDefaults.standard.set(self?.searchHistoryArray, forKey: "recentHistory")
            //            UserDefaults.standard.synchronize()
            
            if gotSearcheResults!.count > 0{
                self?.recentSearchHistory()
            }
            
            self!.activityIndicator.isHidden = true
            
            //            if let viewLoc = self?.view {
            ////                AppUtility.hideLoading(viewLoc)
            //
            //            }
            
            }, onError: {  message in
                if let viewLoc = self.view {
                    AppUtility.hideLoading(viewLoc)
                    self.activityIndicator.isHidden = true
                }
        })
        
        
    }
    
    func searchApiCall(_ searchText: String) {
        //        AppUtility.showLoading(self.view)
        
        self.optionView.isUserInteractionEnabled = false
        self.activityIndicator.isHidden = false
        self.aPIManager.getSearchResults(searchText as NSString, pageNumber: pagingIndex, onSuccess:{ [weak self] gotSearcheResults in
            //            self?.searchedProductList = gotSearcheResults
            
            if gotSearcheResults.productsInfo.count > 0{
                //self?.searchHistoryArray.removeAll()
                
                if !(self?.searchHistoryArray.contains(searchText))!{
                    self?.searchHistoryArray.append(searchText)
                }
                
                UserDefaults.standard.set(self?.searchHistoryArray, forKey: "recentHistory")
                UserDefaults.standard.synchronize()
            }
            
            self?.categoryDetailsInfo = gotSearcheResults
            self?.apiManagerClient.categoryDetailsInfo = gotSearcheResults
            self?.activityIndicator.isHidden = true
            self?.optionView.isUserInteractionEnabled = true
            
            if let viewLoc = self?.view {
                AppUtility.hideLoading(viewLoc)
            }
            //self.searchBar.resignFirstResponder()
            DispatchQueue.main.async {
                //                if (self?.categoryDetailsInfo.productsInfo.count)! > 0{
                //                    self?.searchHistoryArray.append(searchText)
                //                }
                self?.ProductCollectionView.reloadData()
            }
            self?.filterCategories()
            if self?.categoryDetailsInfo.productsInfo.count == 0, let _ = self?.view {
                //AppUtility.showToast("No result found".localized, view:viewLoc)
            }
            }, onError: { [weak self] message in
                self?.activityIndicator.isHidden = true
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                    //AppUtility.showToast(message,view: viewLoc)
                }
        })
    }
    
    func filterCategories() {
        clearAllDataForFilter()
        for _ in self.apiManagerClient.categoryDetailsInfo.PriceRange {
            notFilteredItemsCategories.append("Price")
            notFilteredProductId.append(1000)
        }
        
        for notFilteredItem in self.apiManagerClient.categoryDetailsInfo.notFilteredItems {
            let spAttributeName = notFilteredItem.SpecificationAttributeName!
            let productId = notFilteredItem.ProductId
            if !notFilteredItemsCategories.contains(spAttributeName as String) {
                notFilteredItemsCategories.append(spAttributeName as String)
                notFilteredProductId.append(productId)
            }
        }
        
        var notFilteredItemsSubCategoriesArray = [[String]]()
        var notFilteredSubProductIdArray = [[NSInteger]]()
        for categoryName in notFilteredItemsCategories {
            var arr = [String]()
            var idArr = [NSInteger]()
            for i in 0  ..< self.apiManagerClient.categoryDetailsInfo.notFilteredItems.count {
                let spAttributeName:NSString = categoryName as NSString
                if spAttributeName == self.apiManagerClient.categoryDetailsInfo.notFilteredItems[i].SpecificationAttributeName {
                    if let subCategoryName = self.apiManagerClient.categoryDetailsInfo.notFilteredItems[i].SpecificationAttributeOptionName as String? {
                        arr.append(subCategoryName)
                    }
                    let productId = self.apiManagerClient.categoryDetailsInfo.notFilteredItems[i].ProductId
                    idArr.append(productId)
                }
            }
            notFilteredItemsSubCategoriesArray.append(arr)
            notFilteredSubProductIdArray.append(idArr)
        }
        for (i,subCategoryNameArr) in notFilteredItemsSubCategoriesArray.enumerated() {
            var arr = [String]()
            var idArr = [NSInteger]()
            for (j,name) in subCategoryNameArr.enumerated() {
                if !arr.contains(name) {
                    arr.append(name)
                    idArr.append(notFilteredSubProductIdArray[i][j])
                }
            }
            notFilteredItemsSubCategories.append(arr)
            notFilteredSubProductId.append(idArr)
        }
    }
}

extension SearchProducts: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if self.categoryDetailsInfo.productsInfo.count > 0 {
            collectionView.clearBackground()
        } else {
            if searchStr.count > 1 {
                clearAllDataForFilter()
                collectionView.setBackgroundMessage("No result found".localized)
            } else {
                collectionView.clearBackground()
            }
        }
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if filterProductArray.count > 0 {
            return filterProductArray.count
        }
        else{
            if self.categoryDetailsInfo.productsInfo.count > 0 {
                self.isHideOptions = false
            } else {
                self.isHideOptions = true
            }
            return self.categoryDetailsInfo.productsInfo.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = ProductCollectionView.dequeueReusableCell(withReuseIdentifier: "ProductListCell", for: indexPath) as! ProductListCollectionViewCell
        cell.roundCell(cell)
        cell.delegate = self
        if filterProductArray.count > 0 {
            let object = self.filterProductArray[indexPath.row]
            cell.updateCellWithData(productObject: object)
            return cell
        }
        let object = self.categoryDetailsInfo.productsInfo[indexPath.row]
        cell.updateCellWithData(productObject: object)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //        let itemWidth = HomeConstant.getItemWidth(boundWidth: collectionView.bounds.size.width)
        if isListShow {
            return CGSize(width: collectionView.bounds.size.width, height: collectionView.bounds.size.width/2 + 60)
        }
        return CGSize(width: collectionView.bounds.size.width/2 - 2, height: collectionView.bounds.size.width/2 + 30)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == (self.categoryDetailsInfo.productsInfo.count - 6)  && !self.isLoading {
            loadMoreData()
        }
    }
    
    func loadMoreData() {
        if !self.isLoading {
            self.isLoading = true
            if self.pagingIndex <= self.categoryDetailsInfo.TotalPages {
                self.pagingIndex += 1
                self.aPIManager.getSearchResults(searchStr as NSString , pageNumber: self.pagingIndex, onSuccess:{ [weak self] gotSearcheResults in
                    //            self?.searchedProductList = gotSearcheResults
                    
                    if gotSearcheResults.productsInfo.count > 0{
                        //self?.searchHistoryArray.removeAll()
                        UserDefaults.standard.set(self?.searchHistoryArray, forKey: "recentHistory")
                        UserDefaults.standard.synchronize()
                    }
                    
                    self?.categoryDetailsInfo.productsInfo.append(contentsOf: gotSearcheResults.productsInfo)
                    //self!.pagingIndex += 1
                    if let prodtuctInfoArr = self?.categoryDetailsInfo.productsInfo {
                        self?.apiManagerClient.categoryDetailsInfo.productsInfo = prodtuctInfoArr
                    }
                    self?.ProductCollectionView.reloadData()
                    if let viewLoc = self?.view {
                        AppUtility.hideLoading(viewLoc)
                    }
                    self?.activityIndicator.isHidden = true
                    self?.optionView.isUserInteractionEnabled = true
                    
                    if let viewLoc = self?.view {
                        AppUtility.hideLoading(viewLoc)
                    }
                    //self.searchBar.resignFirstResponder()
                    DispatchQueue.main.async {
                        //                if (self?.categoryDetailsInfo.productsInfo.count)! > 0{
                        //                    self?.searchHistoryArray.append(searchText)
                        //                }
                        self?.ProductCollectionView.reloadData()
                        self?.isLoading = false
                    }
                    self?.filterCategories()
                    if self?.categoryDetailsInfo.productsInfo.count == 0, let _ = self?.view {
                        //AppUtility.showToast("No result found".localized, view:viewLoc)
                    }
                    }, onError: { [weak self] message in
                        self?.activityIndicator.isHidden = true
                        if let viewLoc = self?.view {
                            AppUtility.hideLoading(viewLoc)
                            //AppUtility.showToast(message,view: viewLoc)
                        }
                })
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        if !self.isLoading {
            return CGSize.zero
        } else {
            return CGSize(width: collectionView.bounds.size.width, height: 55)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionView.elementKindSectionFooter {
            let aFooterView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "loadingresuableviewid", for: indexPath) as! LoadingReusableView
            loadingView = aFooterView
            loadingView?.backgroundColor = UIColor.clear
            return aFooterView
        }
        return UICollectionReusableView()
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplaySupplementaryView view: UICollectionReusableView, forElementKind elementKind: String, at indexPath: IndexPath) {
        if elementKind == UICollectionView.elementKindSectionFooter {
            self.loadingView?.activityIndicator.startAnimating()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplayingSupplementaryView view: UICollectionReusableView, forElementOfKind elementKind: String, at indexPath: IndexPath) {
        if elementKind == UICollectionView.elementKindSectionFooter {
            self.loadingView?.activityIndicator.stopAnimating()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        searchBar.resignFirstResponder()
        recentHistory.isHidden = true
        // clearAllDataForFilter()
        if filterProductArray.count > 0 {
            self.productId = self.filterProductArray[indexPath.item].id
            self.productName = self.filterProductArray[indexPath.item].Name as String
            self.performSegue(withIdentifier: "ProductCollectionsToProductDetailsSegue", sender: self)
            return
        }
        if self.categoryDetailsInfo.productsInfo[indexPath.item].id != 0 {
            self.productId = self.categoryDetailsInfo.productsInfo[indexPath.item].id
            self.productName = self.categoryDetailsInfo.productsInfo[indexPath.item].Name as String
            self.performSegue(withIdentifier: "ProductCollectionsToProductDetailsSegue", sender: self)
            return
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        var visibleRect = CGRect()
        visibleRect.origin = ProductCollectionView.contentOffset
        visibleRect.size = ProductCollectionView.bounds.size
        let visiblePoint = CGPoint(x: CGFloat(visibleRect.minX), y: CGFloat(visibleRect.minY))
        let visibleIndexPath: IndexPath? = ProductCollectionView.indexPathForItem(at: visiblePoint)
        if visibleIndexPath != nil {
            self.visibleIndexPath = visibleIndexPath
            print("Visible cell's index is : \(String(describing: self.visibleIndexPath))!")
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.isHideOptions = false
        var visibleRect = CGRect()
        visibleRect.origin = ProductCollectionView.contentOffset
        visibleRect.size = ProductCollectionView.bounds.size
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        guard let indexPath = ProductCollectionView.indexPathForItem(at: visiblePoint) else { return }
        println_debug(indexPath)
    }
}

extension UICollectionView {
    func setBackgroundMessage(_ message: String) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        messageLabel.font = UIFont(name: appFont, size: 15.0)
        messageLabel.text = message
        messageLabel.textColor = .black
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        if let myFont = UIFont(name: appFont, size: 15) {
            messageLabel.font =  myFont
        }
        messageLabel.sizeToFit()
        self.backgroundView = messageLabel
    }
    
    func clearBackground() {
        self.backgroundView = nil
    }
}

extension SearchProducts: SortByDelegate  {
    
    func SortByTestMethod(sortByTableView: UITableView, indexPath: IndexPath, sortingType: String) {
        sortByTableView.reloadData()
        switch sortingType {
            
        case "Position".localized:
            if filterProductArray.count > 0 {
                filterProductArray = filterProductArray.sorted(by: { $0.DisplayOrder < $1.DisplayOrder})
            }else {
                self.categoryDetailsInfo.productsInfo = self.categoryDetailsInfo.productsInfo.sorted(by: { $0.DisplayOrder < $1.DisplayOrder })
            }
            break
        case "Name : A to Z".localized:
            if filterProductArray.count > 0 {
                filterProductArray = filterProductArray.sorted(by: { $0.Name.localizedLowercase < $1.Name.localizedLowercase })
            }else {
                self.categoryDetailsInfo.productsInfo = self.categoryDetailsInfo.productsInfo.sorted(by: { $0.Name.localizedLowercase < $1.Name.localizedLowercase })
            }
            break
        case "Name : Z to A".localized:
            if filterProductArray.count > 0 {
                filterProductArray = filterProductArray.sorted(by: { $0.Name.localizedLowercase > $1.Name.localizedLowercase })
            }else {
                self.categoryDetailsInfo.productsInfo = self.categoryDetailsInfo.productsInfo.sorted(by: { $0.Name.localizedLowercase > $1.Name.localizedLowercase })
            }
            break
        case "Price : Low to High".localized:
            
            if filterProductArray.count > 0 {
                filterProductArray = filterProductArray.sorted(by: { $0.PriceValue < $1.PriceValue })
            }else {
                self.categoryDetailsInfo.productsInfo =  self.categoryDetailsInfo.productsInfo.sorted(by: { $0.PriceValue < $1.PriceValue })
            }
            break
        case "Price : High to Low".localized:
            
            if filterProductArray.count > 0 {
                filterProductArray = filterProductArray.sorted(by: { $0.PriceValue > $1.PriceValue })
            }else {
                self.categoryDetailsInfo.productsInfo =  self.categoryDetailsInfo.productsInfo.sorted(by: { $0.PriceValue > $1.PriceValue })
            }
            break
        case "Created on".localized:
            if filterProductArray.count > 0 {
                filterProductArray = filterProductArray.sorted(by: { $0.CreatedOn > $1.CreatedOn })
            }else {
                self.categoryDetailsInfo.productsInfo =  self.categoryDetailsInfo.productsInfo.sorted(by: { $0.CreatedOn > $1.CreatedOn })
            }
            break
        default:
            
            break
        }
        ProductCollectionView.reloadData()
    }
    
    func sortedCheckMarkArray(checked: [Bool]) {
        self.checked = checked
        if self.checked.count > 0 {
            self.sortBGView.backgroundColor = UIColor.colorWithRedValue(redValue: 230, greenValue: 230, blueValue: 230, alpha: 1)
        } else {
            self.sortBGView.backgroundColor = UIColor.white
        }
    }
    
} 

extension SearchProducts: FilterDelegate {
    
    func filterProducts(finalFilterProductsId: [NSInteger], selectedFilterProductsId: [NSInteger], checkedRightTable: [[Bool]], isPriceFilter: Bool) {
        if !isPriceFilter {
            filterProductArray = []
        }
        self.selectedFilterProductsId = selectedFilterProductsId
        self.checkedRightTable = checkedRightTable
        for object in self.categoryDetailsInfo.productsInfo {
            if finalFilterProductsId.contains(object.ProductId) {
                filterProductArray.append(object)
            }
        }
        ProductCollectionView.reloadData()
        //Filter BG Update-Gauri
        if filterProductArray.count > 0 {
            self.filterBGView.backgroundColor = UIColor.colorWithRedValue(redValue: 230, greenValue: 230, blueValue: 230, alpha: 1)
        } else {
            self.filterBGView.backgroundColor = UIColor.white
        }
    }
    
    func filterProducts(min: String, max: String, minSliderValue: Double, maxSliderValue: Double) {
        minAmount = String(minSliderValue)
        maxAmount = String(maxSliderValue)
        minRange = min
        maxRange = max
        minMaxAmountAfterApply = true
        filterProductArray = []
        let intMin = Float(min) ?? 0
        let intMax = Float(max) ?? 0
        filterProductArray = self.categoryDetailsInfo.productsInfo.filter({ $0.PriceValue <= intMax && $0.PriceValue >= intMin })
        ProductCollectionView.reloadData()
        //Filter BG Update-Gauri
        if filterProductArray.count > 0 {
            self.filterBGView.backgroundColor = UIColor.colorWithRedValue(redValue: 230, greenValue: 230, blueValue: 230, alpha: 1)
        } else {
            self.filterBGView.backgroundColor = UIColor.white
        }
    }
    
    func resetAllData() {
        filterProductArray = []
        ProductCollectionView.reloadData()
        self.sortBGView.backgroundColor = UIColor.white
    }
    
    func applyActionCall(isReset: Bool) {
        if isReset {
            self.filterBGView.backgroundColor = UIColor.white
        } else {
            
            if filterProductArray.count > 0 {
                self.filterBGView.backgroundColor = UIColor.colorWithRedValue(redValue: 230, greenValue: 230, blueValue: 230, alpha: 1)
            } else {
                self.filterBGView.backgroundColor = UIColor.white
            }
        }
    }
}

extension SearchProducts: AddRemoveWishListProtocol {
    func addToWishList(cell: ProductListCollectionViewCell?) {
        if let index = self.ProductCollectionView.indexPath(for: cell!) {
            if filterProductArray.count > 0 {
                let model = self.filterProductArray[index.row]
                model.iswishList = true
            } else {
                let model = self.categoryDetailsInfo.productsInfo[index.row]
                model.iswishList = true
            }
        }
    }
    
    func removeFromWishList(cell: ProductListCollectionViewCell?) {
        if let index = self.ProductCollectionView.indexPath(for: cell!) {
            if filterProductArray.count > 0 {
                let model = self.filterProductArray[index.row]
                model.iswishList = false
            } else {
                let model = self.categoryDetailsInfo.productsInfo[index.row]
                model.iswishList = false
            }
        }
    }
    
    private func updateModel(filterArray: [GetHomePageProducts], status: Bool) {
        if filterArray.count > 0 {
            let prdct = filterArray.first
            prdct?.isWishList = status
            DispatchQueue.main.async {
                self.ProductCollectionView.reloadData()
            }
        }
    }
    
    private func updateModel(filterArray: [ProductsInfo], status: Bool) {
        if filterArray.count > 0 {
            let prdct = filterArray.first
            prdct?.iswishList = status
            DispatchQueue.main.async {
                self.ProductCollectionView.reloadData()
            }
        }
    }
    
    func addToWishList(id: NSInteger) {
        if filterProductArray.count > 0 {
            let filter = self.filterProductArray.filter { (product) -> Bool in
                return product.id == id
            }
            self.updateModel(filterArray: filter, status: true)
        } else {
            let filter = self.categoryDetailsInfo.productsInfo.filter { (product) -> Bool in
                return product.id == id
            }
            self.updateModel(filterArray: filter, status: true)
        }
    }
    
    func removeToWishList(id: NSInteger) {
        if filterProductArray.count > 0 {
            let filter = self.filterProductArray.filter { (product) -> Bool in
                return product.id == id
            }
            self.updateModel(filterArray: filter, status: false)
        } else {
            let filter = self.categoryDetailsInfo.productsInfo.filter { (product) -> Bool in
                return product.id == id
            }
            self.updateModel(filterArray: filter, status: false)
        }
    }
}


extension SearchProducts: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.searchHistoryArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : RecentHistoryCell?
        cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as? RecentHistoryCell
        cell?.setNeedsUpdateConstraints()
        cell?.updateFocusIfNeeded()
        cell?.wrapData(str: self.searchHistoryArray[indexPath.row])
        return cell!
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.TapSubcategoary(indexPath: indexPath)
    }
    
    func TapSubcategoary(indexPath: IndexPath){
        //        self.searchTableView.isHidden = true
        self.recentHistory.isHidden = true
        self.searchBar.text = self.searchHistoryArray[indexPath.row]
        self.searchApiCall(self.searchBar.text!)
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

class RecentHistoryCell: UITableViewCell {
    
    @IBOutlet var itemImageView: UIImageView!
    @IBOutlet var textLbl: UILabel! {
        didSet {
            self.textLbl.font = UIFont(name: appFont, size: 15.0)
            self.textLbl.text = self.textLbl.text?.localized
            
        }
    }
    
    func wrapData(str: String){
        self.textLbl.text = str
    }
    
}
