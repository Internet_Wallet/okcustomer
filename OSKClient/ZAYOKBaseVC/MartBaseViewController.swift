//
//  MartBaseViewController.swift
//  VMart
//
//  Created by Avaneesh on 01/21/20.
//  Copyright © 2020 Avaneesh. All rights reserved.
//

import UIKit
import CoreTelephony

struct CodePreLogin {
    
    var country = "Myanmar"
    var code    = "+95"
    var flag    = "myanmar"
    
    init(cName: String, codes: String, flag: String) {
        self.country = cName
        self.code = codes
        self.flag  = flag
    }
}

struct NetworkMCCMNC {
    var mnc = ""
    var mcc = ""
    
    init(_ mnc: String, _ mcc: String) {
        self.mnc = mnc
        self.mcc = mcc
    }
}

struct NetworkPreLogin {
    
    var status       = ""
    var networkName  = ""
    var operatorName = ""
    
    init(status: String, netName: String,opeName: String) {
        self.status       = status
        self.networkName  = netName
        self.operatorName = opeName
    }
}
var preCodeCountry: CodePreLogin?
var preNetInfo: NetworkPreLogin?
var mccStatus: NetworkMCCMNC?

class MartBaseViewController: UIViewController {
    let imageCache = NSCache<AnyObject, AnyObject>()
    let sAlertController = SAlertController()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.gettingCountryCode()
        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 19.0)!, NSAttributedString.Key.foregroundColor: UIColor.white ]
        //        self.navigationController?.navigationBar.applyNavigationGradient(colors: [UIColor.colorWithRedValue(redValue: 32, greenValue: 89, blueValue: 235, alpha: 1), UIColor.magenta])
        self.navigationController?.navigationBar.applyNavigationGradient(colors: [UIColor.colorWithRedValue(redValue: 13, greenValue: 177, blueValue: 75, alpha: 1), UIColor.init(red: 20.0/255.0, green: 185.0/255.0, blue: 85.0/255.0, alpha: 1.0)])
        self.navigationController?.setStatusBar(backgroundColor: UIColor.colorWithRedValue(redValue: 13, greenValue: 177, blueValue: 75, alpha: 1))//(13, 177, 75)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    func showAlert(alertTitle: String, description: String) {
        sAlertController.ShowSAlert(title: alertTitle, withDescription: description, onController: self)
        let okAlertAction = SAlertAction()
        okAlertAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
            
        })
        sAlertController.addAction(action: [okAlertAction])
    }
    
    func gettingCountryCode() {
        
        let networkInfo = CTTelephonyNetworkInfo.init()
        let carrier     = networkInfo.subscriberCellularProvider
        
        loadCountry { (result) in
            
            if  let mcc = carrier?.mobileCountryCode {
                for data in result {
                    if let dic = data as? Dictionary<String,AnyObject> {
                        if dic["MCC"] as! String == mcc {
                            println_debug(dic)
                            preCodeCountry = CodePreLogin.init(cName: dic["CountryName"] as! String, codes: dic["CountryCode"] as! String, flag: "CountryFlagCode")
                            if  let mnc =  carrier?.mobileNetworkCode {
                                if let network = dic["networks"] as? [Dictionary<String,String>] {
                                    for net in network {
                                        if net["MNC"] == mnc {
                                            preNetInfo = NetworkPreLogin.init(status: net["Status"]!, netName: net["NetworkName"]!, opeName: net["OperatorName"]!)
                                            println_debug(preNetInfo)
                                            if let mncVal = net["MNC"], let mccVal = net["MCC"] {
                                                mccStatus = NetworkMCCMNC.init(mncVal, mccVal)
                                            } else {
                                                println_debug("NetworkMCCMNC initializa failed")
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    func loadCountry(completion: (_ result: NSArray)->()) {
        do {
            if let file = Bundle.main.path(forResource: "mcc_mnc", ofType: "txt") {
                let data = try Data.init(contentsOf: URL.init(fileURLWithPath: file))
                let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                if let object = json as? NSDictionary {
                    if let arr = object.object(forKey: "data") as? NSArray {
                        completion(arr)
                    }
                } else if let object = json as? [Any] {
                    println_debug(object)
                } else {
                    println_debug("JSON is invalid")
                }
            } else {
                println_debug("no file")
            }
        } catch {
            println_debug(error.localizedDescription)
        }
    }
    
    // Helper function inserted by Swift 4.2 migrator.
    func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
        return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
    }
    
    func cacheImage(urlString: String, _ completion: @escaping(_ image: UIImage?) -> Void) {
        guard let url = URL(string: urlString) else { return }
        if let imageFromCache = imageCache.object(forKey: urlString as AnyObject) as? UIImage {
            completion(imageFromCache)
        } else {
            URLSession.shared.dataTask(with: url) {
                data, response, error in
                if let dataVal = data {
                    DispatchQueue.main.async {
                        if let imageToCache = UIImage(data: dataVal) {
                            self.imageCache.setObject(imageToCache, forKey: urlString as AnyObject)
                            completion(imageToCache)
                        }
                    }
                }
            }.resume()
        }
    }
    
    func isNetworkAvailable() -> Bool {
        let status = Reachability2.instance.networkConnectionStatus()
        guard status != NetworkConnectionStatus.notConnection else {
            return false
        }
        return true
    }
    
}

extension UINavigationController {

    func setStatusBar(backgroundColor: UIColor) {
        let statusBarFrame: CGRect
        if #available(iOS 13.0, *) {
            statusBarFrame = view.window?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero
        } else {
            statusBarFrame = UIApplication.shared.statusBarFrame
        }
        let statusBarView = UIView(frame: statusBarFrame)
        statusBarView.backgroundColor = backgroundColor
        view.addSubview(statusBarView)
    }

}
