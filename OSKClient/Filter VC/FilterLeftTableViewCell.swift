//
//  FilterLeftTableViewCell.swift
//  VMart
//
//  Created by Shobhit Singhal on 10/20/18.
//  Copyright © 2018 Shobhit. All rights reserved.
//

import UIKit

class FilterLeftTableViewCell: UITableViewCell {
    @IBOutlet var categoryLbl: UILabel!{
        didSet {
            self.categoryLbl.font = UIFont(name: appFont, size: 15.0)
            self.categoryLbl.text = self.categoryLbl.text?.localized
            
        }
    }
    @IBOutlet var arrowImgView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.arrowImgView.layer.shadowOpacity = 0.40
        self.arrowImgView.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.arrowImgView.layer.shadowRadius = 10
        self.arrowImgView.layer.shadowColor = UIColor.black.cgColor
        self.arrowImgView.layer.masksToBounds = false
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
