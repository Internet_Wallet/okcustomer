//
//  FilterRightTableViewCell.swift
//  VMart
//
//  Created by Shobhit Singhal on 10/20/18.
//  Copyright © 2018 Shobhit. All rights reserved.
//

import UIKit

class FilterRightTableViewCell: UITableViewCell {
    
    var apiManagerClient: APIManagerClient!
    var isSliderChanges : Bool!
    
    @IBOutlet var slider: NHRangeSlider!
    @IBOutlet var subCategorylbl: UILabel!{
        didSet {
            self.subCategorylbl.font = UIFont(name: appFont, size: 15.0)
            self.subCategorylbl.text = self.subCategorylbl.text?.localized
            
        }
    }
    @IBOutlet weak var MinAmtLbl: UILabel!{
        didSet {
            self.MinAmtLbl.font = UIFont(name: appFont, size: 15.0)
            self.MinAmtLbl.text = self.MinAmtLbl.text?.localized
            
        }
    }
    @IBOutlet weak var MaxAmtLbl: UILabel!{
        didSet {
             self.MaxAmtLbl.font = UIFont(name: appFont, size: 15.0)
            self.MaxAmtLbl.text = self.MaxAmtLbl.text?.localized
           
        }
    }
    @IBOutlet var MinAmountLbl: UILabel!{
        didSet {
            self.MinAmountLbl.font = UIFont(name: appFont, size: 15.0)
            self.MinAmountLbl.text = self.MinAmountLbl.text?.localized
            
        }
    }
    @IBOutlet var MaxAmountLbl: UILabel!{
        didSet {
            self.MaxAmountLbl.font = UIFont(name: appFont, size: 15.0)
            self.MaxAmountLbl.text = self.MaxAmountLbl.text?.localized
            
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.apiManagerClient = APIManagerClient.sharedInstance
        self.isSliderChanges = false
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func setMinMaxAmount(){
        if let minAmount = UserDefaults.standard.string(forKey: "min"){
            //self.MinAmountLbl.text = "\(minAmount) MMK"
            self.MinAmountLbl.text = AppUtility.getDigitDisplay("\(minAmount)") + " MMK"
        }
        if let maxAmout = UserDefaults.standard.string(forKey: "max"){
            //self.MaxAmountLbl.text = "\(maxAmout) MMK"
            self.MaxAmountLbl.text = AppUtility.getDigitDisplay("\(maxAmout)") + " MMK"
        }
        self.slider.lowerValue =  0.0
        self.slider.upperValue =  10.0
    }
    
    func setMinMaxAppliedAmount(min_amt: String, max_amt: String, min_Slider: String, max_slider: String) {
        self.MinAmountLbl.text = AppUtility.getDigitDisplay("\(min_amt)") + " MMK"
        self.MaxAmountLbl.text = AppUtility.getDigitDisplay("\(max_amt)") + " MMK"
        
        //        self.MinAmountLbl.text = "\(min_amt) MMK"
        //        self.MaxAmountLbl.text = "\(max_amt) MMK"
        self.slider.lowerValue = Double(min_Slider) ?? 0.0
        self.slider.upperValue = Double(max_slider) ?? 10.0
    }
    
    
    func setInitialMinMaxValue() {
        self.slider.lowerValue = 0.0
        self.slider.upperValue = 10.0
    }
    
    
    @IBAction func sliderValueChanged(_ sender: NHRangeSlider?) {
        
        self.isSliderChanges = true
        let minAmount = UserDefaults.standard.string(forKey: "min")
        let maxAmout = UserDefaults.standard.string(forKey: "max")
        println_debug(((maxAmout?.toDouble())! - (minAmount?.toDouble())!)/10)
        let diff = (((maxAmout?.toDouble())! - (minAmount?.toDouble())!)/10)
        
        if (Int((sender?.lowerValue)!) > 0) {
            let newAmount = (Int((minAmount?.toDouble())!) + (Int((sender?.lowerValue)!) * Int(diff)))
            //self.MinAmountLbl.text = "\(newAmount) MMK"
            self.MinAmountLbl.text = AppUtility.getDigitDisplay("\(newAmount)") + " MMK"
        }else if (Int((sender?.lowerValue)!) < 10) {
            let newAmount = (Int((minAmount?.toDouble())!) - (Int((sender?.lowerValue)!) * Int(diff)))
            //self.MinAmountLbl.text = "\(newAmount) MMK"
            self.MinAmountLbl.text = AppUtility.getDigitDisplay("\(newAmount)") + " MMK"
        }
        
        //if Int((sender?.boundValue(10, toLowerValue: sender!.upperValue, upperValue: sender!.lowerValue))!) == 0 {
        if (Int((sender?.upperValue)!) <= 10) {
            let newAmount = (Int((minAmount?.toDouble())!) + (Int((sender?.boundValue(10, toLowerValue: sender!.lowerValue, upperValue: sender!.upperValue))!) * Int(diff)))
            //self.MaxAmountLbl.text = "\(newAmount) MMK"
            self.MaxAmountLbl.text = AppUtility.getDigitDisplay("\(newAmount)") + " MMK"
        }else if (Int((sender?.upperValue)!) > 0) {
            let newAmount = (Int((minAmount?.toDouble())!) - (Int((sender?.boundValue(10, toLowerValue: sender!.lowerValue, upperValue: sender!.upperValue))!) * Int(diff)))
            //self.MaxAmountLbl.text = "\(newAmount) MMK"
            self.MaxAmountLbl.text = AppUtility.getDigitDisplay("\(newAmount)") + " MMK"
        }
        // }
        
        println_debug("Step: \(String(describing: sender!.stepValue))")
        println_debug("lower to upper: \(Int((sender?.boundValue(10, toLowerValue: sender!.lowerValue, upperValue: sender!.upperValue))!))")
        println_debug("upper to lower: \(Int((sender?.boundValue(10, toLowerValue: sender!.upperValue, upperValue: sender!.lowerValue))!))")        
    }
}
