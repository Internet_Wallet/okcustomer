//
//  Filter.swift
//  VMart
//
//  Created by Shobhit Singhal on 10/20/18.
//  Copyright © 2018 Shobhit. All rights reserved.
//

import UIKit

//MARK: step 1 Add Protocol here.
protocol FilterDelegate: class {
    func filterProducts(finalFilterProductsId: [NSInteger], selectedFilterProductsId: [NSInteger], checkedRightTable: [[Bool]], isPriceFilter: Bool)
    func filterProducts(min:String, max:String, minSliderValue: Double, maxSliderValue: Double)
    func resetAllData()
    func applyActionCall(isReset: Bool)
}

class Filter: MartBaseViewController {
    
    var productsInfo = [ProductsInfo]()
    var notFilteredItems = [NotFilteredItems]()
    weak var FDelegate: FilterDelegate?
    var carsDictionary = [String: [String]]()
    var carSectionTitles = [String]()
    var cars = [String]()
    var notFilteredItemsCategories = [String]()
    var notFilteredItemsSubCategories = [[String]]()
    var searchFilteredSubCategories = [String]()
    var searchFilteredSubCategoriesBool = [Bool]()
    var selectedCategotyIndex = 0
    var checkedLeftTable = [Bool]()
    var checkedRightTable = [[Bool]]()
    var searchActive = false
    var notFilteredSubProductId = [[NSInteger]]()
    var allFilterProductsId = [[NSInteger]]()
    var selectedFilterProductsId = [NSInteger]()
    var finalFilterProductsId = [NSInteger]()
    var homePageProductArray = [GetHomePageProducts]()
    var isResetPressed = false
    var minAmt : String!
    var maxAmt : String!
    var min    : String!
    var max    : String!
    
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var leftView: UIView!
    @IBOutlet weak var leftTableView: UITableView!
    @IBOutlet weak var rightTableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet var cartBarButton: UIBarButtonItem!
    
    
    @IBOutlet var btnReset: UIButton!{
        didSet {
            btnReset.setTitle((btnReset.titleLabel?.text ?? "").localized, for: .normal)
            btnReset.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var btnApply: UIButton!{
        didSet {
            btnApply.setTitle((btnApply.titleLabel?.text ?? "").localized, for: .normal)
            btnApply.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.searchView.isHidden = true
        //        self.cartBarButton = UIBarButtonItem.init(badge: UserDefaults.standard.value(forKey: "badge") as? String, title: "", target: self, action: #selector(self.cartAction(_:)))
        //        self.cartBarButton = UIBarButtonItem.init(badge: UserDefaults.standard.string(forKey: "badge") ?? "", title: "", target: self, action: #selector(self.cartAction(_:)))
        //        self.navigationItem.rightBarButtonItem  = self.cartBarButton
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //        if self.minAmt != ""{
        //            isResetPressed = true
        //        }
        self.navigationItem.title = "Filter".localized
        self.leftView.layer.shadowOpacity = 0.40
        self.leftView.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.leftView.layer.shadowRadius = 6
        self.leftView.layer.shadowColor = UIColor.black.cgColor
        self.leftView.layer.masksToBounds = false
        leftTableView.estimatedRowHeight = 50
        leftTableView.rowHeight = UITableView.automaticDimension
        searchBar.delegate = self
        self.makeLeftCheckArrayToFalse()
        if self.checkedRightTable.count == 0 {
            self.makeRightCheckArrayToFalse()
        }
        self.loaData()
        self.makeFilteredArrayToFalse()
    }
    
    @IBAction func cartAction(_ sender: UIBarButtonItem) {
        let viewController = UIStoryboard(name: "Cart", bundle: Bundle.main).instantiateViewController(withIdentifier: "CartRoot")
        self.present(viewController, animated: true, completion: nil)
    }
    
    func makeLeftCheckArrayToFalse() {
        checkedLeftTable = Array(repeating: false, count: notFilteredItemsCategories.count)
        if notFilteredItemsCategories.count > 0 {
            checkedLeftTable[0] = true
        }
    }
    
    func makeRightCheckArrayToFalse() {
        checkedRightTable = []
        for i in 0..<notFilteredItemsSubCategories.count {
            var arr = [Bool]()
            arr = Array(repeating: false, count: notFilteredItemsSubCategories[i].count)
            checkedRightTable.append(arr)
        }
    }
    
    func makeFilteredArrayToFalse() {
        searchFilteredSubCategoriesBool = []
        for _ in 0..<searchFilteredSubCategories.count {
            searchFilteredSubCategoriesBool.append(false)
        }
        //        var arr = [Bool]()
        //        for (_,data) in checkedRightTable[selectedCategotyIndex].enumerated() {
        //            arr.append(data)
        //        }
        //
        //        var arr2 = [String]()
        //        for (index,data) in notFilteredItemsSubCategories[selectedCategotyIndex].enumerated() {
        //            if arr[index] {
        //                arr2.append(data)
        //            }
        //        }
        //
        //        var indArr = [Int]()
        //        for (index,data) in arr2.enumerated() {
        //            for (ind,val) in searchFilteredSubCategories.enumerated() {
        //                if data == val {
        //                    for (i,check) in arr.enumerated() {
        //                        if index == i {
        //                            if check {
        //                                indArr.append(ind)
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //
        //        print("indArr",indArr)
        //        print("searchFilteredSubCategoriesBool1111111",searchFilteredSubCategoriesBool)
        //
        //        if searchActive {
        //            let arrBool = searchFilteredSubCategoriesBool
        //            searchFilteredSubCategoriesBool = []
        //            for (ind,_) in arrBool.enumerated() {
        //                for indVal in indArr {
        //                    if ind == indVal {
        //                        searchFilteredSubCategoriesBool.append(true)
        //                        break
        //                    }
        //                }
        //                searchFilteredSubCategoriesBool.append(false)
        //            }
        //        }
        //        print("searchFilteredSubCategoriesBool",searchFilteredSubCategoriesBool)
    }
    
    func makeSearchBoolArr() {
        searchFilteredSubCategoriesBool = []
        for _ in 0..<searchFilteredSubCategories.count {
            searchFilteredSubCategoriesBool.append(false)
        }
        var arr = [Bool]()
        for (_,data) in checkedRightTable[selectedCategotyIndex].enumerated() {
            arr.append(data)
        }
        
        var arr2 = [String]()
        for (index,data) in notFilteredItemsSubCategories[selectedCategotyIndex].enumerated() {
            if arr[index] {
                arr2.append(data)
            }
        }
        
        var indArr = [Int]()
        for (_,data) in arr2.enumerated() {
            for (ind,val) in searchFilteredSubCategories.enumerated() {
                if data == val {
                    for (_,check) in arr.enumerated() {
                        if check {
                            indArr.append(ind)
                            break
                        }
                    }
                }
            }
        }
        
        print("indArr",indArr)
        print("searchFilteredSubCategoriesBool1111111",searchFilteredSubCategoriesBool)
        
        if searchActive {
            let arrBool = searchFilteredSubCategoriesBool
            searchFilteredSubCategoriesBool = []
            for (ind,_) in arrBool.enumerated() {
                var check = false
                for indVal in indArr {
                    if ind == indVal {
                        check = true
                        searchFilteredSubCategoriesBool.append(true)
                    }
                }
                if !check {
                    searchFilteredSubCategoriesBool.append(false)
                }
            }
        }
        print("searchFilteredSubCategoriesBool",searchFilteredSubCategoriesBool)
        
    }
    
    
    func loaData(){
        cars = ["Audi", "Aston Martin","BMW", "Bugatti", "Bentley","Chevrolet", "Cadillac","Dodge","Ferrari", "Ford","Honda","Jaguar","Lamborghini","Mercedes", "Mazda","Nissan","Porsche","Rolls Royce","Toyota","Volkswagen"]
        for car in cars {
            let carKey = String(car.prefix(1))
            if var carValues = carsDictionary[carKey] {
                carValues.append(car)
                carsDictionary[carKey] = carValues
            } else {
                carsDictionary[carKey] = [car]
            }
        }
        carSectionTitles = [String](carsDictionary.keys)
        carSectionTitles = carSectionTitles.sorted(by: { $0 < $1 })
    }
    
    @IBAction func backAction(_ sender: UIBarButtonItem) {
        // self.resetClearAll()
        //                if minAmt == "" {
        //                    if let del = self.FDelegate {
        //                        del.resetAllData()
        //                    }
        //                }
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func resetClearAll(){
        finalFilterProductsId = []
        selectedFilterProductsId = []
        selectedCategotyIndex = 0
        self.makeRightCheckArrayToFalse()
        self.makeLeftCheckArrayToFalse()
        self.makeFilteredArrayToFalse()
        isResetPressed = true
        minAmt = ""
        maxAmt = ""
        min = ""
        max = ""
        self.searchView.isHidden = true
        searchBar.text = ""
        leftTableView.reloadData()
        rightTableView.reloadData()
    }
    
    @IBAction func resetBtnAction(_ sender: UIButton) {
        self.resetClearAll()
        if let del = self.FDelegate {
            del.resetAllData()
        }
    }
    
    @IBAction func applyBtnAction(_ sender: UIButton) {
        finalFilterProductsId.removeAll()
        finalFilterProductsId = selectedFilterProductsId.unique(map: {$0})
        let cell = rightTableView.cellForRow(at: IndexPath.init(row: 0, section: 0)) as! FilterRightTableViewCell
        if notFilteredItemsSubCategories[selectedCategotyIndex].count == 0 {
            let minString = cell.MinAmountLbl.text!.replacingOccurrences(of: " MMK", with: "", options: .literal, range: nil)
            let maxString = cell.MaxAmountLbl.text!.replacingOccurrences(of: " MMK", with: "", options: .literal, range: nil)
            self.FDelegate?.filterProducts(min: AppUtility.removeCommaFromDigit(minString), max: AppUtility.removeCommaFromDigit(maxString), minSliderValue: cell.slider.lowerValue, maxSliderValue: cell.slider.upperValue)
            self.FDelegate?.filterProducts(finalFilterProductsId: finalFilterProductsId, selectedFilterProductsId: selectedFilterProductsId, checkedRightTable: self.checkedRightTable, isPriceFilter: true)
            self.FDelegate?.applyActionCall(isReset: isResetPressed)
        }else{
            self.FDelegate?.filterProducts(finalFilterProductsId: finalFilterProductsId, selectedFilterProductsId: selectedFilterProductsId, checkedRightTable: self.checkedRightTable, isPriceFilter: false)
            self.FDelegate?.applyActionCall(isReset: isResetPressed)
        }
        self.dismiss(animated: true, completion: nil)
    }
}

extension Filter: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == leftTableView {
            if notFilteredItemsCategories.count > 0 {
                return notFilteredItemsCategories.count
            }
        } else {
            if notFilteredItemsSubCategories.count > 0 {
                if notFilteredItemsSubCategories[selectedCategotyIndex].count == 0 {
                    return 1
                } else {
                    if !searchActive {
                        if notFilteredItemsSubCategories.count > 0 {
                            return notFilteredItemsSubCategories[selectedCategotyIndex].count
                        }
                    }else {
                        if searchFilteredSubCategories.count > 0 {
                            return searchFilteredSubCategories.count
                        }
                        return 0
                    }
                }
            }
            return 0
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if tableView == rightTableView {
            return UIView()
        } else {
            let headerView = UIView(frame: CGRect(x: 0, y: 0, width: leftTableView.bounds.width, height: 50))
            let backView = UIView(frame: CGRect(x: 0, y: 0, width: leftTableView.bounds.width-12, height: 50))
            backView.backgroundColor = UIColor.white
            let label = UILabel(frame: CGRect(x: 10, y: 10, width: leftTableView.bounds.width-15, height: 25))
            // label.text = "CATEGORIES".localized
            label.text = "".localized
            label.font = UIFont(name: appFont, size: 15.0)
            headerView.addSubview(backView)
            headerView.addSubview(label)
            return headerView
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == leftTableView {
            return 46
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == leftTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FilterLeftCell", for: indexPath) as! FilterLeftTableViewCell
            cell.categoryLbl.text = notFilteredItemsCategories[indexPath.row].localized
            if checkedLeftTable[indexPath.row] {
                cell.arrowImgView.isHidden = false
                cell.categoryLbl.textColor = UIColor.colorWithRedValue(redValue: 13, greenValue: 177, blueValue: 75, alpha: 1)
            } else {
                cell.arrowImgView.isHidden = true
                cell.categoryLbl.textColor = UIColor.black
            }
            return cell
        } else if tableView == rightTableView && notFilteredItemsSubCategories.count > 0 && notFilteredItemsSubCategories[selectedCategotyIndex].count == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "FilterCell", for: indexPath) as! FilterRightTableViewCell
            if isResetPressed {
                cell.setMinMaxAmount()
                //                isResetPressed = false
            }
            if minAmt == "" {
                cell.setMinMaxAmount()
            }else{
                cell.setMinMaxAppliedAmount(min_amt: min, max_amt: max, min_Slider: minAmt, max_slider: maxAmt)
            }
            
            return cell
        } else {
            if !searchActive {
                let cell = tableView.dequeueReusableCell(withIdentifier: "FilterRightCell", for: indexPath) as! FilterRightTableViewCell
                cell.subCategorylbl.text = notFilteredItemsSubCategories[selectedCategotyIndex][indexPath.row]
                if checkedRightTable[selectedCategotyIndex][indexPath.row] {
                    cell.accessoryType = .checkmark
                    cell.subCategorylbl.textColor = UIColor.colorWithRedValue(redValue: 32, greenValue: 90, blueValue: 235, alpha: 1)
                } else {
                    cell.accessoryType = .none
                    cell.subCategorylbl.textColor = UIColor.darkGray
                }
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "FilterRightCell", for: indexPath) as! FilterRightTableViewCell
                cell.subCategorylbl.text = searchFilteredSubCategories[indexPath.row]
                //                if searchFilteredSubCategoriesBool.count == 0 {
                //                    if checkedRightTable[selectedCategotyIndex][indexPath.row] {
                //                        cell.accessoryType = .checkmark
                //                        cell.subCategorylbl.textColor = UIColor.colorWithRedValue(redValue: 32, greenValue: 90, blueValue: 235, alpha: 1)
                //                    } else {
                //                        cell.accessoryType = .none
                //                        cell.subCategorylbl.textColor = UIColor.darkGray
                //                    }
                //                }
                //                else {
                //                    if searchFilteredSubCategoriesBool[indexPath.row] {
                //                        cell.accessoryType = .checkmark
                //                        cell.subCategorylbl.textColor = UIColor.colorWithRedValue(redValue: 32, greenValue: 90, blueValue: 235, alpha: 1)
                //                    }
                //                    else {
                //                        cell.accessoryType = .none
                //                        cell.subCategorylbl.textColor = UIColor.darkGray
                //                    }
                //                }
                
                if searchFilteredSubCategoriesBool[indexPath.row] {
                    cell.accessoryType = .checkmark
                    cell.subCategorylbl.textColor = UIColor.colorWithRedValue(redValue: 32, greenValue: 90, blueValue: 235, alpha: 1)
                }
                else {
                    cell.accessoryType = .none
                    cell.subCategorylbl.textColor = UIColor.darkGray
                }
                
                //                {
                //                    if checkedRightTable[selectedCategotyIndex][indexPath.row] {
                //                        cell.accessoryType = .checkmark
                //                        cell.subCategorylbl.textColor = UIColor.colorWithRedValue(redValue: 32, greenValue: 90, blueValue: 235, alpha: 1)
                //                    }else {
                //                        cell.accessoryType = .none
                //                        cell.subCategorylbl.textColor = UIColor.darkGray
                //                    }
                //                }
                return cell
            }
        }
    }
    
    //    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    //        if tableView == rightTableView{return carSectionTitles[section]} else{return ""}
    //    }
    
    //    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
    //        if tableView == rightTableView{return carSectionTitles}else{
    //            return []
    //        }
    //    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //self.view.endEditing(true)
        if tableView == leftTableView {
            if indexPath.row == 0{
                self.searchView.isHidden = true
            } else {
                self.searchView.isHidden = false
            }
            for (index,_) in checkedLeftTable.enumerated() {
                checkedLeftTable[index] = false
            }
            checkedLeftTable[indexPath.row] = true
            selectedCategotyIndex = indexPath.row
            searchActive = false
            makeFilteredArrayToFalse()
            leftTableView.reloadData()
            rightTableView.reloadData()
        } else {
            selectedFilterProductsId.removeAll()
            if tableView == rightTableView && notFilteredItemsSubCategories.count > 0 && notFilteredItemsSubCategories[selectedCategotyIndex].count == 0{
            }
            else {
                if !searchActive {
                    //Code to check mark on cell
                    if checkedRightTable[selectedCategotyIndex][indexPath.row] {
                        checkedRightTable[selectedCategotyIndex][indexPath.row] = false
                    } else {
                        checkedRightTable[selectedCategotyIndex][indexPath.row] = true
                    }
                    //Code to append seleted product filter id
                    if checkedRightTable[selectedCategotyIndex][indexPath.row] {
                        
                        
                        
                        for i in 0..<notFilteredItems.count{
                            if notFilteredItemsSubCategories[selectedCategotyIndex][indexPath.row] == String(notFilteredItems[i].SpecificationAttributeOptionName ?? "") {
                                print(notFilteredItems[i].ProductId)
                                selectedFilterProductsId.append(notFilteredItems[i].ProductId)
                            }
                        }
                        
                        
                        
                        
                        //                        for i in 0..<notFilteredItems.count{
                        //                            if notFilteredItemsSubCategories[1][indexPath.row] == String(notFilteredItems[i].SpecificationAttributeOptionName ?? "") {
                        //                                print(notFilteredItems[i].ProductId)
                        //                                selectedFilterProductsId.append(notFilteredItems[i].ProductId)
                        //                            }
                        //                        }
                        
                    }else {
                        let productID = allFilterProductsId[selectedCategotyIndex][indexPath.row]
                        if let removeID = selectedFilterProductsId.first(where: {$0 == productID}) {
                            if let index = selectedFilterProductsId.firstIndex(of: removeID) {
                                selectedFilterProductsId.remove(at: index)
                            }
                        }
                    }
                }else {
                    let val = searchFilteredSubCategories[indexPath.row]
                    
                    for (index,data) in notFilteredItemsSubCategories[selectedCategotyIndex].enumerated() {
                        if val == data {
                            if checkedRightTable[selectedCategotyIndex][index] {
                                checkedRightTable[selectedCategotyIndex][index] = false
                            } else {
                                checkedRightTable[selectedCategotyIndex][index] = true
                            }
                        }
                    }
                    
                    if searchFilteredSubCategoriesBool[indexPath.row] {
                        searchFilteredSubCategoriesBool[indexPath.row] = false
                    } else {
                        searchFilteredSubCategoriesBool[indexPath.row] = true
                    }
                    if searchFilteredSubCategoriesBool[indexPath.row] {
                        //                        let productID = allFilterProductsId[selectedCategotyIndex][indexPath.row]
                        //                        selectedFilterProductsId.append(productID)
                        
                        //                        for i in 0..<notFilteredItems.count{
                        //                            if notFilteredItemsSubCategories[1][indexPath.row] == String(notFilteredItems[i].SpecificationAttributeOptionName ?? "") {
                        //                                print(notFilteredItems[i].ProductId)
                        //                                selectedFilterProductsId.append(notFilteredItems[i].ProductId)
                        //                            }
                        //                        }
                    } else {
                        let productID = allFilterProductsId[selectedCategotyIndex][indexPath.row]
                        if let removeID = selectedFilterProductsId.first(where: {$0 == productID}) {
                            if let index = selectedFilterProductsId.firstIndex(of: removeID) {
                                selectedFilterProductsId.remove(at: index)
                            }
                        }
                    }
                    self.makeSearchBoolArr()
                }
            }
            rightTableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == rightTableView && notFilteredItemsSubCategories.count > 0 && notFilteredItemsSubCategories[selectedCategotyIndex].count == 0{
            return 150
        } else {
            return UITableView.automaticDimension
        }
    }
}

extension Filter: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.count > 0 {
            searchActive = true
            let filter = notFilteredItemsSubCategories[selectedCategotyIndex].filter { (category) -> Bool in
                return category.contains(searchText)
            }
            searchFilteredSubCategories = filter
            DispatchQueue.main.async {
                self.makeSearchBoolArr()
                self.rightTableView.reloadData()
            }
        } else {
            searchActive = false
            self.rightTableView.reloadData()
        }
        // makeRightCheckArrayToFalse()
        makeFilteredArrayToFalse()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        if let txt = searchBar.text, txt.count > 0 {
            searchActive = true
        } else {
            searchActive = false
        }
        //makeRightCheckArrayToFalse()
        //        makeFilteredArrayToFalse()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        if let txt = searchBar.text, txt.count > 0 {
            searchActive = true
        } else {
            searchActive = false
        }
        //makeRightCheckArrayToFalse()
        //        makeFilteredArrayToFalse()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let txt = searchBar.text, txt.count > 0 {
            searchActive = true
        } else {
            searchActive = false
        }
        //makeRightCheckArrayToFalse()
        //        makeFilteredArrayToFalse()
        self.searchBar.endEditing(true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
        //makeRightCheckArrayToFalse()
        //        makeFilteredArrayToFalse()
        self.rightTableView.reloadData()
    }
}
